---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# Culture numérique et Apprentissage

## UE - Num 801 - Séance 7
Equipe formateurs numérique - UGA - 2024
 
---
<!-- page_number: true -->
<!-- footer: 2023 - CC:BY-NC-SA -->

# :one: Conception d'une situation d'apprentissage intégrant le numérique (Rappel)
#### Production attendue
Un document rédigé en **binôme** de 4 pages maximum comportant :
* un descriptif de la situation d’apprentissage avec obligatoirement des liens pointants sur les éléments de conception (liens vers les ressouces, copies d’écran des interfaces élèves et/ou enseignants, …),
* une argumentation sur les choix opérés lors de la conception, notamment au regard des apports théoriques,
* un bilan personnel.

---
## 1.2 Description de la situation (Rappel)
#### Eléments attendus dans la description

```
- Discipline 
- Niveau de classe
- Période de l'année, ou place dans une séquence, où la situation a été (ou serait) mise en œuvre.
- Objectif(s) d’apprentissages : 
	- Compétences disciplinaires visées en termes de « être capable de…», (ces compétences sont issues des programmes)
	- Compétences numériques développées (issues du CRCN)
	
- Environnement numérique impliqué dans la situation :
    - Outils physiques (TBI, tablettes, ordinateurs fixes ou portables, caméras, enregistreurs, ...)
    - Organisation matérielle (nb de matériels par élève/classe, salle informatique ou salle de classe)
    - Logiciels ou applications utilisés.
    - Ressources utilisées, ou produites

-  Organisation pédagogique : quelles activités, quand, comment, ...
    
```

---
## 1.3 Analyse et justification des choix (Rappel)

- Justification des choix d'outils et de ressources numériques, en s'appuyant notamment sur le modèle SAMR et les compétences visées.
- Analyse des potentiels effets positifs ou néfastes pour les apprentissages et les apprenants.
- Analyse des problèmes juridiques (droits d'auteurs, droit à la vie privée) et éthiques soulevés.


:warning: Les analyses et justifications doivent tenir compte d'un contexte précis.

---
## 1.4 Bilan personnel
- Conclure sur le ratio bénéfices/risques et les principales précautions à prendre.
- Conclure sur les compétences que vous avez vous-mêmes développées en réalisant ce dossier.


---
# :two: Production

* Finir de rédiger la description de la situation, les analyses et le bilan personnel.
* Sélectionner les ressources et préparer les outils choisis. Faire des copies d'écran si nécessaire et les mettre sur un espace en ligne accessible sans identification (Digipad par exemple)
* Préparez une présentation visuelle (sans texte) de 180 secondes pour le TD 8.



---
## 2.1 Format du document

- Un seul document au format PDF déposé sur la plateforme.
- Police Arial, taille 12, interligne 1,5 cm, marges 2,5 cm.
- 4 pages maximum

Date limite de rendu : **17 mars 23h59**

---
## 2.2 Dépôt des éléments de conception (Annexes)

Déposer les documents, ou des copies d'écran sur une espace de partage en ligne tel que :
- Mur de partage de la Digitale : https://digipad.app/ ou équivalent
- Partage de vidéos de l'UGA : https://videos.univ-grenoble-alpes.fr/


