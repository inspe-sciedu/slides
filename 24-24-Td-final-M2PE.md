---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# EC-33 Culture numérique et apprentissages
## TD Final
 
 
<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2024 - 2025 - CC:BY-NC-SA -->

---
# :zero: Organisation
* Préparation d'une présentation (30 minutes)
* Présentations
* Finalisation du dossier
 
---
# :one: Préparation d'une présentation
Durée de la présentation 3 minutes
Après une rapide description de votre situation d'apprentissage, choisissez et présentez le point fort ou le point faible de la situation qui vous parait le plus important à souligner

---
# :two: Présentations



---
# :three: Finalisation de la rédaction du document à déposer sur la plateforme

La production sera déposée sur la plateforme pour évaluation au plus tard 2 jours après ce TD.

