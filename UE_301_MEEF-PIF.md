---
marp: true
autoscale: true
theme: marp

---
<!-- _paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# UE 301 Évaluer l'apprentissage par ordinateur : Tâches et rétroactions
#### Master 2 MEEF-PIF, Année 2024-25

### [Philippe Dessus](http://pdessus.fr/), LaRAC & Inspé, Univ. Grenoble Alpes & Nelly Tarbouriech, Rectorat de l'académie de Grenoble
![w:350](images/logo-espe-uga.png)

---
<!-- paginate: true -->
<!-- footer: Ph. Dessus & N. Tarbouriech | UE 301 MEEF-PIF, Inspé UGA | CC:BY-NC-SA | 2024–25 -->

# Séance 1 (4 septembre 2024)

- Présentation de l'organisation du cours
- Présentation de quelques réalisations des années antérieures
- Première réflexion sur le domaine du cours interactif

---
# Objectifs de l'UE

À l’issue de l'UE, les étudiant·es seront capables :

- d’analyser un **domaine de connaissances** pour en dégager des concepts, pouvant faire l’objet d’une évaluation automatisée ;
- de connaître les différents **types de rétroactions** et d’en concevoir qui soient appropriées au niveau de connaissances de l’élève ;
- de réaliser un **cours interactif** composé de questions évaluatives, avec un outil de création de QCM et/ou de cours interactifs.

---
# Organisation du cours (1/2)

**Enseignement principalement à distance** : tout le cours est disponible pour lecture dès le début. Du travail est à réaliser à distance et à soumettre dans la plate-forme
- 1\. **Discussion sur le cours lu auparavant, et les activités réalisées**. Chaque séance dédiée à un approfondissement du cours lu au préalable (**poster avis dans le forum correspondant**) :
	- Qu’est-ce que j’ai compris du cours ?
	- Qu’est-ce qu’il me reste à comprendre ?
	- Quelles implication sur ma pratique (actuelle ou future) ?
	
---
# Organisation du cours (2/2)

- 2\. **Travaux dirigés en binômes** : les TD sont de petits projets, donnant lieu à diffusion en cours, permettant d'avancer le projet du cours interactif et de travailler les éléments théoriques (**poster TD dans la zone “devoir” correspondante**)
- 3\. **Réalisation individuelle d'un cours interactif** :  La partie la plus importante de l'UE est la réalisation du cours interactif avec un logiciel spécifique (**poster réalisation dans la zone “devoir” correspondante**)


---
# Niveau et implication

- **Compétences en informatique** : standard, mais avec une volonté d'approfondir les questions du cours
- **Implication** : il est attendu un *travail régulier* tout au long de l'UE : il y a un certain nombre de notions à acquérir et à appliquer et cela prend du temps. Éviter de reporter le travail d'une semaine à l'autre pour éviter de se faire déborder
- :warning: Attention à ne pas passer *trop* de temps à travailler sur chaque séance de cours. Par semaine, une durée de travail d'environ 1 h 15 (± 30 min) est conseillée, selon le niveau initial de chacun·e.

---
# Supports de cours

- Cette présentation : [https://link.infini.fr/301-pif-uga](https://link.infini.fr/301-pif-uga)
- Cours complet : [https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/)
- Cours plate-forme Moodle [https://eformation.univ-grenoble-alpes.fr/course/view.php?id=1027](https://eformation.univ-grenoble-alpes.fr/course/view.php?id=1027) nécessitant l'auto-inscription des étudiants avec une clé : **301MEEFPIF** 
- Les étudiant·e·s muni·e·s d'un ordinateur se connectent au site avec leur identifiant étudiant et saisissent la clé **maintenant** (possibilité d'envoyer un courriel à l'enseignant pour qu'il vous inscrive)
- La plate-forme contient : les documents et présentations diffusés en cours, les forums de discussion à propos du cours, les zones de dépôt des différentes productions


---
# Modalités de travail

- Les étudiant·s travailleront, dans cette UE et dans l'UE “Scénarisation et suivi des apprentissages”, **sur le *même* cours interactif** ; cela de manière à réduire leur charge de travail (leur évaluation portera sur des éléments distincts). Se reporter [ici](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/syl-organisation-MEEFPIF.html#liens-avec-dautres-u-e-du-cursus) pour plus d'informations sur les liens
- Choix de **l'un des deux logiciels** pour construire un cours : eXe Learning ou Lumi/H5P. Voir [ici](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/res_generateurs_cours.html#exe-learning-et-h5p) pour plus d'informations
- :warning: Ne pas hésiter à signaler à l'enseignant toute erreur dans le cours, ou passage peu clair

---
# Vue globale des séances (1/2)


| N° séance                                            | Date                                          | Prés./Dist.  | Travail TD         | Livraison |
| ---------------------------------------------------- | --------------------------------------------- | ------------ | ------------------ | --------- |
| 1                                                    | 4 sept. 24                                    | **Présence** | –                  | –         |
| 2                                                    | Semaine 37                                    | Distance     | TD 1               | –         |
| 3                                                    | Semaine 38                                    | Distance     | TD 2               | TD 1      |
| 4                                                    | Semaine 39                                    | Distance     | TD 3               | TD 2      |
| 5                                                    | 9 oct. 24                                     | **Présence** | Usages péda QCM    | TD 3      |
| 6                                                    | Semaine 42                                    | Distance     | Cours interactif   | –         |

---
# Vue globale des séances (2/2)
| N° séance                                            | Date                                          | Prés./Dist.  | Travail TD         | Livraison |
| ---------------------------------------------------- | --------------------------------------------- | ------------ | ------------------ | --------- |
| 7                                                    | Semaine 43                                    | Distance     | TD 4               | –         |
| 8                                                    | Semaine 45                                    | Distance     | TD 4               | TD 4      |
| –                                                    | 7 semaines pour finaliser le cours interactif |              |                    |
| 9                                                    | 18 déc. 24                                    | **Présence** | Présentation cours |           |
| Livraison des TD et du cours interactif : mi janvier |                                               |              |                    |           |


---
# 1. Quelques exemples de productions

:warning: Les liens ci-dessous sont internes à l'ordinateur de l'enseignant, donc ne pourront être ouverts depuis le PDF diffusé.

- [Apprendre à porter secours, Cycle 3](hook://file/HeFcSTI1L?p=QXJjaGl2ZS9TZcyBdmVyaW5lIEJsYW5jaG9u&n=Apprendre%20a%CC%80%20porter%20secours%20version%20finale%2Ehtml)
- [La circulation sanguine, Cycle 4](hook://file/HeG3OJT5F?p=QXJjaGl2ZS9BdWRyZXkgRGVzbWFycw==&n=LA%20CIRCULATION%20SANGUINE3%2Ehtml)
- [Le devenir de la matière organique, Cycle 3](hook://file/HeG8ypynC?p=QXJjaGl2ZS9TdGXMgXBoYW5pZSBBZ3Jlc3Rp&n=agresti%5Fmatiere%5ForganiqueVF%2Ehtml)
- [Les campagnes au Moyen-Âge, 5e](hook://file/HeGBsc9A3?p=QXJjaGl2ZS9BZGFtIER1cGVycm9u&n=DUPERRON%20Adam%20M2%20MEEF%20PIF%20AE%20DESSUS%2Ehtml)
- [Problèmes ouverts, professeurs](hook://file/HeGI95lQ5?p=QXJjaGl2ZS9MYXVyZW5jZSBNb3NzdXo=&n=Proble%CC%80mes%20ouverts%2Ehtml)
- [Notion de fonction, 3e](hook://file/W3XkqKLss?p=UElGLU1FRUYyMy9XYWxnZW53aXR6IEdhZcyIbGxl&n=UE301%20%2D%20Notion%20de%20fonction%20%2D%20G%20WALGENWITZ%2Ehtml#h5pbookid=2614211806&chapter=h5p-interactive-book-chapter-49f8abd3-4002-4a97-82af-1d81148bb8e0&section=0)


---
# :warning: Usage de robots conversationnels :warning:

Ce cours a été conçu pour vous aider à développer des connaissances et à acquérir de nouvelles compétences qui vous seront utiles en tant que professionnels. Les outils d'IA peuvent être utilisés comme une aide au processus créatif, mais il est entendu qu'ils doivent être accompagnés d'une pensée critique et d'une réflexion. **Les étudiants qui choisissent d'utiliser ces outils sont responsables de toute erreur ou omission résultant de leur utilisation**. Il leur sera également demandé de fournir en annexe du dossier le nom de l’outil utilisé, les prompts utilisés, l'historique des résultats générés, ainsi qu'une réflexion approfondie sur ces résultats (notamment sur leur validité). Le cas échéant, les étudiants peuvent également être invités à examiner les coûts environnementaux et sociaux de l'utilisation des outils.



---
# Quelques conseils et avertissements 

- Il est conseillé d'installer assez rapidement les 2 logiciels (eXe Learning & Lumi), de tester leur fonctionnement afin de choisir celui qui convient le mieux à votre projet
- Séance 5 (en présence) : il est nécessaire de faire un travail avant d'y assister (cf. section dédiée à cette séance)
- Ne pas hésiter à poster une question sur les forums dédiés (projet ou technique)
- Ne pas hésiter à faire remonter (par courriel) aux enseignants tout élément du cours difficilement compréhensible



---
# Début du travail sur le contenu du cours (1/2)

Ce qui suit est tiré du doc. de cours [fin du syllabus](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/syl-organisation-MEEFPIF.html). Commencer à réfléchir à un "contenu de cours" :

- domaine et public au choix (à bien définir *a priori*)
- préférablement **en lien avec vos préoccupations extérieures** (cours à faire), et/ou lié à une autre UE

---
# Début du travail sur le contenu du cours (2/2)

- correspond à un document nécessitant env. **1 h de lecture** (env. 5 p A4 ou 10 000 caract. esp. compris, 2 niveaux de hiérarchie), permettant une bonne compréhension des concepts en jeu (QCM ajouté ultérieurement)
- exempt de plagiat, mais l'utilisation de manuels est conseillée
- commencer à placer les notions sur une carte de concepts avec liens (Fig. 1, doc. [conception](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/QCM_proc_conception.html))



---

<!--
---
# Séance 2 (20 septembre 2023)

- Échange sur ce qui a été compris/non compris des deux exercices (QCM mémoire et conférence sur les QCM)
- Lire le document "[Des tâches pour évaluer les connaissances des élèves](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/tache_eval.html)", notamment le Tab. 1.

---
# Séance 2 (suite)

- Choisir, **par binôme**, une activité citée dans la section "Analyse de tâches", lire attentivement l'article la concernant et servez-vous des informations qu'il contient pour réfléchir et augmenter la ligne du Tab. 1. P. ex. en détaillant 
	- les buts d'enseignement liés
	- les contextes dans lesquels la tâche d'évaluation peut être utilisée,
	- ses avantages et inconvénients (du point de vue de l'enseignant ou des apprenants).
	- ce que l'informatique peut apporter dans l'activité
- Compte rendu oral en fin de séance.

---
# 2. Pour la prochaine séance

- Livrer le travail du TD 1
- Lire les documents :
	- [Les rétroactions : définition et rôle dans l’apprentissage](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/retro_defs.html)
	- [Les rétroactions informatisées](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/retro_info.html) 
	- [Un processus de conception de cours avec QCM](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/QCM_proc_conception.html)


---
# Séance 3 – Analyse de contenu (27 septembre 2023)

- Échange sur ce qui a été compris/non compris à propos des documents à lire :
	- Les rétroactions : définition et rôle dans l’apprentissage
	- Les rétroactions informatisées 
	- Un processus de conception de cours avec QCM
- Commentaires rapides sur le TD

---
# Séance 3 - TD

- Avancer ou finir le TD 1 (30 min)
- Suivre le doc. “Processus de conception de cours”, 
	- étape 1 : finir la carte de concepts
	- étape 2 : identifier les unités de connaissances
- Faire relire par l'enseignant
- :warning: Le contenu développé dans cette U.E. permettra d'avancer aussi le travail de l'U.E. “Scénarisation et suivi des apprentissages” (et _vice-versa_)

---
# Séance 3 

- Pour la Séance 4  du 5 octobre
- Lire les Documents 
	- [Les différentes formes de rétroactions](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/retro_formes.html), 
	- [Les QCM : Bref historique](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/QCM_historique.html), 
	- [Les questionnaires à choix multiple : définitions et critiques](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/QCM_def.html),
	- [Les différents formats de QCM](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/QCM_formes.html).
- Installer *eXe Learning* ou *Lumi* sur votre ordinateur et commencer à l'utiliser
- Envoyer à l'enseignant le TD 1 (act. d'éval.) pour relecture


--- 
# Séance 4 (4 octobre 2023) 1/3

- Échange sur ce qui a été compris/non compris à propos des documents :
	- Les différentes formes de rétroactions, 
	- Les QCM : Bref historique, 
	- Les questionnaires à choix multiple : définitions et critiques,
	- Les différents formats de QCM.
- Point sur l'installation eXeLearning et Lumi : problèmes rencontrés ? Quelques éléments de choix

---
# Séance 4 (2/3)

- Activité du TD :

1. Rédigez, à  propos du thème de votre cours interactif, 3 items de QCM complets (comprenant chacun 1 amorce et 4 choix, dont 1 seul exact), ainsi que les rétroactions reliées *à chaque réponse*
2. Lisez *ensuite* le document "[Rédigez des items de QCM](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/QCM_principes.html)", et commencez à rendre à vos 3 items un format plus approprié, pour le modifier pas à pas, en précisant : 
	- quelle règle a été appliquée, 
	- les modifications réalisées, 
	- quelles rétroactions afficher en fonction des réponses. 


---
# Séance 4 (3/3)

Pour la prochaine séance (avec Nelly Tarbouriech, activité pouvant être menée à plusieurs, 2 ou 3 max) : 

  - Prendre connaissance d’usages pédagogiques de situations d’apprentissage incluant l’utilisation de QCM (lire le document [Quelques usages pédagogiques des QCM](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/usages-peda.html#usages-peda)) ; 
  - observer quelques pratiques et types d’activités parmi celles proposées (dans la section [Situations d’apprentissage proposées](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/usages-peda.html#sit-appr-proposees)) et analyser une situation en vous aidant de la [Grille d’observation de pratiques](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/usages-peda.html#grille-obs-pratiques)). 
 - Chaque groupe peut ensuite consigner son analyse générale / sa réflexion dans le pad collaboratif associé [lien vers le pad](https://annuel.framapad.org/p/masterpif_qcmusagespedagogiques)


---
# Séance 6 (18 oct. 2023)

- Échange sur ce qui a été compris/non compris à propos du document à lire pour aujourd'hui :  “Répondre à un QCM : Aspects cognitifs" et "Les rétroactions, leur réception par  les élèves".
- Travail individuel sur le logiciel de création de cours interactifs
- Livraison par courriel du TD 3 sur les items de QCM (au plus tard vendredi de cette semaine)

---
# Travail de la séance

- Continuer de rédiger le contenu du cours
- Commencer à le “rentrer” dans le logiciel de création de cours interactifs choisi
- D'ici à la semaine prochaine (le cas échéant) : livrer le TD sur le QCM. 
- Pour la séance suivante : avancer dans le cours interactif

--- 
# Séance 8 du 22 novembre 2023

- Séance dédiée à la réalisation du projet, chacun·e finalisant une version “présentable” pour la séance prochaine, où les projets, même s'ils sont encore en travaux, seront présentés à tous
- :warning: Date-butoir de livraison de l'ensemble des documents de l'évaluation le **le dimanche 14 janvier 2024 à 23:59**.

-->
---
# Séance 9 du 18 décembre 2024

---
# Livraison et contenu
- date-butoir de livraison de l'ensemble des documents le **le lundi 13 janvier 2025 à 23:59**.

---
# Documents à livrer pour l'évaluation (1/2)

Faire une archive zippée des documents ci-dessous (en 1 seul PDF) **et** le dossier contenant le cours interactif. La téléverser sur le site e-formation Section “**Livraison finale du cours interactif + TD**”

- Le **TD 1** séances 2-3 (analyse tâche d’apprentissage et évaluation) (env. 1 page).
- Le **TD 2** séance 4 (carte de concepts, “grains de connaissances”) (env. 3 pages).
- Le **TD 3** séance 5 (items de questions avant/après) (env. 1 page).
- Le **TD 4**  séance 7 (Carte mentale sur les apports des QCM)

---
# Documents à livrer  (2/2)
- Le cours interactif (en format HTML zippé, 
	- **Fichier>Exportation>Site Web** dans *eXe Learning*  ; 
	-  **Fichier>Exporter>FIchier HTML tout en un** dans *Lumi* ; si vous avez de nombreux fichiers vidéo choisir l'option **Fichier>Exporter>Fichier HTML et plusieurs fichiers multimedia** et zippez l'ensemble) ; 
	- ou à défaut, l'URL du cours mentionnée dans une page (pour les cours réalisés dans des systèmes en ligne). 
- Zippez ensemble le cours **et** le PDF contenant tous les TD et déposez le fichier dans e-formation>Livraison des travaux>Livraison finale du Cours Interactif+TD

---
# Critères d'évaluation
* TD 1 (tâche éval) : **2 points**
* TD 2 (carte grains) : **2 points**
* TD 3 Items (évalués dans le cours interactif)
* TD 4 Carte mentale : **2 points**
* Qualité du cours : **4 points**
* Qualité des QCM : **5 points**
* Qualité des rétroactions : **4 points**
* Forme : **1 point**

---
# Discussion libre sur l'organisation de l'UE

## Merci de votre travail et engagement durant cette UE !

Nous restons à votre disposition par courriel pour les questions qui vous viendront

---

<!--

---
# Proposition de sujet de stage
## Conception d'une ressourcerie pédagogique flexible dans le supérieur
- Directeur du mémoire : Ph. Dessus
- Maître de stage : F. Ménard, DAPI

---
# Contexte

Les enseignants du supérieur, par exemple à l'univ. Grenoble Alpes (UGA), conçoivent et diffusent, notamment sur les plates-formes d'enseignement (e.g., Moodle, Chamilo), un grand nombre de ressources pédagogiques à destination de leurs étudiants. Pourtant, ces ressources ne sont pas toujours mises en valeur autant qu'elles le pourraient, ce qui permettrait une meilleure réutilisation de ces ressources auprès de leurs collègues. Elles sont aussi d'une grande diversité de formats et de qualité, ce qui empêche leur mutualisation et réutilisation.

---
# But du stage

Ce stage vise à réfléchir aux tenants et aboutissant d'une “ressourcerie pédagogique flexible et de qualité“, qui aurait un rôle de mise en avant de ressources, afin d'aller vers un meilleur développement et partage du travail des enseignants. Il a déjà de nombreuses initiatives locales ou (inter-)nationales qui peuvent guider cette démarche.
L’objet de ce projet est de concevoir un processus de  sélection, labellisation d'un ensemble de ressources de formation pouvant servir d’exemple à la communauté des enseignant·es de l'UGA.

---
# Tâches possibles

Les tâches envisagées dans ce stage sont les suivantes :
- inventorier, sur la plate-forme Moodle, les types de cours à la disposition des étudiants, en proposer quelques indicateurs (domaine, durée, public, méthode, contexte de passation) ;
- spécifier un moyen pour les enseignant·es de décrire la ou les méthode·s pédagogique·s reliée·s à leur cours ;
- faire une liste de critères d'évaluation pour parvenir à une évaluation globale de la qualité d'une ressource, de sa flexibilité (adaptabilité à d'autres contextes et publics) ;
- réfléchir à une procédure d'évaluation transparente et multicatégorielle (comité de rédaction, etc.) ;
- réfléchir à un moyen d'inciter les enseignants à contribuer à la ressourcerie (p. ex., via les Open Badges), et à réfléchir au portage de la ressource dans une licence appropriée.

---
# Merci de votre travail et engagement durant cette UE !

Nous restons à votre disposition par courriel pour les questions qui vous viendront

-->