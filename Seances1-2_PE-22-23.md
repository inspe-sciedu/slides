---
marp: true
theme: default 
paginate: true
---

![](images/logo_UGA_couleur_cmjn.jpg)

# EC 33 - MEEF PE

## Culture numérique et Apprentissages

INSPÉ / UGA -2022

<!-- footer: Equipe de formateurs(trices) numérique - Inspé-UGA - 2022 - CC:BY-NC-SA -->

---

# Séance 1

---
# Objectifs de l'EC

* Être capable de se poser des questions sur les plus‑values supposées des situations d'apprentissage intégrant le numérique lors de leur conception.
* Réduire le plus possible les risques juridiques, éthiques, physiologiques et effets négatifs en termes d'apprentissages induits par les usages du numérique.
* Être capable d'analyser objectivement une situation d'apprentissage déjà réalisée et pouvoir en proposer une ou des évolutions bénéfiques pour les élèves.

---

# Méthode

* Via l'analyse en binôme d'une situation d'apprentissage utilisant le numérique.
* Cette situation sera analysée en vous aidant des apports délivrés au cours de 5 séances thématiques :
  * Effets du numérique sur les apprentissages
  * Évaluer et s’auto‑évaluer avec le numérique
  * Production et usages de ressources numériques
  * Numérique et juridique
  * Enseigner avec le numérique : quelques zones à risque

---

## Processus

![width:600px](images/ProcessusAnalyse.png)

<!-- p style="text-align:center;"><img src="images/ProcessusAnalyse.png" alt="Schéma du processus" width="600"></p -->

---

# Évaluation

* Un document de 6 pages réalisé en **binôme** contenant :
  * Une description de la situation d'apprentissage intégrant le numérique que vous avez choisie
  * Une analyse en lien avec chaque thématique abordée en formation
  * Un bilan réflexif

<!-- :warning: Ce n'est pas la qualité de la situation qui sera jugée, mais l'analyse qui en sera faite. -->

---

# Barème de notation

* Chaque thématique (**/3**)
* Bilan réflexif (**/5**)
* Non-respect du format général du document, des délais et règles d'orthographe (**\-1**)

(Voir le détail dans le [Syllabus](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/syllabus-ue-cult-num-PE.html))

---

# Précision

La description de la situation n’est pas évaluée directement. Ce qui est évalué est votre capacité d'analyse et l'apport d'informations permettant d'éclairer les analyses pour chaque thématique.

---

# Plan des séances

* **Séances 1 & 2** : Présentation de la formation et recherche d’une situation d’apprentissage
* **Séances 3–7** : Apports thématiques durant **1 h 15**, puis **45 min** en présence pour réaliser l'analyse en lien avec les apports thématiques.
* **Séance 8** : **2 h** en présence consacrées à la rédaction du bilan réflexif et à la finalisation du document servant à l'évaluation ainsi qu'une brève présentation au groupe de votre travail.

---

# \:warning:

Les thèmes des séances 3-7 seront traités indépendamment selon un ordre aléatoire, avec des formateurs différents.

---

# En résumé

* Une formation appuyée sur des apports théoriques et pratiques
* Une évaluation fondée sur l'analyse d'une situation de classe

\:warning: vous devez respecter le cahier des charges et surtout les limites !

---

# Connexion au cours (1/4)

* Plate-forme : <https://eformation.univ-grenoble-alpes.fr/>
* Connexion par « compte universitaire »

| Page d'accueil | Page de connexion | 
| :-------------: | :---------: | 
|![width:400px](images/Moodle1.png) |![width:350px](images/moodle18.png) |

---

# Connexion au cours (2/4)

#### Choix de l'établissement

Université Grenoble Alpes ou Université de Savoie ![width:700px](images/Moodle2.png)

Authentification avec ses identifiants universitaires ![width:600px](images/Moodle4.png)

---

# Connexion au cours (3/4)

* Retour à l'accueil du site ![width:300px](images/Moodle5.png)
* Descendre dans la page pour accéder au champ de recherche des cours ![50%](images/Moodle6PE.png)
* Sélectionner le cours EC 33 "Culture numérique et apprentissages" ![width:300px](images/Moodle7PE.png)

---

# Connexion au cours (4/4)

* Vous devez vous auto-inscrire, la clé d’inscription vous sera donnée par le formateur.

![center 60%](images/Moodle8PE.png)

* Si vous vous êtes trompé‑e de groupe, vous pouvez vous désinscrire et rentrer la bonne clé d'auto‑inscription

---

# Compétences numériques

![center](images/cadre_cours_numerique_inspe2.png)
<!-- p style="text-align:center;"><img src="images/cadre_cours_numerique_inspe2.png" alt="Schéma des compétences numériques"></p -->

---

# Textes et sites de référence

1. [Cadre de référence des compétences numériques (CRCN)](https://eduscol.education.fr/721/evaluer-et-certifier-les-competences-numeriques)
2. [Vademecum pour l’éducation aux médias et à l’information ](https://eduscol.education.fr/document/33370/download?attachment)


---

# Ressources complémentaires (1/2) :


* [Les compétences du 21ème siècle](https://www.competencesdu21emesiecle.com/decouvrir/qu-est-ce-que-les-competences-du-21eme-siecle/)
* Citoyenneté numérique : 
   * [Site Eduscol](https://eduscol.education.fr/3289/l-education-la-citoyennete-numerique)
   * [Site du conseil de l'Europe](https://www.coe.int/fr/web/digital-citizenship-education/digital-citizenship)
* Éducation aux médias : 
   * [Site Eduscol](https://eduscol.education.fr/1531/education-aux-medias-et-l-information)
   * [Site du conseil de l'Europe](https://www.coe.int/fr/web/digital-citizenship-education/media-and-information-literacy)

---
# Ressources complémentaires (2/2) :

* Compétences numériques (au niveau européen)
  * [DigComp 2.2 (version mars 2022)](https://publications.jrc.ec.europa.eu/repository/handle/JRC128415)
  * [DigComp Edu](https://joint-research-centre.ec.europa.eu/digcompedu_en)

* Notion d'illectronisme :
   * [Rapport du Sénat du 17 septembre 2020](https://www.senat.fr/rap/r19-711/r19-711_mono.html#toc0),
   * [Étude de l'INSEE du 30 octobre 2019](https://www.insee.fr/fr/statistiques/4241397),
   * [Site Solidarité Numérique (institutionnel)](https://www.solidarite-numerique.fr/)

---
# Recherche d'une situation et constitution des binômes

Recherchez une situation parmi les banques de scénarios suivantes (individuellement ou en binôme):

* Atelier d'usage : https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/ateliers/index.html#ateliers-sur-les-usages-du-numeriques-pe
* Prim à bord https://primabord.eduscol.education.fr/spip.php?page=selection&mots%5B%5D=133#content
* Banque de l'académie de Nantes : https://www.pedagogie.ac-nantes.fr/numerique/scenarios-pedagogiques/ <!--\* PrimLangues https://primlangues.education.fr/ressources/pratiques-innovantes/usages-tice/primtice -->

---

# Objectif

## Réaliser une présentation synthétique de la situation d'apprentissage (:hourglass_flowing_sand: 5 minutes)

---

# Support de la présentation :

* diaporama
* carte mentale
* autre

\:bulb: La présentation peut (doit) être illustrée.

---

# Eléments attendus dans la présentation

```
Contexte :
- Domaine(s) d’enseignement concerné(s) par la situation
- Niveau de classe
- Période de l'année où la situation est mise en œuvre
- Objectif(s) d’apprentissages : 
	- Compétences disciplinaires visées en termes de « être capable de…», (ces compétences sont issues des programmes)
	- Compétences numériques développées (issues du CRCN)
	
Environnement numérique impliqué dans la situation :
- Outils physiques (TBI, tablettes, ordinateurs fixes ou portables, caméras, enregistreurs, ...)
- Organisation matérielle (nb de matériels par élève/classe, salle informatique ou salle de classe)
- Logiciels ou applications utilisés.

Mise en œuvre :
- Organisation pédagogique (quelles activités, quand, comment, ...)
- Production(s) attendue(s)
- Modalités d'évaluation des compétences disciplinaires visées
```

---

# Séance 2

---

# Déroulement

* Rappel des objectifs (5’)
* Inscription sur la plateforme (5’)
* Finalisation de la présentation de la situation d'apprentissage (15')
* Chaque binôme présente la situation d'apprentissage qu'il a choisie
* Rédaction de la description de la situation pour le dossier final.

---

# Inscription des binômes sur la plateforme

2 étapes:

* L’un des étudiants crée un groupe en indiquant les deux noms dans le titre
* Le deuxième étudiant s’inscrit dans le groupe qui vient d’être ouvert

---

# 1\. Création du groupe

Inscription du premier participant

1. ![width:500px](images/GroupeE1PE.png)
2. ![](images/GroupeE2.png)
3. ![width:600px](images/GroupeE3PE.png)

---

# 2\. Inscription du 2e participant

![width:800px](images/GroupeE4PE.png)

Désinscription possible

![width:800px](images/GroupeE5PE.png)

---

# Présentation des situations

* 5 minutes par binôme

---

# Rédaction de la description

Document sous une forme textuelle (traitement de texte)

* Un document par binôme
* Taille : maximum 1 page et demie
* Police : Arial, taille 12, interligne 1,5 cm, marges 2,5 cm

---

# Rappel des élements attendus dans la description

```
Contexte :
- Domaine(s) d’enseignement concerné(s) par la situation
- Niveau de classe
- Période de l'année où la situation est mise en œuvre
- Objectif(s) d’apprentissages : 
	- Compétences disciplinaires visées en termes de « être capable de…», (ces compétences sont issues des programmes)
	- Compétences numériques développées (issues du CRCN)
	
Environnement numérique impliqué dans la situation :
- Outils physiques (TBI, tablettes, ordinateurs fixes ou portables, caméras, enregistreurs, ...)
- Organisation matérielle (nb de matériels par élève/classe, salle informatique ou salle de classe)
- Logiciels ou applications utilisés.

Mise en œuvre :
- Organisation pédagogique (quelles activités, quand, comment, ...)
- Production(s) attendue(s)
- Modalités d'évaluation des compétences disciplinaires visées
```

---

# Rappel des 5 thèmes d'analyse

* Évaluer et s’auto‑évaluer avec le numérique
* Production et usages de ressources numériques
* Numérique et juridique
* Enseigner avec le numérique : quelques zones à risque
* Effets du numérique sur les apprentissages
