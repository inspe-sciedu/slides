---
marp: false
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

## E.C. 33 Enseigner avec le numérique : quelques zones à risques
### Philippe Dessus, Inspé, LaRAC, Univ. Grenoble Alpes
#### *Avec la participation de Christophe Charroud*
#### Master 2 MEEF PE-SD • 2022-23

---
# :zero: Introduction

<!-- ajouts
https://www.cairn.info/revue-deviance-et-societe-2019-3-page-299.htm?ref=doi
{Samochowiec, 2020 $13568}
{Hornsey, 2017 $13633}
https://journals.openedition.org/questionsdecommunication/30076#ftn7
https://www.businessinsider.com/police-getting-help-social-media-to-prosecute-people-seeking-abortions-2023-2?utm_campaign=business-sf&utm_medium=social&utm_source=twitter&r=US&IR=T (aide contre l'avortement des GAFAM)
le pb de la pastèque : https://www.youtube.com/watch?v=ize9m_-kqGE buzzfeed / 4000 semaines>
{Acerbi, 2019 $24459} {Tornberg, 2022 $24476}https://integrityinstitute.org/our-ideas/hear-from-our-fellows/misinformation-amplification-tracking-dashboard
{Braghieri, 2022 $24487}
https://www.cairn.info/revue-internationale-d-intelligence-economique-2021-1-page-119.htm (pour les documenteurs, infox déguisée en information)
https://www.iza.org/publications/dp/15811/daily-use-of-social-media-is-associated-with-more-body-dissatisfaction-of-teenage-girls-in-a-large-cross-cultural-survey
{Allchin, 2022 $13544}
{Ziemer, 2022 $13494}
{McIntyre, 2022 $13499}
{Altay, 2023 $683}
http://eprints.lse.ac.uk/33731/8/EU_Kids_Online_Summary_Risks_safety_internet%28lsero%29.pdf
Bartsch 2023 : Le cerveau identifie *avant et très rapidement* la distraction avant de se mettre en mode focus.
-->

---
# :warning:

Date-butoir de livraison du poster
- 26 avril 2023 pour les stagiaires de Chambéry

---

![bg](images/img-zar/eglise.jpg)
![bg](images/img-zar/limites.jpg)

<!-- Nous sommes formateurs d'enseignants dans le domaine du numérique depuis plus de 20 ans ; jusqu'à il y a env. 10 ans, nous jouions plutôt un rôle de prosélytes : il faut y aller ! Depuis 10 ans, la part de précautions à prendre a augmenté dans notre enseignement. -->


---

<!-- footer: Ph. Dessus • M2 PE-SD | Enseigner avec le numérique : quelques zones à risques • Inspé-UGA 2022-23 • CC:BY-NC-SA-->
<!-- paginate: true -->

# 0. Mais les alertes sont plutôt anciennes

Nombreux, et parfois anciens, débats sur les effets de la télévision, du numérique, des jeux vidéos sur divers comportements des enfants (sommeil, dépression, obésité, suicide, violence, complotisme, harcèlement, crédulité, etc.)

---
# 0. Quelques lanceurs d'alerte connus
- G. Orwell (*1984*), P. K. Dick (*Ubik*), K. Popper (*La télévision, un danger pour la démocratie*)...
- ... et plus récemment : la série *Black Mirror*, E. Snowden, etc.
- ... *Ce qu'Orwell n'a pas prédit est que nous achèterions les caméras nous-mêmes et que notre plus grande crainte serait que personne ne nous regarde* (K. Lowell Jensen)

---
# 0. Au fait, ça marche ou pas ?
*D'un certain point de vue, il y a deux problèmes avec les nouvelles technologies de l'information et de la communication. Le premier est qu'elles ne marchent pas. Le second est qu'elles marchent.* (Marx 2008 p. xiii)

1. On peut gaspiller des ressources, du temps, de la crédibilité à les utiliser
2. On peut créer une société dans laquelle on ne voudra pas vivre

---
# 0. Pas d'effets positifs du numérique ?
L’utilisation du numérique *peut* avoir un effet positif sur l’apprentissage

- pour s'entraîner sur des tâches ciblées, pour accéder à des informations, pour communiquer (J-PAL 2019)
- mais certains effets peuvent aussi être délétères.

Les identifier et y réfléchir est une tâche importante des enseignants

---
# 0. But de la présentation
- Présenter certaines “zones à risques” de l'utilisation éducative du numérique, qui complètent la séance sur les aspects juridiques
- S'appuyer sur les recherches en sciences humaines et sociales
- Donner quelques modestes pistes d'action et de réflexion
- **Ces diapositives sont disponibles ici** : [https://link.infini.fr/404zar](hhttps://link.infini.fr/404zar)
- Le plan de ce cours suit partiellement le travail d'[EthicalOS](https://ethicalos.org/wp-content/uploads/2018/08/EthicalOS_Check-List_080618.pdf)

---
# 0. Plan
 
  1. Vérité, désinformation, déni, & propagande
  2.  Les *big data* pour personnaliser (et gouverner)
  3. Les élèves travaillent-ils pour les GAFA  ?
  4. Le numérique est-il un instrument de surveillance ?
  5. Le numérique favorise-t-il l'addiction, la distraction, le mal-être ?
  6. Le numérique favorise-t-il le harcèlement ? 
  7. Le numérique peut-il accroître l'égalité face à la connaissance ?
  8. L'usage du numérique et un peu d'éthique

---
# :one: Vérité, désinformation, déni, & propagande : Infox


---
# 1. Infox : généralités
- Internet : une gigantesque base d'informations, mais aussi de désinformation, d'infox
- :warning: L'infox ne date pas d'internet, voir les libelles du XVIIIe s. (Darnton 2010)
- Si nous ne nous reposions pas sur les autres nous ne saurions *rien*
- Besoin de partager nos expériences (et nos émotions) :arrow_right: grossir les traits des épisodes :arrow_right: provoquer une réaction émotionnelle :arrow_right:la mémorisation chez autrui :arrow_right: légendes urbaines (Heath 2001; von Hippel 2018)

---
# 1. Infox et chambre d'écho

Le fonctionnement d'Internet favorise la diffusion d'infox, mais pas nécessairement leur création, ni leur croyance

- les réseaux sociaux ne sélectionneraient pas les informations vraies, mais les informations qui se diffusent le plus (monnayables)
- ils favoriseraient la connexion de gens qui ont des opinions voisines (chambres d'écho)
- les entreprises, lobbies, groupes d'opinion influencent les débats à leur avantage (climat, tabac, médicaments, etc., Horel 2018)
- ... mais nous ne passons pas tant de temps que cela à lire les news sur Internet (3 % de notre temps connecté, soit 5 min/jour)

---
# 1. Les biais cognitifs dans la lecture (Britt *et al.* 2019 ; Gigerenzer 2022)

Nous avons certains biais, qui contribuent à la propagation des infox :

- *Réitération* : Des informations répétées sont jugées plus vraies que des infos non répétées  
- *Mémoire* : le but de la lecture influe sur la performance de lecture (qualité de la compréhension, durée de lecture, etc.). 
- *Croyance* : nos croyances modèlent la manière dont on cherche, sélectionne et traite l'information ; on se croit moins biaisé que les autres

... Mais nous disposons aussi d'outils assez efficaces pour détecter la tromperie, les inexactitudes (comme le déjà-vu). donc mieux vaut se battre **pour** l'info que **contre** la désinformation (Attard 2021)

<!-- outils : évaluer la réputation, l'alignement de buts ; voir aussi https://www.futura-sciences.com/sante/actualites/psychologie-battre-desinformation-information-96080/?utm_content=actu&utm_medium=social&utm_source=twitter.com&utm_campaign=futura ;; déjà-vu: https://www.newscientist.com/article/2101089-mystery-of-deja-vu-explained-its-how-we-check-our-memories/ -->


---
# 1. Comment se comporter face à l'infox ?

1. Apprendre à la décoder (présence d'arguments moraux et émotionnels), vérifier avant de partager (van Bavel & Packer 2021)
2. Se centrer sur les faits, éviter de répéter les infox, ne pas se laisser distraire par l'interface  (Pennycock & Rand 2021; Weingarten & Floreak 2020)
3. Donner des informations précises, toujours formulées de la même manière (redondance)  (Weingarten & Floreak 2020)
4. Dans les débats, ne pas solliciter d'arguments compatibles avec l'infox (Chan 2017)
5. Créer les conditions pour scruter et contre-argumenter l'infox, par exemple avec la “lecture latérale” (Chan 2017 ; Gigerenzer 2022 ; Mercier 2020)
6. Étiqueter une infox comme telle, sans arguments, peut ne pas être efficace (Chan 2017)

---
# 1. Discussion

:interrobang: Dans votre situation, quelle est la probabilité que vos élèves puissent tomber sur une infox ?
:interrobang: Avez-vous dû expliquer à vos élèves (ou vos proches) qu'ils s'étaient fait piéger par une infox ? Comment avez-vous fait ?

:scroll:
- Attard J. (2021). [Internet et désinformation, une fake news ?](https://cortecs.org/informations-medias/internet-et-desinformation-une-fake-news/) Blog Cortecs.
- Dessus P. (2018). [Les infox](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/fake-news.html)
- Dessus P. & Charroud C. (2020). [Théories du complot et internet](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/complotisme.html)
- Fondation Descartes (2021). [Comment les Français s'informent-ils sur internet ?](https://www.fondationdescartes.org/wp-content/uploads/2021/03/Etude_Information_Internet_Presentation.pdf?)
- Manoogian, J. [Le codex des biais cognitifs](https://fr.wikipedia.org/wiki/Fichier:The_Cognitive_Bias_Codex_\(French\)_-_John_Manoogian_III_\(jm3\).svg)
- Mercier, H. (2022). [Audition par la Commission Bronner](https://youtu.be/lK5Hl0V-4KQ). :tv:

<!-- Non, sans doute aussi : comment Internet fonctionne, comment nous fonctionnons... -->
  
---
# :two: Les *big data* pour personnaliser et gouverner

<!-- Internet peut-il permettre de régler ces problèmes de biais ? Pas si aisément puisque nous allons voir dans cette 2e section qu'il existe des filtres supplémentaires dans la manière dont l'information est présentée aux lecteurs -->

---
# 2. Biais algorithmiques à des fins de personnalisation
- On dit fréquemment que l'informatique permet d'avoir des informations, des décisions objectives ou neutres. C'est oublier que ce sont des humains qui ont réalisé les programmes et sélectionné les corpus pour les entraîner, avec leurs *a priori*
- Les systèmes détectent moins bien les cas particuliers, minoritaires car ils sont entraînés par des cas représentatifs de la majorité
  
<!--Récemment, les Pays-Bas ont stoppé l'utilisation d'un programme détectant les fraudeurs : il détectait plutôt les immigrés et les pauvres. https://www.wired.com/story/europe-limits-government-algorithm-us-not-much/ -->

---
# 2. Bulle de filtrage

- On ressemble fort au média qu'on consulte, mais les réseaux sociaux et moteurs de recherche nous placeraient dans des “bulles de filtrage”, difficiles à mettre en évidence et éviter (Miconi 2014; Pariser 2011). On a toutefois montré que :
  - relayer une information ne signifie pas nécessairement qu'on y croie, ou qu'on va changer son comportement (Altay *et al*. 2023)
  - plus les personnes utilisaient des réseaux sociaux, plus elles étaient confrontées à des actualités diverses (Jones-Jang & Chung 2022; Scharkow 2020)
  - les personnes proches des idées démocrates devenaient plus libérales après avoir suivi des fils *Twitter* libéraux (Bail *et al*. 2018)
  - les algorithmes de sélection augmentent la diversité des information plus qu'ils ne la réduisent (Arguedas *et al*. 2022)  

---
# 2. Une personnalisation opaque et mal connue

“Pourquoi *FaceBook* [FB] ne montre pas chaque item des gens qu'ils suivent ?” Sur 150 étudiants répondant, seulement 1 mentionne l'existence d'un algorithme (Powers 2017)

- *Popularité* (13) : “FB ne montre que les news les plus populaires ou les plus likées”
- *Contrôle de l'utilisateur* (6) : “C'est filtré parce que j'ai coché l'option pour dire 'je ne veux pas voir ceci', donc ils évitent de montrer un contenu de ce type”
- *Force de la relation* (5) : “Je pense que FB sélectionne certains posts pour te montrer le nombre de fois que tu visites certaines pages et tu interagis avec elles”
- *Récence* (4) : “Les trucs les plus récents sont en haut de la page”

- FB ne permet pas l'utilisation de ses données à des fins de recherche indépendante (Ertzscheid 2021)

---
# 2. Gouverner l'éducation par les données

<!-- _class: t-80 -->

Les données sur l'éducation existent depuis l'essor des statistiques (Hacking 2002) ; les larges enquêtes évaluatives deviennent un but plutôt qu'un moyen d'évaluer les élèves (Lussi Borer & Lawn 2013). Risques :

- *d'usage intensif de données d'apprentissage* pour permettre aux enseignants et parents de monitorer les progrès des élèves (*cf.* écoles chinoises "*high tech*")
- *de vente de l'apprentissage personnalisé*, qui a de multiples sens : aller à son rythme, avoir un avis sur ce qu'on veut faire ensuite, se voir proposer des contenus en lien avec ses compétences/connaissances, avoir un plan d'apprentissage (Garrick et al. 2017)
- *de standardiser l'enseignement*, d'enfermer l'élève dans des "styles d'apprentissage", un profil. Être dépendant de listes de lecture pour apprendre...
- *de déléguer le calcul de notes à des algorithmes*, par délégation (*cf.* le calcul automatique d'une note pour pallier l'absence de notes due à la pandémie, dans le secondaire en Grande-Bretagne, Mead & Neves 2022)
  
---
# 2. Discussion

<!-- _class: t-80 -->

:interrobang: L'apprentissage personnalisé peut-il parfois nuire à l'apprentissage personnel (apprendre par *playlists* ? Décider de ce qu'on doit apprendre ? Qu'y perdrait-on par rapport à l'apprentissage en classe “standard” où l'on peut se confronter à l'avis de tous ?)
:interrobang: Dans votre situation, pourriez-vous utiliser des outils d'évaluation ou d'exercices automatiques ? Avec quelles précautions ?

:scroll:
- Wall Street Journal (2019, 19 sept). [Under AI's watchful eye, China wants to raise smarter students](https://on.wsj.com/330KHw7)
- Chatellier, R. (2017). [Learning analytics: quelles sont les données du problème ?](https://linc.cnil.fr/fr/learning-analytics-quelles-sont-les-donnees-du-probleme)
- Loiseau, M. (2014). [Notions d'algorithmique pour comprendre les médias sociaux : exemples d'enjeux de l'“ouverture”](https://youtu.be/_dNHQ-ucLtM) :tv:
- Watters, A. (2014). [The problem with “personalization”](http://hackeducation.com/2014/09/11/personalization)
- Watters, A. (2017). [The histories of personalized learning](http://hackeducation.com/2017/06/09/personalization)
  
---
# :three: Les élèves travaillent-ils pour les GAFA  ?

---
# 3. Produire sur Internet est un travail immatériel

- Créer un contenu sur Internet est un travail intensif : s'approprier les technologies, les modes d'édition, mettre à jour le contenu, entretenir un réseau social... (Terranova 2004)
- Les compagnies promeuvent la construction individuelle de contenu pour cette raison : travail :arrow_right: accès :arrow_right: attention :arrow_right: valeur
- Les GAFA, et d'autres compagnies, deviennent des “chevaux de Troie” qui s'immiscent de plus en plus dans le travail scolaire (aide à la création de contenu pédagogique)
- Elles sont de plus en plus difficiles à éviter (p. ex. Amazon est le plus important possesseur de puissance de calcul et stockage)
  
---
# 3. Dur, dur d'être un bébé (1D)

- Les vidéos produites par des enfants (parfois seulement âgés de 4 ans) relève plus du travail que du loisir
- Principalement des tests ou déballage de produits
- Cf. les chaînes *YouTube* [Swan the voice/Néo & Swan](https://www.youtube.com/channel/UCzYC9ss2P77Ry2LzIDL5Xsw), [Studio BubbleTea](https://www.youtube.com/user/StudioBubbleTea) et autres
- Loi promulguée en 2020 : [Exploitation commerciale de l'image d'enfants de moins de 16 ans sur internet](http://www.assemblee-nationale.fr/dyn/15/dossiers/exploitation_commerciale_image_enfants) rapporteur, B. Studer)
  
---
# 3. Utilisation de smartphones par des élèves en classe (2D)

Étude (Paakkari *et al*. 2019) sur 3 ans, en Finlande, élèves de 16-18 ans

- usage très variable (de 6 à 22 % du temps scolaire)
- les messages non reliés à l'enseignement occupent 60 % de l'accès (*SnapChat*, *WhatsApp*)
- permettent aux ados d'organiser leur vie

---
# 3. Zoom : le projet screenome

![w:1000](images/img-zar/screenome.jpg)

Utilisation sur 21 jours d'un ado., 14 ans, 186 accès/jour d'env. 1,2 min (Reeves *et al*. 2020)

---
# 3. Le micro-travail : l'uberisation d'internet

- Si beaucoup d'enfants travaillent déjà plus officiellement pour les GAFA (chaînes *YouTube* de tutos ou de jeux vidéos)
- Certains "micro-travaillent" déjà en *via* des plates-formes comme le [Turc mécanique](https://www.mturk.com) (*Mechanical Turk*), [Foule Factory](https://www.foulefactory.com), ou [Click Worker](https://www.clickworker.fr/clickworker/) qui peuvent prendre plus d'importance encore à l'avenir
- Actuellement, env. 3 % des 12-20 ans utilisent le Mechanical Turk aux USA (source [MTurk Trakcker](https://demographics.mturk-tracker.com/#/gender/all))
- Le clic est rémunéré 0,00008 $ US en Inde ; en France, le “salaire” moyen d'un micro-travailleur est de 21 €/mois (Casilli *et al.* 2019)
  
---
# 3. Discussion

<!-- _class: t-80 -->

:interrobang: Vous intéressez-vous aussi **à qui accède** à vous, à vos élèves, et pour savoir quoi ? (plutôt que de regarder à quoi ils ou vous accédez). 
:interrobang: Votre situation nécessite-t-elle un accès au numérique hors école ? Quelles données de vos élèves vont-elles être traitées par des sociétés ? Si oui, comment comptez-vous restreindre cet accès/ces données ?

:scroll:
- Bouteloup, J. (2020). [Logiciels éducatifs, aspirateurs à données personnelles ?](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/tuto-donnees-perso.html)
- Casilli *et al*. (2019). *Le micro-travail en France**. [Diplab](http://diplab.eu)
- Casilli (2021). *En attendant les robots*. Paris : Seuil.
- MENJ (2019, 17 sept.) [Les enjeux de la protection des données au sein de l'éducation](https://www.education.gouv.fr/cid145020/les-enjeux-de-la-protection-des-donnees-au-sein-de-l-education.html%23Infographie_10_principes_cles_pour_proteger_les_donnees_de_vos_eleves)
- Sainz-Pardo, I. [Broccoli](https://youtu.be/EoZHR941yLs) :tv:
- Semuels A. (2018). [The internet is enabling a new kind of poorly paid hell](https://www.theatlantic.com/business/archive/2018/01/amazon-mechanical-turk/551192/). *The Atlantic*.

---
# :four: Le numérique est-il un instrument de surveillance ?

<!-- Même si le mot “surveillance” n'a pas été prononcé, ce qu'on vient de traiter dans les 2 précédentes sections laisse entendre que le numérique est un bon outil de surveillance, à la fois des élèves et des enseignants -->

---
# 4. Une surveillance dès la naissance ?

- Surveillance des élèves : vidéo-“protection”, absences *via* biométrie, etc.
- Surveillance (possible) des enseignants *via* les plate-formes institutionnelles (ENT, Magistère)
- :warning: surveillance sociale plus globale des enfants, *dès le début* (Marx & Steeves 2010) : sites de rencontre, compatibilité génétique, tests prénataux, interphones pour bébés, vêtements RFID, téléphones GPS, logiciels de contrôle de l'activité internet, boîte noire dans voitures, tests d'usage de la drogue ou de rapports sexuels...

---
# 4. Que peut-on connaître de nous ?

Prédire la réussite d'un groupe à partir de sa photo à l'entrée d'un *Escape Game* (Saveski *et al*. 2021).  Le groupe réussit d'autant mieux qu'il comprend plus de personnes, qu'elles sont plus âgées et d'âge peu dispersé, sont plus mixtes, plus souriants (analyse sur 43k photos).

- Performance humaine : **58,5 %** sans entraînement ; **67,4 %** avec entraînement. Performance de la machine : **71,6 %**

![w:700](images/img-zar/escape-room.jpg)

---
## 4. Les limites de l'observabilité (van Est, 2014)

Les avancées du numérique franchissent allègrement les limites habituelles de l'observation humaine :

- *limites naturelles* : l'émotion, le regard, certains paramètres physiologiques sont maintenant traqués
- *limites sociales* : confidentialité
- *limites spatiales et temporelles* : ce qu'on fait ici et maintenant est enregistré

Ce à des fins déclarées de sécurité, monitorage, bien-être (Hoffmann & Mariniello 2021). La durée de conservation des informations recueillies n'est pas connue, ni leur utilisation future

<!-- Sécurité : faire entrer les bonnes personnes ; monitorage : gérer les présents/absents ; bien-être : bracelets de -->

---
# 4. Le recours au numérique pendant la pandémie ([Human Rights Watch](https://www.hrw.org/sites/default/files/media_2022/07/French_EdTech%20Report_Sum%26Recs.pdf), 2022)

> *Sur les 163 produits EdTech examinés, 145 (89 %) semblent s’être livrés à des pratiques de données qui mettent en danger les droits des enfants, ont contribué à les saper ou ont activement enfreint ces droits. Ces produits surveillaient, ou avaient la capacité de surveiller, les enfants, dans la plupart des cas secrètement et sans le consentement des enfants ou de leurs parents, récoltant des données sur qui ils sont, où ils se trouvent, ce qu'ils font en classe, qui sont leur famille et leurs amis, et quel type d'appareil leur famille peut se permettre de leur procurer.*
  
---
# 4. Discussion

:interrobang: Comment se préparer à  contrer des technologies de surveillance qui n'existent pas encore ? Risque d'escalade des fonctions (une fonction banale va pouvoir servir à d'autres fins plus problématiques)
:interrobang: Dans votre situation et/ou dans votre établissement/école, y a-t-il des éléments concourant à une surveillance des élèves ?

:scroll:
- Dessus P. (2013). [Informatique et école : vers une éducation citoyenne ?](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/droitinfosoc.html)

---
# :five: Le numérique favorise-t-il l'addiction, la distraction, le mal-être ?

---
# 5. Estimer le temps passé sur écran ?

Les études sur le temps passé par les adolescents sur le numérique manquent de fiabilité (Orben & Przybylski 2019)

- on perçoit difficilement le temps passé dans toute activité, surtout si elle est prenante
- on a beaucoup de raisons personnelles de distordre ces données (biais de désirabilité), les grands utilisateurs les sous-estiment, les faibles utilisateurs les sur-estiment
- ce temps n'est pas nécessairement mal utilisé : important pour la socialisation des adolescents (Cordier 2020 ; Déage 2018)
- ce temps est variable selon les personnes et les catégories sociales (voir plus haut le projet screenome)

---
# 5. Ce qui est sûr : ça occupe !

![w:800](images/img-zar/soc-media-act.png)

(Meshi *et al.* 2015)

<!-- La comparaison ne porte pas seulement sur ce qui est posté/liké, mais sur les informations liées au statut (nombre de suiveurs, etc.) -->

---
# 5. Comment ça marche ? (Kross *et al.* 2020)

Communiquer *via* les réseaux sociaux (RS) joue sur 2 processus importants dans le bien-être :

- *la présentation de soi* : les RS donnent des opportunités d'agir sur la manière dont on se présente (plus belles photos, raconter le positif), ce qui amène des comparaisons sociales pouvant être en sa faveur (ou défaveur), et agit sur sa réputation
- *partager des émotions* : les RS permettent aisément de partager des émotions, qui sont ensuite validés par les autres ; mais les autres peuvent aussi les invalider, et les RS permettent aussi aisément le harcèlement, ou la diffusion de scandales

Cela montre que les bénéfices peuvent être aussi bien positifs ou négatifs

---
# 5. Liens utilisation du numérique et bien-être (2D)

Une étude à large échelle (+11 000) sur des adolescents et avec rapports quotidiens du temps d'utilisation montre un effet négatif mais très faible (Orben & Przybylski 2019) :

- il faudrait un temps d'usage (extrapolé) de 63 h par jour pour que les ados baissent leur bien-être d'un demi écart-type (valeur considérée comme perceptible)
  
---
# 5. Liens utilisation du numérique et réussite scolaire

Une méta-analyse de plus de 5 500 études (480 000  participants de 4-18 ans), d'Adelantado-Renau *et al*. 2019) montre 

* qu'il n'y a pas de lien entre temps d'écran (qq soit l'écran) et réussite scolaire
* que l'usage de la TV et des jeux vidéo sont négativement associés à la réussite scolaire

Le contexte d'utilisation de l'écran, le type d'écran, l'activité (ce qui est favorisé/gêné), jouent un rôle important qu'il faut analyser avant de se prononcer sur l'effet d'un écran

<!-- Par exemple, il est connu que regarder la TV gêne les interactions sociales. -->

---

![bg 60%](images/img-zar/journaux.jpg)
  

<!-- Évoquer l'étude où les participants restent 15 min et se donnent des chocs élec pour passer le temps -->
  

---
# 5. Discussion

<!-- _class: t-70 -->

:interrobang: Comment peser le coût et le bénéfice de l'utilisation de chaque média sur l'apprentissage ?
:interrobang: Travailler sur le contrôle des outils numériques plutôt que de se faire contrôler par eux
:interrobang: Dans votre situation, bornez-vous le temps d'utilisation du numérique par vos élèves, en classe et à la maison ?

:scroll: 
- Desmurget, M. (2019). *La fabrique du crétin digital*. Paris : Seuil.
- Dessus P. (2018). [Numérique, addiction et attention](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/media-attention.html)
- Erhel S. & Gonthier C (2021). [L'exposition aux écrans est-elle dangereuse pour le cerveau?](https://youtu.be/KdfUiYF1XDU) :tv:
- Forget-Dubois N. (2020). [Les discours sur le temps d'écran](https://www.cse.gouv.qc.ca/wp-content/uploads/2020/09/50-2110-ER-Temps-ecran.pdf). Québec : Le conseil.
- Gazzaley & Rosen (2018).[Remedies for the distracted mind](https://behavioralscientist.org/remedies-distracted-mind/)
- Haidt, J. & Twenge, J. (en cours). [Social media and mental health: A collaborative review](https://docs.google.com/document/d/1w-HOfseF2wF9YIpXwUUtP65-olnkPyWcgF5BiAtBEy0/edit#)
- Ramus, F. (2020). [Les écrans vont-ils nous rendre stupides ?](https://youtu.be/6xpssl5mwgk) :tv:
- Les designers éthiques (2019). [Méthode de diagnostic du design attentionnel](https://attention.designersethiques.org/asset/methode/methodeDeDiagnostic-0.9.pdf)
  
---
# :six: Le numérique favorise-t-il le harcèlement ?

---
## 6. Harcèlement vs. cyberharcèlement (Blaya 2018)

- Violence hors ligne :left_right_arrow: violence en ligne (un élève harcelé hors ligne a 7 fois plus de risque de l'être en ligne)
- *Anonymat amplifié* : réduit les capacités de maîtrise des victimes, limite les niveaux d’empathie des agresseurs
- *Dissémination démultipliée*, diffusion instantanée, pas de répit pour les victimes ; pas de maîtrise sur la diffusion ; nombre de témoins potentiels illimités
- *Information persistante* : Contrairement au harcèlement, pas de nécessité de répétition de l'acte pour parler de cyberharcèlement
- Avoir et garder sa réputation (filles) et sa popularité (garçons) est un effort inégalitaire car demande une vigilance disproportionnée des filles
  
---
# 6. (Cyber)-harcèlement ou violence

<!-- _class: t-80 -->

Pas de consensus quant à leur définition, ni dans les méthodes pour y faire face

- difficulté à établir une prévalence (pour la France et au collège, chiffres de 16 à 42 %)
- Hubert (2018) montre qu'un lycéen sur 5 a subi une violence ou insulte par internet ou téléphone dans l'année...
	- ... et que les victimes de vidéos humiliantes ont doublé en 3 ans (4 à 9 %)
	- difficulté à mesurer les conséquences
	- difficulté à évaluer les stratégies de maîtrise des protagonistes
- propension à cibler le genre féminin (Biros-Bolton 2021 ; Latourès & Couchot-Schiex 2016) et plus récemment les enseignants
  
Si l’outil numérique a un effet loupe, il est plus le symptôme de comportements inadéquats que leur cause et le contexte est un facteur-clé (Blaya 2018)

  ---
# 6. Discussion

:interrobang: Avez-vous déjà été confrontés à des cas de (cyber-)harcèlement ? Qu'avez-vous fait ? 
:interrobang: Dans votre situation, vos élèves vont-ils échanger des informations uniquement par internet ? 

:scroll:
- Signaler un contenu illicite de l'internet : [Pharos](https://www.internet-signalement.gouv.fr/PharosS1/)
- Déage (2018)
- Hubert, T (2018). [Résultats de l’enquête de climat scolaire et victimation auprès des lycéens pour l’année scolaire 2017-2018](https://archives-statistiques-depp.education.gouv.fr/Default/digital-viewer/c-44019). *Note d'information de la DEPP*, 18.33.
- Senden & Galand (2020)
- Stassin B. [Carnet de recherche sur la cyberviolence et le cyberharcèlement](https://eviolence.hypotheses.org)
  

---
# :seven: Le numérique peut-il accroître l'égalité face à la connaissance ?

---
# 7. Connaissances et numérique (Hess 2012)

Connaissance : Idées utiles, information, données, quelle que soit leur forme

- Change rapidement
- L'infrastructure numérique est complexe et mal comprise
- Les droits liés à la propriété intellectuelle ont évolué, sont complexes
- Ils sont de plus en plus restrictifs avec le numérique
- Des connaissances peuvent être perdues ([wayback machine](https://www.web.archive.org))
- Les prix d'accès augmentent continûment (manuels, revues scientifiques)
  

---
# 7. Considérer la connaissance comme un communal

Un communal : “ressource partagée par un groupe de personnes et qui est vulnérable aux dégradations et aux enclosures [privatisations]” (Le Crosnier *et al.* 2011)

- problèmes d'engorgement, de blocage, filtrage par des politiques
- susceptible d'être “pollué” (sabotage, infox, etc.)
- susceptible d'être sous-utilisé
- :warning: utiliser une ressource ne prive pas les autres (≠ avec un *vrai* communal)
  

---
# 7. Les REL, une solution ?

Difficile de définir ce qu'est une REL (Ressource éducative libre) (format ? niveau de grain ? intention pédagogique ? type de licence ?). Ce qu'on peut faire avec : Les 5R (Wiley & Hilton 2018)

- retenir (garder une copie pour soi)
- réutiliser
- réviser
- remixer
- redistribuer
  

---
# 7. MOOC/CLOM, une solution pour démocratiser la connaissance ? (2D)

![](images/img-zar/mooc-public.png)

(Hansen & Reich 2015) Gris : moyenne USA ;  Noir : HarvardX & MITx MOOC


---
# 7. L'exclusion du numérique :arrow_right: exclusion des connaissances (van Winden 2010)

- Les communautés virtuelles sont des réseaux importants :arrow_right: Exclusion sociale
- Internet comme média de participation :arrow_right: Exclusion politique
- Compétences informatiques pour accès à un travail :arrow_right: Exclusion économique
  

---
# 7. Discussion

:interrobang:  Dans votre situation, faites-vous usage de ressources libres et/ou comptez-vous diffuser votre travail sous forme de ressources libres ?

:scroll:
- [Chaire UNESCO RELIA (2022). Créons notre première REL](https://chaireunescorelia.univ-nantes.fr/creons-notre-premiere-rel/)
- [Projet ANR ReVEA: ressources vivantes pour l'enseignement et l'apprentissage](https://www.anr-revea.fr)
- [Le livre blanc contre l'illectronisme du syndicat de la presse sociale](https://sps.fr/illectronisme/laboratoire-didees-et-charte-des-bonnes-pratiques/)
  
---
# :eight: L'usage du numérique et un peu d'éthique

<!-- Pour un enseignant,  l’éthique est lié à la capacité de comprendre de façon critique le contenu et les outils numériques et ne pas nuire. Donc cela inclut le respect de la vie privée sans s'y limiter --> 

---
# 8. Principes fondamentaux (Salganik 2018)
- *Respect des personnes* (autonomes et avoir leur consentement)
- *Tendre à faire le bien* (peser le bénéfice/risque)
- *Justice* (les risques et bénéfices sont distribués convenablement, il y a compensation)
- *Respect de la loi et transparence*

<!-- Ce ne sont pas que les pauvres qui participent aux essais cliniques -->

---
# 8. Discussion

:interrobang: Invoquez-vous des principes éthiques quand vous concevez votre enseignement avec le numérique ?

---
# :nine: Vue et discussion générales

---
# 9. Vue générale, un peu pessimiste

- Certains des effets du numérique sur l'éducation sont positifs. D'autres sont délétères : le numérique s'insère partout !
- Le volet “technologie” d'une innovation ne peut être analysé sans son volet “social” (von Hippel 2018)
- Les technologies de l’éducation s’insèrent bien dans un schéma néo-libéral (Morozov 2014), notamment le “capitalisme cognitif” (ou informationnel) : produire des données par son activité, ensuite monnayables
- Difficile à contrer : les compagnies combattent l'anonymat 
  
---
# 9. Alors, que faire ?

- Réfléchir à l'impact des technologies sur l'humain, qui n'est pas déterministe : effets de boucle (Orlikowski 1992)
- :warning: Euphémisation (protection :arrow_right: surveillance), changement de vocabulaire (“enseignement personnalisé”) ; vocabulaire *bullshit* (“disruptif”)
- Doter les élèves d'outils de compréhension des technologies, mais aussi d'eux-mêmes, de la société
- Avoir soi-même des outils de scrutation éthique (DELICATE, EthicalOS, etc.)
- Se documenter sur les dispositifs non encore commercialisés (classes ambiantes, IA pour corriger, robotique, etc.)
  
---
# 9. Trois questions importantes

* Qui sait ? :arrow_right: rapport à la connaissance 
* Qui décide ?   :arrow_right: rapport à l'autorité
* Qui décide qui décide ?  :arrow_right: rapport à la puissance
(Zuboff 2020)

<!-- laisser de côté -->

---
# 9. Tâche 1
- Par groupe, choisir un des thèmes, selon votre intérêt, de manière à ce qu'ils soient tous choisis
- Reprendre les principaux éléments du thème en s'appuyant sur les notes et la présentation
- Les transposer en quelques conseils (environ  5) pouvant être appliqués en établissement/école, dans votre pratique de tous les jours. Préciser la situation d'enseignement impliquée dans le conseil.
  
 
---
# 9. Tâche 2 - Dans votre situation...

<!-- _class: t-70 -->

1. quelle est la probabilité que vos élèves puissent tomber sur une infox ?
2. pourriez-vous utiliser des outils d'évaluation ou d'exercices automatiques ? Avec quelles précautions ? 
3. nécessite-t-elle un accès au numérique hors école ? Quelles données de vos élèves vont-elles être traitées par des sociétés ? Si oui, comment comptez-vous restreindre cet accès/ces données ?
4. y a-t-il des éléments concourant (à court ou plus long terme) à une surveillance des élèves ?
5. bornez-vous le temps d'utilisation du numérique ?
6. (harcèlement) vos élèves vont-ils échanger des informations uniquement par internet ? 
7. faites-vous usage de ressources libres et/ou comptez-vous diffuser votre travail sous forme de ressources libres ?
8. invoquez-vous des principes éthiques quand vous concevez votre enseignement avec le numérique ?


---
# Merci de votre attention !
# Des questions ?

:e-mail: philippe.dessus@univ-grenoble-alpes.fr

###### Crédits photos [Unsplash](https://unsplash.com) : Eglise : Jeff Sheldon ; Panneaux :  David von Diemar


---
# Pour en savoir encore plus
- Behavioral Scientist (2018). Numéro spécial [Connected State of Mind](https://behavioralscientist.org/special-issue-connected-state-mind/)
- Ferguson, R. & Faye (2018). [A history of panic over entertainment technology](https://behavioralscientist.org/history-panic-entertainment-technology/). *Behav. Sci.*
- Kalz, M. (2019).[Unintended consequences of mainstreaming of TEL in a digitised society](https://www.slideshare.net/mkalz/unintended-consequences-of-mainstreaming-of-technologyenhanced-learning-in-a-digitised-society). Talk to 15th EATEL Summerschool on TEL, Bari.
- O'Neil (2016)
  

---
# Modèle “structurationnel” de la technologie

![w:1000](images/img-zar/techno.jpg)

(Orlikowski 1992)

---
# Références (1/5)

<!-- _class: t-50 -->


- Adelantado-Renau, M., Moliner-Urdiales, D., Cavero-Redondo, I., Beltran-Valls, M. R., Martinez-Vizcaino, V., & Alvarez-Bueno, C. (2019). Association Between Screen Media Use and Academic Performance Among Children and Adolescents: A Systematic Review and Meta-analysis. *JAMA Pediatr*.
- Altay, S., Berriche, M., & Acerbi, A. (2023). Misinformation on Misinformation: Conceptual and Methodological Challenges. *Social Media + Society, 9*(1). https://doi.org/10.1177/20563051221150412 
- Arguedas, A. R., Robertson, C. T., Fletcher, R., & Nielsen, R. K. (2022). Echo Chambers, Filter Bubbles, and Polarisation: a Literature Review. https://royalsociety.org/-/media/policy/projects/online-information-environment/oie-echo-chambers.pdf
- Bail, C. A., Argyle, L. P., Brown, T. W., Bumpus, J. P., Chen, H., Hunzaker, M. B. F., . . . Volfovsky, A. (2018). Exposure to opposing views on social media can increase political polarization. *Proc Natl Acad Sci USA**, 115(37), 9216-9221. doi: 10.1073/pnas.1804840115
- Biros-Bolton, N. (2021). Tech-facilitated violence. Toronto: LEAF.
- Blaya, C. (2018). Le cyberharcèlement chez les jeunes. *Enfance, 3*(3), 421–439.
- Borer, V. L., & Lawn, M. (2013). Governing Education Systems by Shaping Data: From the Past to the Present, from National to International Perspectives. *Europ. Educ. Res. J., 12*(1), 48-52. doi: 10.2304/eerj.2013.12.1.48
- Bozdag, E. (2013). Bias in algorithmic filtering and personalization. *Ethics and Information Technology, 15*(3), 209-227. doi: 10.1007/s10676-013-9321-6
- Britt, M. A., Rouet, J.-F., Blaum, D., & Millis, K. (2019). A Reasoned Approach to Dealing With Fake News. *Policy Insights from the Behavioral and Brain Sciences, 6*(1), 94–101. doi: 10.1177/2372732218814855
- Cardon, D., & Casilli, A. (2015). *Qu'est-ce que le Digital Labor ?*. Bry-sur-Marne : INA, coll. « Etudes et controverses ».
- Cordier, A. (2020). *Des usages juvéniles du numérique aux apprentissages hors la classe*. Paris: CNESCO-CNAM.
- Chan, M.-p. S., Jones, C. R., Jamieson, K. H., & Albarracín, D. (2017). Debunking: A Meta-Analysis of the Psychological Efficacy of Messages Countering Misinformation. *Psychological Science*, 28(11), 1531–1546. doi:10.1177/0956797617714579


---
# Références (2/5)

<!-- _class: t-50 -->

- Darnton, R. (2010). Le diable dans un bénitier. *L'art de la calomnie en France, 1650–1800*. Paris: Gallimard, coll. Essais.
- Déage, M. (2018). S’exposer sur un réseau fantôme. *Réseaux, *208-209*(2), 147-172. https://doi.org/10.3917/res.208.0147 
- Ertzscheid, O. (2021, 6 décembre). Facebook : par-delà le like et la colère. *AOC*. https://aoc.media/opinion/2021/12/05/facebook-par-dela-le-like-et-la-colere/ 
- Forget-Dubois, N. (2020). *Les discours sur le temps d’écran : valeurs sociales et études scientifiques*. Québec: Conseil supérieur de l'éducation.
- Garrick, B., Pendergast, D., & Geelan, D. (2017). *Theorising personalised education. Electronically mediated higher education*. Singapore: Springer.
- Gauvrit, N., & Delouvée, S. (Eds.). (2019). *Des têtes bien faites. Défense de l'esprit critique*. Paris: P.U.F.
- Gigerenzer, G. (2022). *How to stay smart in a smart world*. Random House. 	
- Guillory, J. E., Hancock, J. T., Woodruff, C., & Keilman, J. (2015). Text Messaging Reduces Analgesic Requirements During Surgery. *Pain Medicine, 16*, 667–672.
- Hacking, I. (2002). *L'émergence de la probabilité*. Paris: Seuil.
- Hansen, J. D., & Reich, J. (2015). Democratizing education? Examining access and usage patterns in massive open online courses. *Science, 350*(6265), 1245-1248. doi: 10.1126/science.aab3782
- Heath, C., Bell, C., & Sternberg, E. (2001). Emotional selection in memes: The case of urban legends. *Journal of Personality and Social Psychology, 81(6), 1028-1041. doi: 10.1037/0022-3514.81.6.1028
- Hinds, J., & Joinson, A. (2019). Human and Computer Personality Prediction From Digital Footprints. *Current Directions in Psychological Science, 28*(2), 204–211. doi: 10.1177/0963721419827849


---
# Références (3/5)

<!-- _class: t-50 -->

- Hoffmann, M., & Mariniello, M. (2021). Biometric technologies at work: a proposed use-based taxonomy. Policy Contribution, 23, 1–19. 
- Horel, S. (2018). *Lobbytomie. Comment les lobbies empoisonnent nos vies et la démocratie*. Paris: La Découverte.
- Jones-Jang, S. M., & Chung, M. (2022). Can we blame social media for polarization? Counter-evidence against filter bubble claims during the COVID-19 pandemic. *New Media & Society*. https://doi.org/10.1177/14614448221099591 
- Miconi, A. (2014). Dialectic of Google. In R. König & M. Rasch (Eds.), *Society of the query reader: Reflections on Web search* (pp. 31–40). Amsterdam: Institute of Network Cultures.
- Morozov, E. (2014). *Pour tout résoudre cliquez ici*. Limoges: Fyp.
- O'Neil, C. (2016). *Weapons of math destruction*. New York: Crown [trad. fr., 2018 : “*Algorithmes, la bombe à retardement*”, Paris, Les Arènes].
- Orben, A., & Przybylski, A. K. (2019). Screens, Teens, and Psychological Well-Being: Evidence From Three Time-Use-Diary Studies. *Psychological Science, 30*(5), 682–696. doi: 10.1177/09567976198330329
- Orlikowski, W. J. (1992). The duality of technology: Rethinking the concept of technology in organizations. *Organization Science, 3*(3), 398–427.
- Kross, E., Verduyn, P., Sheppes, G., Costello, C. K., Jonides, J., & Ybarra, O. (2020). Social Media and Well-Being: Pitfalls, Progress, and Next Steps. *Trends Cogn Sci*. doi: 10.1016/j.tics.2020.10.005
- Latourès, A., & Couchot-Schiex, S. (2016). Cybersexisme chez les adolescent·e·s (12-15 ans). Centre Hubertine Auclert.
- Le Crosnier, H., Ertzscheid, O., Peugeot, V. r., Mercier, S. r., Berthaud, C., Charnay, D., & Maurel, L. (2011). Vers les "communs de la connaissance". *Documentaliste – Sciences de l'Information, 48*(3), 48–59.

  

---
# Références (4/5)

<!-- _class: t-50 -->

- Markowitz, D. M., Hancock, J. T., Bailenson, J. N., & Reeves, B. (2017). The Media Marshmallow Test: Psychological and Physiological Effects of Applying Self-Control to the Mobile Phone. doi: 10.2139/ssrn.3086140 
- Marx, G. T. (2008). Foreword. In D. Wright, S. Gutwirth, M. Friedewald, E. Vildjiounaite & Y. Punie (Eds.), *Safeguards in a world of ambient intelligence*. (pp. vii-). Dordrecht: Springer.
- Marx, G. T., & Steeves, V. (2010). From the beginning: Children as subjects and agents of surveillance. *Surveillance & Society, 7*(3/4), 192–230.
- Mead, G., & Barbosa Neves, B. (2022). Contested delegation: Understanding critical public responses to algorithmic decision-making in the UK and Australia. *The Sociological Review*. https://doi.org/10.1177/00380261221105380 	
- Mercier, H. (2020). *Not born yesterday. The science of who we trust and what we believe*. Princeton: Princeton University Press.
- Meshi, D., Tamir, D. I., & Heekeren, H. R. (2015). The Emerging Neuroscience of Social Media. *Trends Cogn Sci, 19*(12), 771-782. doi: 10.1016/j.tics.2015.09.004
- Paakkari, A., Rautio, P., & Valasmo, V. (2019). Digital labour in school: Smartphones and their consequences in classrooms. *Learning, Culture and Social Interaction, 21*, 161-169. doi: 10.1016/j.lcsi.2019.03.004
- Pariser, E. (2011). *The filter bubble: What the Internet is hiding from you*. London: Penguin UK.

---
# Références (5/5)

<!-- _class: t-50 -->

- Pennycook, G., & Rand, D. G. (2021). The psychology of fake news. *Trends in Cognitive Sciences*. https://doi.org/10.1016/j.tics.2021.02.007 	
- Powers, E. (2017). My News Feed is Filtered? *Digital Journalism, 5*(10), 1315-1335. doi: 10.1080/21670811.2017.1286943
- Reeves, B., Robinson, T., & Ram, N. (2020). Time for the Human Screenome Project. *Nature*, 577, 314–317. doi: 10.1038/d41586-020-00032-5
- Salganik, M. J. (2018). *Bit by bit. Social research in the digital age*. Princeton: Princeton University Press.
- Saveski, M., Awad, E., Rahwan, I., & Cebrian, M. (2021). Algorithmic and human prediction of success in human collaboration from visual features. *Nature Scientific Reports*, *11*(1). doi: 10.1038/s41598-021-81145-3
- Scharkow, M., Mangold, F., Stier, S., & Breuer, J. (2020). How social network sites and other online intermediaries increase exposure to news. *Proc Natl Acad Sci USA*. doi:10.1073/pnas.1918279117
- Senden, M., & Galand, B. (2020). Comment réagir face à une situation de harcèlement à l’école ? Une synthèse de la littérature. *Pratiques Psychologiques*. doi: 10.1016/j.prps.2020.09.006
- Terranova, T. (2004). *Network culture. Politics for the information age*. London: Pluto Press.
- Van Bavel, J. J., & Packer, D. J. (2021). *The power of us*. London: Headline.
- van Est, R. (2014). Intimate technology. The battle for our body and behaviour. The Hague: Ratheneau Instituut.
- van Winden, W. (2010). The End of Social Exclusion? On Information Technology Policy as a Key to Social Inclusion in Large European Cities. *Regional Studies, 35*(9), 861–877. doi: 10.1080/00343400120090275
- von Hippel, W. (2018). *The social leap*. New York: HarperCollins.
- Weingarten, E., & Floreak, R. (2020). Why We’re All Likely Spreading Misinformation, and How to Stop. [https://behavioralscientist.org/why-were-all-likely-spreading-misinformation-and-how-to-stop/](https://behavioralscientist.org/why-were-all-likely-spreading-misinformation-and-how-to-stop/)
- Zuboff, S. (2020). *L'âge du capitalisme de surveillance*. Paris : Zulma.
  

---
#Réfs EN

{Arguedas, 2022 $23967}
{Britt, 2019 $21566}
{Blaya, 2018 $21649}
{Borer, 2013 $21660}
{Chan, 2017 $22105}
{Bozdag, 2013 $21658}
{Cordier, 2020 $23750}
{Darnton, 2010 $12964}
{Ertzscheid, 2021 $23969}
{Guillory, 2015 $21646}
{Hacking, 2002 $21661}
{Heath, 2001 $21643}
{Hoffmann, 2021 $23950}
{Markowitz, 2017 $21645}
{Marx, 2008 $21653}
{Miconi, 2014 $21657}
{Morozov, 2014 $17472}
{Orlikowski, 1992 $21654}
{Pariser, 2011 $18031}
{Powers, 2017 $21659}
{Senden, 2020 $23710}
{Biros-Bolton, 2021 #23373}
{van Winden, 2010 #21655}
{von Hippel, 2018 #21642}
{Bail, 2018 $23709}
{Garrick, 2017 $21447}
{Orben, 2019 $21662}
{Terranova, 2004 $21663}
{Paakkari, 2019 $21618}
{Marx, 2010 $21578} 
{Horel, 2018 $21667}
{Hinds, 2019 $21664}
{Meshi, 2015 $21666}
{Mercier, 2020 $22143}
{Le Crosnier, 2011 #21669}
{Salganik, 2018 $21656}
{Hansen, 2015 $21404}
{O'Neil, 2016 #17812}
{Gauvrit, 2019 $21416}
{Adelantado-Renau, 2019 #21919}
{Scharkow, 2020 $22104}
{Kross, 2020 $22603}
{Saveski, 2021 $22753}
{Van Bavel, 2021 #23729}
