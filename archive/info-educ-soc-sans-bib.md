---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

## Enseigner avec le numérique : quelques aspects critiques
### Philippe Dessus
### Inspé, Univ. Grenoble Alpes
#### Master MEEF, Année univ. 2024-25

---
# :zero: Introduction

---
# 0. Lien vers la présentation

![bg right](images/img-zar/qr-critiq.jpg)
- [https://link.infini.fr/meef-uga-num-critique](https://link.infini.fr/meef-uga-num-critique)

---
# 0. Introduction

- On dit souvent que l'informatique est là pour aider l'éducation
- Enseigner *avec* l'informatique signifie qu'on peut aussi enseigner *sans*, que l'informatique n'est qu'un outil qui s'*ajoute* à l'enseignement (pour l'aider, l'assister, l'améliorer)
- On pourrait aussi dire que l'éducation *est là pour aider* l'apprentissage de l'informatique, et toute autre technologie (par exemple, le livre et les moyens de les comprendre, la lecture)

:books: {Wegerif, 2024 $14401}

---
# 0. Vision critique de l'EdTech (technologies éducatives)

- Une partie des utilisations de l'EdTech est problématique et il est nécessaire de les critiquer
    - Quelles valeurs sont poursuivies dans l'utilisation d'une technologie ?
    - Son utilisation sert quels intérêts ? Qui récupère du pouvoir ?
    - Qui est affecté négativement par son utilisation ?

:books: {Selwyn, 2023 $13792}{Nagy, 2024 $14395}

---
# 0. L'EdTech peut tout aussi bien nous permettre…

1.  d'accéder à des **informations fiables**… ou des **fadaises**
2.  de nous aider … ou nous remplacer, nous surveiller
3.  de nous permettre d'apprendre … ou d'apprendre de nous
4. de nous inclure … ou nous exclure

:books: {Selwyn, 2019 $1889}

---
# 0. Rappel : Catégories d'outils EdTech

- Les “**machines à enseigner**” : machines, tout d'abord mécaniques, où l'élève répond à une question sur un disque de papier, et se voit ensuite proposer la réponse. **Behaviorisme, instruction directe**
- Les “**machines à enseigner + IA**” : les réponses de l'élève sont ici analysées automatiquement par divers systèmes statistiques analysant les réponses des élèves
- Les “**outils pour penser avec**“ : agir avec un outil pour réaliser des objets (*e.g.*, programmer avec [Scratch](https://scratch.mit.edu)) **constructionnisme**
- Les “**environnements d'apprentissage**” : utiliser des forums de discussion pour construire des connaissances par le débat  **socio-constructivisme**

:books: {Watters, 2021 $865}{Wegerif, 2024 $14401} 

---
# :one: Fadaises (Infox) ou informations fiables ?


---
# 1. Infox : généralités

- Internet : une gigantesque base d'informations, mais aussi de désinformation, d'infox
- :warning: L'infox ne date pas d'internet, voir les libelles du XVIIIe s. {Darnton, 2010 $10863}
- Si nous ne nous reposions pas sur les autres nous ne saurions *rien*
- Besoin de partager nos expériences (et nos émotions) :arrow_right: grossir les traits des épisodes :arrow_right: provoquer une réaction émotionnelle :arrow_right:la mémorisation chez autrui :arrow_right: légendes urbaines {Heath, 2001 $2289}{von Hippel, 2018 $2290}

---
# 1. Infox et chambre d'écho

Le fonctionnement d'Internet favorise la diffusion d'infox, mais pas nécessairement leur création, ni leur croyance
- les réseaux sociaux ne sélectionneraient pas les informations vraies, mais les informations qui se diffusent le plus (monnayables)
- ils favoriseraient la connexion de gens qui ont des opinions voisines (chambres d'écho)
- les entreprises, lobbies, groupes d'opinion influencent les débats à leur avantage (climat, tabac, médicaments, etc.) {Horel, 2018 $2265}{Oreskes, 2021 $14048} et créent des chambres d'écho

---
# 1. Manipulation dans les réseaux sociaux {van der Linden, 2023 $13874}

Les 6 degrés de la manipulation : **DEPICT**
- **D**iscréditer : attaquer la source de la critique
- **E**motion : avancer des arguments émotionnels (+ ou –)
- **P**olarisation : favoriser les messages qui divisent
- **I**mpersonation (usurpation) : créer des faux experts, ou de faux comptes de vraies personnes
- **C**onspiration : évoquer des théories conspiratrices
- “**T**rolling” : empoisonner les débats avec des remarques provocatrices

---
# 1. Les biais cognitifs dans la lecture {Britt, 2019 $2358}{Gigerenzer, 2022 $67}

<!-- _class: t-90 -->

Nous avons certains biais, qui contribuent à la propagation des infox :

- *Réitération* : Des informations répétées sont jugées plus vraies que des infos non répétées  
- *Mémoire* : le but de la lecture influe sur la performance de lecture (qualité de la compréhension, durée de lecture, etc.). 
- *Croyance* : nos croyances modèlent la manière dont on cherche, sélectionne et traite l'information ; on se croit moins biaisé que les autres

... Mais nous disposons aussi d'outils assez efficaces pour détecter la tromperie, les inexactitudes (comme le déjà-vu). donc mieux vaut se battre **pour** l'info que **contre** la désinformation {Attard, 2021 $14412}

---
# 1. Comment se comporter face à l'infox ?

<!-- _class: t-80 -->

1. Apprendre à la décoder (présence d'arguments moraux et émotionnels), vérifier avant de partager {Van Bavel, 2021 $776}
2. Se centrer sur les faits, éviter de répéter les infox, ne pas se laisser distraire par l'interface  {Pennycook, 2021 $1118}{Weingarten, 2020 $1138}
3. Donner des informations précises, toujours formulées de la même manière (redondance) {Weingarten, 2020 $1138}
4. Dans les débats, ne pas solliciter, ni rediffuser, des arguments compatibles avec l'infox {Chan, 2017 $1844}
5. Créer les conditions pour scruter et contre-argumenter l'infox, par exemple avec la “lecture latérale” {Chan, 2017 $1844}{Gigerenzer, 2022 $67}{Mercier, 2020 $1806}
6. Étiqueter une infox comme telle, sans arguments, peut ne pas être efficace {Chan, 2017 $1844}
7. “Inoculer” des propositions pour mieux faire évaluer les infox {van der Linden, 2023 $13874}

---
# 1. Discussion & références supplémentaires

:interrobang: Dans votre situation, quelle est la probabilité que vos élèves puissent tomber sur une infox ?

:scroll:
- Attard J. (2021). [Internet et désinformation, une fake news ?](https://cortecs.org/informations-medias/internet-et-desinformation-une-fake-news/) Blog Cortecs.
- Dessus P. (2018). [Les infox](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/fake-news.html)
- Dessus P. & Charroud C. (2020). [Théories du complot et internet](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/complotisme.html)
- Fondation Descartes (2021). [Comment les Français s'informent-ils sur internet ?](https://www.fondationdescartes.org/wp-content/uploads/2021/03/Etude_Information_Internet_Presentation.pdf?)
- Mercier, H. (2022). [Audition par la Commission Bronner](https://youtu.be/lK5Hl0V-4KQ). :tv:

---
# :two: L'EdTech : de l'aide à la surveillance ?

---
# 2. Le glissement de fonction (*function creep*) 
<!-- _class: t-80 -->

- Des outils, conçus  pour un but donné (e.g., aider) peuvent être progressivement utilisés pour un autre (e.g., surveiller, contrôler) {Koops, 2021 $14413} 
- L'évolution des moteurs de recherche {Zimmer, 2008 $14415}
  - 1. Donner des informations à des requêtes 
  - 2. Faire un moteur de recherche “parfait” (“qui comprend exactement ce que la requête signifie et retourne exactement ce que tu veux”), s'intéresse au contexte de chacun
  - 3. Récupérer des données personnelles *via* les requêtes 
  - 4. En récupérer des multiples autres outils (docs collaboratifs, courriels, photos, etc.)
  - 5. Faire une “base de données d'intentions”…
  - 6. … potentiellement récupérable pour des fins plus ou moins pertinentes (lutte contre la criminalité, dictature, etc.)

---
# 2. Une surveillance dès la naissance ?

- Surveillance des élèves : vidéo-“protection”, absences *via* biométrie (données personnelles sensibles), etc.
- Surveillance (possible) des enseignants *via* les plate-formes institutionnelles (ENT, Magistère)
- :warning: surveillance sociale plus globale des enfants, *dès le début* (Marx & Steeves 2010) : sites de rencontre, compatibilité génétique, tests prénataux, interphones pour bébés, vêtements RFID, téléphones GPS, logiciels de contrôle de l'activité internet, boîte noire dans voitures, tests d'usage de la drogue ou de rapports sexuels…

---
# 2. Le recours au numérique pendant la pandémie ([Human Rights Watch](https://www.hrw.org/sites/default/files/media_2022/07/French_EdTech%20Report_Sum%26Recs.pdf), 2022)

> *Sur les 163 produits EdTech examinés, 145 (89 %) semblent s’être livrés à des pratiques de données qui mettent en danger les droits des enfants, ont contribué à les saper ou ont activement enfreint ces droits. Ces produits surveillaient, ou avaient la capacité de surveiller, les enfants, dans la plupart des cas secrètement et sans le consentement des enfants ou de leurs parents, récoltant des données sur qui ils sont, où ils se trouvent, ce qu'ils font en classe, qui sont leur famille et leurs amis, et quel type d'appareil leur famille peut se permettre de leur procurer.*

---
# 2. Protéger ses données personnelles
- Les sites utilisés en éducation sont des **aspirateurs à données personnelles** (Lai et al. 2023)

![w:600](images/img-zar/cookies-sites.png)

  
---
# 2. Discussion

:interrobang: Comment se préparer à  contrer des technologies de surveillance qui n'existent pas encore ? Risque d'escalade des fonctions (une fonction banale va pouvoir servir à d'autres fins plus problématiques)
:interrobang: Dans votre situation et/ou dans votre établissement/école, y a-t-il des éléments concourant à une surveillance des élèves ?

:scroll:
- Dessus P. (2013). [Informatique et école : vers une éducation citoyenne ?](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/droitinfosoc.html)
- {Selwyn, 2019 $701} 


---
# :three: Faire apprendre, ou apprendre de nous ?

---
# 3. Gouverner l'éducation par les données

<!-- _class: t-80 -->

- Les données sur l'éducation existent depuis l'essor des statistiques {Hacking, 2002 $2271} ; les larges enquêtes évaluatives deviennent un but plutôt qu'un moyen d'évaluer les élèves {Borer, 2013 $2272}. 
- Les données recueillies peuvent permettre :
  - de réaliser des profils d'élèves, et de leur procurer des exercices en fonction de ces profils
  - de prédire
  - de surveiller et contrôler l'activité des élèves, grâce à des données plus fines à propos de leur activité

---
# 3. De nombreux risques…

- *d'usage intensif de données d'apprentissage* pour permettre aux enseignants et parents de monitorer les progrès des élèves (*cf.* écoles chinoises "*high tech*")
- *de vente de l'apprentissage personnalisé*, qui a de multiples sens : aller à son rythme, avoir un avis sur ce qu'on veut faire ensuite, se voir proposer des contenus en lien avec ses compétences/connaissances, avoir un plan d'apprentissage {Garrick, 2017 $2475}
- *de standardiser l'enseignement*, d'enfermer l'élève dans des “styles d'apprentissage”, un profil. Être dépendant de listes de lecture pour apprendre…
- *de déléguer le calcul de notes à des algorithmes*, par délégation (*cf.* le calcul automatique d'une note pour pallier l'absence de notes due à la pandémie, dans le secondaire en Grande-Bretagne) {Mead, 2022 $108}

---
# 3. Un exemple : Solutionnisme EdTech en Grande-Bretagne

- Un plan de financement de 4 M£ annoncé en G.-B. fin août 2024 pour créer un outil d'IA générative “aidant” les enseignant.es pour créer des plans de cours, corriger automatiquement le travail des élèves, réaliser des tâches admin…
- …dans le but d'alléger la charge des enseignants (que, par ailleurs, les politiques chargent de plus en plus)
- Solutionnisme technologique, ou des micro-solutions pour des macro-problèmes : il faut trouver des applications à une technologie, et ces applications vont résoudre sans peine les problèmes sociaux les plus difficiles
- Il va sans dire que cet argent va bénéficier aux entreprises de la EdTech, pas aux enseignants

:books: {Morozov, 2014 $6398} [(Williamson, 2024)](https://codeactsineducation.wordpress.com/2024/08/29/automated-austerity-schooling/)

---
# 3. Discussion

<!-- _class: t-80 -->

:interrobang: L'apprentissage personnalisé peut-il parfois nuire à l'apprentissage personnel (apprendre par *playlists* ? Décider de ce qu'on doit apprendre ?) Qu'y perdrait-on par rapport à l'apprentissage en classe “standard” où l'on peut se confronter à l'avis de tous ? Risque d'insertion d'informations non voulues ?
:interrobang: Dans votre situation, pourriez-vous utiliser des outils d'évaluation ou d'exercices automatiques ? Avec quelles précautions ?

:scroll:
- Chatellier, R. (2017). [Learning analytics: quelles sont les données du problème ?](https://linc.cnil.fr/fr/learning-analytics-quelles-sont-les-donnees-du-probleme)
- Loiseau, M. (2014). [Notions d'algorithmique pour comprendre les médias sociaux : exemples d'enjeux de l'“ouverture”](https://youtu.be/_dNHQ-ucLtM) :tv:
- Watters, A. (2014). [The problem with “personalization”](http://hackeducation.com/2014/09/11/personalization)
- Watters, A. (2017). [The histories of personalized learning](http://hackeducation.com/2017/06/09/personalization)
  

---
# :four: Inclusion et exclusion

---
# 4. Inclusion & exclusion

- Il y a des inégalités dans la manière dont les élèves (les gens) utilisent les technologies. Ceux les mieux dotés (culturellement, socialement) en bénéficient le plus
- Importance de les enseigner à l'école, mais l'accès ne suffit pas : il y a aussi des inégalités de genre ou liées aux situations de handicap

---
# 4. L'exclusion du numérique :arrow_right: exclusion des connaissances

- Les communautés virtuelles sont des réseaux importants :arrow_right: Exclusion sociale
- Internet comme média de participation :arrow_right: Exclusion politique
- Compétences informatiques pour accès à un travail :arrow_right: Exclusion économique

:books: {van Winden, 2010 $2277}

---
# 4. Aides informatisées aux élèves en situation de handicap : une revue mondiale au niveau primaire

- *Déficience auditive* : bénéfice d'utiliser l'informatique pour communiquer pour apprendre la langue des signes
- *Déficience visuelle* : pas de bénéfices particuliers de l'informatique comparée aux terminaux braille
- *Dyslexie* : pas de bénéfices avérés de l'informatique
- *Spectre de l'autisme* : Peu d'études à ce jour, souvent avec des outils très sophistiqués (réalité virtuelle)

:books: {Lynch, 2024 $14402}

---
# 4. Inégalités de genre dans l'usage de l'EdTech

<!-- _class: t-80 -->

- Les garçons se jugent plus efficaces que les filles en informatique
- Le stéréotype de genre oriente les filles vers les tâches administratives et les garçons vers les tâches productives ou créatives
- Biais de l'IA envers le feminin,  vision binaire du genre {Katyal, 2021 $14420}
- Les outils de surveillance affectent plus particulièrement les élèves LGBTQ, en les identifiant et amenant une “discipline” ou des punitions
- Sur le genre et les réseaux sociaux, voir les travaux de {Déage, 2023 $13705}

:books: {Campbell, 2014 $14419}

---
# 4. Discussion

:interrobang: Que faire pour que l'usage de l'EdTech n'augmente pas les différences interindividuelles en défaveur des minorités, n'augmente pas les stéréotypes ?

:scroll:

- [Violences scolaires : e-reputation et mauvais genre](https://youtu.be/NX6_txr1qaE?si=W6jaUjJzKg9_QXH7). Entretien avec I. Clair & M. Déage :tv:

---
# :five: Travail dirigé


---
# 5. Un cas d'école, l'entreprise [Compilatio](http://compilatio.net)

1. Entreprise qui, depuis 20 ans, a développé un logiciel de détection de similitudes…
2. Pouvant être utilisé pour un but éducatif : lutter contre les mauvaises conduites académiques (plagiat)
3. Recueil massif de données (et souvent sans accord explicite des propriétaires), servant à entraîner des systèmes. Situation de monopole en France
4. Lancement récent d'un outil de “détection d'usage de l'IA générative”, [Magister+](https://www.compilatio.net/magister-plus)
5. Lancement imminent de [Gingo](https://www.gingo.ai), un outil de correction de copies… avec accès gratuit la 1re année, le temps d'entraîner le système

---
# 5. Tâche : Concevons notre EdTech !

- par groupe de 3-4 étudiant.es, 
- définir un but éducatif, pour un public et une matière déterminés (peut aussi impliquer les enseignant.es)
- trouver une approche EdTech créative et innovante (peut ne pas exister encore) pour remplir ce but éducatif 
- pour cela, passer par les 5 couches de la diapositive suivante en cherchant rapidement, le cas échéant, des informations sur internet à leur propos
- raffiner la description de votre approche en ajoutant des fonctionnalités proposées par les couches. Les recherches peuvent être distribuées dans le groupe
- dépôt de votre EdTech dans ce pad : https://annuel2.framapad.org/p/ed-tech-meef-uga-aa2v et exposé rapide des différentes EdTech conçues

---
# 5.  Les 5 couches de l'EdTech

1. **Exploitation** : L'EdTech fait usage d'une grande quantité d'énergie, de matériaux et (notamment dans l'IA) de micro-travail humain
2. **Monopoles** : L'EdTech recourt à une infrastructure monopolistique pour le stockage “dans les nuages” des données et leur traitement des couches suivantes 
3. **Hyperconnexion** : L'EdTech peut utiliser l'“internet des objets” (*Internet of Things*) pour récupérer des données au plus près du contexte
4. **Biométrie** : L'EdTech récupère et stocke des données personnelles et biométriques pour identifier les personnes de manière non ambiguë 
5. **Traitement multimodal** : L'EdTech traite les données multimodales (images, sons, textes, GPS, etc.) récupérées dans les couches 3-4 pour remplir des buts éducatifs : de prédiction, de classification, etc.

:books: {Swist, 2024 $14411}


---
# 5. Conception d'EdTech : un patron

<!-- _class: t-90 -->

1. **Exploitation** : Bien qu'une exploitation conséquente, à la fois en termes énergétiques et en travail humain [précisions] soit faite dans l'EdTech…
2. **Monopoles** : … et que la récolte et l'analyse massives de données personnelles [précisions], stockées dans des serveurs, asseoit un peu plus leur monopole…
3. **Hyperconnexions** : … pour remplir le but éducatif *B* [précisions], l'EdTech va utiliser le ou les outils connectés [précisions]…
4. **Biométrie** : … et recueillir certaines données personnelles biométriques [précisions], identifiant de manière non ambiguë les élèves et enseignant.es…
5. **Multimodalité** : … les données multimodales des couches précédentes sont traitées pour diverses opérations d'analyse, de profilage, de prédiction [précisions] en lien avec le but éducatif *B* [précisions] 


---
# 5. Questions subsidiaires

- Qui récupère du pouvoir dans l'usage de votre EdTech
- Qui en perd ?
- Quels sont les risques de “glissements de fonction” ?


---
# Références

