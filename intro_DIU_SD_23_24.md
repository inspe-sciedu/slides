---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# 
# Enseigner avec le numérique - DIU SD
## Objectifs, organisation, attendus
 
<!-- page_number: true -->
<!-- footer: Pôle enseignement numérique INSPE - UGA - 2023 - 2024 - CC:BY-NC-SA -->

---
# Objectifs DIU
Ce module a pour objectif de vous accompagner dans l'acquisition des compétences nécessaires pour utiliser le numérique en classe en prenant en compte les dimensions, efficacité de l’enseignement, respect des normes juridiques et éthiques et développement de la culture numérique des élèves. 

Cette formation articule apports théoriques et activités plus pratiques aboutissant à la conception d’une situation d’apprentissage intégrant le numérique.

---
# Objectifs pour l'après INSPE (on espère que ce sera après...)
Préparer aux exigences de la certification PIX+EDU.

La certification PIX+EDU comporte deux volets :
* un volet automatisé
* un volet pratiques professionnelles : **conception**, mise en oeuvre (avec preuves), bilan réflexif.

---
# Organisation semestre 1
Module 1 – 8h :
-       Apports théoriques et conception d’une situation d’apprentissages intégrant le numérique
-       Entrée dans le dispositif PIX+EDU (volet automatisé)

## Évaluation semestre 1 : aucune
:warning: Vous aurez différents intervenants 

---
# Organisation semestre 2
 Module 2 – 6h :
-       Fin de la conception, avec si possible mise en œuvre en classe et analyse réflexive de cette situation d'apprentissage.

## Évaluation au semestre 2 :
-       Rédaction d'un document destiné à répondre (partiellement ou totalement) aux attentes PIX+EDU (volet pratiques professionnelles)



---
# :sos: Ressources
#

#### Cours sur la plateforme eformation :arrow_right:  https://eformation.univ-grenoble-alpes.fr






