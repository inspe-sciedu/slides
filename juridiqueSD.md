---
marp: true
paginate: true
---

![10%](images/logo_UGA_couleur_cmjn.jpg)

# Thématique « Numérique et juridique »

Master et DIU MEEF 2nd degré 

<!-- footer: Laurence Osete - INSPE-UGA - 2022 - CC:BY-NC-SA -->

<!-- page_number: true -->

---

# Rôles de l'enseignant

![](images/img-juridique/roles.png)

---

# Compétences visées

(source : référentiel de compétences des enseignants, voir aussi le [CRCN](<http://espe-rtd-reflexpro.u-ga.fr/docs/scied-cours-num/fr/latest/crcn.html>))

* CC2 : Connaître les grands principes législatifs qui régissent le système éducatif, le cadre réglementaire de l'école et de l'établissement scolaire, les droits et obligations des fonctionnaires ainsi que les statuts des professeurs et des personnels d'éducation.
* CC6 :
  * Respecter et faire respecter le règlement intérieur et les chartes d'usage.
  * Respecter la confidentialité des informations individuelles concernant les élèves et leurs familles
* CC9 : Participer à l'éducation des élèves à un usage responsable d'internet.

---

# Compétences travaillées

* Capacité à **comprendre le contexte** dans lequel il évolue et à utiliser ses connaissances, recherches, réflexions à propos de la législation et des règlementations institutionnelles liés aux TICE **pour une mise en œuvre raisonnée et responsable** dans le cadre de son activité professionnelle.

---

# Les responsabilités de l'enseignant (rappel)

* Responsabilité civile
* Responsabilité pénale
* Responsabilité administrative

---

# Quelques obligations de l'enseignant (rappel)


* D'obéissance hiérarchique
* De discrétion professionnelle
* De signalement
* De neutralité

---

# Les principaux sujets abordés

1. Le droit à la vie privée
2. Les droits des auteurs
3. La sécurité des élèves
4. Le principe de neutralité

---

# Etude des cas

## Par groupe de 3 à 6 personnes (30 mn)

* Consulter le cas et les ressources documentaires : https://lstu.fr/casjuridiquessd-4
* Répondre aux questions (uniquement partie "Questionnement")
* Préparer une rapide présentation orale de 3 min maximum

---

# A retenir

---

# Droit à la vie privée

---

# Collecte et traitement de données à caractère personnel (DCP)

DCP : "toute information se rapportant à une personne physique identifiée ou identifiable [ ] ; est réputée être une «personne physique identifiable» une personne physique qui peut être identifiée, directement ou indirectement, notamment par référence à un identifiant, tel qu'un nom, un numéro d'identification, des données de localisation, un identifiant en ligne, ou à un ou plusieurs éléments spécifiques propres à son identité physique, physiologique, génétique, psychique, économique, culturelle ou sociale;" (RGPD)

---
# Licéité du traitement

- la personne concernée a **consenti** au traitement
- nécessaire à l'exécution d'un contrat
- nécessaire au respect d'une obligation
- nécessaire à la sauvegarde des intérêts vitaux de la personne
- nécessaire à l'exécution d'une **mission d'intérêt public** ou relevant de l'exercice de l'autorité publique 
- nécessaire aux fins des **intérêts légitimes** poursuivis par le responsable du traitement ou par un tiers.

---
# Consentement des enfants en ce qui concerne les services de la société de l'information

- est licite à partir de  16 ans. En dessous le consentement est donné ou autorisé par le titulaire de l'autorité parentale.
- Les États membres peuvent prévoir par la loi un âge inférieur entre 13 et 16 ans. (**15 ans en France**)
- Le responsable du traitement s'efforce raisonnablement de vérifier l'obtention du consentement.

---
# Responsabilités

- Le responsable du traitement est responsable du respect du RGPD et doit être en mesure de le démontrer (responsabilité)
- Pour cela, il doit :
	- **Tenir un registre** des traitements et des violations.
	- S’assurer du consentement des personnes concernées quand nécessaire.
	- **Informer** les personnes concernées (élèves, parents, personnels, intervenants extérieurs, …)
	- S’assurer de la **sécurité des données**.
- Pour les établissements du secondaire, le responsable est le **Chef d'établissement**


---
# Précautions à prendre

#### Obligations RGPD :

- Signaler toute collecte au chef d'établissement
- Minimiser les données collectées
- Limiter la conservation dans le temps
- Informer les personnes, et dans certains cas, demander le consentement aux responsables légaux.
- Sécuriser les données
- Signaler au chef d'établissement la violation des données.

#### Précautions :
- Obtenir le consentement des deux parents en cas de personnes séparées.

---
# Le droit d'auteur

---
# Loi en vigueur

Le code de la propriété intellectuelle, composé de :
- La propriété littéraire et artistique
- La propriété industrielle : brevets, dessins et modèles, marques.

![](images/img-juridique/DefDroitDauteur.png)

---
# Les grands principes

- S'applique à toute création originale.
![](images/img-juridique/DroitDauteur.png)


---
# Titulaires du droit d'auteur (Articles L113-1 à L113-10)

- Personne qui créé l'œuvre et la divulgue
- Cas des œuvres créées à plusieurs :
	- tous les co-auteurs pour une œuvre collaborative 
	- la personne qui dirige la création et qui la divulgue pour une œuvre collective
- Cas particulier d'une œuvre intégrant une autre œuvre. Elle est dite composite. Elle est la propriété de l'auteur qui l'a réalisée, sous réserve des droits de l'auteur de l'œuvre préexistante 

---
# Droit d'auteur : Exceptions (Article L122-5)

Quelques exceptions pour un usage collectif,

3° sous réserve que soient indiqués clairement le nom de l'auteur et la source 
a)  Les analyses et courtes citations justifiées par le caractère critique, polémique, pédagogique, scientifique ou d'information de l’œuvre à laquelle elles sont incorporées ;

4° La parodie, le pastiche et la caricature, compte tenu des lois du genre;

11° Les reproductions et représentations d'œuvres architecturales et de sculptures, placées en permanence sur la voie publique, réalisées par des personnes physiques, à l'exclusion de tout usage à caractère commercial.

**12° La représentation ou la reproduction d'extraits d'œuvres à des fins exclusives d'illustration dans le cadre de l'enseignement et de la formation professionnelle, dans les conditions prévues à l'article L. 122-5-4 ;**

---
# Droit d'auteur : Exception pédagogique (article L. 122-5-4)

* La représentation ou la reproduction d'**extraits d’œuvres**, […]
* à l'exclusion de toute activité ludique ou récréative,
* à un public composé majoritairement d'élèves,
* compensée par une rémunération négociée.

---

# Conditions particulières (Accords)

* n'utiliser que des **extraits d'œuvres**\*, sauf pour
  * œuvre courte (poème);
  * diffusion intégrale ou interprétation en classe d'une musique;
  * diffusion intégrale en classe d'une vidéo provenant d'un service non payant;
  * œuvres des arts visuels (limité à 20 œuvres par travail, définition max 800x800 px en 72 DPI)

\* Les œuvres doivent avoir été acquises légalement

Tableau de synthèse : https://classetice.fr/v2/wp-content/uploads/2022/07/tableau_exception_peda_2016.pdf

---

# Précautions à prendre

#### Obligations :

* Citer l'auteur, l'éditeur (obligatoire mais pas toujours suffisant).
* Ne pas diffuser les œuvres des élèves sans autorisation.
* Consulter les conditions d'utilisation (licences)
* Respecter les CGU des logiciels et services 

#### Précautions :
* Utiliser si possible des ressources sous licence "libre" et gratuite (Creative Commons ou GNU GPL).

---
# Neutralité commerciale


Document de référence : [Code de bonne conduite des interventions des entreprises en milieu scolaire](https://www.education.gouv.fr/botexte/bo010405/MENG0100585C.htm)

#### Article III.5 Le partenariat pour l'usage de produits multimédias
*"L'utilisation de produits multimédias par les établissements scolaires, à des fins d'enseignement, est libre. La consultation de sites Internet privés ou l'utilisation de cédéroms qui comportent des messages publicitaires ne sauraient être regardée comme une atteinte au principe de neutralité (9)."*

*"En revanche, la réalisation de sites Internet par les services de l'éducation nationale et les établissement scolaires est tenue au respect du principe de la neutralité commerciale."*

---
# Précautions à prendre

* ne pas publier de contenus sur des services diffusant de la publicité.
* ne pas imposer de materiels, logiciels ou services.

Plus d'infos à [Informatique et école : vers une éducation citoyenne ?](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/droitinfosoc.html)

---
# Sécurité des élèves

* Protéger
* Secourir
* Alerter - signaler
* Éduquer

---

# Sécurité des élèves

#### Obligations :

* Système de filtrage des sites inappropriés (chef d'établissement)
* Mettre en place des règles --> Charte
* Surveiller les élèves
* Signaler les contenus inappropriés
  * https://www.internet-signalement.gouv.fr/
  * http://www.pointdecontact.net/
* Signaler les incidents aux parents et à l'administration

#### Précautions :

* Vérifier le fonctionnement des systèmes de filtrage.
* Utiliser un moteur de recherche adapté à l’âge des élèves ou des listes de sites.
* Éduquer aux dangers d'internet.

