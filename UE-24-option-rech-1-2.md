---
marp: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# UE 24 Option de recherche “Interactions enseignant-élèves“
## Philippe Dessus, Inspé, Univ. Grenoble Alpes

## Séances 1 & 2 - Les IEE
##### Année universitaire 2024-25

![w:350](images/logo-espe-uga.png)

---
<!-- footer: Ph. Dessus • M1 PE • TD Option de recherche “Interactions enseignant-élèves" • Inspé-UGA-USMB 2024-25 • CC:BY-NC-SA-->
<!-- paginate: true -->


<!-- ajouter : 
- {Goudeau, 2024 $14559}
https://x.com/adfillon/status/1777598235617415192
 -->

:one: 0. Vue générale des 10 séances de TD

1. Les interactions enseignant-élèves (IEE)
2. Le *Classroom Assessment Scoring System*
3. Les études à cas unique
4. Trouver et comprendre des articles sur les IEE
5. Les méta-analyses et revues systématiques
6. Recueillir des données sur les IEE
7. Aspects légaux et éthiques
8. Analyser et représenter des données sur les IEE
8. Stratégies de rédaction du mémoire
10. Communiquer des travaux de recherche

---
# :one: 0. Introduction 
- Le syllabus du TD est [ici](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/syl-options-rech.html), raccourci : https://link.infini.fr/td-opt-rech-uga, 
- C'est la porte d'entrée à tout le matériel utilisé (présentations, documents)
![w:300](images/link-opt-rech-m1-pe.jpg)

---
# :one: 0. Conseils

- Amener un ordinateur ou une tablette à chaque TD
- Ne pas hésiter à faire des liens avec votre problématique et à poser des questions pour faire ces liens

---
# :one: 0. Évaluation de l'UE

- Chaque étudiant·e aura à produire un document d'environ 6 pages qui reprendra entre 4 et 6 *copies* de passages du mémoire en cours d'écriture qui auront bénéficié (ou qui auraient pu bénéficier) des apports des TD “outils de recherche”
- Après chaque copie de passage, un paragraphe d'une dizaine de lignes indiquera le thème du TD concerné, et justifiera l'intérêt d'utiliser son apport dans le mémoire
- Les étudiant·es par binôme pourront rendre un document pour deux

---
# :one: 0. Préambule (1/2) : Règles de fonctionnement

- Une partie du travail est fondée sur la confrontation entre participants de leurs pratiques de classe, leur compréhension et leur amélioration. Il s’agit de les rapporter le plus précisément possible et, en contre-partie, d’éviter de porter des jugements sur la personne qui a rapporté les pratiques, mais d’évaluer le plus objectivement possible ce qui s’est passé (et non la personne qui est à l’origine possible de ces pratiques)
- Ce travail d’analyse des pratiques peut être outillé par le CLASS (un outil d’observation de la qualité des interactions enseignant-élèves). Le formateur et les participants mettront tout en œuvre pour permettre une parole libre, positive et constructive à propos des pratiques de chacun

---
# :one: 0. - Préambule (2/2)

- Le TD fera l’objet d’une évaluation, et non les pratiques relatées au cours de l’atelier
- Les propos tenus au sein du groupe reposent sur la **confidentialité** (c.-à-d. que les participants ne peuvent rapporter ce qui s’est dit dans le cours à des personnes n’en faisant pas partie) et le respect de la parole de chacun des membres
- Le CLASS n'est pas un outil de l'évaluation de l'enseignant·e, mais bien de ce qui se joue entre l'enseignant et ses élèves

---
# :one: 0.- Organisation sommaire des TD

- Une partie théorique
- Une partie de travail sur le mémoire, amenée par quelques questions et guidée par le formateur
- :warning: Le guidage du formateur **ne peut se substituer** à celui de la directeur·e de mémoire


---
:one: 1. Plan de la séance 1

1. Définition des IEE
2. Liens avec la réussite des élèves
3. Présentation du CLASS (cont. séance 2)

---
# :one: 1. Définitions - Les dimensions de la qualité éducative

![w:1000](images/qualite-educ.png)

:books: Duval *et al*. 2021 p. 225

---
# :one: 1 Attention !  (1/2)

- Nous savons que nous allons pouvoir capturer **qu'une partie** de la qualité des IEE
- Personne, avec aucun outil, ne peut envisager capturer **l'ensemble** des variables en jeu dans les IEE
- Et personne ne peut trier avec certitude les variables “efficaces” de celles qui ne le seraient pas : cela dépend du contexte, des valeurs...
- Même si certains résultats commencent à montrer des pistes

:books: Cook *et al*. 2012

---
## 1.7 Prenons garde ! disent d'autres (2/2)

- La poursuite non réfléchie de la qualité pourrait être un moyen de mieux contrôler les individus (**technoscience**)
- Les situations d'enseignement-apprentissage  ne peuvent être réduites à des quantifications
- Évoquer la qualité de quelque chose, c'est pouvoir positionner ce qui est “mieux” au-dessus de ce qui est “moins bien”, et la recherche ne peut être prescriptive


:books: Dahlberg et al. 2007 ; Marcel 2017 ; Pereira 2018

---
# :one: 1 Alors, dé-chiffrer ou chiffrer pour mieux déchiffrer ?

- **D'un côté** : quantification de la qualité à outrance : :smile: :smirk: :disappointed: 
- **De l'autre** : pas de quantification de la qualité : tout se vaut
- **Voie intermédiaire** : tirer parti des travaux de recherche pour orienter son enseignement de manière réfléchie, 
	- 	sans tomber dans la quantification à tout prix, 
	- 	ni l'aveuglement à la tradition
	- 	ni surcharger les enseignants

:books: Crahay 1986 ; Ogien 2021

---
# :one: 1. La qualité des IEE peut avoir un effet sur la réussite des élèves

1. Étude randomisée sur les effets de *My Teaching Partner* sur 2 ans, fondé sur CLASS (*N* = 173) en **maternelle**. Effet sign. : développement socio-émo :arrow_right: engagement des élèves :arrow_right: perf. en littéracie
2. Étude randomisée sur les effets d'un atelier de DP (*N* = 54) en **maternelle** (programme proche du CLASS). Le socio-émo. a un effet sign. sur l'intérêt des élèves ; la gestion de classe et le soutien à l'appr. sur l'engagement
3. Étude randomisée sur les effets de MTP sur 1 an fondé sur CLASS (*N* = 78), **2d degré**. Effet faible mais significatif de l'intervention sur les résultats des élèves


:books: 1. Pianta *et al*. 2022 ; 2. Fauth *et al.* 2019 ; 3.  Allen *et al*. 2011

---
# :one: 1. Plus d'informations

- https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/class/technique-CLASS.html

---
# :one: 2. Le rôle de CLASS

- CLASS est un outil d'observation de la qualité des interactions enseignant·e/élèves
- Nous allons l'utiliser plutôt, dans ces séances de TD, comme un outil
	- permettant d'avoir un vocabulaire commun
	- permettant de trouver ou raffiner des pistes d'analyse utiles pour votre mémoire

---
# :one: 2. :warning: Ce que ce TD n'est pas :warning:

- Une session de certification au CLASS
- Un moyen de tout connaître sur le CLASS
- Le TD s'intéresse au CLASS en tant qu’outil de réflexion et de compréhension (des sortes de lunettes métaphoriques) de ce qui se passe dans votre classe 
- Il est un cadre, pas le sujet de recherche lui-même : **ce n’est pas obligatoire de référer au CLASS dans son mémoire**

---
# :one: 2. Le CLASS

- *Classroom Assessment Scoring System*: Système d’observation et d’évaluation des interactions enseignant-élèves
- Conçu par Robert Pianta, Karen La Paro & Bridget Hamre, univ. Virginie (USA)
- Éditeur : Teachstone (https://teachstone.com)
Dérivé sur tous les niveaux scolaires :  primaire-secondaire. 
- Une version en français pour les maternelles 3-5 ans existe
- La version 2, toujours pour ce niveau, est sortie en 2023

---
# :one: 2.  Les fondations théoriques du CLASS

- S’intéresser aux processus de classe proximaux vs (centrés expérience de l’élève) plutôt que distaux (centrés expérience de l’enseignant)
- Arrière-plan **Soutien émotionnel**
	- Théorie de l’attachement (Ainsworth et al. 1978; Bowlby, 1969) :arrow_right: sécurité affective
	- Théorie de l’autodétermination (Ryan & Deci, 2017)
- Arrière-plan **Organisation de la classe**
	- Habiletés d’autorégulation (Blair, 2002; Raver, 2004)
- Arrière-plan **Soutien à l’apprentissage**
	- Développement cognitif et langagier de  l’enfant (Piaget, etc.)

---
# 1.1. M1 - Les recherches sur le CLASS

- Testé sur plus de 4 000 classes, l'un des systèmes d'observation les plus étudiés
- Structure des dimensions du CLASS
- Fidélité inter-observateurs 
- Stabilité des mesures dans le temps (cycles, jours,  année)
- Validité de construit : corrélations entre CLASS et d’autres tests de but similaire
- Validité prédictive : corrélation entre les scores CLASS et les performances des élèves

---
# :one: 2. Les domaines et dimensions du CLASS

                                                      ┌────────────────────────────────┐                                  
                                                      │     Interactions en classe     │                                  
                                                      └────────────────────────────────┘                                  
                                                                       │                                                  
                                                                       │                                                  
                                                                       │                                                  
                                       ┌───────────────────────────────┼───────────────────────────────────┐              
                                       │                               │                                   │              
                                       │                               │                                   │              
                                       ▼                               ▼                                   ▼              
                            ┌─────────────────────┐     ┌────────────────────────────┐      ┌────────────────────────────┐
    Domaines                │ Soutien émotionnel  │     │ Organisation de la classe  │      │ Soutien à l'apprentissage  │
                            └─────────────────────┘     └────────────────────────────┘      └────────────────────────────┘

---
# :one: 2. - Pour résumer !

Un environnement propice à l’apprentissage et engageant les élèves :
- Est émotionnellement sûr et aidant
- Offre des expériences bien structurées et prédictibles
- Est riche et structuré du point de vue conceptuel et langagier

---
# :one: 2. - Domaine 1 – Soutien émotionnel

- Le fonctionnement émotionnel et social de l’élève au sein de sa classe est reconnu comme un indicateur important de réussite scolaire
- Un élève motivé et en relation avec ses pairs dès les plus petites classes a plus de chances que d’autres de réussir ultérieurement, que ce soit d’un point de vue social ou même académique

---
# :one: 2. - Domaine 1 – Soutien émotionnel
- Des relations chaleureuses, soutenantes avec les adultes et les autres enfants
- La joie et le désir d'apprendre
- Une motivation à s'engager dans les activités d'apprentissage
- Un sentiment de confort (aisance) dans le milieu éducatif
- La volonté de relever des défis sur le plan social et académique
- Un niveau approprié d'autonomie

---
# :one: 2.- Soutien émotionnel - 1 Climat positif (C+)
Reflète le lien émotionnel entre l'enseignant et l'enfant, entre les enfants eux-mêmes ainsi que la chaleur, le respect et le plaisir communiqué dans les interactions verbales et non-verbales

**Indicateurs**
- Relations
- Affect positif
- Communication positive 
- Respect

---
# :one: 2.- Soutien émotionnel - 2 Climat négatif (C–)
N'est pas simplement l'absence de climat positif mais bien la présence de comportements négatifs. Reflète l'ensemble de la négativité exprimée dans la classe. La fréquence, l'intensité et la nature de la négativité de l'enseignant et des enfants sont des éléments-clés de cette dimension

**Indicateurs**
- Affect négatif
- Contrôle punitif
- Sarcasme/Irrespect
- Sévère négativité

---
# :one: 2. - Soutien émotionnel - 3 Sensibilité de l'enseignant (SE)
Englobe la capacité de l'enseignant à être attentif·ve et à répondre aux besoins émotionnels et académiques des enfants. Un haut niveau de sensibilité offre un soutien aux enfants dans l'exploration et l'apprentissage puisque l'enseignant leur offre de manière constante du réconfort et de l'encouragement

**Indicateurs**
- Conscience/vigilance
- Réceptivité
- Réponse aux problèmes
- Confort des élèves

---
#  :one: 2. - Soutien émotionnel - 4 Prise en considération du point de vue de l'enfant (PVE)
Permet de rendre compte dans quelle mesure les interactions de l'enseignant avec les enfants et les activités offertes mettent l'accent sur les intérêts des enfants, leur motivation et leur point de vue tout en encourageant la responsabilité de l'enfant et son autonomie

**Indicateurs**
- Souplesse et attention centrée sur l'élève
- Soutien à l'autonomie et au leadership
- Expression des élèves
- Restriction de mouvement

---
# :one: 2. - Domaine 2 – Organisation de la classe

Comment l'enseignant organise et gère différents aspects de la classe (comportement des élèves, temps, attention). Il y a une meilleure opportunité d'apprentissage et la classe fonctionne mieux si les élèves ont un comportement adéquat, cohérent avec les tâches qui leur sont allouées, et sont intéressés et engagés dans ces dernières.

---
# :one: 2. - Organisation classe - 5 Gestion des comportements (GC)
Englobe l'habileté de l'enseignant à établir des attentes comportementales claires et à utiliser des méthodes efficaces pour prévenir et rediriger les comportements inappropriés

**Indicateurs**
- Attentes comportementales claires
- Proactivité
- Redirection des comportements
- Comportements des enfants

---
# :one: 2. - Organisation classe  – 6 Productivité (P)

Dans quelle mesure l'enseignant gère le déroulement des activités et des routines de manière à optimiser le temps disponible à l'apprentissage ?

**Indicateurs**
- Maximisation du temps d'apprentissage
- Routines
- Transitions
- Préparation du matériel nécessaire à la séance

---
# :one: 2. - Organisation classe - 7 Modalités d'apprentissage (MA)

Reflète la manière dont l'enseignant maximise l'intérêt des enfants, leur engagement et leur capacité à apprendre au cours des leçons et activités offertes

**Indicateurs**
- Accompagnement efficace
- Diversité des modalités et des matériels
- Intérêt de l'enfant
- Clarté des objets d'apprentissage

---
# :one: 2. - Domaine 3 – Soutien à l'apprentissage

S'intéresse à décrire comment les enseignants aident les enfants à 
- apprendre à résoudre des problèmes, à raisonner et à penser
- utiliser les rétroactions pour approfondir des habiletés et des connaissances
- développer des habiletés langagières plus élaborées

---
# :one: 2. - Soutien appr — 8. Développement de concepts (DC)

Utilisation par l'enseignant de stratégies diverses (discussions, activités) afin d'amener l'enfant à développer une pensée plus élaborée, une plus grande compréhension des phénomènes de son environnement plutôt que de favoriser un apprentissage par cœur

**Indicateurs**

- Analyse et raisonnement
- Créativité
- Intégration d'éléments déjà rencontrés dans les cours précédents
- Lien avec la vie réelle

---
# :one: 2. - Soutien appr — 9. Qualité de la rétroaction (QR)

Le niveau selon lequel l'enseignant offre des rétroactions aux enfants qui optimisent l'apprentissage et qui favorisent une participation maintenue

**Indicateurs**
- Étayage
- Rétroactions en boucle
- Stimuler les processus de la pensée
- Fournir de l'information
- Encouragement et affirmation
  
---
# :one: 2. - Soutien appr — 10. Modelage langagier (ML)

Rend compte de l'efficacité et de la quantité des techniques de stimulation du langage utilisées par l'enseignant.

**Indicateurs**
- Conversations fréquentes
- Questions ouvertes
- Répétition et extension
- *Self-talk* et *parallel-talk*
- Niveau de langage élaboré


---
# :one: 2.  Tour de table (1/2)
- Quel est le domaine (ou des dimensions dans un domaine) pour lequel vous vous sentez le/la plus à l'aise ?
- Quel est le domaine (ou des dimensions dans un domaine) pour lequel vous pensez avoir des difficultés ?
- :warning: Ces 2 impressions sont causées majoritairement par vous, mais vos élèves y ont une part !


---
# :one: 2.  Liste des dimensions
1. Climat positif (C+)
2. Climat négatif (C–)
3. Sensibilité de l'enseignant (SE)
4. Prise en considération du point de vue de l'enfant (PVE)
5. Gestion des comportements (GC)
6. Productivité (P)
7. Modalités d'apprentissage (MA)
8. Développement de concepts (DC)
9. Qualité de la rétroaction (QR)
10. Modelage langagier (ML)

---
# :two: Séance 2 - Plan

1. Fin du détail des autres dimensions du CLASS
2. Visionnement vidéo 
3. Réflexion sur les IEE dans le stage

---
# :two: CP-CE1 Phonèmes et préparation de dictée

---
# :two: Proposition de cotation vidéo "CP-CE1 Phonèmes-Préparation dictée” (1/5)

* **Climat + (+ + + +) \: 7**.  M a une voix posée et chaleureuse ; est plutôt proche des E. Fait parfois preuve d'humour ("la grève des services") et rit parfois. Quelques formules de politesse. N'élève jamais la voix pour reprendre ou réorienter un E. Procure des encouragements  et des compliments ("vous savez super bien faire", "c'est bien Éva", "vous en êtes déjà au 3, ah ben dis donc !", "c'est bien écrit")

* **Climat – (– – – –) \: 1**.  Aucun exemple de climat négatif.

* **Sensibilité de l’enseignant (+ + + +) \: 7**. M circule près de tous les E et est très attentive à leur suivi (même dans le groupe où elle ne travaille pas), elle repère les élèves levant le doigt et ne les laisse pas sans aide ; elle chuchote fréquemment des mots d'aide. E sont volontaires pour participer. M anticipe les problèmes de lecture en lisant à l'avance les mots représentés par les images.

---
# :two: Proposition de cotation vidéo "CP-CE1 Phonèmes-Préparation dictée” (2/5)
			
* **Prise en considération du PV de l’enfant (= = = +) \: 4**. Un E est préposé à l'écriture de la date (même si M semble l'écrire au bout du compte) . Les E peuvent se déplacer librement. Une certaine flexibilité est prévue puisque les E peuvent réviser leur poésie s'ils ont fini, et elle indique que les syllabes (CE1) sont optionnelles ; elle passe un moment à expliquer l'accord au pluriel.  Mais il y a peu d'exemples de *leadership* des élèves sur la leçon, qui est très dirigée par M, et peu de moments de parole des élèves.

* **Gestion des comportements (+ + + +) \: 7**. Aucun exemple de comportement problématique des E. Quelques redirections rapides de certains E ("Ethan regarde la boîte à outils"), non gênantes  . Aucune défiance de la part des E. 

---
# :two: Proposition de cotation vidéo "CP-CE1 Phonèmes-Préparation dictée”(3/5)

* **Productivité \: (= + + +) \: 6**. Gère bien la distribution des tâches entre les 2 niveaux, sans en perdre de vue aucun. Fonctionnement de classe "bien huilé", même si les CP passent beaucoup de temps sur l'écriture de la date. Les E peuvent continuer sur d'autres tâches quand ils ont fini. Le matériel est prêt (le nom des CE1 est déjà écrit sur leurs fiches). L'usage des boîtes à outils paraît routinier.

* **Modalités d’apprentissage (+ = + +) \: 6**. M utilise une assez grande variété de supports (ardoise, tableau blanc, cahier, référence à diverses boîtes à outils. Les E ont à colorier/écrire, sur des supports assez variés. Elle présente clairement l'agenda de la journée et prend du temps à expliciter le travail à faire. Elle pose des questions régulièrement pour faire participer les E. 

---
# :two: Proposition de cotation vidéo "CP-CE1 Phonèmes-Préparation dictée” (4/5)

* **Développement de concepts  (= – – –)\: 2**. Les E ont à recomposer des mots à partir de syllabes. M pose quelques rares questions d'analyse (“jardine est-il un nom ou un verbe”), et il y a une connexion avec un exercice précédent, pas à la vie de tous les jours.
 
* **Qualité de la rétroaction (+ = = = +)\: 4**. Etaye parfois les E (le “j” de jaune). Donne des pistes pour accorder "il" en nombre, pour utiliser la boîte à outils. Explique à un E comment compter le nombre total de jours par mois avec sa main.  Sollicite assez peu les E pour qu'ils expliquent leur manière de faire. 

---
# :two: Proposition de cotation vidéo "CP-CE1 Phonèmes-Préparation dictée” (5/5)

* **Modelage Langagier (= = = – +)\: 4**.  Pose quelques questions ouvertes (“qu'est-ce qu'on met quand il y en a beaucoup”, “est-ce que c'est un nom ou un verbe”) et fait quelques extensions (à propos de la classe grammaticale de jardin). Pas de *self/parallel talk*. Bon niveau de langage, mais persiste bizarrement à dire “il y en a beaucoup” au lieu de “pluriel”. Ne laisse pas beaucoup les élèves discuter du contenu.

---
# :two: 3. Questions pour le mémoire

- Quelle·s dimension·s du CLASS vous semble·nt la ou les plus proche·s de celle·s que vous comptez étudier dans votre mémoire ?
- Reprenez leurs caractéristiques (accès aux dimensions [ici](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/class/index.html#presentation-des-trois-domaines)) et réfléchissez aux manières 
  - 1/ de les améliorer dans votre pratique 
  - 2/ de les prendre en compte dans votre mémoire
- Ce [document](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/class/tuto_CLASS_que_faire.html) contient d'utiles prescriptions pour réfléchir aux questions ci-dessus