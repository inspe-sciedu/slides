---
marp: true
autoscale: true
theme: uga
---

<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# UE 24 Option de recherche “Interactions enseignant-élèves“
## Philippe Dessus, Inspé, Univ. Grenoble Alpes

## Séance 5 — Méta-analyses et revues systématiques
##### Année universitaire 2024-25

![w:350](images/logo-espe-uga.png)

---
<!-- footer: Ph. Dessus • M1 PE • TD Option de recherche “Interactions enseignant-élèves” • Inspé-UGA 2024-25 • CC:BY-NC-SA-->
<!-- paginate: true -->

# :five: 0. Niveau des études

- **Étude primaire** : Analyse des données recueillies à la fin de répondre à un problème
- **Etude secondaire** : Ré-analyse des données d'études primaires, mais avec de nouvelles méthodes (plus précises, fiables, etc.)
- **Méta-analyse** : Compile et présente sous une forme comparable, standardisée, des résultats d'études primaires et secondaires

---
# :five: 0. Définitions - Méta-analyse

- Une méta-analyse est une méthode de recherche **quantitative** qui agrège de manière standardisée de **nombreux résultats d'études différentes** mais partageant la même question de recherche : 
  - ”est-ce que la variable *x* (souvent appelée intervention) a un effet sur une variable *y* (souvent appelée “*outcome*”, ou critère de jugement) ?” 
- et permettant de déterminer la **taille (grandeur) de cet effet**. 
- La taille de l'effet mesure donc la différence standardisée entre deux groupes : celui qui a reçu un traitement expérimental et celui qui n'en a pas reçu (groupe-contrôle) 
- La méta-analyse a donc une visée intégrative, synthétique

---
# :five: 0. Les étapes d'une méta-analyse

1. *Définir le problème* : Quelle est la question de recherche posée ?
2. *Collecter la littérature sur ce problème* : Cette collecte se fait de la manière la plus large et systématique possible (en interrogeant plusieurs bases de travaux et en créant des requêtes sophistiquées), en se fixant des **critères d'inclusion/exclusion**

3. *Coder les résultats* : Coder les résultats des travaux quantitativement et de manière standardisée selon les caractéristiques qui peuvent jouer un rôle dans les *outcomes* recueillis (apprentissage, motivation, etc.)

4. *Transformer les résultats en tailles d'effet* :  Recalculer de manière standardisée des résultats pertinents. La valeur comparable choisie est souvent la taille d'effet (*cf. infra*)

---
# :five: 0. Définitions - Revue systématique

- Une recension d'un ensemble de résultats de recherches sur un thème donné, débutant avec les mêmes étapes qu'une méta-analyse mais *sans calcul synthétique* (donc reprenant les 3 premières étapes ci-dessus)
- Regain d'intérêt des méta-analyses et revues systématiques depuis le mouvement de recherche de l'**éducation fondée sur les données probantes** (*evidence-based education*)
- Des revues se spécialisent sur ces recherches ([Rev. Educ. Res.](https://journals.sagepub.com/home/rer) ; [Educ. Res. Rev.](https://www.sciencedirect.com/journal/educational-research-review))

---
# :five: 0. La taille de l'effet

- C'est la différence de performances entre deux groupes en unités d'écart-type des deux groupes (*d* de Cohen). 
- Un *d* de 1 signale que la performance moyenne du  groupe-contrôle est un écart-type inférieure à celle du groupe expérimental 
  - Si les performances des 2 groupes suivent une distribution normale, cela signifie le score d'une personne moyenne du groupe expérimental est supérieur ou égal à 84 % des personnes du groupe-contrôle, ou, bien sûr, à 1 écart-type au-dessus
- Un *d* négatif signale que le groupe-contrôle a de meilleures performances moyennes que le groupe expérimental
- Le [site de Magnusson](https://rpsychologist.com/cohend/) donne une visualisation interactive du *d* de Cohen.

---
# :five: 1. Lire les résultats d'une méta-analyse (1/3)

### Une méta-analyse sur l'effet-signal (Schneider et al. 2018)

<!-- _class: t-70 -->

| Outcome | Nb études | Nb particip. | T. d'effet | Int. Conf. 95 % |
|---|---|---|----|----|
| Mémorisation | 139 | 11 571 | 0.53*** | [.42, .64] |
| Transfert | 70 | 5 499 | 0.33*** | [0.22, 0.43]
| Charge cog. | 27 | 2 994 | 0.25* | [0.04, 0.45]
| Motivation | 13 | 1 616 | 0.13** | [0.04, 0.22] |
| Durée d'appr. | 27 | 2 220 | –0.3* | [–0.55, -0.05]


---
# :five: 1. Lire les résultats d'une méta-analyse  (2/3)

<!-- _class: t-70 -->


| Modérateurs | Nb études  | T. d'effet | Int. Conf. 95 % |
|---|---|---|----|
**Niveau éducatif**
| Primaire | 7 | 0.19 | [-0.22, 0.61]
| Secondaire | 19 | 0.34* | [0.08, 0.60] |
| Univ. | 108 | 0.6*** | [0.49, 0.71]
| **Niveau de connaissance initial** |
| Bas | 37 | 0.42*** | [0.18, 0.65]
| Haut | 20 | 0.67*** | [0.35, 1.0]

---
# :five: 1. Lire les résultats d'une méta-analyse  (3/3)

<!-- _class: t-70 -->

| Modérateurs | Nb études  | T. d'effet | Int. Conf. 95 % |
|---|---|---|----|
**Signal textuel**
| Mise en évidence organisation | 57 | 0.71*** | [0.54, 0.88]
| Réf. imagée | 5 | 0.38 | [–0.21, 0.97]
| Couleur | 2 | 0.70 | [0.21, 1.6]
**Signal imagé**
| Geste pointage | 19 | 0.33* | [0.004, 0.66]
| Couleur | 18 | 0.44* | [0.08, 0.79]
| Étiquetage | 6 | 0.32 | [-0.29, 0.94]
| Flash | 3 | –0.56 | [–1.38, –0.25]



---
# :five: 1. Les méta-analyses : Intérêts 

- Les méta-analyses donnent une vue globale, **agrégée et exhaustive**, d'un grand nombre de résultats d'études primaires, sur un sujet donné
- Ce faisant, on gagne en puissance statistique en considérant un grand nombre de participants (les analyses des études individuelles pouvaient en manquer)
- Cela peut être très utile de s'y référer quand on commence à défricher un champ de recherche, ou bien lorsque les résultats d'un champ paraissent contradictoires
- Cela permet aussi de voir ce qui marche, ce qui a été beaucoup ou peu testé, etc.

---
# :five: 1. Interpréter les méta-analyses (1/2)

- Il faut étudier le comparateur des études : à quoi sont comparées les études expérimentales ? un groupe-contrôle sans intervention ? un autre type d'intervention ? 
- Il est préférable de se restreindre à l'étude d'un seul *outcome* et de relever les tailles des effets de chaque intervention ciblant cet *outcome*. Se centrer sur un niveau de population (p. ex., 1er ou 2d degré) est également utile

---
# :five: 1. Interpréter les méta-analyses (2/2)

- :warning: Les personnes d'un groupe-contrôle, donc ne participant à aucune intervention spécifique, ont des performances qui augmentent dans le temps. Par exemple, entre le CM1 et CM2, la taille d'effet de la progression en lecture d'un élève états-unien moyen est de 0,30

- La taille de l'effet varie selon le type d'*outcomes* étudiés. Lorsque ces derniers sont aisés à changer, fondés sur des mesures simples à recueillir et mesurés juste après l'intervention, ils sont plus importants que des *outcomes* sur lesquels il est plus difficile d'agir, mesurés à plus long terme et avec des tests moins spécialisés

---
# :five: 1. Les méta-analyses : Limites et critiques (1/2)

- Elles amènent une **focalisation excessive sur une valeur** (la taille d'effet) pour juger de l'efficacité d'une intervention, presque indépendamment d'autres éléments de contexte. Or, qui voudrait d'une intervention efficace (les élèves apprendraient effectivement), mais qui en même temps enlèverait tout intérêt des élèves pour la matière ? (cf. effets secondaires, TD 4)

- La qualité de la méta-analyse **ne peut être supérieure à celle des études qui la composent** (*garbage in, garbage out*). En d'autres termes, si la phase de sélection des études est mal réalisée, avec des critères de choix insuffisamment précis, la sélection va comporter des études trop hétérogènes pour être comparables, et donc introduire des biais



---
# :five: 1. Les méta-analyses : Limites et critiques (2/2)

-  Un autre biais lié à la qualité des études sélectionnées est celui des **pratiques de recherche non-éthiques**, “questionnables”, ou méthodologiquement faibles, où les chercheurs, soit ont triché pour avoir des résultats significatifs, soit utilisent des méthodes d'analyse qui ne correspondent pas ou plus aux canons modernes
  
- Les compilations de méta-analyses (méta-méta-analyses ou méga-analyses) mélangent trop facilement les pommes et les poires. Les tailles d'effet varient en fonction de paramètres non directement reliés à la qualité de l'intervention ; **il faut donc considérer les méga-analyses avec la plus grande prudence**

---
# :five: 2. Écrire une revue systématique narrative ?

1. **Choisir un sujet**, l'étendue de sa prise en compte (niveau, types d'élèves, domaine)
2. **Justifier l'importance** et la nouveauté du sujet
3. **Formuler la requête** (en Fr. et Eng.) et l'utiliser dans diverses bases de données, catalogues
4. **Collecter, analyser, filtrer, et organiser les sources** (1 : à partir de la lecture des résumés ; 2 : filtrer les travaux pertinents en déterminant des critères ; 3 : lire les articles entiers ; 4 : filtrer à nouveaux les travaux pertinents)
5. **Grouper les sources** par types de données/niveau de preuve ; synthétiser les informations dans un tableau
6. **Structurer** le propos en sous-thèmes

:book: Gasparyan et al. (2011)

---
# :five: 2. L'art des requêtes

- Il est difficile de formuler une requête efficace du premier coup. Voici quelques requêtes : 
  - Effet de l'auto-explication : self-expla\* Bisra et al. (2018)
  - Effet des classes inversées : (flip* OR invert*) AND (class* OR learn) AND history (Lo, 2020)
- Dans la plupart des moteurs de recherche il est aussi possible d'indiquer des bornes de dates de publication

---
# :five: 3. Tâches de réflexion (1/2)

- Reprendre les mots-clés de la requête utilisée dans le TD précédent et y ajouter le mot-clé “méta-analyse” ou “revue systématique” (resp. *meta-analysis* et *systematic review* ou *scoping review*)
- Choisir un article le plus proche possible de sa problématique et commencer à le lire et à l'interpréter, en s'assurant que la méta-analyse ou la revue systématique prenne bien en compte son niveau (primaire)

---
# :five: 3. Tâches de réflexion (2/2)

- Repérer les outcomes les plus étudiés
- Repérer les modérateurs les plus étudiés (dont le niveau scolaire)
- Choisir les interventions donnant les effets les plus élevés
- Naviguer dans les sites recensant des résultats de méta-analyses (cf. dernière diapositive)

---
# :five: Références

- Bisra, K., Liu, Q., Nesbit, J. C., Salimi, F., & Winne, P. H. (2018). Inducing Self-Explanation: a Meta-Analysis. *Educational Psychology Review*. https://doi.org/10.1007/s10648-018-9434-x 
- Gasparyan, A. Y., Ayvazyan, L., Blackmore, H., & Kitas, G. D. (2011). Writing a narrative biomedical review: considerations for authors, peer reviewers, and editors. *Rheumatol Int, 31*(11), 1409-1417. https://doi.org/10.1007/s00296-011-1999-3 
- Lo, C. K. (2020). Systematic reviews on flipped learning in various education contexts. In O. Zawacki-Richter et al. (eds.), *Systematic Reviews in Educational Research* (pp. 129-143). Springer.
- Schneider, S., Beege, M., Nebel, S., & Rey, G. D. (2018). A meta-analysis of how signaling affects learning with media. *Educational Research Review, 23*, 1-24. https://doi.org/10.1016/j.edurev.2017.11.001 

---
# :five: Pour en savoir plus

- Document sur les [méta-analyses en éducation](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/meta-analyse.html)
- On peut consulter les principaux sites méga-analytiques suivants et *via* cette page : https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/meta-analyse

## Sites recensant des résultats de méta-analyses

- [Best Evidence Encyclopedia](https://bestevidence.org)
- [Education Endowment Foundation](https://educationendowmentfoundation.org.uk/education-evidence/teaching-learning-toolkit)
- [Visible learning](https://visible-learning.org/hattie-ranking-influences-effect-sizes-learning-achievement/)

---
# Remerciement

- Nous remercions Ignacio Atal pour son aide dans la réalisation de cette présentation.