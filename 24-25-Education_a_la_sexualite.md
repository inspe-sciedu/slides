---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# 
# Éducation à la santé
### Parler d'éducation à la sexualité à l'école



Christophe Charroud - UGA

<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr -2025 - CC:BY-NC-SA -->


---
# L'éducation à la sexualité à l'école
### Un cadre réglementaire
Conformément aux dispositions de l'article L. 312-16 du Code de l'éducation, une éducation à la sexualité est organisée **à raison d'au moins trois séances annuelles** et par groupes d'âge homogène, selon une durée qui peut varier en fonction de l'âge des élèves. Ces séances doivent respecter le cadre fixé par la circulaire n° 2018-111 du 12 septembre 2018 relative à l'éducation à la sexualité.

---
# L'éducation à la sexualité à l'école
### Un cadre réglementaire, oui, mais pas vraiment respecté...

L'effectivité de ces séances demeure très inégale depuis plusieurs années, alors que les élèves sont souvent confrontés, notamment dans l'univers numérique, à des représentations sexistes, voire dégradantes.

(Extraits du Bulletin officiel n° 36 du 30 septembre 2022)

---
# L'éducation à la sexualité à l'école
### Un cadre réglementaire renforcé
Les inspecteurs et inspectrices de l'éducation nationale du premier degré, les directeurs et directrices d'école et les chefs d'établissement organiseront donc le renforcement de l'éducation à la sexualité au bénéfice des élèves. L'objectif premier consiste à assurer **la mise en œuvre effective, dès cette année scolaire, des trois séances annuelles d'éducation à la sexualité.**

Il conviendra d'être particulièrement vigilant à ce que les sujets abordés lors de ces séances ... explicités auprès des familles afin d'éviter toute méprise sur ce qu'est réellement cette éducation au respect de soi et des autres.

(Extraits du Bulletin officiel n° 36 du 30 septembre 2022)


---
# L'éducation à la sexualité à l'école
# 
# 
### Vous devrez mettre en place ces seéances de façon obligatoire 

---
# Au programme de ce parcours

## Aujourd'hui
* Quelques rappels (ou informations) sur le développement psychosexuel de l'enfant
* Découverte de quelques ressources
* Mise en groupe de 4 par niveau de classe (PS-MS; MS-GS; CP-CE1; CE1-CE2; CM1; CM2)
* Travail de conception en vu de la mise en place d'une séance

## Lors de la prochaine séance 
* Poursuite et fin du travail de conception de séance
* Mise en commun 












