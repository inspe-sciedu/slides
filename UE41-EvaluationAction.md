---
marp: false
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

## UE 41 Evaluation de l'action
### Laurence Osete
#### Master 2 MEEF EE • 2023-24

---
# :one: Rappels

---
## 1.1 Les fonctions de d'évaluation

- Au début de l'action :arrow_right: visée diagnostique
- Au cours de l'action :arrow_right: visée formative
- À la fin de l'action :arrow_right: visée certificative

---
## 1.2 Les formes d'évaluation

- Sondage, enquête
- QCM, exerciseurs
- Portfolio
- Cartes mentales
- Productions écrites ou orales
- Mises en situation

---
## 1.3 Quelles formes d'évaluation prévoyez‑vous d'utiliser dans vos projets ?
Sur le tableau collaboratif, classez les par fonction dans l'évaluation
https://digiboard.app/b/4f1196b8010d2

![](images/img-UE41/TableauCollaboratif.png )

---
# :two: Outils numériques


---
## 2.1 Les tests automatisés

Logiciel proposant un questionnement avec une évaluation
automatique de la réponse, avec ou sans rétroaction pour l'apprenant.

- [Digistorm](https://digistorm.app/)
- Questionnaire ENT
- [H5P (Moodle ENT)](https://h5p.org/)
- [Socrative](https://www.socrative.com/)
- [Plickers](https://www.plickers.com/)
- [Kahoot](https://kahoot.com/)
- [Quizinière](https://www.quiziniere.com/)

---
## 2.2 Les sondages

- [Framaforms](http://framaforms.org/)
- [Digistorm](https://digistorm.app/)
- [Plickers](https://www.plickers.com/)

---
## 2.3 Les Portfolios

- Espaces de partage de documents de l'ENT
- Espaces de stockage [Apps Education](https://apps.education.fr/)
- Murs individuels ou collaboratifs ([Digipad](https://digipad.app/) ou [Padlet](https://padlet.com/))

---
## 2.4 Les cartes mentales

- [Framindmap](https://framindmap.org/abc/fr/)
- [Digimindmap](https://ladigitale.dev/digimindmap/#/)

---
## 2.5 Outils numériques envisagés

Quels outils numériques pensez‑vous utiliser pour évaluer votre action ?

Compléter le tableau collaboratif avec les outils que vous envisagez d'utiliser pour votre action.
https://digiboard.app/b/4f1196b80

![](images/img-UE41/TableauCollaboratif.png )

---
# :three: Critères de choix

Selon vous, quels sont les critères de choix des outils numériques pour l'évaluation de votre action ?

---
## 3.1 Activité (30 mn)

- Concevoir une ébauche de votre évaluation sur l'outil choisi, ou un de ceux proposés.
- Tester l'évaluation auprès de vos pairs.
- Analyser les informations recueillies et leur modalité d'accès.
- Analyser les avantages et inconvénients de l'outil. 

---
## 3.2 Tutoriels

- [Outils La Digital](https://digipad.app/p/4707/35503ca112661)
- [Outils Framasoft](https://framatube.org/c/framatuto/videos)
- [Pronote](https://www.index-education.com/fr/tutoriels-video-pronote-professeurs.php)
- [Quizinière](https://tube-sciences-technologies.apps.education.fr/w/71a386f5-0533-45c6-8c51-5a4524166cf1)

---
## 3.4 Comptes génériques pour Tester

- [H5P (Moodle ENT)](https://h5p.org/) (departement.tice.grenoble@gmail.com ; CompteH5PetuESPE)
- [Socrative](https://www.socrative.com/) (departement.tice.grenoble@gmail.com ; mdpSocrative)
- [Plickers](https://www.plickers.com/) (departement.tice.grenoble@gmail.com ; ComptePlickersESPE)
- [Framaforms](http://framaforms.org/) (ProfESPE ; &VI3BDV1BeI#AN4*)
- [Framindmap](https://framindmap.org/abc/fr/) (departement.tice.grenoble@gmail.com; CompteFramindmapDpt)