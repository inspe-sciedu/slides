---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
   
# Culture numérique et apprentissages
## TD 2
### DIU PE - Grenoble G1 et G2

Christophe Charroud - UGA

<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2023 - 2024 - CC:BY-NC-SA -->


---
# :one: Rappels
* TD 1 :  Mise en application des apports théoriques sur les ressources et les aspects juridiques et éthiques - Début de la conception.
* **TD 2 : Mise en application des apports théoriques sur les effets du numériques sur les apprentissages - poursuite de la conception**
* TD 3 : Apports pratiques sur la formation AU numérique et l'évaluation des élèves en référence au CRCN - découverte de PIX et PIX+EDU - Poursuite de la conception.
* TP : Mise en commun des conceptions et retour d'expérience pour celles et ceux qui auront réalisé une mise en oeuvre. :arrow_right: PIX+EDU.

 
---
## Organisation du TD

1 - Rappels théoriques (5 minutes)
2 - Mise en pratique : Analyse d'usages (1h15)


---

# :two: Ce que l'on sait
## L’utilisation du numérique ***peut*** avoir un effet positif sur l’apprentissage

- **Pour accéder à des informations**

- **Pour communiquer**

- **Pour s'entraîner sur des tâches ciblées**

> (J-PAL, 2019)


---
## S'entrainer sur des tâches ciblées 
* Nécessité de feedback
* Feedback, si possible, immédiat :arrow_right: biais de mémoire
* Si la réponse comporte une erreur :arrow_right: remédiation
* Si la réponse est correcte (tant que la connaissance n’est pas parfaite, le cerveau continue d’apprendre) => nécessité de surapprentissage (réviser, remettre à l’épreuve…)
* Des rappels expansés pour consolider les connaissances.
:arrow_right: On peut aussi évlauer les élèves avec le numérique


---
# 
# 
### :arrow_right: *Facile à dire en théorie mais en pratique c'est moins simple...*






---
# :three: Pratique
##  :pencil2: TP : Test d'un outil ou service numérique

* Modalités : Par groupe de 3 à 5 personnes (par discipline ou niveau de classe)
* Essayez de déterminer **une forme** d'entraînement **numérique** réaliste que vous utilisez ou que vous pourriez utiliser dans vos classes.
* Jouez **réellement** cet entraînement au sein du groupe en vous mettant du point de vue de l'élève et du point de vue de l'enseignant

---
## Quelques exemples d'applications à tester (il y en a d'autres)
* Quizinière 
* Quizlet
* La digital
* Kahoot 
* Learning Apps 
* Socrative...

---
## Production attendue : une présentation de 5 minutes
<!-- _class: t-80 -->

**Contenu de la présentation :**
* Description et démonstration de l'entraînement choisi
* Analyse critique
    * L'interface est-elle conforme aux préconisations de la recherche
    * Y a-t-il des feedbacks ? De quelle nature ?
    * Y a-t-il des traces exploitables à postériori par l'enseignant ? Si oui lesquelles
    * L'application ou le services est-il payant ?
    * Y a-t-il un lien avec des sociétés commerciales ?
    * Le RGPD est-il respecté ?
* Conclusion : Recommandez-vous cet entraînement ?
:warning: **Vous êtes en formation = les erreurs et approximations sont normales, pas de stress !**


---


# Présentations


---
# :four: Conception d'une situation d'apprentissage intégrant le numérique 
#### Production attendue
Un document rédigé en binôme de 4 pages maximum comportant :
* un descriptif de la situation d’apprentissage avec obligatoirement des liens pointants sur les éléments de conception (liens vers les ressouces, copies d’écran des interfaces élèves et/ou enseignants, …),
* une argumentation sur les choix opérés lors de la conception, notamment au regard des apports théoriques,
* un bilan personnel.


---
## 4.1 Recherche de situation 

* L'idée de la situation d'apprentissage peut provenir d'une situation **vécue, observée ou rapportée**. 
* Elle doit intégrer au moins une ressource numérique, ou hybride.
* Elle peut être commune à plusieurs personnes mais les analyses devront prendre en compte le contexte de mise en œuvre.

---
## 4.2 Description de la situation
#### Eléments attendus dans la description

```
- Domaine(s) d’enseignement concerné(s) par la situation
- Niveau de classe
- Période de l'année, ou place dans une séquence, où la situation a été (ou serait) mise en œuvre.
- Objectif(s) d’apprentissages : 
	- Compétences disciplinaires visées en termes de « être capable de…», (ces compétences sont issues des programmes)
	- Compétences numériques développées (issues du CRCN)
	
- Environnement numérique impliqué dans la situation :
    - Outils physiques (TBI, tablettes, ordinateurs fixes ou portables, caméras, enregistreurs, ...)
    - Organisation matérielle (nb de matériels par élève/classe, salle informatique ou salle de classe)
    - Logiciels ou applications utilisés.
    - Ressources utilisées, ou produites

-  Organisation pédagogique : quelles activités, quand, comment, ...
    
```

---
## 4.3 Analyse et justification des choix

- Justification des choix d'outils et de ressources numériques, en s'appuyant notamment sur le modèle SAMR.
- Analyse des potentiels effets positifs ou néfastes pour les apprentissages et les apprenants (TD 1).
- Analyse des problèmes juridiques (droits d'auteurs, droit à la vie privée) et éthiques soulevés.


:warning: Les analyses et justifications doivent tenir compte du contexte.



---
# En avant !
* Essayez d'identifier un apprentissages pour lequel le numérique peut apporter une plus-value réelle.
* Restez réaliste :arrow_right: Mise en oeuvre conseillée. 
