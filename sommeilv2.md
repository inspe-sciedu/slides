---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# Sommeil et apprentissages
![w:500](images/img-sante/accueil.jpg)



<!-- page_number: true -->
<!-- footer: Éducation à la santé - 2024 - CC:BY-NC-SA -->

 
---
## « Sommeil et apprentissages », un travail d’équipe
<!-- _class: t-70 -->
**Stéphanie BIOULAC,**  
* PU-PH, Professeur de Pédopsychiatrie, Chef de service de psychiatrie de l’enfant et l’adolescent au CHU GA.
* Membre du LPNC, Equipe Développement et Apprentissages, UGA.

**Sébastien BAILLIEUL**
* MCF-PH, Pneumologue, Somnologue, service pneumologie-physiologie pôle thorax et vaisseaux au CHU GA. 
* Membre du laboratoire HP2 de l’Université Grenoble Alpes.

**Christophe Charroud** 
* Responsable équipe "éducation à la santé"  
 
![bg right:20%](images/img-sante/equipe.jpg)

<!-- page_number: true -->
<!-- footer: inspe-mission-sante@univ-grenoble-alpes.fr - UGA - 2024 - CC:BY-NC-SA -->

---
# Au programme
<!-- _class: t-70 -->

**1 - Physiologie**
* 1.1 Le sommeil : quelques repères.
* 1.2 A quoi sert le sommeil?
* 1.3 Mesure du sommeil ?
* 1.4 Le sommeil une histoire de cycle
* 1.5 Mise en place du rythme veille sommeil du fœtus à l’adulte

**2 - École et sommeil**
* 2.1 Dormir oui, mais combien de temps ?
* 2.2 Impacts cognitifs de la restriction de sommeil
* 2.3 À quelle heure devrait-on commencer l’école ?
* 2.4 Troubles pathologiques du sommeil et impacts sur l’école : SAOS 
* 2.5 Le sommeil un allié des apprentissages

---
# :one: Physiologie du sommeil
#

* C'est quoi le sommeil ?
* À quoi ça sert ?
* Comment ça marche ?
* ...
 
---
## 	1.1 Le sommeil : quelques repères.

![w:450](images/img-sante/reperes.jpg)

 
### À 75 ans … on a passé 25 ans à dormir et 5 années à rêver.
### Nous passons 1/3 de notre vie à dormir

---
### Le Sommeil : une définition
* **État altéré de la conscience** dans lequel plusieurs fonctions
physiques et mentales continuent d’opérer :arrow_right: état à part entière et actif
* À différencier du coma ou de l’anesthésie

### 4 critères comportementaux :
* Activité motrice réduite
* Réponses aux stimuli externes diminuées
* Posture stéréotypée (couché et les yeux fermés chez l’humain)
* Réversibilité relativement facile

---
### Le sommeil, une fonction régulée et cyclique
![w:600](images/img-sante/cycle24.jpg)

---
### Régulation du sommeil
#### Le processus circadien
![w:600](images/img-sante/circatemp.jpg)



---
### Régulation du sommeil
#### Le processus circadien
![w:600](images/img-sante/circamela.jpg)

#### :arrow_right: « Je dors car il est l'heure de dormir »
---
### Régulation du sommeil
#### Le processus homéostasique ou homéostatique
![w:600](images/img-sante/circahomeo.jpg)



---
### Régulation du sommeil
#### Le processus homéostasique ou homéostatique
![w:650](images/img-sante/homeostasie.jpg)



---
### Régulation du sommeil
#### Le processus homéostasique ou homéostatique
![w:600](images/img-sante/circasieste.jpg)


#### :arrow_right: « Je dors car je suis somnolent »

---
## 1.2 Pourquoi dormons-nous ?
* Récupérer
* Croissance
* Régénération organique tissulaire et cellulaire 
* Maintenir un métabolisme sain
* Résistance aux infections
* Régulation des émotions
* Maturation cérébrale/ Développement 
* Apprendre / consolidation mémoire

---
## 1.3 Mesure du sommeil
Il existes plusieurs façons de mesurer le sommeil :
* Polysomnographie
* Actigraphie
* Agenda du sommeil

---
### Polysomnographie
![w:300](images/img-sante/Polysomnographie.jpg)

---
### Polysomnographie
![w:600](images/img-sante/Polygraph.png)
Des résultats très précis **mais** une mesure difficile à mettre en oeuvre

---
### Actigraphie
![w:300](images/img-sante/actiwatch.jpg)
* Capteur qui mesure les accélérations, donc l’amplitude et la fréquence des mouvements
* Permet une mesure du cycle repos-activité sur de longues périodes
* Examen sur plusieurs jours/semaines
* Mesure objective des phases d’activité et de repos

---
### Actigraphie
![w:600](images/img-sante/actigraphie.jpg)
Des résultats informants et objectifs :arrow_right: utilisable avec des élèves

---
### L'agenda du sommeil
![w:600](images/img-sante/agenda.jpg)

---
### L'agenda du sommeil

* Facile à mettre en oeuvre
* :warning: Déclaratif (biais de désirabilité, parents...)
* Peut-être une première source d'information sur le sommeil des élèves
* Utilisable dans le cadre de défis comme "la semaine sans écran"

---
## 1.3 Le sommeil une histoire de cycle
![w:500](images/img-sante/train.jpg)
Une nuit = 4-6 trains ou cycles

---
### Des cycles différents et importants

![w:600](images/img-sante/hypnogramme.jpg)

---
### Des cycles différents et importants
* Les premiers cycles sont indispensables pour la récupération et la régénération
* Les phases de mémorisations et de consolidations interviennent sur différents moments :
    * Mémorisation verbale et spatiale durant les phases de sommeil lent profond
    * Gestion des émotions et consolidation de la mémoire les phases de sommeil paradoxal

---
## 1.4 Mise en place du rythme veille sommeil du fœtus à l’adulte
### En phase intra-utérine 

* Rythmes dépendants aussi des synchroniseurs maternels (sécrétion maternelle cortisol, mélatonine).
* Fœtus probablement sensible aussi à la régularité du sommeil maternel.
* Futures mamans avec rythmes couchers/levers réguliers au cours du 3ème trimestre :arrow_right: **bébés qui auraient tendance à faire des nuits plus rapidement.**
(Wulff & al., 2002)

---
### Une mise en place progressive de 0 à 6 ans
![w:400](images/img-sante/evolutionsieste.jpg)
(Chevrot & Schröder, 2020)

---
### Sieste et variations inter-individuelles
![w:600](images/img-sante/etudesieste.jpg)
(Staton & al., 2015) )

---
#### Sieste et variations inter-individuelles
:warning: Entre 3 et 6 ans 
Sommeil majoritairement nocturne, mais persistance du sommeil diurne: SIESTE en début d’après-midi
* Importance d’offrir des bonnes conditions de siestes à l’école même pour MS et GS
* La sieste disparait avec une **GRANDE VARIABILITE INTER-INDIVIDUELLE** (bonne stabilité du rythme et du besoin de sommeil intra-individuelle)
* Importance de connaitre le rythme de l'enfant (échange avec les parents)
* À la rentrée au CP: sommeil nocturne uniquement

---
# :two: École et sommeil

* Combien de temps faut-il dormir pour une bonne scolarité ?
* Que se passe-t-il si on ne dort pas assez ?
* À quelle heure commencer les cours ?
* Une pathologie du sommeil est-elle gênante pour les apprentissages ?
* Bien dormir pour bien apprendre, c'est vrai ?

---
## 2.1 Dormir oui, mais combien de temps ?
Besoins de sommeil :
* Quantité de sommeil dont on a besoin pour être en forme le lendemain
* Durée moyenne chez les « bons dormeurs » varie entre 7-8.5h/nuit

Néanmoins, il existe des différences individuelles
* Petits dormeurs : moins de 6 heures
* Longs dormeurs : plus de 9 heures

---
### Des durées différentes et des chronotypes différents

| Les alouettes | Les hiboux |
| :--- | ---: |
| Sujets du matin | Sujets du soir |
| Se réveillent en forme | Réveil difficile |
| Performant le matin | Performant le soir|
| Somnolent le soir | En forme le soir |

---
### Chronotype en population général
![w:600](images/img-sante/chronotype.jpg)
*(Roenneberg et al., 2007)*

---
### Chronotype ados
![w:600](images/img-sante/chronotypeado.jpg)
*(Roenneberg et al., 2007)*

---
![w:600](images/img-sante/sleepduration.jpg)

---
![w:600](images/img-sante/sleepdurationeleves.png)

---
## 2.2 Impacts cognitifs de la restriction de sommeil
![w:400](images/img-sante/urrila.jpg)
* 177 adolescents 14.4 ans ( +/-0.5 ans) 
* Questionnaires: Horaires sommeil: Semaine/WE
*(Urrila & al., 2017)*

---
### Impacts cognitifs de la restriction de sommeil
Mesures en imagerie cérébrale 
![w:300](images/img-sante/urrilaconclusions.jpg)

En cas de durée plus courte passée au lit pendant les jours de semaine et d'horaires de sommeil plus tardif le WE (classique...) la recherche constate :
* Une diminution de volume de matière grise dans le cortex frontal et le cortex cingulaire
* Ce qui implique de mauvais résultats scolaires associés
*(Urrila & al., 2017)*

---
### Impacts cognitifs de la restriction de sommeil
En cas de privation de sommeil en semaine/décalage des horaires de sommeil :
* Mauvais comportements ayant un impact sur le développement cérébral.
* Des effets sur l’attention.

***Mais le problème est souvent ignoré... car notre perception est biaisée***

---
### Impacts cognitifs de la restriction de sommeil

![w:400](images/img-sante/agostini.jpg)
Expérimentation avec 12 adolescents contrôles 15-17 ans
Protocole
* 2 nuits passées au lit 10 h: 21h30-07h30 
* 5 nuits passées au lit 5 h : 02h30-07h30 
* 2 nuits passées au lit 10 h: 21h30-07h30
(Agostini & al., 2017)

---
### Impacts cognitifs de la restriction de sommeil
Les sujets sont soumis à 2 tests 5 fois par jour
#### Test 1 :PVT (Psychomotor Vigilance Test) 
Donne une indication précise sur le niveau d'attention.
![w:300](images/img-sante/pvt.jpg)
On mesure le temps de réaction :arrow_right: permet d'avoir une **mesure** d'un niveau d'attention.

---
### Impacts cognitifs de la restriction de sommeil
Les sujets sont soumis à 2 tests 5 fois par jour 
#### Test 2 : Echelle de somnolence de Karolinska
![w:600](images/img-sante/karolinska.jpg)
Auto-évaluation de la somnolence :arrow_right: état **déclaratif**.

---
### Impacts cognitifs de la restriction de sommeil
Résultats de la mesure du test PVT
![w:100](images/img-sante/pvt.jpg)
![w:900](images/img-sante/pvtresult.png)

---
### Impacts cognitifs de la restriction de sommeil
Résultats du déclaratif sur l'échelle de somnolence de Karolinska
![w:900](images/img-sante/karolinskaresult.jpg)

---
### Impacts cognitifs de la restriction de sommeil
![w:700](images/img-sante/agostiniresult.jpg)
**:warning: Divergence entre évaluation de la somnolence diurne et les performances
Mauvaise perception de cette diminution de performance**

---
### Impacts cognitifs de la restriction de sommeil
Quels effets en général ?
![w:300](images/img-sante/wheaton_haut.jpg)
![w:300](images/img-sante/weathon_bas.png)
(Wheaton & al., 2015)

---
### Impacts cognitifs de la restriction de sommeil
![w:800](images/img-sante/weathonresult.jpg)

---
### Impacts cognitifs de la restriction de sommeil
![w:800](images/img-sante/weathonconclusions.jpg)

---
## 2.3 À quelle heure devrait-on commencer l’école ?

![w:500](images/img-sante/minges.jpg)

Expérimentation en internat
* Décalage des heures de début des cours
(Minges & al., 2015) (Biller & al.2022)
---
### À quelle heure devrait-on commencer l’école ?

Retarder le début des cours de **25 à 60 min**
:arrow_right: Diminution somnolence diurne 
:arrow_right: Diminution symptômes dépressifs 
:arrow_right: Diminution consommation de caféine 
:arrow_right: Diminution lenteur en classe

---
### À quelle heure devrait-on commencer l’école ?

Une expérimentation en cours en France, pilotée par Stéphanie Mazza (Lyon 1)
* Problématique :
**Le fait de retarder d'une heure le début des cours, améliore-t-il les performances scolaires ?**

À suivre...

---
## 2.4 Troubles pathologiques du sommeil et impacts sur l’école : Un exemple le SAOS
### SAOS : Syndrome d’Apnées Obstructives du Sommeil
**Symptomatologie NOCTURNE**
* Ronflements
* Pauses respiratoires •
* Respiration buccale, bruyante 
* Sueurs
* Nycturie

---
### Troubles pathologiques du sommeil et impacts sur l’école : Un exemple le SAOS
**Symptomatologie DIURNE**
* Céphalées matinales
* Hypersialorrhée,bouche sèche 
* Trouble de la concentration
* Irritabilité
* Somnolence diurne chez l’adulte 
* Agitation chez l’enfant
![w:150](images/img-sante/saosjour.jpg)

---
### Troubles pathologiques du sommeil et impacts sur l’école : Un exemple le SAOS
#### SAOS : Syndrome d’Apnées Obstructives du Sommeil, un syndrome pas si rare...

* Entre 1% et 4% de la population est atteinte de SOAS
* La fréquence du SAOS est estimée à presque 3 % chez les 3-5 ans
:arrow_right: **soit en moyenne environ 1 élève concerné par classe...**

---
### Impact cognitif du SAOS
![w:400](images/img-sante/csabi.jpg)
Tâches, retenir une histoire
* Encodage : Répéter le plus précisément possible après la première lecture par l’examinateur
* Restitution : Après une période de 12h principalement constituée de sommeil
(Csábi & al., 2017)
---
### Impact cognitif du SAOS
Les enfants avec SAOS présentaient des performances significativement moins élevées que les enfants témoins :
* Lors de l’apprentissage (encodage)
* Lors de la période de restitution 
#
Mais pas que...



---
### Impact cognitif du SAOS
![w:700](images/img-sante/galland.jpg)

(Galland & al., 2015)

---
### Impact cognitif du SAOS
#### Des effets sur le cerveau
![w:400](images/img-sante/philby.jpg)
Diminution significative de volume de la matière grise chez enfants avec SAOS (Cortex prefrontal et frontal supérieur et cortex parietal supéro-lateral) 

:arrow_right: Régions impliquées dans le contrôle cognitif et la régulation de l’humeur
(Philby & al., 2017)

---
## A retenir 
### Impacts cognitifs de la restriction de sommeil
![w:700](images/img-sante/badsleep.jpg)
Et ce, quel que soit la raison de la dette de sommeil.
:arrow_right: Importance de la durée de sommeil, mais aussi de l’heure du réveil...

---
## 2.5 Le sommeil un allié des apprentissages.
#
### Le sommeil est FONDAMMENTAL pour les processus de développement et d’apprentissages


---
## Processus mnésiques
### Acquisition et maintien des apprentissages (Rappel)
![w:800](images/img-sante/processusmnesique.jpg)

---
## Sommeil et encodage
### Privation de sommeil et encodage
Expérimentations chez l'adulte 

![w:700](images/img-sante/expeencodage.jpg)

---
## Sommeil et encodage
### Privation de sommeil et encodage
Différents types d’apprentissages
• listes de mots (Drummond et al, 2000)
• mots émotionnels (Walcker et al 2006)
• associations entre visages et mots (Mander et al 2011)

**Résultats : Difficultés au niveau de la tache d’apprentissage et de la phase de restitution**
### :arrow_right: Incidence sur l'encodage

---
### Sommeil et encodage
La privation de sommeil avant un apprentissage provoque une diminution de l’activation hippocampique lors de la phase d’encodage
![w:300](images/img-sante/hippo.jpeg)

### :arrow_right: Directement en lien avec le sommeil lent profond
(Van Der Werf,& al., 2009)

---
### Sommeil lent et encodage
#### Expérimentation : Sommeil lent perturbé
Le sommeil du sujet est mesuré via polysomnographie
![w:350](images/img-sante/werf.jpg)
#### :arrow_right: Signal sonore déclenché dès l’apparition des ondes lentes (sommeil lent perturbé)

---
### Sommeil lent et encodage
#### Expérimentation : Sommeil lent perturbé

* Nette diminution quantité sommeil lent profond
    * Activité hippocampique plus faible lors de phase encodage
        * **Apprentissages moins bons**

(Van Der Werf,& al., 2009)

---
### Sommeil lent et encodage
#### Expérimentation : Sommeil lent favorisé par stimulation transcranienne lors de siestes
![w:200](images/img-sante/werf.jpg)
* Augmentation activité ondes lentes
    * **Amélioration des performances mémoire déclarative**

(Antonenko & al., 2013)

---
## Sommeil et consolidation des apprentissages
Chez l’enfant
* Beaucoup moins de travaux.
* Paradigme de privation partielle ou totale de sommeil difficile à utiliser sans risque sur le développeement cérébral.

:arrow_right: Implique un changement de méthodologie expérimentale

---
### Sommeil et consolidation des apprentissages
Utilisation paradigme: comparaison des performances obtenues après des périodes d’éveil ou de sommeil où l'on va mesurer les capacités d'apprentissage/restitution.

![w:700](images/img-sante/consolidationgeneral.jpg)

---
### Sommeil et consolidation des apprentissages
Enfant 9-12 ans : phase d’apprentissage et restitution 
* 2 phases
* Paires de mots

![w:700](images/img-sante/consolidationmethodo.jpg)
(Backhaus & al., 2008)

---
### Sommeil et consolidation des apprentissages
Enfant 9-12 ans : phase d’apprentissage et restitution 
* 2 phases
* Paires de mots

![w:700](images/img-sante/consolidationresult.jpg)
(Backhaus & al., 2008)

---
### Sommeil et consolidation des apprentissages
**Chez l’enfant** 
Effets bénéfiques du sommeil sur la consolidation des apprentissages même si la période de sommeil ne suit pas immédiatement l’apprentissage.
**Il faut dormir pour apprendre**
(Backhaus & al., 2008)(Wilhelm et al, 2008)

---
### Un sommeil plus long et régulier pour améliorer les performances scolaires
![w:200](images/img-sante/okano.jpeg)
La durée et la qualité du sommeil pour le mois et la semaine précédant un test étaient en corrélation avec de meilleures notes avec près de 25 % de l'écart de rendement scolaire. (Okano & al., 2019)

---
# :three: Conclusion
### Sommeil et apprentissages des liens étroits
* Avant l’apprentissage, la privation de sommeil entraine des difficultés d’encodage
* Après l’apprentissage, stockage et restitution de meilleur qualité après une période de "bon" sommeil (privation de sommeil = mauvaise mémorisation)
* Rôle spécifique de certains stades de sommeil :arrow_right: cycles...
* Evenements qui pertubent sommeil (dette de sommeil, pathologies du sommeil): impacts cognitifs (repercussions sur les apprentissages) impacts sur le développement du cerveau

---
# :three: Conclusion
### Sommeil et apprentissages des liens étroits
### :warning: Des points de vigilance :warning:
* Le temps de la sieste à l’école maternelle
* La restriction du temps de sommeil chez les adolescents
* L'heure de début des cours

---
![bg left](images/img-sante/fin.jpg)

### Prenons soin de notre sommeil et de celui des élèves
#
### Merci pour votre attention

---
# Bibliographie
Agostini, A., Carskadon, M. A., Dorrian, J., Coussens, S., & Short, M. A. (2017). An experimental study of adolescent sleep restriction during a simulated school week: changes in phase, sleep staging, performance and sleepiness. Journal of sleep research, 26(2), 227-235.

Antonenko, D., Diekelmann, S., Olsen, C., Born, J., & Mölle, M. (2013). Napping to renew learning capacity: enhanced encoding after stimulation of sleep slow oscillations. European Journal of Neuroscience, 37(7), 1142-1151.

Backhaus, J., Hoeckesfeld, R., Born, J., Hohagen, F., & Junghanns, K. (2008). Immediate as well as delayed post learning  sleep but not wakefulness enhances declarative memory consolidation in children. Neurobiology of learning and memory, 89(1), 76-80.

Biller, A. M., Meissner, K., Winnebeck, E. C., & Zerbini, G. (2022). School start times and academic achievement-a systematic review on grades and test scores. Sleep medicine reviews, 61, 101582.

Chevrot, R. M., & Schröder, C. M. (2020). Décryptages et recommandations sur le sommeil des enfants et des adolescents. Soins Pédiatrie/Puériculture, 41, 12-17.

---
Csábi, E., Benedek, P., Janacsek, K., Zavecz, Z., Katona, G., & Nemeth, D. (2016). Declarative and non-declarative memory consolidation in children with sleep disorder. Frontiers in human neuroscience, 9, 709.

Galland, B., Spruyt, K., Dawes, P., McDowall, P. S., Elder, D., & Schaughency, E. (2015). Sleep disordered breathing and academic  performance: a meta-analysis. Pediatrics, 136(4), e934-e946.

Mazza, S. (2022). Conférence “Sommeil et apprentissage de l’enfant”.

Minges, K. E., & Redeker, N. S. (2016). Delayed school start times and adolescent sleep: A systematic review of the experimental evidence. 
Sleep medicine reviews, 28, 86-95.

---

Okano, K., Kaczmarzyk, J. R., Dave, N., Gabrieli, J. D., & Grossman, J. C. (2019). Sleep quality, duration, and consistency are associated with better academic performance in college students. NPJ science of learning, 4(1), 16.

Philby, M. F., Macey, P. M., Ma, R. A., Kumar, R., Gozal, D., & Kheirandish-Gozal, L. (2017). Reduced regional grey matter volumes in pediatric obstructive sleep apnea. Scientific reports, 7(1), 44566.

Roenneberg, T., & Merrow, M. (2007, January). Entrainment of the human circadian clock. In Cold Spring Harbor symposia on quantitative biology (Vol. 72, pp. 293-299). Cold Spring Harbor Laboratory Press.

Staton, S., Smith, S., & Thorpe K. (2015) "Do I Really Need a Nap?": The Role of Sleep Science in Informing Sleep Practices in Early Childhood Education and Care Settings. Translational Issues in Psychological Science. 1(1). 30-44.

---

Urrila, A. S., Artiges, E., Massicotte, J., Miranda, R., Vulser, H., Bézivin-Frere, P., ... & Martinot, J. L. (2017). Sleep habits, academic performance, and the adolescent brain structure. Scientific reports, 7(1), 41678.

Wheaton, A. G., Jones, S. E., Cooper, A. C., & Croft, J. B. (2018). Short sleep duration among middle school and high school students—United States, 2015. Morbidity and Mortality Weekly Report, 67(3), 85.

Wilhelm, I., Diekelmann, S., & Born, J. (2008). Sleep in children improves memory performance on declarative but not procedural tasks. Learning & memory, 15(5), 373-377.

Wulff, K., & Siegmund, R. (2002). Emergence of circadian rhythms in infants before and after birth: evidence for variations by parental influence. Zeitschrift fur Geburtshilfe und Neonatologie, 206(5), 166-171.
 







