---
marp: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# UE 24 Option de recherche “Interactions enseignant-élèves“
### Philippe Dessus, Inspé, Univ. Grenoble Alpes
### Séance 6 - Recueillir des données
##### Année universitaire 2023-24

![w:350](images/logo-espe-uga.png)

---
<!-- footer: Ph. Dessus • M1 PE • TD Option de recherche “Interactions enseignant-élèves” • Inspé-UGA 2023-24 • CC:BY-NC-SA-->
<!-- paginate: true -->

# :six: 0. But de la séance

- Prendre connaissance de diverses manières de recueillir des données sur les IEE (interactions enseignant-élèves), *via* questionnaires ou observations directes

---
# :six: 1. Rendre compte de ce qui se passe en classe

<!-- _class: t-80 -->

-	Observer une séance (en temps réel ou vidéo) et

1. prendre des notes et interroger l'enseignant après la séance (**narratif**)
2. se centrer sur certains comportements et reporter leur fréquence (**checklist**)
3. identifier rapidement un comportement donné toutes les *n* secondes (**échantillonnage**) 
4. enregistrer tout ce qui se dit/fait dans une période donnée, puis le classer (**interactions**)
5. évaluer la qualité d'événements scolaires dans une période donnée (**échelles d'évaluation**)

% ajouter : concevoir un questionnaire / Aspects sociométriques

---
# :six: 1. Observer :  Un *continuum* d'instruments

* Dans tous les cas, nécessité de disposer d'un instrument d'observation qui pré-sélectionne des variables d'intérêt (on ne peut **tout** observer)
- Ces instruments se placent dans un **continuum** : 

	- capturer **l'intégralité** des variables d'une situation *vs*. **une seule** variable à 2 modalités (*e.g*., parle/ne parle pas)
- On se place quelque part au milieu de ce continuum

:books: Dessus 2007 ; Simon & Boyer 1970

---
# :six: 1. Les étapes du processus d'observation-analyse des IEE (1/2)

:warning: Chaque étape ci-dessous induit une **réduction** des données

1. **segmenter** les événements spatialement et temporellement (*e.g*., scruter telle partie de la classe, tels élèves, toutes les *n* minutes ou globalement)
2. **filtrer** les événements (pertinents vs. non pertinents), en fonction du problème de recherche (*e.g*., les paroles, les gestes, les déplacements...)

---
# :six: 1. Les étapes du processus d'observation-analyse des IEE (2/2)

3. **coder** les événements pertinents, leur attribuer une ou des (sous-)catégorie·s : épisode-leçon-unité ; but pédagogique
4. **réduire les données**, utiliser des traitements graphiques ou statistiques pour comparer les données (accord inter-juges), réduire les dimensions et mieux appréhender ce qui se passe dans la classe, selon une théorie, une problématique choisie *a priori*

:books: Bocquillon 2020 ; Dessus 2007

---
# :six: 2. Présentation de quelques outils

**2.1** : Un questionnaire pour les élèves, le QTI (*Questionnaire for Teacher Interaction*)
**2.2** : Un questionnaire pour l'enseignant, le STRS (*Student-Teacher Relationship Scale*) (Pianta 2001)
**2.3** : Un instrument pour observer l'engagement comportemental des élèves (Altet, Bressoux, Bru, & Lamber, 1994)
**2.4** : Un instrument pour observer simplement les épisodes de classe (Hora et al. 2013)
**2.5** : Le *Classroom Assessment Scoring System* (voir TD 1 et 2 en se centrant sur 1 domaine)

---
# :six: 2.1 Le QTI (Questionnaire for Teacher Interaction)

- Questionnaire de 64 items (à l'origine) visant à mesurer les perceptions que les élèves ont sur les comportements et attitudes de leur enseignant, évaluables par les élèves sur une échelle de Likert (5 points)
- Intéressant à faire passer, car env. 70 % des enseignants se perçoivent plus positivement que leurs élèves ne le font 
- Traduit en français, testé et validé en école primaires et établissements secondaires québécois (Lapointe & Legault 1999), sans doute à adapter un peu

---
# :six: 2.1 Le QTI, quelques items (Lapointe & Legault 1999)


## Leadership
- C’est un bon leader.
- Il capte notre attention.
- On apprend beaucoup avec lui.
- Il explique les choses clairement.
## Bienveillance
- Sa classe est agréable.
- Il se montre vraiment amical avec nous. 
- Il nous aide beaucoup dans notre travail. 
- C’est quelqu’un sur qui on peut compter.
## Indulgence
- Il est patient.
- Il nous fait confiance.
- Si on n’est pas d’accord avec lui, on peut lui en parler.
- Il est vraiment à l’écoute lorsqu’on a quelque chose à dire.

---
# :six: 2.2 Le STRS : *Student-Teacher Relationship Scale* (Pianta 2001)

- Questionnaire en 28 items à l'origine (Likert à 5 points) pour que l'enseignant évalue ses relations avec un élève donné (donc, à remplir pour tous les élèves concernés), ou bien pour la classe entière (cet élève -> la classe)
- Version réduite à 16 items traduite en français par Simard (2020, Annexe 2). 
- Exemples d'items :
  - Je partage une relation affectueuse et chaleureuse avec cet enfant.
  - Cet enfant et moi avons toujours tendance à lutter l'un contre l'autre.
  - Lorsque bouleversé, cet enfant vient vers moi chercher du réconfort.
  - Cet élève accorde de la valeur à leur relation avec moi.
---
# :six: 2.3 Un instrument pour observer l'engagement comportemental des élèves (Bressoux & Lima 2020)

Ce [document](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/tuto-engagement-comp.html) décrit la procédure complète

---
# :six: 2.3 Un exercice d'entraînement

- Codage de la vidéo : Se centrer sur l'élève en bas à droite de l'écran 
- À chaque fois qu'apparaît un code à l'écran (A1, ..., A19), soit toutes les 30 s, juger de l'engagement comportemental de cette élève dans le segment *précédent* (on juge A1 quand le code A2 apparaît, etc.)
- :warning: Ne se centrer que sur le comportemental, ne pas inférer de cognitif (l'élève pense ceci ou cela)
- :warning: Scruter aussi l'image en haut à gauche, qui filme la professeure et le reste de la classe

---
# :six: 2.4 Le TDOP, pour segmenter les épisodes en classe (1/3)

Voir ce [document](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/tuto-tdop.html) 

---
# :six: 2.4 Le TDOP pour segmenter les épisodes en classe (2/3)

- Le TDOP est très utile pour segmenter les différents épisodes d'une séance de classe et indiquer plus précisément les méthodes pédagogiques utilisées, le matériel, le type d'interactions enseignant-élèves
- Il est plus facile d'utilisation que le CLASS
- S'intéresse à des aspects quantitatifs plutôt que qualitatifs

---
# :six: 2.4 Le TDOP (3/3)

- Centration sur le PROF, les EL(EVES), ou les INTERACTIONS. Quelques exemples :
  - **PROF-C** : le prof. fait Cours, explique des éléments sans aide visuelle
  - **PROF-AV** : le prof. fait Cours avec des aides préparées.
  - **EL-TRAV-IND** : les élèves travaillent individuellement à leur bureau
  - **EL-EXE-INT** : cours avec des exercices réalisés interactivement, avec correction immédiate

---
# :six: 2.5 Le CLASS

- Et le CLASS, plus complexe et nécessitant plus d'entraînement, est aussi utilisable
- Il est préférable de se centrer sur une ou deux dimension.s seulement

---
# :six: 3. Tâches

- Répondez aux questions suivantes. Dans quelle mesure serait-il utile de mesurer une VD, dans votre contexte de classe :
  - sur les IEE, recueillies par questionnaire, du point de vue des élèves (QTI) ou de l'enseignant (STRS), considérant les élèves individuellement ou en classe entière ?
  - sur l'engagement comportemental des élèves (outil d'observation de Altet et al.) ?
  - sur ce qui se passe à plus gros grain et quantitativement dans votre classe ? (TDOP)
  - sur les IEE, observées plus finement et qualitativement (CLASS)

---
# :six: 4. Pour la prochaine fois...

- Travail sur la rédaction du mémoire ou bien sur l'analyse des données ?


---
# :six: Références

<!-- _class: t-60 -->

- Altet, M., Bressoux, P., Bru, M., & Lambert, C. (1994). Étude exploratoire des pratiques d'enseignement en classe de CE2. *Les Dossiers d'Éducation et Formations*, 70.
- Bocquillon, M. (2020). *Quel dispositif pour la formation initiale des enseignants ? Pour une observation outillée des gestes professionnels en référence au modèle de l’enseignement explicite* [Thèse de sciences de l'éducation, Université de Mons]. Mons. 
- Dessus, P. (2007). Systèmes d'observation de classes et prise en compte de la complexité des événements scolaires. *Carrefours de l'Education, 23*, 103–117. 
- Hora, M. T., Oleson, A., & Ferrare, J. J. (2013). [Teaching Dimensions Observation Protocol (TDOP) User's Manual](http://tdop.wceruw.org/Document/TDOP-Users-Guide.pdf). Madison: Univ. of Wisconsin-Madison.
- Lapointe, J., & Legault, F. (2022). Version francophone du Questionnaire for Teacher Interaction en contexte québécois. *Mesure et Evaluation en Education, 22*(2-3), 1-19. https://doi.org/10.7202/1091248ar 
- Pianta, R. C. (2001). [STRS. Student-Teacher Relationship Scale](https://education.virginia.edu/documents/ehdstrs-professional-manualpdf). PAR.
- Simard, D. (2020). [Sentiment d’efficacité personnelle et relation enseignant – élève chez
de futur(e)s enseignant(e)s en éducation préscolaire et en enseignement primaire](https://semaphore.uqar.ca/id/eprint/1939/1/Dominic_Simard_decembre2020.pdf). Université du Québec à Rimouski. Rimouski, mémoire de maîtrise. 
- Simon, A., & Boyer, E. G. (1970). *Mirrors of Behavior II: An Anthology of Observation Instruments*. Classroom Interaction Newsletter. 

---
# Remerciement

Merci à Pascal Bressoux & Laurent Lima pour leur mise à notre disposition du matériel sur l'engagement des élèves.