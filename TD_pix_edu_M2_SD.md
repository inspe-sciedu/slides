---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  

# TD PIX+EDU
M2 MEEF SD
## 
![](images/img-pix_edu/logo_pix_edu_with.png)

<!-- page_number: true -->
<!-- footer: Pôle enseignement numérique INSPE - UGA - 2023 - 2024 - CC:BY-NC-SA -->


---
# Présentation disponible

[https://link.infini.fr/inspe-gre-pix](https://link.infini.fr/inspe-gre-pix)

![width:400px](images/img-pix_edu/qr-code-pix.jpg)

---
# :zero: Préambule

### Historique

* **Janvier 2022** : Le MENJ décide de la création d'une nouvelle certification PIX+EDU concernant l'usage du numérique en situation d'enseignement :arrow_right: commande aux universités et aux rectorats.
Certification en deux volets :
    * Un volet pratique professionnelle réalisé à l'INSPE (conception, mise en oeuvre réelle ou simulée, analyse réflexive) :arrow_right: Obtention d'un niveau pour la certification (initié ou confirmé)
    * Un volet automatisé initié à l'INSPE et terminé lors des premières années de titularisation :arrow_right: certification
---
### Historique (suite)

* **2022 - 2023** : Année d'expérimentation dans certains INSPE (pas à Grenoble) :arrow_right: résultats d'expérimentation conduisant les universités à demander à une première série d'ajustements
* **Septembre 2023** : Le MENJS annonce la généralisation de PIX+EDU mais en l'absence de texte officiel et sans attribution de moyens :arrow_right: l'INSPE - UGA refuse de mettre en œuvre la certification PIX+EDU dans ces conditions :arrow_right: **Discussion avec le Rectorat de Grenoble pour une entrée partielle dans le dispositif PIX+EDU**

---
### Dernières avancées

* **Octobre 2023** : Plusieurs universités font remonter des difficultés ou des blocages dans la mise en œuvre de la certification PIX+EDU, idem du côté de certains Rectorats
* **Fin novembre 2023** : Le MENJ et le MESR proposent (enfin) un projet plus réaliste pour la rentrée 2024... mais toujours pas de texte officiel à ce jour

---
### Prévision du dispositif PIX+EDU pour la rentrée 2024

![width:1000px](images/img-pix_edu/dep_pix.png)

---
# :one: PIX+EDU à l'INSPE de Grenoble cette année

### Volet pratique professionnelle
* **Pas d'obligation de réaliser le volet pratique professionnelle**, nous vous encourageons cependant à essayer de mettre en oeuvre une situation d'apprentissage intégrant le numérique (conservez les traces) :warning: Pas de feedback sur les éventuelles mises en œuvre.

### Volet automatisé 
* Inscription sur la plateforme PIX
* Entrée dans le dispositif automatisé PIX+EDU (résultats conservés sur la plateforme)

---
# :two: Et ensuite...
- Lorsque vous serez en poste, les rectorats vous inciteront (non obligatoire) à passer la certification PIX+EDU (volet pratique professionnelle et volet automatisé)
- Cette certification pourra maintenant être présentée et prise en compte lors des rendez-vous de carrière


---
# :three: Entrée dans le dispositif automatisé PIX+EDU

## :pencil2: Connexion
* Allez sur [pix.fr](https://pix.fr)
* Si vous avez déjà un compte PIX, cliquez sur "Connexion"
* Si vous n'en n'avez pas encore, créez-vous un compte sur [pix](https://pix.fr) :warning: **si possible avec une adresse @ac-grenoble.fr**

---
## :pencil2: Accès à la campagne PIX+EDU

- Cliquez sur "code"

![](images/img-pix_edu/code_pix.png)

- Saisissez le code **XNDNHD463** et commencez à répondre aux questions


---
# :four: Quelques conseils

- **Le reste de la séance est dédié à avancer le plus possible sur Pix+Edu**
- Quand vous ne savez pas répondre à une question, passez ; vous pouvez aussi essayer la réponse la plus plausible
- Quand le bilan intermédiaire s'affiche, prenez un moment pour comprendre vos erreurs (le cas échéant)
- :warning: Beaucoup de questions concernent l'utilisation du numérique *en général*. Cela ne vous empêche pas de réfléchir au transfert de certaines compétences en situation d'enseignement : notez les idées qui vous viennent







