---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
# 


# Addictions
## Les reconnaître pour essayer de les prévenir



### Mission éducation à la santé
   
 
<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - UGA - Prévention des conduites addictives - 2023 - CC:BY-NC-SA -->

---
# :zero: Plan
* 1 - Addictions : définition 

---
# :one: Addiction : définition
 Moyen mnémotechnique : les 5 C
* #### **C**ompulsif 
* #### **C**hronique
* #### Perte de **C**ontrôle
* #### **C**raving ( = Envie irrésistible de consommer)
* #### **C**onséquences
> ### :warning: Sur une période de 12 mois = ADDICTION

---

