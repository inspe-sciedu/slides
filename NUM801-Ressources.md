---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

## Usages d’outils et ressources numériques.
### Laurence Osete
### Inspé, Univ. Grenoble Alpes

### ![w:350](images/logo-espe-uga.png)

---

<!-- footer: L.Osete • M1-SD •  Inspé-UGA 2024-25 • ![CC:BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)-->
<!-- paginate: true -->


# :zero: Questions à se poser avant d'utiliser une ressource.

<!-- _class: t-80 -->

**1. Quoi ?**
<!-- De quelle nature de ressource ai-je besoin ? -->

**2. Pourquoi ?**
<!-- A quoi va servir cette ressource ? Quel est l'objectif pédagogique visé ? -->

**3. Quand ? Comment ?**
<!-- A quel(s) moment(s) de la séquence (ou séance) l'utiliser ? Pendant le temps de classe, ou à la maison ? -->
<!-- Qui va l'utiliser? Sur quel support ?  -->

**4. Quels prérequis ?**
<!-- De quelles compétences a besoin l'élève (ou l'enseignant) pour l'utiliser ? -->

**5. Quelles précautions légales prendre ?**

---
# :one: Qu'est-ce qu'une ressource pédagogique ?


#### Une ressource pédagogique est une entité, un "grain", numérique ou non, utilisée dans un processus d’enseignement, de formation ou d’apprentissage. 


| Les différents types de ressources pédagogiques | 
| :-------------: | 
| ![center](https://4.bp.blogspot.com/-bZMMqFxAcjk/Vhk9KCpMv9I/AAAAAAAAAEc/DPi0lLLbRBY/s640/pyramide%2Bobjets%2Bp%C3%A9dagogiques.jpg) (1) | 

---
# 1. Quelle nature de ressource ?

<!-- _class: t-80 -->

| Papier ou tangible | Hybride |Numérique|
| :------: | :------: | :-------: |
| ![w:300](https://cdn.pixabay.com/photo/2017/09/23/15/04/chart-2779132_1280.jpg) (2) | ![w:300](https://upload.wikimedia.org/wikipedia/commons/f/f4/App_iSkull%2C_an_augmented_human_skull.jpg?20130221100233) (3)| ![w:300](https://cdn.pixabay.com/photo/2017/09/05/10/08/office-2717014_960_720.jpg) (2)|

---
# 1. :warning: Ne pas confondre Outil et Ressources

L'outil sert à afficher, diffuser, ou manipuler une ressource (un contenu)

##### Exemples

| Outil | Ressource |
| ------ | ------ |
|    Navigateur    |    Une page Web    |
|    Vikazimut    | des parcours de course d'orientation  |
|    Projet Voltaire    | Des exercices d'orthographe  |
|    Géoportail    | Des cartes géographiques  |

* Parfois les deux sont indissociables.


<!---
# :pencil2: Activité 

- Constituer six groupes.
- Prendre une feuille A4 en position paysage, et tracer un tabeau avec six colonnes.
![w:150](images/img-ressources/Tableau.png). 
- Noter en entête les six questions à se poser : Quoi, pourquoi, quand, comment, quels prérequis, et quelles précautions prendre ?

<!---
# : :pencil2: Activité suite

- Consulter le site [EduBase](https://edubase.eduscol.education.fr/) et parcourir les différents usages du numérique fait par les enseignants de terrain de votre discipline.
- Choisir un usage, identifier la ou les ressources utilisées et remplir la première colonne du tableau.

-->

---
# :question: Questionnaire

![w:300](images/img-ressources/QuizzM1SD.png)
https://eformation.univ-grenoble-alpes.fr/mod/evoting/view.php?id=273267#

---
# :two: Pourquoi utiliser une ressource numérique ?

Deux sous-questions :

* pour faire quoi ?
* qu'est-ce que cela change ?

---
## 2.1 Les fonctions pédagogiques possibles (Tricot, 2020)

<!-- _class: t-70 -->

[24 fonctions pédagogiques](https://cloud.univ-grenoble-alpes.fr/s/FKNkiGw6kke38tE), dont :

1. Présenter de l’information
7. Poser des questions, demander de l’aide
8. Rechercher de l’information
10. S’entraîner
13. Coopérer
15. Évaluer, s’autoévaluer, suivre les progrès et les difficultés des élèves


:scroll: Tricot, A. (2020). _[Quelles fonctions pédagogiques bénéficient des apports du numérique ?](https://www.cnesco.fr/wp-content/uploads/2024/07/210218_Cnesco_Tricot_Numerique_Fonctions_pedagogiques.pdf)_ . Paris : Cnesco-Cnam.

---
# :question: Questionnaire (suite)
---
### 2.1.1 Fonction "Présenter de l'informations"

**Les types de ressources :** des textes, des images, des sons, des vidéos, des diaporamas, des cartes mentales, ...

**Les outils :** les suites bureautiques, un navigateur, un lecteurs audio, ou vidéo.


| **Textes** | **Mur de partage** | **Carte mentale** |
| :------: | :------: | :------: |
| ![w:300](images/img-ressources/liseuse.jpg "Crédit : Pixabay") (4)| ![w:350](images/img-ressources/Padlet2.png "Crédit : Fjuca, CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons") (5) | ![w:350](images/img-ressources/CarteMentale.png "Crédit : Vvirginie, CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons") (6) |


---
### 2.1.2 Fonction "Poser des questions, demander de l'aide"

**Les types de ressources :** Des textes, des images, des sons, des vidéos, ...

**Les outils :** une messagerie électronique, un forum, une messagerie instantannée, une FAQ, ...

| **Forum** | **Messagerie instantannée** | **Foire aux questions** |
| :------: | :------: | :------: |
| ![w:300](https://upload.wikimedia.org/wikipedia/commons/1/10/Discourse_group_activity_page.png "Crédit : SilversmithJo, CC BY-SA 4.0, via Wikimedia Commons") (7) | ![w:300](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Gossip-chat.png/640px-Gossip-chat.png "Crédit : Yesenia Estrella, GPL <http://www.gnu.org/licenses/gpl.html>, via Wikimedia Commons") (8) | ![w:300](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Frequently_asked_questions_and_answers_regarding_highly_qualified_teachers_-_DPLA_-_feabce3573be1ca4c850b55af24b2327.jpg/640px-Frequently_asked_questions_and_answers_regarding_highly_qualified_teachers_-_DPLA_-_feabce3573be1ca4c850b55af24b2327.jpg "Crédit : State Library of Ohio, Public domain, via Wikimedia Commons") (9) |

---
### 2.1.3 Fonction "Coopérer"

**Les types de ressources :** des textes, des tableaux, des diaporamas, des cartes mentales,  des agendas, ...

**Les outils :** des éditeurs de documents collaboratifs, des espaces de partage de documents, des outils de gestion de taches, ou de temps.

| **Partage de documents** | **Co-édition** |  **Organisation** |
| :------: | :------: | :------: | 
| ![w:300](https://mediascol.ac-clermont.fr/impulsion-numerique-educatif63/wp-content/uploads/sites/10/2023/01/Nuage.jpg "Outils de partage de documents de AppsEducation. Source https://mediascol.ac-clermont.fr/impulsion-numerique-educatif63/2023/09/27/nuage-le-cloud-national/") (10) | ![w:300](https://framapad.org/abc/img/fr/screenshot.png "Editeur collaboratif Framapad. Source : https://framapad.org/abc/img/fr/screenshot.png ") (11) | ![w:300](https://framablog.org/public/framasoft/framadate/accueil.jpg "Créateur de sondages Framadate. Source : https://framablog.org/public/framasoft/framadate/accueil.jpg") (12) |

---
### 2.1.4 Fonction "Faciliter l’accès à l’école et à l’apprentissage pour les élèves à besoins éducatifs particuliers (accessibilité numérique)" 

**Les types de ressources :** toutes ...

**Les outils :** Ceux avec fonctions d'accessibilité numérique.

| **Synthèse vocale** | **Reconnaissance vocale** | **Prédiction de mots** | **Adaptation de textes** |
| :------: | :------: | :------: | :------: |
| ![h:250](https://alain-michel.canoprof.fr/eleve/tutoriels/balabolka/Lire-un-texte-avec-balabolka/res/balabolka00_1.jpg "Crédit Canoprof") (13) | ![h:300](https://stileex.xyz/wp-content/uploads/2019/11/dictation-oi-logiciel-reconnaissance-vocale-600x1200.png "Crédit : Stileex") (14) | ![w:250](https://lh3.googleusercontent.com/blogger_img_proxy/AEn0k_uj9KGP8tK5B5uaT3kVDCqzx3nXFtwG7CYvvqnYzr3WxJpynYSBJxoVCqwa8nvnV2yDIzB1PDBF=s0-d "Source : https://jeprog.blogspot.com/2015/01/top-5-des-meilleurs-claviers-pour-ios-8.html") (15) | ![w:250](https://occitanie-canope.canoprof.fr/eleve/numerique-differenciation/le-numerique-au-service-des-ebep/res/texte3_1.png "Crédit : Canopé Occitanie") (16) |

Pour aller plus loin : https://occitanie-canope.canoprof.fr/eleve/numerique-differenciation/le-numerique-au-service-des-ebep/

---
## 2.2 Analyser les changements induits par l'usage des technologies : Deux modèles

---
### 2.2.1 Modèle SAMR (Puentedura, 2006 )

| **Modèle** | **Analogie** | 
| :------: | :------: | 
| ![w:400](https://primabord.eduscol.education.fr/local/cache-vignettes/L676xH599/samr-a89ff.jpg?1710153891) (17) | ![w:500](images/img-effets/Effets-tapis.png) (18) | 

:pencil2: Par groupe, noter sur une feuille un exemple pour chaque niveau d'intégration.

---
#### Les questions à se poser sur les outils et ressources numériques

![](images/img-ressources/mermaid-diagram-2024-09-20-134904.png "Crédit : L.Osete")(19)

#### Avantage-inconvénient du modèle : 

* il permet de se poser des questions sur les fonctionalités apportées par les ressources et outils numériques.
* il ne permet pas d'évaluer leur efficacité.

---
### 2.2.2 Modèle passif-participatif (Romero, 2014)

Représentation des niveaux d’engagement créatif de l’apprenant dans des activités d’apprentissage médiatisées par le numérique.
![w:800](https://margaridaromero.blog/wp-content/uploads/2019/09/cinq_niveaux_dusages_des_technologies_de_m._romero_20151.png)
(20)

- Ce modèle permet de questionner la posture de l'élève dans les activités.

:pencil2: Par groupe, noter sur la même feuille un exemple pour chaque niveau d'engagement.

---
### 2.2.3 Exemples d'application des modèles

#### - Présenter de l'information

| **Niveau SAMR**| **Substitution** | **Augmentation** | **Modification** | 
| :------ | :------ | :------ | :------ |
| Exemple | Projeter un document au vidéoprojecteur | Ajouter des informations interactives sur des images ([exemple](https://ladigitale.dev/digiquiz/q/650063febaa5d "Exercice réalisé avec un éditeur H5P")) | Questionner les élèves lors de la consultation d'une vidéo ([exemple](https://ladigitale.dev/digiquiz/q/6500495049035 "Vidéo interactive réalisée avec un éditeur H5P")) |
| Engagement de l'apprenant | Consommation passive|Consommation interactive | Consommation interactive | 

---
#### - Coopérer

| **Niveau SAMR**| **Substitution** | **Modification** | **Redéfinition** | 
| :------ | :------ | :------ | :------ |
| **Exemple** | Produire un document sur traitement de texte l'un après l'autre | Rédiger un document à plusieurs en même temps ([exemple](https://framapad.org/abc/img/fr/screenshot.png "Editeur collaboratif Framapad")) | Créer des articles sur Wikipédia |
| **Engagement de l'apprenant** | Création de contenu | Cocréation de contenu | Cocréation participative de connaissances | 

---
#### - Faciliter l’accès à l’école et à l’apprentissage pour les élèves à besoins éducatifs particuliers

| **Niveau SAMR**| **Substitution (compensation)** | **Amélioration** | **Amélioration** | 
| :------ | :------ | :------ | :------ |
| **Exemple** | Lire un texte par synthèse vocale | Prédire les mots au cours de la saisie en production d'écrits | Produire un texte à l'oral transformé à l'écrit (reconnaissance vocale) |
| **Engagement de l'apprenant** | Consommation passive | Création de contenu | Création de contenu |

---
# :three:  Quand ? 

## A quel(s) moment(s) ? Pour quelle(s) fonction(s) ?

<!-- _class: t-70 -->

### Avant ? 
 * Ecouter un document sonore, regarder vidéo, lire un document multimédia
 * Rechercher de l'information

### Pendant ?
 * Découvrir des concepts abstraits
 * Créer un objet technique, une œuvre picturale ou sonore
 * Expérimenter

### Après la classe ?
* Mémoriser, apprendre par cœure
* s'entraîner
* Évaluer, s’autoévaluer, suivre les progrès et les difficultés des élèves

<!---
# 3. :pencil2: Activité

Remplir la troisième colonne en indiquand à quel(s) moment(s) la ressource était utilisée.
-->

---
# :three:  Comment ? 

## Quelle organisation pédagogique ?

L'organisation pédagogique peut avoir un impact sur la posture des élèves.

* En classe entière :arrow_right: cours "magistral" :arrow_right: Consommation passive
* En groupe :arrow_right: travaux de groupes :arrow_right: possiblement cocréation de contenus
* En individuel :arrow_right: entrainement, évaluation, remédiation, ou différenciation. :arrow_right: Consommation interactive, ou création de contenus


---
# :three:  Comment ?

## Quel mode de diffusion ?

Le mode de diffusion va avoir un impact sur l'accessibilité des ressources dans le temps et l'espace.

* Un fichier sur l'ordinateur de l'établissement, ou de l'enseignant•e, sur une clé USB, un lecteur MP3, ou MP4
* Dans une application sur tablette, ou en ligne
* Un lien dans les favoris du navigateur, un raccourcis sur le bureau pointant vers une page internet (ENT comme [ELEA](https://dane.web.ac-grenoble.fr/elea), mur de partage, ...).

<!---
# 4. :pencil2: Activité

Remplir la quatrième colonne du tableau en indiquand le mode d'accès à la ressource.
-->

---
# :five:  Prérequis ? 

## De quoi ont besoin les élèves, ou les enseignants ?

* De materiels
* De logiciels
* **De compétences** : Carde de référence des compétences numériques 
   * des enseignants [CRCN-Édu](https://eduscol.education.fr/document/47366/download)
   * des élèves [CRCN](https://eduscol.education.fr/721/evaluer-et-certifier-les-competences-numeriques#summary-item-1), 
   
---
## 5.1 CRCN c'est quoi ?

16 compétences réparties dans 5 domaines :
![h:400](images/img-ressources/CRCN.jpg)

---
## 5.2 Plusieurs niveaux d'acquisition

![w:1100](images/img-ressources/CRCN-Niveaux-Interagir.jpg)
![w:1100](images/img-ressources/CRCN-Niveaux-Collaborer.jpg)

>  Pour en savoir plus, consulter le [tableau de compétences par niveau d'acquisition](https://eduscol.education.fr/document/20392/download) disponible sur Eduscol.
 
---
## 5.3 Evaluation des compétences : Pix

* Positionnement dès la 6ème et tout au long de la scolarité de l'élève.

* Certification :
  - 3ème
  - Terminal

* :warning: L'élève doit s'être positionné sur au moins 5 compétences.


---
# :pencil2: Mise en application 

- Constituer des groupes.
- Prendre une feuille A4 en position paysage, et tracer un tabeau avec cinq colonnes.
![w:200](images/img-ressources/Tableau5.png). 
- Noter en entête les questions à se poser : Quoi ?, pourquoi ?, quand et comment ?, quels compétences ?, et quelles précautions prendre ?

---
# :pencil2: Activité suite

- Consulter les banques de ressources et d'outils disponibles dans le cours.
- Choisir quelques ressources et/ou outils, remplir les cinq premières colonnes du tableau (une ressource par ligne, maximum 3).
- Présenter vos analyses à un autre groupe.

---
# Pour aller plus loin

Tester vos connaissances à l'aide de deux tests sur Pix.

:warning: ils ne peuvent être réalisés qu'une seule fois.

---
# Crédits (1/2)

<!-- _class: t-60 -->

- (1) [Laurent FLORY](https://www.enssib.fr/bibliotheque-numerique/documents/1234-les-caracteristiques-d-une-ressource-pedagogique-et-les-besoins-d-indexation-qui-en-resultent.pdf), La pyramide des objets pédagogiques
- (2) [Lukas](https://pixabay.com/users/goumbik-3752482/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2717014) via [Pixabay](https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2717014)
- (3) Hagustin, CC BY-SA 3.0, via Wikimedia Commons URL : https://commons.wikimedia.org/wiki/File:App_iSkull,_an_augmented_human_skull.jpg
- (4) Pixabay 
- (5) Fjuca, [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0), via Wikimedia Commons URL : https://commons.wikimedia.org/wiki/File:EPortafolio.jpg
- (6) Vvirginie, [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0), via Wikimedia Commons URL : https://commons.wikimedia.org/wiki/File:CarteMentaleAdaptationClasse.jpg
- (7) SilversmithJo, [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0), via Wikimedia Commons URL : https://commons.wikimedia.org/wiki/File:Discourse_group_activity_page.png
- (8) Yesenia Estrella, [GPL](http://www.gnu.org/licenses/gpl.html), via Wikimedia Commons URL : https://commons.wikimedia.org/wiki/File:Gossip-chat.png
- (9) State Library of Ohio, Public domain, via Wikimedia Commons URL : https://commons.wikimedia.org/wiki/File:Frequently_asked_questions_and_answers_regarding_highly_qualified_teachers_-_DPLA_-_feabce3573be1ca4c850b55af24b2327.jpg
- (10) Académie de Clermont-Ferrand URL : https://mediascol.ac-clermont.fr/impulsion-numerique-educatif63/2023/09/27/nuage-le-cloud-national/

---
# Crédits (2/2)

<!-- _class: t-60 -->

- (11) Framasoft URL : https://framapad.org/abc/img/fr/screenshot.png 
- (12) Framasoft URL : https://framablog.org/public/framasoft/framadate/accueil.jpg
- (13) Albin Michel, Canoprof URL : https://alain-michel.canoprof.fr/eleve/tutoriels/balabolka/Lire-un-texte-avec-balabolka/res/balabolka00_1.jpg
- (14) Source : Steelix URL : https://stileex.xyz/wp-content/uploads/2019/11/dictation-oi-logiciel-reconnaissance-vocale-600x1200.png
- (15) Source : Je Prog URL : https://jeprog.blogspot.com/2015/01/top-5-des-meilleurs-claviers-pour-ios-8.html
- (16) Canopé Occitanie URL : https://occitanie-canope.canoprof.fr/eleve/numerique-differenciation/le-numerique-au-service-des-ebep/res/texte3_1.png 
- (17) Qu’est-ce que le modèle SAMR ? Primàbord URL : 
- (18) Source inconnue 
- (19) L.Osete, [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)
- (20) Romero, M., & Laferrière, T. (2015). Usages pédagogiques des TIC : de la
consommation à la cocréation participative https://www.innovation-pedagogique.fr/article273.html
