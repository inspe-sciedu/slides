---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
   
# Module "Enseigner avec le numérique"
## TD 3
### DIU SD
 
Laurence Osete - UGA
 
<!-- page_number: true -->
<!-- footer: L.Osete • DIU SD • Inspé-UGA 2023-24 • ![CC:BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png) -->

---
# :zero: Rappels
* TD 1 : Mise en application des apports théoriques sur les effets du numériques sur les apprentissages - Début de la conception :arrow_right: avoir une première proposition de situation d'apprentissage.
* TD 2 : Apports pratiques sur la formation AU numérique et l'évaluation des élèves en référence au CRCN - Poursuite de la conception.
* **TD 3 : Mise en application des apports théoriques sur les ressources et les aspects juridiques et éthiques - Finalisation de la conception.**
* TP : Mise en commun des conceptions et retour d'expérience pour celles et ceux qui auront réalisé une mise en oeuvre. :arrow_right: PIX+EDU.


---
## :zero: Organisation du TD

1 - Rappels et consignes rendu du dossier (15 minutes)
2 - Mise en application des apports théoriques sur les ressources et les aspects juridiques et éthiques : Etude de cas (1h15)
3 - Finalisation du dossier d'évaluation et préparatoin d'une présentation (30 minutes)

<!---
# :one: Rôles de l'enseignant

![w:900](images/img-juridique/roles.png)

---

# 1. Compétences travaillées

* Capacité à **comprendre le contexte** dans lequel il évolue et à utiliser ses connaissances, recherches, réflexions à propos de la législation et des règlementations institutionnelles liés aux TICE **pour une mise en œuvre raisonnée et responsable** dans le cadre de son activité professionnelle.

---

# 1. Les responsabilités de l'enseignant (rappel)

* Responsabilité civile
* Responsabilité pénale
* Responsabilité administrative

---

# 1. Quelques obligations de l'enseignant (rappel)


* D'obéissance hiérarchique
* De discrétion professionnelle
* De signalement
* De neutralité

--->
---
# :one: Conception d'une situation d'apprentissage intégrant le numérique (Rappel)
#### Production attendue
Un document rédigé en binôme de 4 pages maximum comportant :
* un descriptif de la situation d’apprentissage avec obligatoirement des liens pointants sur les éléments de conception (liens vers les ressouces, copies d’écran des interfaces élèves et/ou enseignants, …),
* une argumentation sur les choix opérés lors de la conception, notamment au regard des apports théoriques,
* un bilan personnel.

---
## 1.1 Description de la situation (Rappel)
#### Eléments attendus dans la description

```
- Domaine(s) d’enseignement concerné(s) par la situation
- Niveau de classe
- Période de l'année, ou place dans une séquence, où la situation a été (ou serait) mise en œuvre.
- Objectif(s) d’apprentissages : 
	- Compétences disciplinaires visées en termes de « être capable de…», (ces compétences sont issues des programmes)
	- Compétences numériques développées (issues du CRCN)
	
- Environnement numérique impliqué dans la situation :
    - Outils physiques (TBI, tablettes, ordinateurs fixes ou portables, caméras, enregistreurs, ...)
    - Organisation matérielle (nb de matériels par élève/classe, salle informatique ou salle de classe)
    - Logiciels ou applications utilisés.
    - Ressources utilisées, ou produites

-  Organisation pédagogique : quelles activités, quand, comment, ...
    
```

---
## 1.2 Analyse et justification des choix (Rappel)

- Justification des choix d'outils et de ressources numériques, en s'appuyant notamment sur le modèle SAMR.
- Analyse des potentiels effets positifs ou néfastes pour les apprentissages et les apprenants (TD 1).
- Analyse des compétences numériques nécessaires et/ou développées par les élèves (TD 2).
- Analyse des problèmes juridiques (droits d'auteurs, droit à la vie privée) et éthiques soulevés (TD 3).

:warning: Les analyses et justifications doivent tenir compte d'un contexte.

---
## 1.3 Mise à disposition des éléments de conception

Des illustrations des ressources et/ou outils mobilisés dans la situation doivent être fournies afin de mieux comprendre cette dernière et les analyses associées : 
- Soit par des liens vers des ressources en ligne
- Soit par des liens vers des copies d'écran, notamment pour montrer le fonctionnement des outils et/ou leur paramétrage.

Les copies d'écran pourront être déposées sur un mur de partage (type [digipad](https://digipad.app/) ou [padlet](https://padlet.com/)), ou dans un dossier partagé avec l'application "[Nuage](https://nuage.apps.education.fr/)" de AppsEducation par exemple.

---
## 1.4 Critères d'évaluation

Le dossier sera évalué au regard de la grille d’évaluation suivante [:inbox_tray:](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/_downloads/f799c13851a0009b1c539d2b8d3441ce/Grille_UE801.pdf). 

Cette grille d’évaluation est issue de la grille de compétences du dispositif PIX+EDU.

---
## 1.5 Déclaration des binômes

Afin de déposer le dossier et d'être évalué en tant que binôme, vous devez créer le groupe sur la plateforme de cours à l'aide de l'outil suivant :

![w:500](images/TD/ConstitutionBinomes.png)

![width:500px](images/seance6M1SD/GroupeE2.png)
![width:500px](images/seance6M1SD/GroupeE3PE.png)
Nom du groupe de la forme C2G1 ou C2G2

---

# 1.5 Inscription du 2e participant

Inscription du deuxième participant
![width:800px](images/seance6M1SD/GroupeE4PE.png)

Désinscription possible
![width:800px](images/seance6M1SD/GroupeE5PE.png)



---
# :two: Mise en application des apports théoriques sur les ressources et les aspects juridiques et éthiques
## Les principaux sujets abordés

1. Le droit à la vie privée
2. Les droits des auteurs
3. La sécurité des élèves
4. Le principe de neutralité

---
# 2.1 Etudes de cas

## Par groupe de 3 à 6 personnes (30 mn)

- Consulter le cas attribué disponible ici : https://lstu.fr/casjuridiquessd-4
- En vous aidant des ressources documentaires et du cas similaire corrigé, répondre à la situation problème.

## Présentations (30 mn)

- En vous appuyant sur les questions de la partie "Synthèse", rendre compte de l'analyse de la situation à l'**oral** (3 min maximum).

---

# 2.2 Mise en commun

