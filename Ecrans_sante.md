---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# 
# Numérique, écrans, santé
## Apports théoriques

Christophe Charroud - UGA

<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2024-2025 - CC:BY-NC-SA -->

 

---
# :one: Écrans et santé sommatique
La science dit que les écrans en tant que technologie présentent des risques aujourd’hui établis sur certains aspects de la santé physique des enfants et des adolescents.

---
### 1.1 Le sommeil

Consensus très net sur les effets négatifs, directs et indirects, des écrans sur le sommeil :arrow_right: Diminution du temps de sommeil en deçà des recommandations.
* Exposition à un défilement rapide d’images, de sons, de lumières et de mouvements diffusés sur écrans. (Hirshkowitz & al., 2015)
* Exposition à la « lumière bleue » :arrow_right: décalage pic de mélatonine. Les « filtres à lumière bleue » n’apportent pas de bénéfice sur la qualité de sommeil (Arns & al., 2022)
* Perturbations du rythme circadien en lien avec le temps passé sur les écrans, y compris en journée (Dauvilliers, 2019).

:arrow_right: Conférence sommeil et apprentissages au cours du premier semestre.

----
### 1.2 Sédentarité
##### La place prise par les écrans et les usages qui en sont faits favorisent la sédentarité et le manque d’activité physique

Conséquence: surpoids, voire obésité :arrow_right: responsables de nombreuses pathologies chroniques.

---
#### Sédentarité - les écrans une cause indirecte
* Le temps passé sur les écrans conduit à une diminution de la dépense calorique. (Lanningham-Foster & al., 2006)
* Une diminution du temps d'écran augmente la dépense calorique (Pedersen, 2022)
* Le temps passé sur les écrans est plus fréquemment associé à des comportements alimentaires conduisant à une augmentation de l’apport énergétique par l’alimentation (Courbet & Fourquet-Courbet, 2019)(Bellissimo & al., 2007)(Boyland & al., 2019)

---
#### Sédentarité - Les effets 
* Sédentarité :arrow_right: manque d’activité physique :arrow_right: surpoids = facteur de risque importants et reconnus de maladies cardio- vasculaires et métaboliques (Duclos, 2021)(Mounier-Vehier & al., 2019)
* Mais pas que...(O'Donnell & al., 2016)
    * hypertension artérielle
    * perturbations des lipides
    * diabète de type 2
    * syndrome d’apnées du sommeil
    * puberté précoce chez les filles (Li & al., 2017)

---
### 1.3 Altération de la vue
Le visionnage intensif d’écrans a des effets néfastes pour la vue et pourrait entrainer des conséquences préoccupantes à long terme.

:warning: L’œil de l’enfant et de l'ado est encore en formation, son développement se termine vers l’âge de 16 ans.

La lumière joue un rôle essentiel dans la maturation de l’œil et le développement des fonctions visuelles.

---
#### Altération de la vue, constats et prévisions
* Les écrans contribueraient en particulier à l’épidémie de myopie qui touche les sociétés modernes. 
* La prévalence de la myopie est en augmentation depuis le milieu du XXe siècle et s’est accélérée ces dernières décennies.
* Il est estimé qu’en 2050, la moitié de l’humanité souffrira de myopie, à un stade sévère pour 10 % d’entre-elle.
(Matamoros & al., 2015)(Grzybowski & al., 2020)(Haarman & al., 2020)

---
#### Altération de la vue, causes établies ou potentielles
* Moins de variation courte et longue distance (accoutumance)
* Exposition plus faible à la lumière naturelle et plus importante à la lumière artificielle (Jones-Jordan & al., 2012)(Wu & al., 2013)
* La lumière bleue émise par la majorité des écrans à LED **semblerait** présenter, à forte dose, des effets phototoxiques inquiétants sur la rétine (Cao & al., 2020)(Foreman & al., 2021), à suivre ...
* « digital eye strain » (a minima à 50% chez les usagers d’ordinateurs) :arrow_right: augmentation de la sensation d’œil sec, sensations de fatigue visuelle, flou visuel (Sheppard & al., 2018).

---
### 1.4 Autres effets sommatiques potentiels 
* Existence possible (**mais non prouvée à ce jour**) d’effets liés à l’exposition aux rayonnements radiofréquences (Charroud & Choucroune, 2016)(De Vasconcelos & al., 2023).
* Toxicité de certains des matériaux ou des substances utilisés pour la fabrication des écrans (Abdallah & Harrad, 2018)(Tansel, 2022)(Wang & al., 2023).
* Possibles troubles musculosquelettiques (TMS), la main, poignet, dos, nuque (David & al., 2021).

---
# :two: Écrans VS développement neurologique et socio-relationnel de l’enfant et de l’adolescent.
# 
**Des effets négatifs ou positifs, variables selon l'âge et les usages.**

:warning: Très dépendant des inégalités sociales. 

Grands programmes de recherche : ELFE, ABCD, EDEN...


---
### 2.1 Écrans et développement cérébral 0 - 5 ans
Technoférence :
* du côté du parent : altération de la sensibilité, de l’étayage, de la disponibilité et de la réactivité de la réponse parentale.
* chez l’enfant : altération du développement du langage, de la régulation des émotions et des compétences socio-relationnelles.(Braune‐Krickau & al., 2021)(Corkin & al., 2021)

**:warning: Temps d’écran limité et l’âge de première exposition tardif :arrow_right: meilleures compétences langagières. (Madigan & al.,2020)(Massaroni & al., 2023)**

#### :arrow_right: Pas d'écran avant 3 ans!

---
### 2.2 Écrans et développement cérébral plus de 6 ans
* Pour certaines études : Écrans = possible baisse des performances en lecture et numératie (Supper & al. , 2021) (Mundy & al., 2020) chez les 6-9 ans.
* Chez les 9-17 ans : Temps d’écrans récréatif supérieur à 2H/j :arrow_right: moindres performances cognitives globales et moins bonnes performances scolaires.(Marciano & Camerini, 2021)(Howie & al., 2020)(Ramer & al.,2022) **à confimer**.
* Mais aucun effets sur le développement cérébral programme ABCD (Miller & al., 2023) voire des effets positifs reconnus programme ELFE (Fischer, 2023)

**:arrow_right: Le milieu social d’origine est la variable la plus explicative des différences observées dans le domaine cognitif.**

---
### 2.3 Écrans et capacités attentionnelles 
* Une exposition prolongée aux écrans des enfants de moins de 12 ans peut être associée à de moindres capacités attentionnelles (Santos & al., 2022)
* Chez les 15-16 ans, le « media multitasking » est un facteur important de perturbations des processus attentionnels et de la mémorisation (Madore & al., 2020)
* Effets positifs, faibles à modérés, associés à la pratique du jeu vidéo d’action (Bediou & al., 2023)

---
### 2.4 Écrans et troubles du neurodéveloppement 
###
### :warning: Les écrans ne sont pas à l’origine du TSA et du TDA/H
###
:arrow_right: Une vigilance est requise par rapport à l'usage excessif d'écrans pour éviter l’amplification des symptômes liés à ces troubles du neurodéveloppement.
(Beyens & al., 2018)(Bioulac, Charroud, Pellencq, 2022)(Lin & al., 2022)(Ophir & al. ,2023).

---
### 2.5 Résumé des effets présumés des écrans chez l’enfant et l’adolescent
*Extrait du rapport "Enfants et écrans À la recherche du temps perdu" 2024*
<!-- _class: t-90 -->

* avant 2 ans, exposition aux écrans = moins bonnes performances au niveau du langage et des capacités attentionnelles.

* de 2 à 6 ans, un temps d’écran supérieur à une heure par jour ou de télévision supérieur à 30 minutes par jour est souvent associé à de moins bonnes performances cognitives globales, attentionnelles, langagières et socio-émotionnelles.

* de 6 à 17 ans : un temps d’écran supérieur à deux heures par jour **pourrait** être associé pour certains usages à de moindres capacités attentionnelles et à de moindres performances en lecture et scolaires, **mais cela reste à confirmer.**

* entre 15 et 18 ans : un usage à haute fréquence du smartphone (plusieurs fois par jour) a été associé à une augmentation des symptômes de type inattention, impulsivité et hyperactivité.


---
# :three: Écrans et santé mentale
Dépression, anxiété, addictions... en progression chez les ados en France.
* Les études scientifiques manquent aujourd’hui pour établir un lien de causalité entre les usages du numérique et le bien-être mental des jeunes.
* Effets contrastés des réseaux sociaux.
* Le bien-être mental est toujours multifactoriel et dépend de facteurs individuels, familiaux et environnementaux.

---
### 3.1 Quelques précisisons... pour ajouter à la confusion
* Chez les 8-10 ans (Sauce & al., 2022) : 
    * Réseaux sociaux = effets nuls  
* Chez les 10-12 ans (Flannery & al., 2024)
    * Réseaux sociaux néfastes si vulnérabilité neuropsychologique préexistante
    * :arrow_right: Symptômes dépressifs chez les filles mais pas chez les garçons

___
### 3.2 Écrans et santé mentale - recommandations

La commision d'expert (Rapport "Enfants et écrans À la recherche du temps perdu", 2024) considère que les éléments sont suffisants pour indiquer qu’une consommation excessive des réseaux sociaux constitue un facteur aggravant de risque pour les jeunes présentant des vulnérabilités :arrow_right: à suivre.

---
# :four: Numérique et santé, conlusion

* Risques somatiques avérés
* Troubles du développement neurologique avérés chez les enfants en âge pré-scolaire, pas de concensus pour les écoliers et ados.
* Troubles avérés de l'attention chez les ados.
* Santé mentale à surveiller.

Mais aussi des effets positifs (Bediou & al., 2023),(Fischer, 2023).... à surveiller.

---
# Références 

<!-- _class: t-50 -->

Abdallah, M. A. E., & Harrad, S. (2018). Dermal contact with furniture fabrics is a significant pathway of human exposure to brominated flame retardants. Environment international, 118, 26-33.
Arns, M., Kooij, J. S., & Coogan, A. N. (2021). Identification and management of circadian rhythm sleep disorders as a transdiagnostic feature in child and adolescent psychiatry. Journal of the American Academy of Child & Adolescent Psychiatry, 60(9), 1085-1095.
Bediou, B., Rodgers, M. A., Tipton, E., Mayer, R. E., Green, C. S., & Bavelier, D. (2023). Effects of action video game play on cognitive skills: A meta-analysis.
Bellissimo, N., Pencharz, P. B., Thomas, S. G., & Anderson, G. H. (2007). Effect of television viewing at mealtime on food intake after a glucose preload in boys. Pediatric research, 61(6), 745-749.
Beyens, I., Valkenburg, P. M., & Piotrowski, J. T. (2018). Screen media use and ADHD-related behaviors: Four decades of research. Proceedings of the National Academy of Sciences, 115(40), 9875-9881.
Bioulac, S., Charroud, C., Pellencq, C. (2022). Le numérique face aux troubles du déficit de l’attention et à l’Hyperactivité. Consulté à https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/cours_TDA-H.html
Boyland, E. J., Nolan, S., Kelly, B., Tudur-Smith, C., Jones, A., Halford, J. C., & Robinson, E. (2016). Advertising as a cue to consume: a systematic review and meta-analysis of the effects of acute exposure to unhealthy food and nonalcoholic beverage advertising on intake in children and adults. The American journal of clinical nutrition, 103(2), 519-533.
Braune‐Krickau, K., Schneebeli, L., Pehlke‐Milde, J., Gemperle, M., Koch, R., & von Wyl, A. (2021). Smartphones in the nursery: Parental smartphone use and parental sensitivity and responsiveness within parent–child interaction in early childhood (0–5 years): A scoping review. Infant Mental Health Journal, 42(2), 161-175.
Bioulac, S., Charroud, C., Pellencq, C. (2022). Le numérique face aux troubles du déficit de l’attention et à l’Hyperactivité. Consulté à https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/cours_TDA-H.html
Cao, K., Wan, Y., Yusufu, M., & Wang, N. (2020). Significance of outdoor time for myopia prevention: a systematic review and meta-analysis based on randomized controlled trials. Ophthalmic research, 63(2), 97-105.
Charroud, C., & Choucroune, P. (2016). Numérique, wifi, téléphone, les ondes à l’école. Consulté à https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/cours_Ondes.html .
Corkin, M. T., Henderson, A. M., Peterson, E. R., Kennedy-Costantini, S., Sharplin, H. S., & Morrison, S. (2021). Associations between technoference, quality of parent-infant interactions, and infants’ vocabulary development. Infant Behavior and Development, 64, 101611.

---

<!-- _class: t-50 -->

Courbet, D., & Fourquet-Courbet, M. P. (2019). Usages des écrans, surpoids et obésité. Obésité, 14(3), 131-138.
Dauvilliers, Y. (2019). Les troubles du sommeil. Elsevier Health Sciences.
David, D., Giannini, C., Chiarelli, F., & Mohn, A. (2021). Text neck syndrome in children and adolescents. International journal of environmental research and public health, 18(4), 1565.
De Vasconcelos, A. P., Andreeva, V., Boarini, S., Bourdieu, A., Burkhardt, J. M., Chaumet-Riffaud, P., ... & Roth-Delgado, O. (2023). Avis de l'Anses relatif aux lignes directrices visant à limiter l’exposition des personnes aux champs électromagnétiques (100 kHz–300 GHz) (Doctoral dissertation, Anses).
Duclos, M. (2021). Épidémiologie et effets sur la morbi-mortalité de l’activité physique et de la sédentarité dans la population générale. Revue du Rhumatisme Monographies, 88(3), 177-182.
Fischer, J. P. (2023). L’utilisation précoce des écrans est-elle néfaste? Une première réponse avec la cohorte Elfe. Psychologie Française, 68(1), 55-70.
Flannery, J. S., Burnell, K., Kwon, S. J., Jorgensen, N. A., Prinstein, M. J., Lindquist, K. A., & Telzer, E. H. (2024). Developmental changes in brain function linked with addiction-like social media use two years later. Social Cognitive and Affective Neuroscience, 19(1), nsae008.
Foreman, J., Salim, A. T., Praveen, A., Fonseka, D., Ting, D. S. W., He, M. G., ... & Dirani, M. (2021). Association between digital smart device use and myopia: a systematic review and meta-analysis. The Lancet Digital Health, 3(12), e806-e818.
Grzybowski, A., Kanclerz, P., Tsubota, K., Lanca, C., & Saw, S. M. (2020). A review on the epidemiology of myopia in school children worldwide. BMC ophthalmology, 20, 1-11.
Haarman, A. E., Enthoven, C. A., Tideman, J. W. L., Tedja, M. S., Verhoeven, V. J., & Klaver, C. C. (2020). The complications of myopia: a review and meta-analysis. Investigative ophthalmology & visual science, 61(4), 49-49.
Hirshkowitz M, Whiton K, Albert SM, et al. National Sleep Foundation's sleep time duration recommendations: methodology and results summary. Sleep Health. 2015 Mar;1(1):40-43.
Howie, E. K., Joosten, J., Harris, C. J., & Straker, L. M. (2020). Associations between meeting sleep, physical activity or screen time behaviour guidelines and academic performance in Australian school children. BMC public health, 20, 1-10.
Jones-Jordan, L. A., Sinnott, L. T., Cotter, S. A., Kleinstein, R. N., Manny, R. E., Mutti, D. O., ... & Zadnik, K. (2012). Time outdoors, visual activity, and myopia progression in juvenile-onset myopes. Investigative ophthalmology & visual science, 53(11), 7169-7175.
Lanningham-Foster, L., Jensen, T. B., Foster, R. C., Redmond, A. B., Walker, B. A., Heinz, D., & Levine, J. A. (2006). Energy expenditure of sedentary screen time compared with active screen time for children. Pediatrics, 118(6), e1831-e1835.

---

<!-- _class: t-50 -->

Li, W., Liu, Q., Deng, X., Chen, Y., Liu, S., & Story, M. (2017). Association between obesity and puberty timing: a systematic review and meta-analysis. International journal of environmental research and public health, 14(10), 1266.
Lin, Y. J., Chiu, Y. N., Wu, Y. Y., Tsai, W. C., & Gau, S. S. F. (2022). Developmental changes of autistic symptoms, ADHD symptoms, and attentional performance in children and adolescents with autism spectrum disorder. Journal of autism and developmental disorders, 1-15.
Madigan, S., McArthur, B. A., Anhorn, C., Eirich, R., & Christakis, D. A. (2020). Associations between screen use and child language skills: a systematic review and meta-analysis. JAMA pediatrics, 174(7), 665-675.
Madore, K. P., Khazenzon, A. M., Backes, C. W., Jiang, J., Uncapher, M. R., Norcia, A. M., & Wagner, A. D. (2020). Memory failure predicted by attention lapsing and media multitasking. Nature, 587(7832), 87-91.
Marciano, L., & Camerini, A. L. (2021). Recommendations on screen time, sleep and physical activity: associations with academic achievement in Swiss adolescents. Public health, 198, 211-217.
Massaroni, V., Delle Donne, V., Marra, C., Arcangeli, V., & Chieffo, D. P. R. (2023). The Relationship between Language and Technology: How Screen Time Affects Language Development in Early Life—A Systematic Review. Brain Sciences, 14(1), 27.
Matamoros, E., Ingrand, P., Pelen, F., Bentaleb, Y., Weber, M., Korobelnik, J. F., ... & Leveziel, N. (2015). Prevalence of myopia in France: a cross-sectional analysis. Medicine, 94(45), e1976.
Miller, J., Mills, K. L., Vuorre, M., Orben, A., & Przybylski, A. K. (2023). Impact of digital screen media activity on functional brain organization in late childhood: evidence from the ABCD study. cortex, 169, 290-308.
Mounier-Vehier, C., Nasserdine, P., & Madika, A. L. (2019). Stratification du risque cardiovasculaire de la femme: optimiser les prises en charge. La Presse Médicale, 48(11), 1249-1256.
Mundy, L. K., Canterford, L., Hoq, M., Olds, T., Moreno-Betancur, M., Sawyer, S., ... & Patton, G. C. (2020). Electronic media use and academic performance in late childhood: A longitudinal study. PLoS One, 15(9), e0237908.
O'Donnell, M. J., Chin, S. L., Rangarajan, S., Xavier, D., Liu, L., Zhang, H., ... & Yusuf, S. (2016). Global and regional effects of potentially modifiable risk factors associated with acute stroke in 32 countries (INTERSTROKE): a case-control study. The lancet, 388(10046), 761-775.
Ophir, Y., Rosenberg, H., Tikochinski, R., Dalyot, S., & Lipshits-Braziler, Y. (2023). Screen Time and Autism Spectrum Disorder: A Systematic Review and Meta-Analysis. JAMA Network Open, 6(12), e2346775-e2346775.
Pedersen, J., Rasmussen, M. G. B., Sørensen, S. O., Mortensen, S. R., Olesen, L. G., Brønd, J. C., ... & Grøntved, A. (2022). Effects of limiting recreational screen media use on physical activity and sleep in families with children: a cluster randomized clinical trial. JAMA pediatrics, 176(8), 741-749.

---

<!-- _class: t-50 -->

Ramer, J. D., Santiago-Rodríguez, M. E., Vukits, A. J., & Bustamante, E. E. (2022). The convergent effects of primary school physical activity, sleep, and recreational screen time on cognition and academic performance in grade 9. Frontiers in Human Neuroscience, 16, 1017598.
Santos, R. M. S., Mendes, C. G., Marques Miranda, D., & Romano-Silva, M. A. (2022). The association between screen time and attention in children: a systematic review. Developmental neuropsychology, 47(4), 175-192.
Sauce, B., Liebherr, M., Judd, N., & Klingberg, T. (2022). The impact of digital media on children’s intelligence while controlling for genetic differences in cognition and socioeconomic background. Scientific reports, 12(1), 7720.
Sheppard, A. L., & Wolffsohn, J. S. (2018). Digital eye strain: prevalence, measurement and amelioration. BMJ open ophthalmology, 3(1), e000146.
Supper, W., Guay, F., & Talbot, D. (2021). The relation between television viewing time and reading achievement in elementary school children: A test of substitution and inhibition hypotheses. Frontiers in Psychology, 12, 580763.
Tansel, B. (2022). PFAS use in electronic products and exposure risks during handling and processing of e-waste: A review. Journal of Environmental Management, 316, 115291.
Wang, J., Lou, Y., Mo, K., Zheng, X., & Zheng, Q. (2023). Occurrence of hexabromocyclododecanes (HBCDs) and tetrabromobisphenol A (TBBPA) in indoor dust from different microenvironments: levels, profiles, and human exposure. Environmental Geochemistry and Health, 45(8), 6043-6052.
Wu, P. C., Tsai, C. L., Wu, H. L., Yang, Y. H., & Kuo, H. K. (2013). Outdoor activity during class recess reduces myopia onset and progression in school children. Ophthalmology, 120(5), 1080-1085.






