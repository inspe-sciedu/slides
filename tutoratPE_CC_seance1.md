---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# 
# Tutorat M2 PE
## Premier contact


<!-- page_number: true -->
<!-- footer:  christophe.charroud@univ-grenoble-alpes.fr – 2023 - CC:BY-NC-SA  -->



---
# :one: Tour de table 
Présentez-vous : nom, prénom, niveau de classe, école, contexte de l'école, et si vous le souhaitez vous pouvez partager quelques ressentis (positifs et/ou négatifs)

---
# :two: Faire vs Apprendre
### Un questionnement légitime mais difficile.
Lors d'une activité il est facile d'observer et décrire ce qu'on fait les élèves mais il est beaucoup plus difficile de constater ce qu'ils ont appris...


---
# Alors comment faire ?
<!-- _class: t-90 -->
* Ne pas s'arreter à la granularité des programmes
* Un objectif clair pour l'enseignant et pour l'élève 
    * Des consignes simples et rémanentes 
* Des critères et des indicateurs de réussite identifiés et identifiables
    * "Contrat" avec l'apprenant, feed-back, accompagnement
- Une progression 
    * Trâce des progrès, portfolio ?
- Méta-cognition

---

# Discussion


---
# :three: Le cas des élèves perturbateurs

> Extrait de :
> "Réguler efficacement les comportements des élèves" de **Franck Ramus**, CNRS, Département d’Etudes Cognitives, Ecole Normale Supérieure, Université PSL, Paris

---
## 1. Comprendre les causes

![](images/img-effets/modeleacc.png)

---
## 2. Antécédents et conséquences : comprendre ce qui déclenche et renforce un comportement perturbateur
<!-- _class: t-90 -->
 Les antécédents :
 * Les instructions données par l’enseignant, le comportement de celui-ci, l’intonation de sa voix, s’il sourit ou pas. 
 * Un événement extérieur, une insulte, des moqueries, des provocations, une injustice subie ou perçue, 
 * Le fait pour l’élève de se voir refuser quelque chose, le stress, une sensation externe ou interne à l’élève. 
 
 Les conséquences (ce qui se passe après le comportement) 
  * Le retour d’information de l’enseignant sur ce que fait l’élève.
  *  Une conséquence peut être positive ou négative.


---
## Récompense ou punition face à un élève perturbateur ?
**Récompense** : toute conséquence qui augmente la probabilité du comportement. Les récompenses typiques sont l’obtention de nourriture, de bien matériel, d’argent, les récompenses « sociales » (le rire, le toucher, l’attention, l’affection), les privilèges (avoir le droit de), le fait de faire cesser des stimuli aversifs.

Une **punition** est a contrario toute conséquence qui diminue la probabilité du comportement : la douleur, la faim, la soif, la perte ou privation d’un bien ou d’un privilège, la désapprobation, le reproche, la réprimande, l’ostracisme…

---
## La règle du 5 pour 1
(Gottman, 1993)(Hart & Risley, 1995)
#### Cinq fois plus de commentaires positifs que négatifs

*Il ne s’agit pas d’être des bisounours, et il ne s’agit pas d’un nombre magique, prévient le chercheur. C’est une règle essentiellement empirique qui permet de donner l’intuition de ce qu’on veut viser. L ‘idée est qu’on a tous besoin d’information sur notre performance, de positif et de négatif. L’effet du négatif peut être dévastateur et il faut du positif en face pour maintenir la motivation. Dans les classes l’environnement est principalement répressif. On est souvent très loin de ce ratio de 5 pour 1.*

---
## En quoi les punitions peuvent-elles se révéler délétères voire contre-productives ?
Elles engendrent de la souffrance et des émotions négatives mais aussi des comportements indésirables (la volonté d’y échapper, l’agression), elles peuvent susciter une aversion acquise pour l’enseignant et pour l’école.

Et surtout :
Quand quelque chose de positif arrive, on s’en attribue le mérite, quand quelque chose de négatif survient, c’est la faute des autres.
> :arrow_right: L’élève pense que s’il est puni, c’est la faute de l’enseignant, pas la sienne. La vertu pédagogique est quasi nulle.

---
## Identifier et renforcer le comportement positif opposé au comportement perturbateur
L'enseignant doit chercher à identifier le comportement positif opposé qu’on voudrait que l’élève adopte. Il doit expliciter ce comportement :
* décomposer le comportement
* modeler : l’enfant possède des bribes du comportement opposé positif, on récompense ces ébauches, on tire vers le haut en ayant commencé au niveau où se situe l’élève
* simuler : on guide le comportement de l’élève

:warning: Il faut savoir se satisfaire de réalisations imparfaites, sans vouloir aller trop vite

---
## Agir sur les antécédents du comportement négatif
* La politesse (dire « s’il te plaît »)
* Ton aimable, calme, sourire
* Lorsque le démarrage est difficile, aider à commencer plutôt qu’ordonner
* Etre clair et spécifique sur la nature du comportement demandé
* Etre le plus proche possible du comportement souhaité
* Etre physiquement proche 
* Offrir des choix (ne serait-ce qu’apparents)
* Faire précéder les demandes à faible probabilité par des demandes à haute probabilité
* Lancer des défis

---
## Les récompenses : très efficaces à certaines conditions


* Pas besoin de récompenses coûteuses (une récompense matérielle peu chère, un privilège, les activités, les jeux, la récréation, un système de points, des jetons). 

* Il est préférable de donner de petites récompenses fréquentes et immédiates que de grandes récompenses rares et lointaines

* Lorsque le comportement positif est bien installé, alors il faut moduler la fréquence du renforcement et à un moment, passer en mode atténuation et/ou on peut augmenter progressivement les exigences. :arrow_right: Flexibilité, adaptation.

---
## Apprendre à ne pas renforcer les comportements négatifs ou l’art délicat de l’extinction

**Exctinction** = Priver le comportement négatif de son renforcement positif, s’abstenir de donner du renforcement positif.

* S’abstenir de réagir :warning: c'est très difficile.

Au début de la mise en pratique de l’extinction, on voit généralement apparaître une recrudescence du comportement indésirable.

---
## À retenir

Pour diminuer un comportement indésirable, il est nécessaire de :
* Identifier les antécédents et les conséquences qui ont renforcé ce comportement, les réduire et les éliminer.
* Identifier et expliciter les comportements opposés positifs, les récompenser.
* Utiliser les antécédents positifs pour augmenter la probabilité des comportements désirés (récompenses).

---

# :four: Vos questions...
