---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# 
# Effets du numérique sur les apprentissages
## TD
 
Christophe Charroud - UGA
 
<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2023 - 2024 - CC:BY-NC-SA -->

---
# :zero: Objectifs
### Objectif général de la formation
Concevoir une situation d'apprentissage intégrant le numérique réaliste et réalisable dans votre classe :arrow_right: Objectif mise en oeuvre.
### Objectifs du TD
* Mettre en pratique les concepts théoriques sur les thématiques
    * Communiquer avec le numérique
    * Entrainement sur des tâches ciblées :arrow_right: Evaluer et s'auto-évaluer avec le numérique
 
---

# :one: Ce que l'on sait
Rappels des apports théoriques

## L’utilisation du numérique ***peut*** avoir un effet positif sur l’apprentissage

- **Pour accéder à des informations**

- **Pour communiquer**

- **Pour s'entraîner sur des tâches ciblées**

> (J-PAL, 2019)

---
#### Il est indispensable d'analyser ses pratiques au regard des résultats de la recherche si l'on veut éviter les problèmes....
# 
###  :warning: Les effets que l'on pense positifs peuvent :warning: rapidement se révéler négatifs :

Outre une perte de temps, d'énergie et de crédibilité: le risque principal est de générer une **augmentation de la différence entre les élèves**
(Zheng, 2016)



---
# :two: Communiquer avec le numérique

### Des points positifs :
* Sources variées
* Attention

### Des points négatifs 
* Sources variées
* Attention

# :rage:



---
### :exclamation: À retenir :exclamation:

* Des ressources épurées.
* Des vidéos contrôlables par l'élève, avec des pauses.
* Ne pas oublier que la perception de l'élève est différente de celle de l'adulte (neurones miroirs).
* Et enfin il ne faut pas que l'interface soit un obstacle aux apprentissages

---

### :pencil2: TP : durée 30 minutes
###  Vos documents numériques sont-ils efficaces?
* Regroupez vous par cycle ou niveau de classe en essayant de faire des groupes de 4 ou 5 max.
* Choisissez un ou des documents numériques que vous utilisez en classe et faites en une analyse critique.
* De cette analyse, préparez une présentation de 3 minutes que vous présenterez au reste du groupe TD



---
# :three: S'entrainer sur des tâches ciblées 
#
#### :arrow_right: Évaluer et s'auto-évaluer avec le numérique

---
## Évaluer et s'auto-évaluer (avec ou sans numérique).
### Ce que l'on sait.

---
## Évaluer - Une définition

L’évaluation en éducation est le processus par lequel **on délimite, obtient, et fournit des informations utiles permettant de juger des décisions possibles”**. ([Stufflebeam et al., 1980] p. 48)

:arrow_right: Évaluer ce n'est pas nécessairement donner une note


---
## Types d'évaluation (1/2)

Les types d'évaluation varient (doivent varier) en fonction :
* du moment de l'apprentissage (au début, pendant, à la fin, plus tard...)
* du support (écrit, oral, physique, comportemental...)
* des processus cognitifs scrutés (savoir, réflexion, construction d'un raisonnement...)
* du contexte (individuellement, en groupe, en stage...)
* des décisions à prendre (positionnement, régulation, certification, orientation...)

---
## Types d'évaluation (2/2)

”Lorsque le cuisinier goûte la soupe, c'est formatif ; lorsque les invités goûtent la soupe, c'est sommatif” (R. Stake cité par Scriven 1991 p. 169)

- *évaluation POUR l'apprentissage* (formative), **évaluer en jugeant**, où la qualité de la production est *directement* évaluée par l'élève, aidé par l'enseignant

- *évaluation DE l'apprentissage* (sommative), **évaluer en testant**, où des inférences sont faites entre l'évaluation de réponse à un test et les compétences de l'élève (on goûte une soupe pour évaluer la qualité du cuisinier)

:warning: Il ne s’agit pas de dire que l’une de ces deux est meilleure que l’autre, les deux sont nécessaires.



---
# 
## Évaluer pour l'apprentissage : les rétroactions 
## :warning: Très important pour les apprentissages

Trois types de rétroactions, informantes pour l'élève (Hattie & Timperley 2007 ; voir aussi Lepareur 2016)

- Où est-ce que je vais ? (quels sont les buts ?)
- Comment est-ce que j'y vais ? (quels sont les progrès que j'ai faits vers ces buts ?)
- Que faire ensuite ? (quelles tâches dois-je réaliser pour mieux y arriver ?)

---
# 
## Rappel sur les rétroactions pour l'appentissage
* Nécessité de feedback
* Feedback, si possible, immédiat :arrow_right: biais de mémoire
* Si la réponse comporte une erreur :arrow_right: remédiation
* Si la réponse est correcte (tant que la connaissance n’est pas parfaite, le cerveau continue d’apprendre) => nécessité de surapprentissage (réviser, remettre à l’épreuve…)

(Dehaene, 2018)


---
# 
## Évaluer avec le numérique, c'est mieux ?



---
## Évaluer, une tâche chronophage

![](images/img-effets/evaluation_bryant.jpg)

Total en moyenne 49,5h, évaluation env. 15%

---
## Et avec le numérique ?
Le numérique permet de réallouer 20-30 % du temps hebdomadaire sur un total en moyenne 49,5h. (Bryant et al. 2020) :
* évaluation/rétroactions : gain 3 h



### C'est déjà un avantage, il y a en d'autres selon les utilisations ...
## :warning: Mais il faut toujours s'interroger sur l'évaluation avec le numérique


---
## Numérique vs. papier-crayon ? :thumbsup:

- peut recueillir des **processus** (durées, hésitations, etc.) analysables par la suite
- traitement **automatique**, donc moins de "fatigue évaluative" côté enseignant, plus d'entraînement côté élèves
- attribue des niveaux de manière plus **fiable et rapide**, reposant sur des statistiques
- peut proposer des **rétroactions** (semi-)automatiquement juste après la production
- offre des **visualisations synthétiques** pour suivre les progrès des élèves
- s'adapte aisément **au nombre** (2 fois plus d'évaluations < 2 fois plus de travail)
- peut être considéré comme **moins sujet aux stéréotypes** ou aux effets d'ancrage

---
## Numérique vs. papier-crayon   :thumbsdown:

- données plus aisément **piratables, modifiables**
- difficile de ne recourir qu'au libre et gratuit, donc **aspiration de données personnelles** difficile à contourner
- **plus de données** ≠ meilleure évaluation 

---
# 
## :warning: Attention aux fausses bonnes idées !

- fausse impression de **personnalisation** : les parcours automatisés peuvent enfermer l'élève dans des "profils"
- fausse impression d'**objectivité** : si on scrute les mauvaises choses, on obtient une mauvaise évaluation
- fausse impression d'**exhaustivité** : avoir beaucoup de données sur l'apprentissage nécessite beaucoup de temps pour l'analyser

---
# 
# :four: Pratique

---
#  :pencil2: TP : Entrainement - Évaluation numérique 

Par groupe, vous allez essayer de déterminer et analyser **une forme** d'entrainement avec évaluation ou auto-évaluation **numérique** réaliste que vous utilisez ou que vous pourriez utiliser dans vos classes.


**Production attendue :** une présentation de 3 à 5 minutes par groupe.

Temps de préparation : 45 minutes maximum

---
## Contenu de la présentation :
* Description de la situation, de l'entrainement, de l'évaluation ou de l'auto-évaluation envisagée (niveau, discipline, contexte), type d'évaluation (positionnement, régulation, certification...)
* Compétences et connaissances **précises** qui seront évaluées.
:arrow_right: Processus cognitifs scrutés (savoir, réflexion, construction d'un raisonnement...)
* Outil(s) numérique(s) utilisé(s)
* Quelles rétroactions (feed-back), vers l'apprenant, l'enseignant, l'administration ?
* Quelles traces le numérique peut-il vous fournir ?
* Quelles différences avec une évaluation non numérique ? (bénéfices/contraines)


##### :warning: Vous êtes en formation = les erreurs et approximations sont normales, pas de stress !

---


# Présentations


---

# 
# :five: Évaluer avec le numérique
# OU
# Évaluer au numérique
  
---
# Évaluer au numérique
Le numérique, des compétences à évaluer
* Un cadre de référence : [CRCN](https://eduscol.education.fr/721/evaluer-et-certifier-les-competences-numeriques)
* S’applique à tous les niveaux de l’école
* :warning: Aucun niveau exigé par l'institution mais scruté par certains employeurs
* Un outil d’évaluation national : [PIX](https://pix.fr)
* Auto-évaluation des élèves
* Auto-formation (tutoriels)
* Certification d’un niveau d’acquisition : 3ème, Terminale, Université.




