---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# Culture numérique et apprentissages
## TD 2
### DIU SD
 
Christophe Charroud - UGA
 
<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2023 - 2024 - CC:BY-NC-SA -->


---
## "Mini" conférence sommeil et apprentissages
Au choix :
* **jeudi 7 mars**, 12h30-13h30 Amphi INSPE
* **Mercredi 13 mars** 12h30-13h30 Amphi INSPE
* **Mardi 26 mars** 12h30-13h30 Amphi INSPE

Intéressé :arrow_right: inscrivez vous ![140%](images/img-effets/qrsommeil.png)
https://link.infini.fr/sommeil


---
# :zero: Rappels
* TD 1 : Mise en application des apports théoriques sur les effets du numériques sur les apprentissages - Début de la conception :arrow_right: avoir une première proposition de situation d'apprentissage.
* **TD 2 : Apports pratiques sur la formation AU numérique et l'évaluation des élèves en référence au CRCN - découverte de PIX et PIX+EDU - Poursuite de la conception**.
* TD 3 : Mise en application des apports théoriques sur les ressources et les aspects juridiques et éthiques - Finalisation de la conception.
* TP : Mise en commun des conceptions et retour d'expérience pour celles et ceux qui auront réalisé une mise en oeuvre. :arrow_right: PIX+EDU.

---
#
### Former avec le numérique
###          VS
###   Former au numérique



---
# :one: Former et évaluer au numérique
Le numérique, des compétences à développer :arrow_right: à évaluer
* Un cadre de référence : [CRCN](https://eduscol.education.fr/721/evaluer-et-certifier-les-competences-numeriques)
* S’applique à tous les niveaux de l’école
* :warning: Aucun niveau exigé par l'institution mais scruté par certains employeurs
* Un outil d’évaluation national : [PIX](https://pix.fr)
* Auto-évaluation des élèves
* Auto-formation (tutoriels)
* Certification d’un niveau d’acquisition : 3ème, Terminale, Université.

---
# 
## PIX : Évaluation et certification de compétences d'usages des technologies numérique.
#

### Allez sur le site pix.fr

---
# Évaluer au numérique : PIX
## :pencil2: TP - Découverte de PIX 
* Si vous n'en n'avez pas encore, créez-vous un compte sur pix.fr :warning: si possible avec une adresse @ac-grenoble.fr
#
***Référentiel de formation : CRCN***
**:arrow_right: Public cible : élèves (école-collège-lycée), et étudiants 1er cycle (licence)**

---
# PIX+EDU
## :pencil2: TP - Découverte de PIX+EDU (durée 30 minutes)

* Connectez vous à la campagne PIX+EDU "les essentiels" :warning: Campagne de test
- Cliquez sur "j'ai un code"

![](images/img-pix_edu/code_pix.png)
* Code de connexion: **VNJRDG813**

* Effectuez quelques tests
#
***Référentiel de formation : CRCNé***
**:arrow_right: Public cible : étudiants MEEF, stagiaires, enseignants en poste**

---
# :two: Conception d'une situation d'apprentissage intégrant le numérique 
#### Production attendue
Un document rédigé en binôme de 4 pages maximum comportant :
* un descriptif de la situation d’apprentissage avec obligatoirement des liens pointants sur les éléments de conception (liens vers les ressouces, copies d’écran des interfaces élèves et/ou enseignants, …),
* une argumentation sur les choix opérés lors de la conception, notamment au regard des apports théoriques,
* un bilan personnel.


---
## 2.1 Recherche de situation 

* L'idée de la situation d'apprentissage peut provenir d'une situation **vécue, observée ou rapportée**. 
* Elle doit intégrer au moins une ressource numérique, ou hybride.
* Elle peut être commune à plusieurs personnes mais les analyses devront prendre en compte le contexte de mise en œuvre.

---
## 2.2 Description de la situation
#### Eléments attendus dans la description

```
- Domaine(s) d’enseignement concerné(s) par la situation
- Niveau de classe
- Période de l'année, ou place dans une séquence, où la situation a été (ou serait) mise en œuvre.
- Objectif(s) d’apprentissages : 
	- Compétences disciplinaires visées en termes de « être capable de…», (ces compétences sont issues des programmes)
	- Compétences numériques développées (issues du CRCN)
	
- Environnement numérique impliqué dans la situation :
    - Outils physiques (TBI, tablettes, ordinateurs fixes ou portables, caméras, enregistreurs, ...)
    - Organisation matérielle (nb de matériels par élève/classe, salle informatique ou salle de classe)
    - Logiciels ou applications utilisés.
    - Ressources utilisées, ou produites

-  Organisation pédagogique : quelles activités, quand, comment, ...
    
```

---
## 2.3 Analyse et justification des choix

- Justification des choix d'outils et de ressources numériques, en s'appuyant notamment sur le modèle SAMR.
- Analyse des potentiels effets positifs ou néfastes pour les apprentissages et les apprenants (TD 1).
- Analyse des problèmes juridiques (droits d'auteurs, droit à la vie privée) et éthiques soulevés.


:warning: Les analyses et justifications doivent tenir compte du contexte.



---
# En avant !
* Essayez d'identifier un apprentissages pour lequel le numérique peut apporter une plus-value réelle.
* Restez réaliste :arrow_right: Mise en oeuvre conseillée. 
