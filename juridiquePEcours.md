---
marp: true
paginate: true
---

![10%](images/logo_UGA_couleur_cmjn.jpg)

# Thématique « Numérique et juridique »

M2 et DIU MEEF 1er degré


<!-- 
footer: Laurence Osete - INSPE-UGA - 2022 - CC:BY-NC-SA -->

<!--
page_number: true -->

---
# Rôles de l'enseignant

![](images/img-juridique/roles.png)

---
# Compétences travaillées

- Capacité à **comprendre le contexte** dans lequel il évolue et à utiliser ses connaissances, recherches, réflexions à propos de la législation et des règlementations institutionnelles liés aux TICE **pour une mise en œuvre raisonnée et responsable** dans le cadre de son activité professionnelle.

---
# Les principaux sujets abordés

1. La sécurité des élèves
2. Les droits des auteurs
3. Le droit à la vie privée
4. Le principe de neutralité

---
# Respect de la vie privée et numérique

## Règlementations

---
# Lois en vigueur

![](images/img-juridique/RGPD.png)

---
# Les données à caractère personnel

- Identité (identifiants, **biométrie**, intérêts, **opinions**)
- Contexte (déplacement, habitudes)
- Comportements (navigation web, consommation)
- Communication (messages, contacts, groupes) 
- **Santé (dossiers médicaux)**
- Finance (revenu, données bancaires)
- Scolarité (notes, compétences, performances, …)

---
# Les grands principes de la règlementation



La collecte et le traitement doit être :
- **licite**, loyale et transparente ;
- **collectées pour des finalités déterminées**, explicites et légitimes ;
- adéquates, pertinentes et **limitées à ce qui est nécessaire** ;
- exactes et tenues à jour ;
- conservées pendant une **durée n'excédant pas celle nécessaire** au regard des finalités pour lesquelles elles sont traitées;
- traitées de façon à **garantir une sécurité** appropriée des données 



---
# Licéité du traitement

- la personne concernée a consenti au traitement
- nécessaire à l'exécution d'un contrat
- nécessaire au respect d'une obligation
- nécessaire à la sauvegarde des intérêts vitaux de la personne
- nécessaire à l'exécution d'une mission d'intérêt public ou relevant de l'exercice de l'autorité publique 
- nécessaire aux fins des intérêts légitimes poursuivis par le responsable du traitement ou par un tiers.

---
# Consentement des enfants en ce qui concerne les services de la société de l'information

- est licite à partir de  16 ans. En dessous le consentement est donné ou autorisé par le titulaire de l'autorité parentale.
- Les États membres peuvent prévoir par la loi un âge inférieur entre 13 et 16 ans. (15 ans en France)
- Le responsable du traitement s'efforce raisonnablement de vérifier l'obtention du consentement.

---
# Responsabilités

- Le responsable du traitement est responsable du respect du RGPD et est en mesure de le démontrer (responsabilité)
- Pour cela, il doit :
	- Tenir un registre des traitements et des violations.
	- S’assurer du consentement des personnes concernées quand nécessaire.
	- Informer les personnes concernées (élèves, parents, personnels, intervenants extérieurs, …)
	- S’assurer de la sécurité des données.
- Pour les écoles primaires, le responsable est le **DASEN**


---
# Précautions à prendre

Avant d'utiliser une logiciel ou une application collectant des DCP

- Signaler la collecte au DASEN
- Informer les parents
- Leur demander l'autorisation si nécessaire
- Assurer la sécurité des données

---
# Cas particuliers des photos ou enregistrements d'élèves


- Nécessite le consentement des personnes (un des tuteurs légaux)
- Ne peut être faite pour l'année, pour tous types d'usages et de diffusion.
- Doit être explusivement positive.
- Les articles de loi doivent apparaitre ainsi que les modalités d'exercice des droits des personnes (droit d'accès, de rectification, ...)


Modèle d'autorisation : https://eduscol.education.fr/398/protection-des-donnees-personnelles-et-assistance


---
# Le Droit d'auteur

---
# Loi en vigueur

Le code de la propriété intellectuelle, composé de :
- La propriété littéraire et artistique
- La propriété industrielle : brevets, dessins et modèles, marques.

![](images/img-juridique/DefDroitDauteur.png)

---
# Les grands principes

- S'applique à toute création originale.
![80%](images/img-juridique/DroitDauteur.png)


---
# Titulaires du droit d'auteur (Articles L113-1 à L113-10)

- Personne qui créé l'œuvre et la divulgue
- Cas des œuvres créées à plusieurs :
	- tous les co-auteurs pour une œuvre collaborative 
	- la personne qui dirige la création et qui la divulgue pour une œuvre collective
- Cas particulier d'une œuvre intégrant une autre œuvre. Elle est dite composite.Elle est la propriété de l'auteur qui l'a réalisée, sous réserve des droits de l'auteur de l'œuvre préexistante 

---
# Droit d'auteur : Exceptions (Article L122-5)


Quelques exceptions pour un usage collectif,

3° sous réserve que soient indiqués clairement le nom de l'auteur et la source 
a)  Les analyses et courtes citations justifiées par le caractère critique, polémique, pédagogique, scientifique ou d'information de l’œuvre à laquelle elles sont incorporées ;

4° La parodie, le pastiche et la caricature, compte tenu des lois du genre;

11° Les reproductions et représentations d'œuvres architecturales et de sculptures, placées en permanence sur la voie publique, réalisées par des personnes physiques, à l'exclusion de tout usage à caractère commercial.

12° **"Exception pédagogique"**


---
# Droit d'auteur : Exception pédagogique

- La représentation ou la reproduction d'extraits d’œuvres, […]
- à l'exclusion de toute activité ludique ou récréative, 
- à un public composé majoritairement d'élèves, 
- compensée par une rémunération négociée.

---
# Accords encadrant les usages

- Usages numériques
	- [Bulletin Officiel N°35 du 29 septembre 2016](https://www.education.gouv.fr/bo/16/Hebdo35/MENE1600684X.htm) (livres, œuvres musicales éditées, publications périodiques et œuvres des arts visuels)
	- [Avenant au protocole (Bulletin officiel n° 7 du 13 février 2020](https://www.education.gouv.fr/bo/20/Hebdo6/MENE2000032X.htm?cid_bo=148987)
	- Bulletin Officiel n° 5 du 4 février 2010 ([œuvres cinématographiques et audiovisuelles](https://www.education.gouv.fr/bo/2010/05/menj0901120x.html), [œuvres musicales](https://www.education.gouv.fr/bo/2010/05/menj0901121x.html))
- Photocopies : 
	- [Bulletin officiel n°13 du 1er avril 2021](https://www.education.gouv.fr/bo/21/Hebdo13/MENE2108987C.htm)

---
# Cas particuliers

- n'utiliser que des **extraits** d'œuvres\*, sauf pour
	- Projection en classe d'une œuvre de l'écrit
	- œuvre courte (poème)
	- diffusion intégrale en classe d'une musique, ou d'une vidéo provenant d'un service non payant. 
	- œuvres des arts visuels (limité à 20 œuvres par travail, définition max 800x800 px en 72 DPI)

\* Les œuvres doivent avoir été acquises légalement

Tableau de synthèse : https://classetice.fr/v2/wp-content/uploads/2022/07/tableau_exception_peda_2016.pdf



---
# Exemple 1


![50%](images/img-juridique/joconde.png)

---
# Exemple 2


![50%](images/img-juridique/Londre.png)

---
# Précautions à prendre
- Citer l'auteur, l'éditeur (obligatoire mais pas toujours suffisant).
- Consulter les conditions d'utilisation (licences)
- Ne pas diffuser les œuvres des élèves sans autorisation.
- Utiliser si possible des ressources sous licence "libre".
- Respecter les CGU des logiciels et services.

---
# Sécutité des élèves

---
# Sécurité des élèves
- Vérifier le fonctionnement des systèmes de filtrage
- Mettre en place des règles --> Charte
- Utiliser un moteur de recherche adapté à l’âge des élèves
- Signaler les contenus inappropriés
- Signaler les incidents aux parents et à la direction de l'école.

---
# Neutralité commerciale

Document de référence : [Code de bonne conduite des interventions des entreprises en milieu scolaire](https://www.education.gouv.fr/botexte/bo010405/MENG0100585C.htm)

#### Article III.5 Le partenariat pour l'usage de produits multimédias
*"L'utilisation de produits multimédias par les établissements scolaires, à des fins d'enseignement, est libre. La consultation de sites Internet privés ou l'utilisation de cédéroms qui comportent des messages publicitaires ne sauraient être regardée comme une atteinte au principe de neutralité (9)."*

*"En revanche, la réalisation de sites Internet par les services de l'éducation nationale et les établissement scolaires est tenue au respect du principe de la neutralité commerciale."*

---
# Précautions à prendre

- ne pas promouvoir de logiciels ou services
- ne pas publier de contenus sur des services diffusant de la publicité

- *éviter les outils ou service qui utilisent les traces des utilisateurs pour faire de la publicité ciblée (lien avec les "zones à risque").*
