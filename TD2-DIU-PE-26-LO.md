---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# Culture numérique et apprentissages
## TD 2
 
Laurence Osete - UGA
 
<!-- page_number: true -->
<!-- footer: laurence.osete@univ-grenoble-alpes.fr - 2023 - 2024 - CC:BY-NC-SA -->



---
# :one: Rappels
* TD 1 :  Mise en application des apports théoriques sur les effets du numériques sur les apprentissages - Début de la conception.
* **TD 2 : Mise en application des apports théoriques sur les ressources et les aspects juridiques et éthiques - poursuite de la conception**
* TD 3 : Apports pratiques sur la formation AU numérique et l'évaluation des élèves en référence au CRCN - découverte de PIX et PIX+EDU - Poursuite de la conception.
* TP : Mise en commun des conceptions et retour d'expérience pour celles et ceux qui auront réalisé une mise en oeuvre. :arrow_right: PIX+EDU.

 
---
## Organisation du TD

1 - Etudes de cas (1h15 minutes)
2 - Conception (45 mn)

---
# :one: Mise en application par l'étude d'un cas

## Par groupe de 3 à 6 personnes (30 mn)

- Consulter le cas attribué disponible ici : https://lstu.fr/casjuridiquespe
- En vous aidant des ressources documentaires et du cas similaire corrigé, répondre à la situation problème.


## Présentations (30 mn)

- En vous appuyant sur les questions de la partie "Synthèse", réaliser une rapide présentation orale de 3 min maximum.

---
# Neutralité commerciale

Document de référence : [Code de bonne conduite des interventions des entreprises en milieu scolaire](https://www.education.gouv.fr/botexte/bo010405/MENG0100585C.htm)

#### Article III.5 Le partenariat pour l'usage de produits multimédias
*"L'utilisation de produits multimédias par les établissements scolaires, à des fins d'enseignement, est libre. La consultation de sites Internet privés ou l'utilisation de cédéroms qui comportent des messages publicitaires ne sauraient être regardée comme une atteinte au principe de neutralité (9)."*

*"En revanche, la réalisation de sites Internet par les services de l'éducation nationale et les établissement scolaires est tenue au respect du principe de la neutralité commerciale."*

---
# Précautions à prendre

* ne pas publier de contenus sur des services diffusant de la publicité.
* ne pas imposer de materiels, logiciels ou services.

Plus d'infos à [Informatique et école : vers une éducation citoyenne ?](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/droitinfosoc.html)

---
# Sécurité des élèves

* Protéger
* Secourir
* Alerter - signaler
* Éduquer

---

# Sécurité des élèves

#### Obligations :

* Système de filtrage des sites inappropriés (chef d'établissement)
* Mettre en place des règles --> Charte
* Surveiller les élèves
* Signaler les contenus inappropriés
  * https://www.internet-signalement.gouv.fr/
  * http://www.pointdecontact.net/
* Signaler les incidents aux parents et à l'administration

#### Précautions :

* Vérifier le fonctionnement des systèmes de filtrage.
* Utiliser un moteur de recherche adapté à l’âge des élèves ou des listes de sites.
* Éduquer aux dangers d'internet.



---
# :two: travail de Conception

#### Rappel de l'objectif pour l'évaluation :
Concevoir une situation d'apprentissage intégrant le numérique réaliste et réalisable dans votre classe.

#### Rappel de l'objectif de la formation (donc à la sortie de l'INSPE):
Si possible, mise en oeuvre en classe :arrow_right: PIX+EDU.

---
# :two: Recherche de situation

* La situation d'apprentissage peut provenir d'une situation **vécue, observée ou rapportée**. 
* l'idée peut être issue d'une banque de scénarios comme :
    - [la banque de l'académie de Nantes](https://www.pedagogie.ac-nantes.fr/numerique/scenarios-pedagogiques/)
    - [Prim à bord](https://primabord.eduscol.education.fr/spip.php?page=selection&mots%5B%5D=133#content)
    - ou autre
* Elle doit intégrer au moins une ressource numérique, ou hybride.
* Elle peut être commune à plusieurs personnes mais les analyses devront prendre en compte le contexte de mise en œuvre.

---
# :two: Conception de la situation

Vous devez décrire la situation telle qu'elle a été conçue, ou telle qu'elle le sera si vous aviez à la mettre en œuvre. 

#### Eléments attendus dans la description

```
- Domaine(s) d’enseignement concerné(s) par la situation
- Niveau de classe
- Période de l'année, ou place dans une séquence, où la situation a été (ou serait) mise en œuvre.
- Objectif(s) d’apprentissages : 
	- Compétences disciplinaires visées en termes de « être capable de…», (ces compétences sont issues des programmes)
	- Compétences numériques développées (issues du CRCN)
	
- Environnement numérique impliqué dans la situation :
    - Outils physiques (TBI, tablettes, ordinateurs fixes ou portables, caméras, enregistreurs, ...)
    - Organisation matérielle (nb de matériels par élève/classe, salle informatique ou salle de classe)
    - Logiciels ou applications utilisés.
    - Ressources utilisées, ou produites

-  Organisation pédagogique : quelles activités, quand, comment, ...
    
```

---
# :two: Analyse et justification des choix

- Analyse des plus-values des outils et ressources numériques, en s'appuyant notamment sur le modèle SAMR ainsi que les effets positifs prouvés par la recherche.
- Analyse des problèmes : effets négatifs, problèmes juridiques (droits d'auteurs, droit à la vie privée) et éthiques soulevés.

:warning: Les analyses doivent tenir compte du contexte.

---
#  En avant !
Conseils :
* Essayez d'identifier un apprentissages pour lequel le numérique peut apporter une plus-value.
* Restez réaliste :arrow_right: Mise en oeuvre conseillée.

