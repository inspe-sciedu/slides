---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
   
# Compétences numériques des élèves et des enseignants


Christophe Charroud - UGA
 
<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2024 - • ![CC:BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)  -->


---
#
### Former avec le numérique
###          VS
###   Former au numérique

---
# :zero: Cadre général

### [Article L312-9 du code de l'éducation 21 mai 2024.](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000049571494#:~:text=Version%20en%20vigueur%20depuis%20le%2023%20mai%202024,-Modifié%20par%20LOI&text=Elle%20contribue%20au%20développement%20de,apprentissage%20de%20la%20citoyenneté%20numérique.)

---
![w:700](images/img-effets/article.jpg)

---
![w:700](images/img-effets/articlerouge.jpg)

---
### Aspect réglementaire

**A l'issue de l'école primaire et du collège**, les élèves reçoivent **une attestation certifiant** qu'ils ont bénéficié d'une sensibilisation au bon usage des outils numériques et de l'intelligence artificielle, de tous types de contenus générés par ceux-ci et des réseaux sociaux ainsi qu'aux dérives et aux risques liés à ces outils et aux contenus générés par l'intelligence artificielle ainsi qu'à la lutte contre la désinformation.

Cette attestation est **obligatoire** pour tous les élèves à l'issue de la première année de collège et doit être renouvelée à l'issue de la dernière année de collège.


---
# :one: Compétences numériques de l'élève
Le numérique, des compétences à développer :arrow_right: à évaluer
* Un cadre de référence : [CRCN](https://eduscol.education.fr/721/evaluer-et-certifier-les-competences-numeriques)
* S’applique à tous les niveaux de l’école :arrow_right: intégré au LSU en primaire.
* :warning: Aucun niveau exigé par l'institution mais scruté par certains employeurs
* Un outil d’évaluation national pour l'école (en version de test) collège et le lycée : [PIX](https://pix.fr) 
* Auto-évaluation des élèves
* Auto-formation (tutoriels)
* Certification d’un niveau d’acquisition : 3ème, Terminale, Université. (à venir en CM2 )


---
## Compétences numériques au primaire dans le LSU (PE)

![w:500](images/img-effets/lsu1.jpg)

---

![W:500](images/img-effets/lsu2.jpg)

---

![center](images/img-effets/lsu3.jpg)

---
## Explorer le CRCN 
### :pencil2: TP - Recherche de compétences numériques atteignables par les élèves dans une situation
A partir de la situation d'apprentissage présentée via le lien sur Eformation, essayez de déterminer les compétences issues du CRCN qui pourraient-êtres atteintes par les élèves.



---
# Un outil national 

## PIX : Évaluation et certification de compétences d'usages des technologies numériques.
#

### Allez sur le site pix.fr

---
## Évaluer au numérique : PIX
### :pencil2: TP - Découverte de PIX 
* Si vous n'en n'avez pas encore, créez-vous un compte sur pix.fr :warning: si possible avec une adresse @ac-grenoble.fr
#
***Référentiel de formation : CRCN***
**:arrow_right: Public cible : élèves (école-collège-lycée), et étudiants 1er cycle (licence)**

---
## Retour d'expériences

- Qu'avez-vous pensé des questions ?
- Sur quoi portaient-elles ?
- Qu'avez-vous pensé des retours sur les réponses et les tutoriels ?
- Que pensez-vous de l'introduction de ce dispositif à l'école primaire (PE)
- Pensez-vous que ce dispositif suffit à former les élèves ?

---
# :two: Compétences numériques des enseignants 


---
## Que dit la réglementation :
**Article L312-9** du code de l’éducation du Le 23 mai 2024 :
“Afin de renforcer et de valoriser la culture numérique professionnelle des membres du personnel enseignant et d'éducation, **les membres volontaires** peuvent également bénéficier d'une attestation de leurs compétences numériques professionnelles.”


---
## Dans les faits 

Le M.E.N. souhaite que les enseignants soient certifiés à deux niveaux :

1. PIX :arrow_right: Maitrise des compétences techniques pour l'usages des outils numériques contemporains.

2. PIX+EDU :arrow_right: Maitrise des compétences métier pour l'usages du numérique en situation d'enseignement.
![w:150](images/img-effets/logo_pix_edu_small.png)

---
## PIX+EDU

Certification comportant deux volets :

1. Volet "automatisé" :arrow_right: sur la plateforme PIX.

2. Volet "pratique professionnelle" :arrow_right: sur le terrain, avec une mise en oeuvre d'une situation d'apprentissage intégrant le numérique. 



#### :warning: À ce jour, il n'y a pas de texte officiel concernant l'obligation de la certification PIX+EDU :arrow_right: à l'INSPE-UGA le dispositif PIX+EDU est mis en oeuvre très partiellement...

---
## PIX+EDU à la rentrée 2025 (prévision)

![w:600](images/img-pix_edu/pixedu.png)


---
# PIX+EDU Volet automatisé
## :pencil2: TP - Découverte de PIX+EDU

- Connectez vous à la campagne PIX+EDU "les essentiels" :warning: Campagne de test
- Cliquez sur "j'ai un code"

![](images/img-pix_edu/code_pix.png)
- Code de connexion - :warning: Premier degré : **CMGVZH838**
- Code de connexion - :warning: Second degré : **MGVYCX189**
- Effectuez quelques tests
#
***Référentiel de formation : CRCNé***
**:arrow_right: Public cible : étudiants MEEF, stagiaires, enseignants en poste**

---
# Retour d'expériences

- Qu'avez-vous pensé des questions ?
- Sur quoi portaient-elles ?
    * sur des compétences transversales  (7 compétence du CRCN)
    * sur des compétences professionnelles (12 compétences du CRCNE)
- Qu'avez-vous pensé des retours sur les réponses et les tutoriels ?

---
# :three: Production attendue pour votre document d'analyse 

>   4. Identifier les compétences numériques des apprenants pré-requises et développées.

En lien avec votre situation d'apprentissage :
* identifiez les compétences du CRCN que les élèves peuvent valider au cours de cette situation en précisant les critères et indicateurs
* si dans votre situation d'apprentissage, aucune compétence issue du CRCN ne semble mise en jeu, proposez une évolution de votre situation pour qu'au moins une le soit.

---
