---
marp: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# UE 24 Option de recherche “Interactions enseignant-élèves“
## Philippe Dessus, Inspé, Univ. Grenoble Alpes

## Séance 4 — Veille pédagogique
##### Année universitaire 2024-25


![w:300](images/logo-espe-uga.png)

---
<!-- footer: Ph. Dessus • M1 PE • TD Option de recherche “Interactions enseignant-élèves • Inspé-UGA 2024-25 • CC:BY-NC-SA-->
<!-- paginate: true -->


# :four: But et Plan

- **But** : Cette séance est dédiée à la “veille pédagogique“ sur les IEE (et plus largement sur votre thème de mémoire), c'est-à-dire aux moyens de chercher et trouver des informations sur une problématique de recherche
- **Plan**
1. Pourquoi utiliser la recherche en éducation
2. Le processus de veille
3. Une fiche de recherche

---
# :four: 1. Pourquoi utiliser la recherche en éducation ?

- Pour changer mes pratiques, avoir d'autres idées
- Pour m'informer
- Pour être plus compétent.e, me former
- Pour rompre ma routine,
- Parce que c'est une compétence professionnelle du [référentiel de compétences des métiers du professorat et de l'éducation](https://www.education.gouv.fr/bo/13/Hebdo30/MENE1315928A.htm?cid_bo=73066) 
  - (Art. 14 : “Se tenir informé des acquis de la recherche afin de pouvoir s'engager dans des projets et des démarches d'innovation pédagogique visant à l'amélioration des pratiques.”)
- etc.

---
# :four: 1. Pourquoi ces infos (plutôt que d'autres) ?

- Plus **représentatives** (fondées sur des échantillons de participants plus importants, donc plus représentatives de la population ciblée : élèves français)
- Plus **objectives** (fondées sur des méthodes de recueil ou d'analyse plus élaborées, systématiques)
- Plus **reproductibles** (mieux expliquées, pouvant mieux être réutilisées)
- Plus “pointues”, spécialisées
- etc.

---
# :four: 1. Ces infos pour quoi faire ? Utilisation…

- **Conceptuelle** : Utiliser le modèle *X* de lecture me permet de mieux comprendre comment les élèves apprennent à lire, de me faire une meilleure idée des tenants et aboutissants de cette activité complexe
- **Symbolique** : Utiliser le modèle *X* de lecture me permet de justifier une pratique, par exemple pour l'expliquer dans un projet, à mes collègues
- **Instrumentale** : Utiliser le modèle *X* de lecture me permet de mieux concevoir des exercices, des ressources, des rétroactions pour aider mes élèves

---
#  :four: 1. Mais ai-je vraiment besoin des produits de la recherche pour savoir comment travailler ?

- En éducation, comme dans tous les domaines, les connaissances sont souvent établies par “le peuple”, qui trouve lui-même, par essais-erreurs, les pratiques les plus efficaces (Conner 2011)
- Voir C. Freinet, M. Montessori, P. Freire, etc., qui ont pu élaborer des pratiques montrant une certaine efficacité sans être des chercheurs “professionnels”

---
#  :four: 1. Toutefois, les recherches peuvent présenter des avantages

- Avoir une meilleure idée des concepts, des processus à l'œuvre
- Difficile de comprendre quelque chose (un état, un processus) tant qu'on n'a pas essayé de le mesurer 
- Fonder cette idée sur des échantillons plus grands
- Combattre ou freiner de possibles “mythes”, idées fondées sur des problèmes de perception, des *a priori*

---
#  :four: 1. Mais, attention, d'éventuels problèmes sont toujours possibles !

- *Mutations létales* : Des interprétations de pratiques efficaces peuvent être modifier jusqu'à se révéler inefficaces (*e.g*., proposer du texte avec des illustrations distractives n'est pas du double codage efficace) [[article EEF](https://www.tes.com/magazine/tes-explains/what-are-lethal-mutations)]
- *Effets secondaires* : Toute intervention ayant des effets positifs peut également avoir des effets secondaires. Mesurer des apprentissages avec des tests standardisés ne suffit pas : il faut vérifier que la motivation et d'autres caractéristiques importantes ne sont pas impactées (Zhao, 2017)

---
# :four: 1. Critères pour utiliser une recherche ? 

1. Est-ce qu'elle permet de résoudre un problème que j'ai ?
2. Est-ce qu'elle permet une meilleure réussite ou performance ? À quelle hauteur ?
3. Qu'est-ce qu'elle coûtera (en argent et temps de travail) ?
4. Est-ce que je peux la mettre en œuvre dans mon école ou établissement ?
5. Est-ce que je comprends quoi faire ?

[Wiliam 2023](https://youtu.be/p6oukyR2F0Q)

---
# :four: 2. Veille pédagogique (SLED)

## 1. **Sélectionner des informations**

- Quelques sites de recherche d'articles :
  - [Matilda](https://matilda.science/?l=fr)
  - [Google Scholar](https://scholar.google.com)
  - [Beluga UGA](http://beluga.univ-grenoble-alpes.fr)
  - [ERIC](https://eric.ed.gov)
  - [Connected Papers](https://www.connectedpapers.com)
  - [Consensus](https://consensus.app/search/)
  - [Elicit](https://elicit.org) 
  - [Scinapse](https://www.scinapse.io/)


---
# :four: 2. Veille pédagogique (SLED)

## 1. **Sélectionner des informations**

- Il est aussi possible de suivre des éditeurs ou des chercheurs sur les réseaux sociaux (comme [Bluesky](https://bsky.app)); ou bien encore s'abonner à des flux RSS de revues pédagogiques, ou de sites comme le [Café Pédagogique](https://www.cafepedagogique.net)
- Ce [site](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/res-ens-preuves.html) réfère à de nombreux sites sur l'éducation fondée sur les preuves, voir notamment [L'education endowment foundation](https://educationendowmentfoundation.org.uk/), et [What works in education](https://ies.ed.gov/ncee/wwc/FWW)
  
---
# :four: 2. Veille pédagogique (SLED)

## 1. **Sélectionner des informations (en français)**

- En français, voir aussi les [conférences de consensus du CNESCO](https://www.cnesco.fr/presentation-dune-conference-de-consensus/), les ressources du [CSEN](https://www.reseau-canope.fr/conseil-scientifique-de-leducation-nationale-site-officiel/publications-et-ressources.html) et les [dossiers Veilles et analyses de l'Ifé](http://veille-et-analyses.ens-lyon.fr)
  
---
# :three: 2. **Lire des travaux** et prendre des notes

## Prendre intelligemment des notes (1/2)

- Lire des travaux ne suffit pas : lisez en annotant (information intéressante dans le document, idée qui vous vient)


---
# :three: 2. **Lire des travaux** et prendre des notes

## Prendre intelligemment des notes (2/2)


- Ce qui compte, c'est ce qu'on écrit pendant qu'on lit, plutôt que ce qu'on lit
- Consigner ses notes dans un outil de gestion de notes (plutôt qu'une suite Office), cf. la démarche [Zettelkasten](https://imarnaud.medium.com/quest-ce-que-la-méthode-zettelkasten-de-prise-de-notes-3c8f2123abd0) ou Ahrens (2017), ou sur des fiches-papier (cf. [les cartes à jouer](https://cramer.hypotheses.org/496) du physicien du XVIIIe s. G.-L. Le Sage, et Bert 2018)
- Il est ensuite assez aisé d'organiser ses fiche pour réaliser un état de l'art (mode plan d'ordinateur, ou sur une grande table pour des fiches-papier)
- 3 types de notes : - éphémères (choses à faire) ; - permanentes (contient de l'info. toujours utile) ; - par projet (supprimées quand le projet est terminé)

:book: Ahrens 2017

---
# :four: 2. **Écrire un document** 

- Écrire les sections de votre mémoire au fur et à mesure, en commençant par la structure, et en le remplissant au fur et à mesure (voir plus d'informations en [TD 9](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE-24-option-rech-9.pdf))

---
# :four: 2. **Diffuser ses travaux** 

- Rédiger le mémoire (voir [TD 9](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE-24-option-rech-9.pdf))
- Le présenter (voir [TD 10](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE-24-option-rech-10.pdf))

---
# :four: 3. Travail sur la fiche

- Cette [fiche de recherche](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/tuto-trouver-travaux) est un guide pour pré-sélectionner quelques travaux scientifiques sur votre thème
- Suivez-la en spécifiant le mieux possible votre sujet de mémoire
- Vous pouvez aussi réfléchir à l'organisation du matériau de votre état de l'art sous forme de fiches (papier ou électroniques)
  
---
# :four: 4. Plus d'informations

- Ce [document](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/veillepeda.html) vous donnera plus d'informations sur le processus de veille
- Ce [document](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/veille-sci-ressources.html) liste un grand nombre de ressources de veille en éducation
- Et, pour rappel, la [fiche de recherche](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/tuto-trouver-travaux)

---
# :four: Références

- Ahrens, S. (2017). *How to take smart notes*. CreateSpace. 
- Bert, J.-F. (2018). *Comment pense un savant ? Un physicien des Lumières et ses cartes à jouer*. Anamosa. 
- Conner, C. D. (2011). *Une histoire populaire des sciences*. L’échappée. 
- Zhao, Y. (2017). What works may hurt: Side effects in education. *Journal of Educational Change, 18*(1), 1-19. https://doi.org/10.1007/s10833-016-9294-4 

