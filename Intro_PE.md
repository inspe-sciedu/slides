---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# 
# EC 33 "Culture numérique et apprentissage"
## Objectifs, organisation, attendus


<!-- page_number: true -->
<!-- footer: Pôle enseignement numérique INSPE - UGA - 2024 - 2025 - CC:BY-NC-SA -->

---
# Objectifs EC 33
Cette UE a pour objectif de vous accompagner dans l'acquisition des compétences nécessaires pour utiliser le numérique en classe en prenant en compte les dimensions, efficacité de l’enseignement, respect des normes juridiques et éthiques et développement de la culture numérique des élèves. 

Cette formation articule apports théoriques et activités pratiques aboutissant à l’analyse réflexive d’une ou plusieurs situations d’apprentissage intégrant le numérique.


---
# Organisation

* 3 Cours sous forme d'apports théoriques
* 5 TD 
:warning: Vous aurez différents intervenants 

---
# Attendus EC33
- Un document rédigé **en binôme** de 4 pages maximum consistant à l'analyse d'une situation d'apprentissage intégrant le numérique

:arrow_right: Syllabus


#### Cours sur la plateforme eformation :arrow_right:  https://eformation.univ-grenoble-alpes.fr


 





