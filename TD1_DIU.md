---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
   
# Culture numérique et apprentissages
## TD 1
 
Christophe Charroud - UGA
 
<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2023 - 2024 - CC:BY-NC-SA -->

---
# :zero: Préambule - Information sur PIX+EDU

### Historique

* **Janvier 2022** : Le MENJ décide de la création d'une nouvelle certification PIX+EDU concernant l'usage du numérique en situation d'enseignement :arrow_right: commande aux universités et aux rectorats.
Certification en deux volets :
    * Un volet pratique professionnelle réalisé à l'INSPE (conception, mise en oeuvre réelle ou simulée, analyse réflexive) :arrow_right: Obtention d'un niveau pour la certification (initié ou confirmé)
    * Un volet automatisé initié à l'INSPE et terminé lors des premières années de titularisation :arrow_right: certification
---
### Historique (suite)

* **2022 - 2023** : Année d'expérimentation dans certains INSPE (pas à Grenoble) :arrow_right: résultats d'expérimentation conduisant les universités à demander à une première série d'ajustements.
* **Septembre 2023** : Le MENJ annonce la généralisation de PIX+EDU mais en l'absence de texte officiel et sans attribution de moyen :arrow_right: l'INSPE - UGA refuse de mettre en oeuvre la certification PIX+EDU dans ces conditions :arrow_right: **discussion avec le Rectorat de Grenoble pour une entrée partielle dans le dispositif PIX+EDU.**

---
### Dernières avancées

* **Octobre 2023** : Plusieures universités font remonter des difficultés ou des blocages dans la mise en oeuvre de la certification PIX+EDU, idem du côté de certains Rectorats
* **Fin novembre 2023** : Le MENJ et le MESR proposent (enfin) un projet plus réaliste pour la rentrée 2024... mais toujours pas de texte officiel à ce jour.

---
### Prévision du dispositif PIX+EDU pour la rentrée 2024

![](images/img-pix_edu/dep_pix.png)

---
# :one: Objectifs 
### Objectif général de la formation INSPE-UGA 
* Concevoir une situation d'apprentissage intégrant le numérique réaliste et réalisable dans votre classe (Obligation)
* Si possible, réaliser la mise en oeuvre (pas d'obligation, pensez à bien conserver toutes les traces).
* Rentrer dans la partie automatisé de PIX+EDU.
### Objectifs du TD
* Mettre en pratique les concepts théoriques sur la thématique "s'entrainer sur des tâches ciblées" :arrow_right: Evaluer et s'auto-évaluer avec le numérique.
 
---
## Organisation du TD

1 - Rappels théoriques (5 minutes)
2 - Mise en pratique : Analyse d'usages (1h45)
3 - Recherche d'une situation d'apprentissage réaliste (10 minutes)

---

# :two: Ce que l'on sait
## L’utilisation du numérique ***peut*** avoir un effet positif sur l’apprentissage

- **Pour accéder à des informations**

- **Pour communiquer**

- **Pour s'entraîner sur des tâches ciblées**

> (J-PAL, 2019)


---
## S'entrainer sur des tâches ciblées 
* Nécessité de feedback
* Feedback, si possible, immédiat :arrow_right: biais de mémoire
* Si la réponse comporte une erreur :arrow_right: remédiation
* Si la réponse est correcte (tant que la connaissance n’est pas parfaite, le cerveau continue d’apprendre) => nécessité de surapprentissage (réviser, remettre à l’épreuve…)
* Des rappels expansés pour consolider les connaissances.
:arrow_right: On peut aussi évlauer les élèves avec le numérique


---
# 
# 
### :arrow_right: *Facile à dire en théorie mais en pratique c'est moins simple...*






---
# :three: Pratique
##  :pencil2: TP : Test d'un outil ou service numérique

* Modalités : Par groupe de 3 à 5 personnes (par discipline ou niveau de classe)
* Essayez de déterminer **une forme** d'entraînement **numérique** réaliste que vous utilisez ou que vous pourriez utiliser dans vos classes.
* Jouez **réellement** cet entraînement au sein du groupe en vous mettant du point de vue de l'élève et du point de vue de l'enseignant


---
## Production attendue : une présentation de 5 minutes
<!-- _class: t-80 -->

**Contenu de la présentation :**
* Description et démonstration de l'entraînement choisi
* Analyse critique
    * L'interface est-elle conforme aux préconisations de la recherche
    * Y a-t-il des feedbacks ? De quelle nature ?
    * Y a-t-il des traces exploitables à postériori par l'enseignant ? Si oui lesquelles
    * L'application ou le services est-il payant ?
    * Y a-t-il un lien avec des sociétés commerciales ?
    * Le RGPD est-il respecté ?
* Conclusion : Recommandez-vous cet entraînement ?
:warning: **Vous êtes en formation = les erreurs et approximations sont normales, pas de stress !**


---


# Présentations


---
# :four: Travail de conception

#### Rappel de l'objectif pour l'évaluation :
Concevoir une situation d'apprentissage intégrant le numérique réaliste et réalisable dans votre classe.

#### Rappel de l'objectif de la formation (donc à la sortie de l'INSPE):
Si possible, mise en oeuvre en classe :arrow_right: PIX+EDU.


---
## Modalités de conception

* **TD 1 : Mise en application des apports théoriques sur les effets du numériques sur les apprentissages** - Début de la conception :arrow_right: avoir une première idée de situation d'apprentissage.
* TD 2 : Mise en application des apports théoriques sur les ressources et les aspects juridiques et éthiques - Poursuite de la conception
* TD 3 : Apports pratiques sur la formation AU numérique et l'évaluation des élèves en référence au CRCN - découverte de PIX et PIX+EDU - Poursuite de la conception
* TD 4 : Retour d'expérience pour celles et ceux qui auront réalisé une mise en oeuvre. :arrow_right: PIX+EDU

---
# En avant !
Conseils :
* Regroupez vous par niveau de classe. ( :warning: une situation indivuelle au final)
* Essayez d'identifier un apprentissages pour lequel le numérique peut apporter une plus-value.
* Restez réaliste :arrow_right: Mise en oeuvre conseillée.

 









