---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# 
# Enfants à écrans : À la recherche du temps perdu 
### Méthodologie et résultats de la recherche



Christophe Charroud - UGA

<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr -2025 - CC:BY-NC-SA -->


---

# :zero: Préambule 

![w:300](images/img-effets/intro_sante.jpeg)


---
## Écrans et santé
### Connaissance scientifique vs connaissance communue

* Qui a déjà pris connaissance du rapport "Enfants et écrans : À la recherchedu temps perdu" ?


--- 
## Écrans et santé : un débat médiatisé
### Des phrases percutantes mais...
![w:600](images/img-effets/cocaine.png)

---
## Écrans et santé : des ouvrages médiatisés mais...
![w:600](images/img-effets/livres.jpg)


---
### Un exemple
![w:1000](images/img-effets/cose.jpg)

---
### Un contre-exemple
![w:800](images/img-effets/margarine.jpg)

---
### Un exemple qui devient une contre-exemple
![w:1000](images/img-effets/cose.jpg)

---
## Des faiblesses méthodologiques récurrentes

* Des affirmations extrapolées de constats locaux limités dans le temps.
* Des interprétations parfois (souvent) erronnées.
* :arrow_right: **Une confusion entre corrélation et lien de causalité**.

---
### Corrélation et lien de causalité
![w:1000](images/img-effets/Causal1.jpeg)

---
### Corrélation et lien de causalité
![w:1000](images/img-effets/Causal2.jpeg)

---
### Corrélation et lien de causalité
![w:1000](images/img-effets/Causal3.jpeg)

---
### Méthodologie pour avoir quelques indications

* Des études longitudinales :arrow_right: 20 ans 
* Des cohortes stables et avec des effectifs suffisants
* Une prise en compte du niveau socio-économique des familles
* Des entretiens réguliers

---
## Cohortes et études longitudinales, ça existe!

En France 
* **EDEN** 2000 enfants nés entre mai 2003 et juillet 2006 suivis pendant 5 ans puis à 8 et 10-13 ans et enfin 16-19 ans.

* **ELFE** 18 000 enfants nés en France métropolitaine en 2011 suivis pendant 20 ans.

Ailleurs 
* **ABCD** aux U.S.A.
* et d'autres...

--- 
# Des résultats, des publications, et quelques effets

* Des publications internationales, des effets dans le milieu de la recherche et de l'enseignement supérieur.

* Pas vraiment de prise en compte par l'Éducation Nationale :arrow_right: **En Avril 2024, publication du rapport "Enfants et écrans : À la recherche du temps perdu"**. Une revue de littérature scientifique très accessible :

https://www.elysee.fr/admin/upload/default/0001/16/fbec6abe9d9cc1bff3043d87b9f7951e62779b09.pdf 
 
* Quels effets de ce rapport dans l'E.N. ? 

---
## :one: Écrans et développement neurocognitif de l’enfant et de l’adolescent.

![](images/img-effets/brain_dev.png)

---
## 1. Écrans et développement neurocognitif de l’enfant et de l’adolescent.
# 
**Des effets négatifs ou positifs, variables selon l'âge et les usages.**

### :warning: Très dépendant des inégalités sociales. 

:arrow_right: Cf. conférence de Corentin Gonthier


---
### Écrans et développement neurocognitif de l’enfant et de l’adolescent, conclusions
<!-- _class: t-90 -->


Moins de 6 ans :
* Utilisation d'écrans **sans** étayage d'un adulte :arrow_right: altération des compétences langagières, émotionnelles et relationnelles :x:.
* Utilisation d'écrans **avec** étayage d'un adulte :arrow_right: effets positifs modérés :white_check_mark:.

Plus de 6 ans
* Pas d'impact avéré sur le développement neurologique :white_check_mark:
* Possibles troubles de l'attention avec **certains usages** :x:, attention renforcée avec d'autres :white_check_mark:.

Les écrans ne sont pas la cause des troubles des apprentissages (dys) ni du TDA/H ni du TSA :white_check_mark:.

**:arrow_right: Le milieu social d’origine est la variable la plus explicative des différences observées dans le domaine cognitif.**


---
## :two: Écrans et santé somatique

![w:300](images/img-effets/somatique.png)


La science dit que **les écrans en tant que technologie** présentent des risques aujourd’hui établis sur certains aspects de la santé physique des enfants et des adolescents.

---
### 2.1 Le sommeil

![w:300](images/img-effets/sommeil.jpeg)

Consensus très net sur les effets négatifs, directs et indirects, des écrans sur le sommeil :arrow_right: Diminution du temps de sommeil en deçà des recommandations.

:warning: Mercredi 12 février à 12h30 conférence "Sommeil et apprentissages"


---
### 2.1 Le sommeil


* :waning_crescent_moon: Exposition à un défilement rapide d’images, de sons, de lumières et de mouvements diffusés sur écrans. (Hirshkowitz & al., 2015)
* :waning_crescent_moon: Exposition à la « lumière bleue » :arrow_right: décalage pic de mélatonine. **Les « filtres à lumière bleue » n’apportent pas de bénéfice sur la qualité de sommeil** (Arns & al., 2022)
* :sun_with_face: Perturbations du rythme circadien en lien avec le temps passé sur les écrans, y compris en journée (Dauvilliers, 2019).


---
### 2.2 Sédentarité
Le temps passé sur les écrans :arrow_right: diminution de la dépense calorique. (Lanningham-Foster & al., 2006)

![w:300](images/img-effets/sedentaire.png)


---
### 2.2 Sédentarité

* Sédentarité :arrow_right: surpoids
* Pathologies chroniques, risques importants de maladies cardiovasculaires (Mounier-Vehier & al., 2019)

![w:200](images/img-effets/coeur.png)


* mais pas que..., hypertension artérielle, perturbations des lipides, diabète de type 2, puberté précoce chez les filles (Duclos, 2021)(O'Donnell & al., 2016)(Li & al., 2017)
* Le temps passé sur les écrans est associé à de mauvais comportements alimentaires (Courbet & Fourquet-Courbet, 2019)(Bellissimo & al., 2007)(Boyland & al., 2019)


---
### Sédentarité - les écrans une cause indirecte et réversible
##### La place prise par les écrans et les usages qui en sont faits favorisent la sédentarité et le manque d’activité physique.


* :arrow_right: Une diminution du temps d'écran augmente la dépense calorique et permet de recouvrer des paramètres biologiques "standards"  (Pedersen, 2022)


---
### 2.3 Altération de la vue

![w:400](images/img-effets/myopie.png)

---
### 2.3 Altération de la vue, un constat

Les écrans contribueraient en particulier à l’épidémie de myopie qui touche les sociétés modernes :
* La prévalence de la myopie est en augmentation depuis le milieu du XXe siècle et s’est accélérée ces dernières décennies.
* Il est estimé qu’en 2050, la moitié de l’humanité souffrira de myopie, dont 10 % à un stade sévère.
(Matamoros & al., 2015)(Grzybowski & al., 2020)(Haarman & al., 2020)


---
#### Altération de la vue, causes établies ou potentielles
:warning: L’œil de l’enfant et de l'ado est encore en formation, son développement se termine vers l’âge de 16 ans.
* Moins de variations courte et longue distance (accoutumance)
* Exposition plus faible à la lumière naturelle et plus importante à la lumière artificielle (Jones-Jordan & al., 2012)(Wu & al., 2013)
* La lumière bleue émise par la majorité des écrans à LED **semblerait** présenter, à forte dose, des effets phototoxiques inquiétants sur la rétine (Cao & al., 2020)(Foreman & al., 2021), à suivre ...
* « digital eye strain » (a minima à 50% chez les usagers d’ordinateurs) :arrow_right: augmentation de la sensation d’œil sec, sensations de fatigue visuelle, flou visuel (Sheppard & al., 2018).

---
### 2.4 Autres effets somatiques potentiels 
* Existence possible (**mais non prouvée à ce jour**) d’effets liés à l’exposition aux rayonnements radiofréquences (Charroud & Choucroune, 2016)(De Vasconcelos & al., 2023)(Karipidis & al., 2024)
* Toxicité de certains des matériaux ou des substances utilisés pour la fabrication des écrans (Abdallah & Harrad, 2018)(Tansel, 2022)(Wang & al., 2023).
* Possibles troubles musculosquelettiques (TMS), la main, poignet, dos, nuque (David & al., 2021).

![w:150](images/img-effets/textneck.png)

---
## Écrans et santé somatique, conclusions
Des effets scientifiquement avérés sur :
* Le sommeil :arrow_right: impact très important sur les apprentissages :x:
* La sédentarité :arrow_right: impact très important en termes de santé publique :x:
* La vue :arrow_right: prévalence de la myopie en forte hausse. :x:

Des effets possibles mais non scientifiquement avérés à ce jour :
* Ondes électromagnétiques : probabilité en baisse
* Substances toxiques : à surveiller
* TMS : en hausse, à surveiller


---
## :three: Écrans et santé mentale
Dépression, anxiété, addictions... en progression chez les ados en France.
* Les études scientifiques manquent aujourd’hui pour établir un lien de causalité entre les usages du numérique et le bien-être mental des jeunes.
* Effets contrastés des réseaux sociaux.
* **Le bien-être mental est toujours multifactoriel et dépend de facteurs individuels, familiaux et environnementaux.**

---
### 3.1 Quelques précisions... pour ajouter à la confusion
* Chez les 8-10 ans (Sauce & al., 2022) : 
    * Réseaux sociaux = effets nuls  

* Chez les 10-12 ans (Flannery & al., 2024)
    * Réseaux sociaux néfastes si vulnérabilité neuropsychologique préexistante
    * :arrow_right: Symptômes dépressifs chez les filles mais pas chez les garçons

* Mal-être et écrans chez les plus âgés
    * Une étude à large échelle (+11 000) sur des adolescents et avec rapports quotidiens du temps d'utilisation montre un effet non perceptible (Orben & Przybylski 2019) :



---
## Écrans et santé mentale, conclusions

Le rapport "Enfants et écrans À la recherche du temps perdu" (2024) indique qu’une consommation excessive des réseaux sociaux constitue un facteur aggravant de risque pour les jeunes présentant des vulnérabilités, mais non une cause :arrow_right: **à surveiller.**


---
# Ne restez pas sédentaire !
#
Une courte vidéo qui met en évidence la nécessité de ne pas rester sédentaire et dépendant de vos notifications.

https://youtu.be/aD19Owac68c?feature=shared

Merci à Claire MOUNIER-VEHIER cofondatrice de "Agir pour le coeur des femmes"  https://www.agirpourlecoeurdesfemmes.com



---
# :four: Quelles recommandations pour le monde enseignant

* La commission "Enfants et écrans" a formulé 29 propositions.
* Ces propositions dépassent la simple relation enfants-écrans. 
* La plupart des ces propositons dépassent le cadre de l'Éducation Nationale.
* L'Éducation Nationale doit intégrer une partie de ces recommandations dans ses pratiques


---
## 4.1 Que faire avec vos élèves ?
### :pencil2: TP faire évoluer les pratiques pour tenir compte du rapport "Enfants et écrans : à la recherche du temps perdu" 

* Modalité : en petits groupes disciplinaires ou de niveaux de classe
* Travail demandé : imaginez une évolution **réaliste dans votre discipline** permettant de sensibiliser les élèves à une "bonne hygiène d'usage des écrans".
* Important : Ne pas stigmatiser 

---
## 4.2 Que faire avec vos élèves ?
### :pencil2: TP faire évoluer les pratiques pour tenir compte du rapport "Enfants et écrans : à la recherche du temps perdu" 

* Comment faire ?
    * Utilisez les 29 recommandations du rapport "Enfants et écrans : À la recherche du temps perdu" 
    * Consultez les pages de la DRANE consacrées à ce rapport :
    
    https://dane.web.ac-grenoble.fr/actualites-nationales/commission-ecrans


---
# Références 

<!-- _class: t-50 -->

Abdallah, M. A. E., & Harrad, S. (2018). Dermal contact with furniture fabrics is a significant pathway of human exposure to brominated flame retardants. Environment international, 118, 26-33.
Arns, M., Kooij, J. S., & Coogan, A. N. (2021). Identification and management of circadian rhythm sleep disorders as a transdiagnostic feature in child and adolescent psychiatry. Journal of the American Academy of Child & Adolescent Psychiatry, 60(9), 1085-1095.
Bediou, B., Rodgers, M. A., Tipton, E., Mayer, R. E., Green, C. S., & Bavelier, D. (2023). Effects of action video game play on cognitive skills: A meta-analysis.
Bellissimo, N., Pencharz, P. B., Thomas, S. G., & Anderson, G. H. (2007). Effect of television viewing at mealtime on food intake after a glucose preload in boys. Pediatric research, 61(6), 745-749.
Beyens, I., Valkenburg, P. M., & Piotrowski, J. T. (2018). Screen media use and ADHD-related behaviors: Four decades of research. Proceedings of the National Academy of Sciences, 115(40), 9875-9881.
Bioulac, S., Charroud, C., Pellencq, C. (2022). Le numérique face aux troubles du déficit de l’attention et à l’Hyperactivité. Consulté à https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/cours_TDA-H.html
Boyland, E. J., Nolan, S., Kelly, B., Tudur-Smith, C., Jones, A., Halford, J. C., & Robinson, E. (2016). Advertising as a cue to consume: a systematic review and meta-analysis of the effects of acute exposure to unhealthy food and nonalcoholic beverage advertising on intake in children and adults. The American journal of clinical nutrition, 103(2), 519-533.
Braune‐Krickau, K., Schneebeli, L., Pehlke‐Milde, J., Gemperle, M., Koch, R., & von Wyl, A. (2021). Smartphones in the nursery: Parental smartphone use and parental sensitivity and responsiveness within parent–child interaction in early childhood (0–5 years): A scoping review. Infant Mental Health Journal, 42(2), 161-175.
Bioulac, S., Charroud, C., Pellencq, C. (2022). Le numérique face aux troubles du déficit de l’attention et à l’Hyperactivité. Consulté à https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/cours_TDA-H.html
Cao, K., Wan, Y., Yusufu, M., & Wang, N. (2020). Significance of outdoor time for myopia prevention: a systematic review and meta-analysis based on randomized controlled trials. Ophthalmic research, 63(2), 97-105.
Charroud, C., & Choucroune, P. (2016). Numérique, wifi, téléphone, les ondes à l’école. Consulté à https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/cours_Ondes.html .
Corkin, M. T., Henderson, A. M., Peterson, E. R., Kennedy-Costantini, S., Sharplin, H. S., & Morrison, S. (2021). Associations between technoference, quality of parent-infant interactions, and infants’ vocabulary development. Infant Behavior and Development, 64, 101611.

---

<!-- _class: t-50 -->

Courbet, D., & Fourquet-Courbet, M. P. (2019). Usages des écrans, surpoids et obésité. Obésité, 14(3), 131-138.
Dauvilliers, Y. (2019). Les troubles du sommeil. Elsevier Health Sciences.
David, D., Giannini, C., Chiarelli, F., & Mohn, A. (2021). Text neck syndrome in children and adolescents. International journal of environmental research and public health, 18(4), 1565.
De Vasconcelos, A. P., Andreeva, V., Boarini, S., Bourdieu, A., Burkhardt, J. M., Chaumet-Riffaud, P., ... & Roth-Delgado, O. (2023). Avis de l'Anses relatif aux lignes directrices visant à limiter l’exposition des personnes aux champs électromagnétiques (100 kHz–300 GHz) (Doctoral dissertation, Anses).
Duclos, M. (2021). Épidémiologie et effets sur la morbi-mortalité de l’activité physique et de la sédentarité dans la population générale. Revue du Rhumatisme Monographies, 88(3), 177-182.
Fischer, J. P. (2023). L’utilisation précoce des écrans est-elle néfaste? Une première réponse avec la cohorte Elfe. Psychologie Française, 68(1), 55-70.
Flannery, J. S., Burnell, K., Kwon, S. J., Jorgensen, N. A., Prinstein, M. J., Lindquist, K. A., & Telzer, E. H. (2024). Developmental changes in brain function linked with addiction-like social media use two years later. Social Cognitive and Affective Neuroscience, 19(1), nsae008.
Foreman, J., Salim, A. T., Praveen, A., Fonseka, D., Ting, D. S. W., He, M. G., ... & Dirani, M. (2021). Association between digital smart device use and myopia: a systematic review and meta-analysis. The Lancet Digital Health, 3(12), e806-e818.
Grzybowski, A., Kanclerz, P., Tsubota, K., Lanca, C., & Saw, S. M. (2020). A review on the epidemiology of myopia in school children worldwide. BMC ophthalmology, 20, 1-11.
Haarman, A. E., Enthoven, C. A., Tideman, J. W. L., Tedja, M. S., Verhoeven, V. J., & Klaver, C. C. (2020). The complications of myopia: a review and meta-analysis. Investigative ophthalmology & visual science, 61(4), 49-49.
Hirshkowitz M, Whiton K, Albert SM, et al. National Sleep Foundation's sleep time duration recommendations: methodology and results summary. Sleep Health. 2015 Mar;1(1):40-43.
Howie, E. K., Joosten, J., Harris, C. J., & Straker, L. M. (2020). Associations between meeting sleep, physical activity or screen time behaviour guidelines and academic performance in Australian school children. BMC public health, 20, 1-10.
Jones-Jordan, L. A., Sinnott, L. T., Cotter, S. A., Kleinstein, R. N., Manny, R. E., Mutti, D. O., ... & Zadnik, K. (2012). Time outdoors, visual activity, and myopia progression in juvenile-onset myopes. Investigative ophthalmology & visual science, 53(11), 7169-7175.
Karipidis, K., Baaken, D., Loney, T., Blettner, M., Brzozek, C., Elwood, M., ... & Lagorio, S. (2024). The effect of exposure to radiofrequency fields on cancer risk in the general and working population: A systematic review of human observational studies–Part I: Most researched outcomes. Environment International, 108983.
Lanningham-Foster, L., Jensen, T. B., Foster, R. C., Redmond, A. B., Walker, B. A., Heinz, D., & Levine, J. A. (2006). Energy expenditure of sedentary screen time compared with active screen time for children. Pediatrics, 118(6), e1831-e1835.

---

<!-- _class: t-50 -->

Li, W., Liu, Q., Deng, X., Chen, Y., Liu, S., & Story, M. (2017). Association between obesity and puberty timing: a systematic review and meta-analysis. International journal of environmental research and public health, 14(10), 1266.
Lin, Y. J., Chiu, Y. N., Wu, Y. Y., Tsai, W. C., & Gau, S. S. F. (2022). Developmental changes of autistic symptoms, ADHD symptoms, and attentional performance in children and adolescents with autism spectrum disorder. Journal of autism and developmental disorders, 1-15.
Madigan, S., McArthur, B. A., Anhorn, C., Eirich, R., & Christakis, D. A. (2020). Associations between screen use and child language skills: a systematic review and meta-analysis. JAMA pediatrics, 174(7), 665-675.
Madore, K. P., Khazenzon, A. M., Backes, C. W., Jiang, J., Uncapher, M. R., Norcia, A. M., & Wagner, A. D. (2020). Memory failure predicted by attention lapsing and media multitasking. Nature, 587(7832), 87-91.
MANDIGOUT, S., GUIGNARD, L., & GELINEAU, A. (2024). La thérapie digitale EndeavorRx auprès des enfants atteints de TDA/H: une étude de portée. Revue scientifique des travaux de fin d'étude en rééducation et réadaptation, (2).
Marciano, L., & Camerini, A. L. (2021). Recommendations on screen time, sleep and physical activity: associations with academic achievement in Swiss adolescents. Public health, 198, 211-217.
Massaroni, V., Delle Donne, V., Marra, C., Arcangeli, V., & Chieffo, D. P. R. (2023). The Relationship between Language and Technology: How Screen Time Affects Language Development in Early Life—A Systematic Review. Brain Sciences, 14(1), 27.
Matamoros, E., Ingrand, P., Pelen, F., Bentaleb, Y., Weber, M., Korobelnik, J. F., ... & Leveziel, N. (2015). Prevalence of myopia in France: a cross-sectional analysis. Medicine, 94(45), e1976.
Miller, J., Mills, K. L., Vuorre, M., Orben, A., & Przybylski, A. K. (2023). Impact of digital screen media activity on functional brain organization in late childhood: evidence from the ABCD study. cortex, 169, 290-308.
Mounier-Vehier, C., Nasserdine, P., & Madika, A. L. (2019). Stratification du risque cardiovasculaire de la femme: optimiser les prises en charge. La Presse Médicale, 48(11), 1249-1256.
Mundy, L. K., Canterford, L., Hoq, M., Olds, T., Moreno-Betancur, M., Sawyer, S., ... & Patton, G. C. (2020). Electronic media use and academic performance in late childhood: A longitudinal study. PLoS One, 15(9), e0237908.
O'Donnell, M. J., Chin, S. L., Rangarajan, S., Xavier, D., Liu, L., Zhang, H., ... & Yusuf, S. (2016). Global and regional effects of potentially modifiable risk factors associated with acute stroke in 32 countries (INTERSTROKE): a case-control study. The lancet, 388(10046), 761-775.
Ophir, Y., Rosenberg, H., Tikochinski, R., Dalyot, S., & Lipshits-Braziler, Y. (2023). Screen Time and Autism Spectrum Disorder: A Systematic Review and Meta-Analysis. JAMA Network Open, 6(12), e2346775-e2346775.
Orben, A., & Przybylski, A. K. (2019). Screens, Teens, and Psychological Well-Being: Evidence From Three Time-Use-Diary Studies. Psychological Science, 30(5), 682–696. doi: 10.1177/09567976198330329


---

<!-- _class: t-50 -->

Pedersen, J., Rasmussen, M. G. B., Sørensen, S. O., Mortensen, S. R., Olesen, L. G., Brønd, J. C., ... & Grøntved, A. (2022). Effects of limiting recreational screen media use on physical activity and sleep in families with children: a cluster randomized clinical trial. JAMA pediatrics, 176(8), 741-749.
Ramer, J. D., Santiago-Rodríguez, M. E., Vukits, A. J., & Bustamante, E. E. (2022). The convergent effects of primary school physical activity, sleep, and recreational screen time on cognition and academic performance in grade 9. Frontiers in Human Neuroscience, 16, 1017598.
Santos, R. M. S., Mendes, C. G., Marques Miranda, D., & Romano-Silva, M. A. (2022). The association between screen time and attention in children: a systematic review. Developmental neuropsychology, 47(4), 175-192.
Sauce, B., Liebherr, M., Judd, N., & Klingberg, T. (2022). The impact of digital media on children’s intelligence while controlling for genetic differences in cognition and socioeconomic background. Scientific reports, 12(1), 7720.
Sheppard, A. L., & Wolffsohn, J. S. (2018). Digital eye strain: prevalence, measurement and amelioration. BMJ open ophthalmology, 3(1), e000146.
Supper, W., Guay, F., & Talbot, D. (2021). The relation between television viewing time and reading achievement in elementary school children: A test of substitution and inhibition hypotheses. Frontiers in Psychology, 12, 580763.
Tansel, B. (2022). PFAS use in electronic products and exposure risks during handling and processing of e-waste: A review. Journal of Environmental Management, 316, 115291.
Wang, J., Lou, Y., Mo, K., Zheng, X., & Zheng, Q. (2023). Occurrence of hexabromocyclododecanes (HBCDs) and tetrabromobisphenol A (TBBPA) in indoor dust from different microenvironments: levels, profiles, and human exposure. Environmental Geochemistry and Health, 45(8), 6043-6052.
Wu, P. C., Tsai, C. L., Wu, H. L., Yang, Y. H., & Kuo, H. K. (2013). Outdoor activity during class recess reduces myopia onset and progression in school children. Ophthalmology, 120(5), 1080-1085.

