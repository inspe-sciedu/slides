---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# Culture numérique et apprentissages
## TD 2
 
Laurence Osete - UGA
 
<!-- page_number: true -->
<!-- footer: laurence.osete@univ-grenoble-alpes.fr - 2023 - 2024 - CC:BY-NC-SA -->

---
# :zero: Objectifs
### Objectif général de la formation
Concevoir une situation d'apprentissage intégrant le numérique réaliste et réalisable dans votre classe :arrow_right: Objectif mise en oeuvre. (PIX+EDU).
### Objectifs du TD
* Mettre en pratique les concepts théoriques sur la thématique "Production, usages de ressources et droits d'auteur" et "Enseigner avec le numérique : infox, IA et éthique".


---
# 0. travail de Conception


#### Rappel de l'objectif pour l'évaluation :
Concevoir une situation d'apprentissage intégrant le numérique réaliste et réalisable dans votre classe.

#### Rappel de l'objectif de la formation (donc à la sortie de l'INSPE):
Si possible, mise en oeuvre en classe :arrow_right: PIX+EDU.

---
# 0. Modalités de conception

- TD 1 : Mise en application des apports théoriques sur les effets du numérique sur les apprentissages - Début de la conception :arrow_right: avoir une première proposition de situation d'apprentissage.
- **TD 2 : Mise en application des apports théoriques sur les ressources numériques, les droits d'auteurs et les problèmes éthiques - Début de la conception**
- TD 3 : Apports pratiques sur la formation AU numérique et l'évaluation des élèves en référence au CRCN - découverte de PIX et PIX+EDU - Poursuite de la conception
- TD 4 : Finalisation de la conception. :arrow_right: Évalué cette année  
- TD 5 : Retour d'expérience pour celles et ceux qui auront réalisé une mise en oeuvre. :arrow_right: PIX+EDU

---
# :one: Mise en application par l'étude d'un cas

## Par groupe de 3 à 6 personnes (30 mn)

- Consulter le cas attribué disponible ici : https://lstu.fr/casjuridiquespe
- En vous aidant des ressources documentaires et du cas similaire corrigé, répondre à la situation problème.


## Présentations (30 mn)

- En vous appuyant sur les questions de la partie "Synthèse", réaliser une rapide présentation orale de 3 min maximum.

---
# :two: Recherche de situation (1 h)

* La situation d'apprentissage peut provenir d'une situation **vécue, observée ou rapportée**. 
* l'idée peut être issue d'une banque de scénarios comme :
    - [la banque de l'académie de Nantes](https://www.pedagogie.ac-nantes.fr/numerique/scenarios-pedagogiques/)
    - [Prim à bord](https://primabord.eduscol.education.fr/spip.php?page=selection&mots%5B%5D=133#content)
    - ou autre
* Elle doit intégrer au moins une ressource numérique, ou hybride.
* Elle peut être commune à plusieurs personnes mais les analyses devront prendre en compte le contexte de mise en œuvre.

---
# :two: Conception de la situation

Vous devez décrire la situation telle qu'elle a été conçue, ou telle qu'elle le sera si vous aviez à la mettre en œuvre. 

#### Eléments attendus dans la description

```
- Domaine(s) d’enseignement concerné(s) par la situation
- Niveau de classe
- Période de l'année, ou place dans une séquence, où la situation a été (ou serait) mise en œuvre.
- Objectif(s) d’apprentissages : 
	- Compétences disciplinaires visées en termes de « être capable de…», (ces compétences sont issues des programmes)
	- Compétences numériques développées (issues du CRCN)
	
- Environnement numérique impliqué dans la situation :
    - Outils physiques (TBI, tablettes, ordinateurs fixes ou portables, caméras, enregistreurs, ...)
    - Organisation matérielle (nb de matériels par élève/classe, salle informatique ou salle de classe)
    - Logiciels ou applications utilisés.
    - Ressources utilisées, ou produites

-  Organisation pédagogique : quelles activités, quand, comment, ...
    
```

---
# :two: Analyse et justification des choix

- Justification des choix d'outils et de ressources numériques, en s'appuyant notamment sur le modèle SAMR.
- Analyse des potentiels effets positifs ou néfastes pour les apprentissages et les apprenants (TD 1).
- Analyse des problèmes juridiques (droits d'auteurs, droit à la vie privée) et éthiques soulevés.


:warning: Les analyses et justifications doivent tenir compte du contexte.

---
#  En avant !
Conseils :
* Essayez d'identifier un apprentissages pour lequel le numérique peut apporter une plus-value.
* Restez réaliste :arrow_right: Mise en oeuvre conseillée.

