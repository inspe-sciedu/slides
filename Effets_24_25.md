---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# 
# Numérique, écrans, santé, apprentissages  
## Apports théoriques

Christophe Charroud - UGA

<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2024-2025 - CC:BY-NC-SA -->
 
---

# 
# :zero: Préambule 

 
### À votre avis, si l'on s'intéresse aux apprentissages et à la santé des élèves, utiliser le numérique en classe c'est ?
* Positif
* Négatif
...ou entre les deux....



---
# Vos représentations
## :pencil2: TP : Durée 5 minutes
En binôme, sur une feuille comportant une colonne "effets positifs" et une colonne "effets négatifs", listez des effets du numérique sur les apprentissages. (5 minutes)

---

# Vous êtes-vous appuyés sur des arguments scientifiques, sur des représentations ou sur des opinions  ?

#
### Pourquoi une telle question?


---
# 
## « Il y a une rupture entre connaissance commune et connaissance scientifique »

> Bachelard : le matérialisme rationnel, 1953



## Dans l'éducation, quand on parle de numérique, les doxas* tiennent souvent lieu de pensée. 
> (Ferone. G, 2019)



*Doxa : Ensemble d'opinions, de préjugés, de présuppositions généralement admises et évaluées positivement ou négativement.



---

# Alors, que dit la recherche ?


Les effets **positifs se limitent à des usages circonscrits**.

Hors de ces usages, le numérique produit (souvent) des effets négatifs sur les apprentissages.


## En clair, la recherche dit que numérique pour les apprentissages, ce n'est pas magique:exclamation: 


---
# Au programme :
##### 1  - Numérique et santé
##### 2  - Numérique et apprentissages : mythes et légendes les plus courants
##### 3  - Des effets positifs 
##### 4  - Un esprit critique nécessaire


---

# :one: Numérique et santé

* Numérique et santé : Contexte
* Écrans et développement neurologique et socio-relationnel de l’enfant et de l’adolescent
* Écrans et santé somatique;
* Écrans et santé mentale


---
## 1.1 Numérique et santé : Contexte
**Avant la rentrée 2024 :**
* En formation à l'INSPE, sujet abordé scientifiquement "hors programme" uniquement à Grenoble.
* Sur le terrain, des questions, beaucoup d'assertions, souvent des positions tranchées (pour ou contre).
* **Avril 2024, parution du rapport "Enfants et écrans, à la recherche du temps perdu"**
* Quelques réactions et annonconces (avec effets ?)

**Rentrée 2024**
* En formation à l'INSPE, sujet abordé scientifiquement "moins hors programme"


---
## 1.2 Écrans et développement neurologique et socio-relationnel de l’enfant et de l’adolescent.
# 
**Des effets négatifs ou positifs, variables selon l'âge et les usages.**

:warning: Très dépendant des inégalités sociales. 

Grands programmes de recherche : ELFE, ABCD, EDEN...


---
### 1.2.1 Écrans et développement cérébral 0 - 5 ans
Technoférence :
* du côté du parent : altération de la sensibilité, de l’étayage, de la disponibilité et de la réactivité de la réponse parentale.
* chez l’enfant : altération du développement du langage, de la régulation des émotions et des compétences socio-relationnelles.(Braune‐Krickau & al., 2021)(Corkin & al., 2021)

**:warning: Temps d’écran limité et l’âge de première exposition tardif :arrow_right: meilleures compétences langagières. (Madigan & al.,2020)(Massaroni & al., 2023)**

#### :arrow_right: Pas d'écran avant 3 ans!

---
### 1.2.2 Écrans et développement cérébral plus de 6 ans
* Pour certaines études : Écrans = possible baisse des performances en lecture et numératie (Supper & al. , 2021) (Mundy & al., 2020) chez les 6-9 ans.
* Chez les 9-17 ans : Temps d’écrans récréatif supérieur à 2H/j :arrow_right: moindres performances cognitives globales et moins bonnes performances scolaires.(Marciano & Camerini, 2021)(Howie & al., 2020)(Ramer & al.,2022) **à confimer**.
* Mais aucun effets sur le développement cérébral programme ABCD (Miller & al., 2023) voire des effets positifs reconnus programme ELFE (Fischer, 2023)

**:arrow_right: Le milieu social d’origine est la variable la plus explicative des différences observées dans le domaine cognitif.**

---
### 1.2.3 Écrans et capacités attentionnelles 
* Une exposition prolongée aux écrans des enfants de moins de 12 ans peut être associée à de moindres capacités attentionnelles (Santos & al., 2022)
* Chez les 15-16 ans, le « media multitasking » est un facteur important de perturbations des processus attentionnels et de la mémorisation (Madore & al., 2020)
* Effets positifs, faibles à modérés, associés à la pratique du jeu vidéo d’action (Bediou & al., 2023)

---
### 1.2.4 Écrans et troubles du neurodéveloppement 
###
### :warning: Les écrans ne sont pas à l’origine du TSA et du TDA/H
###
:arrow_right: Une vigilance est requise par rapport à l'usage excessif d'écrans pour éviter l’amplification des symptômes liés à ces troubles du neurodéveloppement.
(Beyens & al., 2018)(Bioulac, Charroud, Pellencq, 2022)(Lin & al., 2022)(Ophir & al. ,2023).

---
### 1.2.5 Résumé des effets présumés des écrans chez l’enfant et l’adolescent

<!-- _class: t-90 -->

* avant 2 ans, exposition aux écrans = moins bonnes performances au niveau du langage et des capacités attentionnelles.

* de 2 à 6 ans, un temps d’écran supérieur à une heure par jour ou de télévision supérieur à 30 minutes par jour est souvent associé à de moins bonnes performances cognitives globales, attentionnelles, langagières et socio-émotionnelles.

* de 6 à 17 ans : un temps d’écran supérieur à deux heures par jour **pourrait** être associé pour certains usages à de moindres capacités attentionnelles et à de moindres performances en lecture et scolaires, **mais cela reste à confirmer.**

* entre 15 et 18 ans : un usage à haute fréquence du smartphone (plusieurs fois par jour) a été associé à une augmentation des symptômes de type inattention, impulsivité et hyperactivité.

---
## 1.3 Écrans et santé sommatique
La science dit que les écrans en tant que technologie présentent des risques aujourd’hui établis sur certains aspects de la santé physique des enfants et des adolescents.

---
### 1.3.1 Le sommeil

Consensus très net sur les effets négatifs, directs et indirects, des écrans sur le sommeil :arrow_right: Diminution du temps de sommeil en deçà des recommandations.
* Exposition à un défilement rapide d’images, de sons, de lumières et de mouvements diffusés sur écrans. (Hirshkowitz & al., 2015)
* Exposition à la « lumière bleue » :arrow_right: décalage pic de mélatonine. Les « filtres à lumière bleue » n’apportent pas de bénéfice sur la qualité de sommeil (Arns & al., 2022)
* Perturbations du rythme circadien en lien avec le temps passé sur les écrans, y compris en journée (Dauvilliers, 2019).

:arrow_right: Conférence sommeil et apprentissages au cours du premier semestre.

----
### 1.3.2 Sédentarité
##### La place prise par les écrans et les usages qui en sont faits favorisent la sédentarité et le manque d’activité physique

Conséquence: surpoids, voire obésité :arrow_right: responsables de nombreuses pathologies chroniques.

---
#### Sédentarité - les écrans une cause indirecte
* Le temps passé sur les écrans conduit à une diminution de la dépense calorique. (Lanningham-Foster & al., 2006)
* Une diminution du temps d'écran augmente la dépense calorique (Pedersen, 2022)
* Le temps passé sur les écrans est plus fréquemment associé à des comportements alimentaires conduisant à une augmentation de l’apport énergétique par l’alimentation (Courbet & Fourquet-Courbet, 2019)(Bellissimo & al., 2007)(Boyland & al., 2019)

---
#### Sédentarité - Les effets 
* Sédentarité :arrow_right: manque d’activité physique :arrow_right: surpoids = facteur de risque importants et reconnus de maladies cardio- vasculaires et métaboliques (Duclos, 2021)(Mounier-Vehier & al., 2019)
* Mais pas que...(O'Donnell & al., 2016)
    * hypertension artérielle
    * perturbations des lipides
    * diabète de type 2
    * syndrome d’apnées du sommeil
    * puberté précoce chez les filles (Li & al., 2017)

---
### 1.3.3 Altération de la vue
Le visionnage intensif d’écrans a des effets néfastes pour la vue et pourrait entrainer des conséquences préoccupantes à long terme.

:warning: L’œil de l’enfant et de l'ado est encore en formation, son développement se termine vers l’âge de 16 ans.

La lumière joue un rôle essentiel dans la maturation de l’œil et le développement des fonctions visuelles.

---
#### Altération de la vue, constats et prévisions
* Les écrans contribueraient en particulier à l’épidémie de myopie qui touche les sociétés modernes. 
* La prévalence de la myopie est en augmentation depuis le milieu du XXe siècle et s’est accélérée ces dernières décennies.
* Il est estimé qu’en 2050, la moitié de l’humanité souffrira de myopie, à un stade sévère pour 10 % d’entre-elle.
(Matamoros & al., 2015)(Grzybowski & al., 2020)(Haarman & al., 2020)

---
#### Altération de la vue, causes établies ou potentielles
* Moins de variation courte et longue distance (accoutumance)
* Exposition plus faible à la lumière naturelle et plus importante à la lumière artificielle (Jones-Jordan & al., 2012)(Wu & al., 2013)
* La lumière bleue émise par la majorité des écrans à LED **semblerait** présenter, à forte dose, des effets phototoxiques inquiétants sur la rétine (Cao & al., 2020)(Foreman & al., 2021), à suivre ...
* « digital eye strain » (a minima à 50% chez les usagers d’ordinateurs) :arrow_right: augmentation de la sensation d’œil sec, sensations de fatigue visuelle, flou visuel (Sheppard & al., 2018).

---
### 1.3.4 Autres effets sommatiques potentiels 
* Existence possible (**mais non prouvée à ce jour**) d’effets liés à l’exposition aux rayonnements radiofréquences (Charroud & Choucroune, 2016)(De Vasconcelos & al., 2023).
* Toxicité de certains des matériaux ou des substances utilisés pour la fabrication des écrans (Abdallah & Harrad, 2018)(Tansel, 2022)(Wang & al., 2023).
* Possibles troubles musculosquelettiques (TMS), la main, poignet, dos, nuque (David & al., 2021).

---
## 1.4 Écrans et santé mentale
Dépression, anxiété, addictions... en progression chez les ados en France.
* Les études scientifiques manquent aujourd’hui pour établir un lien de causalité entre les usages du numérique et le bien-être mental des jeunes.
* Effets contrastés des réseaux sociaux.
* Le bien-être mental est toujours multifactoriel et dépend de facteurs individuels, familiaux et environnementaux.

---
### 1.4.1 Quelques précisisons... pour ajouter à la confusion
* Chez les 8-10 ans (Sauce & al., 2022) : 
    * Réseaux sociaux = effets nuls  
* Chez les 10-12 ans (Flannery & al., 2024)
    * Réseaux sociaux néfastes si vulnérabilité neuropsychologique préexistante
    * :arrow_right: Symptômes dépressifs chez les filles mais pas chez les garçons

___
### 1.4.2 Écrans et santé mentale - recommandations

La commision d'expert (Rapport "Enfants et écrans À la recherche du temps perdu", 2024) considère que les éléments sont suffisants pour indiquer qu’une consommation excessive des réseaux sociaux constitue un facteur aggravant de risque pour les jeunes présentant des vulnérabilités :arrow_right: à suivre.

---
### 1.4.5 Numérique et santé, conlusion

* Risques somatiques avérés
* Troubles du développement neurologique avérés chez les enfants en âge pré-scolaire, pas de concensus pour les écoliers et ados.
* Troubles avérés de l'attention chez les ados.
* Santé mentale à surveiller.


---

### :pencil2: TP : Un bilan intermédiaire (durée 1 minute)

Reprenez la liste effets positifs/effets négatifs que vous avez rédigée, faite un premier bilan...



---
# :two: Numérique et apprentissages : mythes et légendes les plus courants


###### 1  - Numérique et santé
#### 2  - Numérique et apprentissages : mythes et légendes les plus courants
###### 3  - Des effets positifs 
###### 4  - Un esprit critique nécessaire


---


### :pencil2: TP : Durée 5 minutes

En Binôme, réfléchissez à ces affirmations, sont-elles vraies ou fausse ?

* Le numérique permet de s'adapter aux styles d’apprentissage des élèves.
* Les *Digital Natives* apprennent différemment car leur cerveau est différent.
* Le numérique favorise l'autonomie des apprenants.
* La lecture sur écran réduit les compétences de lecture traditionnelle.
* Le numérique c'est bien pour les apprentissages car c'est ludique.




---

## Les mythes et légendes autour du numérique en classe
Les plus souvents entendus lors de dicussions :

* Le numérique permet de s'adapter aux styles d’apprentissage des élèves :arrow_right: Mythe...

* Les *Digital Natives* sont différents :arrow_right: Mythe...

#
> [Quelques mythes dans la recherche en éducation](http://espe-rtd-reflexpro.u-ga.fr/docs/sciedu-general/fr/latest/mythes_education.html?highlight=mythes) (Dessus & Charroud, 2016)



---
## Autres "mythes" récurrents

* **Le numérique favorise l'autonomie des apprenants** :arrow_right: faux, l'autonomie est une compétence à acquérir pour pouvoir apprendre avec le numérique et non le contraire. (Lehmann, 2014).

* **La lecture sur écran réduit les compétences de lecture traditionnelle** :arrow_right: faux, la lecture numérique fait appel à des compétences partagées avec la lecture papier. (Baccino, 2004) (Britt & Rouet, 2012)

---
## Le pire de tous

* Le numérique c'est bien pour les apprentissages car c'est ludique.

:warning: Le mot "ludique" employé dans le sens faible, désignant le fait que l'environnement est amusant, **n'a pas de rapport établi avec les apprentissages** (Vogel et al., 2006) :warning:

Il faut bien différencier le jeux qui est un moteur d'apprentissage de l'aspect ludique.



---

### :pencil2: TP : Un bilan intermédiaire (durée 1 minute)

Reprenez la liste effets positifs/effets négatifs que vous avez rédigée, y a-t-il des modifications ?

---
# :three: Des effets positifs
##  et oui, il y en a ....
 

###### 1  - Numérique et santé
###### 2  - Numérique et apprentissages : mythes et légendes les plus courants
#### 3  - Des effets positifs 
###### 4 -  Un esprit critique nécessaire





---
## L’utilisation du numérique ***peut*** avoir un effet positif sur l’apprentissage

- **Pour accéder à des informations**

- **Pour communiquer**

- **Pour s'entraîner sur des tâches ciblées**

> (J-PAL, 2019)

---
## L’utilisation du numérique ***peut*** avoir un effet positif sur l’apprentissage
#### :arrow_right: Il est indispensable d'analyser ses pratiques au regard des résultats de la recherche si l'on veut éviter les problèmes....
# 
###  :warning: Les effets que l'on pense positifs peuvent :warning: rapidement se révéler négatifs :

Outre une perte de temps, d'énergie et de crédibilité: le risque principal est de générer une **augmentation de la différence entre les élèves**
(Zheng, 2016)

---
## Liste des fonctions pédagogiques du numérique 

Téléchargez la liste des fonctions pédagogiques du numérique (Tricot, 2020) :

[https://link.infini.fr/liste](https://cloud.univ-grenoble-alpes.fr/s/FKNkiGw6kke38tE)

ou :
![130%](images/img-effets/QR_peda_num.png)

Conservez cette liste, elle vous sera utile durant tout la formation.

---
# Accéder à l'information

Fonctions pédagogiques du numérique concernées :
* 8.Rechercher de l’information
* 14.Apprendre à distance

---
## 3.1 Accéder à l'information
### Des points positifs:
* Information disponible tout le temps et en tous lieux (ou presque).
* Quantité d'information 
* Supports variés 

### Des points négatifs:
* Quantité d'information :arrow_right: tri, validité, perte de temps
* Supports variés :arrow_right: nécessite matériels et applications 
* Informations payantes :arrow_right: frustration
* Recherche sur internet :warning: lecture

---
# Communiquer

Fonctions pédagogiques du numérique concernées :
* 1.Présenter de l’information
* 2.Lire et comprendre un texte, apprendre à lire
* 3.Écouter un document sonore, écouter un texte sonorisé
* 4.Regarder / lire un document multimédia
* 5.Regarder une vidéo, une animation
* 8.Rechercher de l’information
* 14.Apprendre à distance

---
## 3.2 Communiquer

### Des points positifs :
* Sources variées
* Attention

### Des points négatifs 
* Sources variées
* Attention

# :rage:


---
## 3.2.1 Quelques explications pour limiter les mésusages de la communication avec le numérique
* Multiplier les sources dans un même document, bonne ou mauvaise idée ?
* Écran et charge cognitive
* Capter et maintenir l'attention
* Vidéo et apprentissages...



---
### Multiplier les sources dans un même document, bonne ou mauvaise idée ?


> On raconte qu'il suffit de présenter à l'apprenant une même information sous différents formats pour être efficace.




### Que dit la recherche ?
> Effects of prior knowledge on learning from different compositions of representations in a mobile learning environment (Liu, Lin & Paas, 2014)

---
![140%](images/img-effets/Effets-liu11.png)

Texte + Photo

---
![140%](images/img-effets/Effets-liu12.png)

Texte + Photo + Exemple réel

---
![140%](images/img-effets/Effets-liu13.png)

Texte + Schéma + Exemple réel


---
### À votre avis quel groupe a le mieux appris ?

Groupe 1 : Texte + Image
Groupe 2 : Texte + Image + Plante réelle
Groupe 3 : Texte + Schéma + Plante réelle



---
### Résultats
Les meilleurs apprentissages ont été obtenus par :
# Le premier groupe (texte+image)

---

### Explication rationnelle
La multiplication des sources d’information (texte + image + objet réel) a provoqué une division de l’attention qui a gêné l’apprentissage des élèves.

---

# :exclamation: À retenir :exclamation:

**Ne vous laissez pas entraîner par la facilité à multiplier les sources avec le numérique.**

Utilisez un document avec 2 sources :
* **une source verbale** (écrite ou sonore)
* **une source picturale** (fixe ou animée)




***Sinon il sera profitable seulement aux meilleurs élèves.***

---
### 3.2.2 Écran et charge cognitive


#### Que dit la recherche ?
> La charge cognitive dans l’apprentissage (Charroud & Dessus, 2016)



---
#### Régle de base 
### Les sources doivent être épurées.
* Éviter les distracteurs 
* Utiliser des représentations simples et cohérentes
* Éliminer autant que possible la redondance


---

#### Apprentissage de règle logique : écran vs réelle

> Prefrontal cortex and executive function in young children. (Moriguchi & Hiraki, 2013). 

Adulte face à un écran (Neurones miroirs OK) :
:arrow_right: Apprentissage = Inférence de la règle

Élève face à un écran (Neurones miroirs en développement) :
:arrow_right: Apprentissage = Inférence de la règle + Interprétation empathique


> **L'apprentissage va demander beaucoup d'effort à l'élève.** (Ferrari, 2014) 

---
#### Ne pas oublier l'interface...


![30%](images/img-effets/Effets-geogebra.jpg)



Il faut respecter la simplicité d'utilisation, donnant accès aux fonctions facilitant les apprentissages scolaires. (Tijus, 2006)
    

---
### 3.2.3 Capter et maintenir l'attention

#### L’attention
– Alerte (quand faire attention)
– Orientation (à quoi faire attention)
– Contrôle exécutif (comment faire)

> (Dehaene, 2018)

---

*Les élèves sont-ils égaux devant un document numérique ? Même bien conçu...*


### Que dit la recherche ?

> Eye-movement patterns (Mason, Tornatora, & Pluchino, 2013).

---
![110%](images/img-effets/Effets-masson1.png)

---
![110%](images/img-effets/Effets-masson2.png)

---
### Résultats :

### Les meilleurs apprentissages sont réalisés pas le 3ème type d'élève.

Plus les apprenants réalisent de traitements d'intégration entre la source verbale (texte ou son) et la source picturale (illustrations) plus ils apprennent.

---
![110%](images/img-effets/Effets-masson2.png)

---

## bilan :
* Proposer 2 sources dans un document et pas une seule...

* :warning: À partir d'un même document, les apprenants n'ont pas la même stratégie d'apprentissage, **seuls les meilleurs profitent du document**.


---
### Comment inciter les transitions entre les sources ?

> Utilisation d'un document avec commentaires sonores (Jamet, 2014).

![150%](images/img-effets/Effets-jamet14.png)

---
# :exclamation: À retenir :exclamation:

Avec le **guidage**, les apprenants accordent davantage d’attention (temps de fixation total) aux informations pertinentes grâce à la signalisation.

### Résultats :
* Des effets sur la complétude et la rétention (Mémorisation)
	
* Mais pas d'effet sur les apprentissages profonds (Compréhension)





---

### 3.2.4 Vidéo et apprentissages...

Pour qu'une vidéo/animation soit efficace pour les apprentissages, il faut :
* que l'apprenant ait un contrôle minimal sur le rythme de défilement d’une animation ou d’une vidéo;
* que l'appenant fasse des pauses dans le défilement (ou imposer les pauses dans le défilement)
* présenter de façon animée des informations elles-mêmes dynamiques;

Vérifiez bien les vidéos (même celles issues de sites institutionnels) avant de les utiliser....

(Biard, Cojean & Jamet, 2018)(Cojean, Jamet2017)



---
### :exclamation: À retenir :exclamation:

* Des ressources épurées.
* Des vidéos contrôlables par l'élève, avec des pauses.
* Ne pas oublier que la perception de l'élève est différente de celle de l'adulte (neurones miroirs).
* Et enfin il ne faut pas que l'interface soit un obstacle aux apprentissages

---

### :pencil2: TP (durée 5 minutes)
###  Vos documents numériques sont-ils efficaces?
* Regroupez vous par discipline (ou non) en essayant de faire des groupes de 4 ou 5 max.
* Choisissez un ou des documents numériques que vous utilisez en classe et faites en une analyse critique et essayez de proposer un ou des améliorations 

---
# S'entrainer
* 10.S’entraîner
* 15.Évaluer, s’autoévaluer, suivre les progrès et les difficultés des élèves
* 23.Apprendre à faire sur simulateur ou en réalité virtuelle
* 24.Mémoriser, apprendre par cœur


---
## 3.3 S'entrainer (évaluer, s'auto évaluer, mémoriser)

### Rappel sur les rétroactions pour l'appentissage
* Nécessité de feedback
* Feedback, si possible, immédiat 
* Si la réponse comporte une erreur :arrow_right: remédiation
* Si la réponse est correcte (tant que la connaissance n’est pas parfaite, le cerveau continue d’apprendre) => nécessité de surapprentissage (réviser, remettre à l’épreuve…)

(Dehaene, 2018)

---


### Entrainement sur des tâches ciblées
![10%](images/img-effets/Effets-charge.jpg)

**Automatisation :**
Systématisation :arrow_right: Exerciseurs :arrow_right: **Tuteurs intelligents**

---
## Aider des élèves qui ont de troubles de l’apprentissage ou de la cognition numérique
<!-- _class: t-90 -->

Ex. Opération simple ( ex. 5+9=14)  sous observation I.R.M
* Dyscalculiques et typiques utilisent les mêmes zones cérébrales.
* Dyscalculique recrutent beaucoup plus ces zones.

:arrow_right: entraînement avec feedback immédiat pendant trois semaines sur les éléments simples (ex. 3+4=7),  pas besoin de compter ou de réfléchir mais simplement de mémoriser.
:arrow_right: Très forte réduction de la différence entre les dyscalculiques et les typiques, normalisation du réseau neurologique qui est impliqué dans la réalisation de petites opérations
(Iuculano & al. 2015)


---
### Pourquoi ça marche ?

* Feedback immédiat (renforcement).
* Patience infinie.... 
* et d'autres avantages (traces, adapatation, souplesse horaire...)

#### I.A. et systèmes d'entrainement :arrow_right: TD "S'entraîner avec le numérique".



---
# :exclamation: À retenir :exclamation:


### Les exerciseurs et/ou tuteurs intelligents donnent de bons résultats MAIS qui ne dépassent pas le tutorat humain!
(Kulik, & Fletcher 2016)

#

*...Pour votre employeur, il est plus facile de multiplier les tuteurs numériques que les tuteurs humains...*




---

## 3.4.2 Mémoire et numérique 
*Encore un entrainenement*
## La consolidation
– Passer d’un traitement lent, conscient, avec effort (sous contrôle du cortex préfrontal) à un fonctionnement rapide, inconscient, automatique :
– Répéter :arrow_right: Automatisation
– Libérer des ressources cognitives pour éviter le « goulot d’étranglement cognitif »

> (Dehaene, 2018)

---
## Consolider, un sujet déjà assez ancien

![170%](images/img-effets/Effets-ebbinghaus.jpg)

---
### Mémoire : quelques rappels sur l'anatomie cérébrale



![](images/img-effets/Effets-myelinreal.jpg)

---
### Le processus de myélinisation

![50%](images/img-effets/Effets-myelinzoom.jpg)

---
### Importance de cette couche de Myéline

* L’épaisseur du gainage de myéline est en relation directe avec nos aptitudes et nos performances.
* L'épaisseur de la myéline dépend du nombre et de l'espacement des sollicitations (Rappel expansé)
(Fields & Bukalo, 2020)(Pan & al., 2020)(Kern & Kheirbek,2022)

:arrow_right: Le numérique permet de poser des questions **"au bon moment"** en s'adaptant à la physiologie

Exemple **non exhaustif** : L'application ANKI

---


![100%](images/img-effets/Effets-anki_ex.jpg)



---
# :four: Un esprit critique nécessaire
###### 1  - Numérique et santé
###### 2  - Numérique et apprentissages : mythes et légendes les plus courants
###### 3  - Des effets positifs 
#### 4 -  Un esprit critique nécessaire

---

# 4 - Un esprit critique nécessaire

Face au numérique, défendons plutôt une posture critique ayant du sens, une
finalité, de l’objectivité et de la rigueur. 

Cela implique d’aborder les technologies avec **méfiance et scepticisme 
mais toujours dans un esprit constructif** plutôt qu’avec cynisme.

> (Selwyn, 2018)

---

## Un exemple d'usage necessitant une posture critique
### Un entrainement intéressant 
### 
###  (ou pas)
### 

---
## Le numérique pour entrainer des compétences transversales nécessaires aux apprentissages :

* Améliorer la vision
* Réduire les déficits d'attention
* Favoriser le traitements multitâches


---
## Que dit la recherche ?
### :warning: C'est très contre-intuitif
Certains jeux vidéo souvent considérés comme les pires (les jeux de tir à la première personne) font partie des usages qui permettent le plus d'entraîner des circuits cérébraux impliqués dans des tâches transversales nécessaires aux apprentissages....

---
## Dans le détail 1/3
Les jeux vidéo d'action permettent d'améliorer la vision  en entraînant :

* l'identification de petits détails au milieu du désordre
* la capacité à distinguer et identifier les niveaux de gris

> (Eichenbaum, 2014)


---
## Dans le détail 2/3
Les jeux vidéo d'action permettent de réduire les déficits d'attention (Green, 2003) :	
* Résolution de conflits cognitifs plus rapide
* Augmentation de la capacité à suivre plus d'objets dans un environnement évolutif (3-4 objets pour un non joueur, jusqu'à 7 objets pour un joueur...)

IRM :arrow_right: des changements visibles sur des réseaux corticaux des joueurs:
	* Cortex pariétal qui contrôle l’attention et l’orientation
	* Cortex frontal qui nous aide à maintenir notre attention (inhibition)
	* Cortex cingulaire antérieur qui contrôle comment nous affectons et régulons notre attention et résolvons les conflits.

---
## Dans le détail 3/3
Les jeux vidéo d'action permettent d'entraîner aux traitements multitâches  (Bavelier, 2018).

Les gens qui font des jeux vidéo d’action sont très très bons, ils passent d’une tâche à l’autre très rapidement avec un faible coût cognitif.
Les gens qui pratiquent uniquement le multitâche multimédia (utilisation simultanée d'un navigateur web, en écoutant de la musique, en gardant un œil sur son smartphone...) sont très nettement moins performants.

---
## :warning: Que retenir de l’effet des jeux vidéo :
* La sagesse collective n’a pas de valeur :arrow_right: c'est très contre-intuitif
* Tous les médias ne naissent pas tous égaux, ils ont des effets totalement différents sur différents aspects de la cognition de la perception et de l’attention :arrow_right: chaque jeu doit faire l'objet de test...
* Ces jeux vidéo ont des effets puissants pour la plasticité cérébrale, l’attention, la vision.
## Mais aussi pleins d'effets non désirables (santé, psychologie, sociale) , il faudrait les consommer avec modération et au bon âge... 


---
# 
## Les jeux vidéo pour apprendre, pour de vrai et sans effets négatifs... un jour peut-être ?
Il faudrait :
* Comprendre quels sont les "bons" ingrédients pour produire des effets positifs en termes d'apprentissages. (Brocolis)
* Réaliser des produits attirants auxquels on ne peut pas résister. (Chocolat)
* Réunir les deux :arrow_right: ce n'est pas simple (Le brocoli au chocolat ce n’est pas terrible)

### C'est en cours d'exploration....



---
# 

## Avec le numérique, il est nécessaire de toujours évaluer le rapport bénéfices/risques 

![100%](images/img-effets/Effets-balance.jpeg)

---

## Eviter le numérique à tout prix !

![100%](images/img-effets/Effets-tapis.png)

Réaliser exactement la même tâche avec ou sans technologie :arrow_right: Très faible  intérêt !




---


#  Conclusion
#### Le numérique peut être efficace pour les apprentissages... mais pas dans tous les cas, et pas à tous les âges.

#### Avancez avec prudence, essayez de pas vous laisser entraîner par la connaissance commune, mais appuyez-vous sur la connaissance scientifique !

#### Gardez une posture critique et contructive

#### Pensez à la santé de vos élèves (et à la votre!)




---
## À méditer :

Le numérique n’est pas une boîte à outils, une valise d’applications et de logiciels qui viennent agrémenter l’action pédagogique et les processus d’apprentissage ou se substituer à d’autres méthodes d’enseignement-apprentissage alors jugées moins innovantes. 

Le numérique est un objet complexe, englobant des acceptions multiples, et caractérisant des objets et outils dont il ne suffit pas de se saisir, de façon pragmatique, pour comprendre le monde et exercer un esprit critique."


> Extrait d'un rapport publié par le Centre national d’étude des systèmes scolaires (Cnesco) sur la thématique : Numérique et apprentissages scolaires. (Cordier, 2020)

---
# Références - Partie numérique et santé

<!-- _class: t-50 -->

Abdallah, M. A. E., & Harrad, S. (2018). Dermal contact with furniture fabrics is a significant pathway of human exposure to brominated flame retardants. Environment international, 118, 26-33.
Arns, M., Kooij, J. S., & Coogan, A. N. (2021). Identification and management of circadian rhythm sleep disorders as a transdiagnostic feature in child and adolescent psychiatry. Journal of the American Academy of Child & Adolescent Psychiatry, 60(9), 1085-1095.
Bediou, B., Rodgers, M. A., Tipton, E., Mayer, R. E., Green, C. S., & Bavelier, D. (2023). Effects of action video game play on cognitive skills: A meta-analysis.
Bellissimo, N., Pencharz, P. B., Thomas, S. G., & Anderson, G. H. (2007). Effect of television viewing at mealtime on food intake after a glucose preload in boys. Pediatric research, 61(6), 745-749.
Beyens, I., Valkenburg, P. M., & Piotrowski, J. T. (2018). Screen media use and ADHD-related behaviors: Four decades of research. Proceedings of the National Academy of Sciences, 115(40), 9875-9881.
Bioulac, S., Charroud, C., Pellencq, C. (2022). Le numérique face aux troubles du déficit de l’attention et à l’Hyperactivité. Consulté à https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/cours_TDA-H.html
Boyland, E. J., Nolan, S., Kelly, B., Tudur-Smith, C., Jones, A., Halford, J. C., & Robinson, E. (2016). Advertising as a cue to consume: a systematic review and meta-analysis of the effects of acute exposure to unhealthy food and nonalcoholic beverage advertising on intake in children and adults. The American journal of clinical nutrition, 103(2), 519-533.
Braune‐Krickau, K., Schneebeli, L., Pehlke‐Milde, J., Gemperle, M., Koch, R., & von Wyl, A. (2021). Smartphones in the nursery: Parental smartphone use and parental sensitivity and responsiveness within parent–child interaction in early childhood (0–5 years): A scoping review. Infant Mental Health Journal, 42(2), 161-175.
Bioulac, S., Charroud, C., Pellencq, C. (2022). Le numérique face aux troubles du déficit de l’attention et à l’Hyperactivité. Consulté à https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/cours_TDA-H.html
Cao, K., Wan, Y., Yusufu, M., & Wang, N. (2020). Significance of outdoor time for myopia prevention: a systematic review and meta-analysis based on randomized controlled trials. Ophthalmic research, 63(2), 97-105.
Charroud, C., & Choucroune, P. (2016). Numérique, wifi, téléphone, les ondes à l’école. Consulté à https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/cours_Ondes.html .
Corkin, M. T., Henderson, A. M., Peterson, E. R., Kennedy-Costantini, S., Sharplin, H. S., & Morrison, S. (2021). Associations between technoference, quality of parent-infant interactions, and infants’ vocabulary development. Infant Behavior and Development, 64, 101611.

---

<!-- _class: t-50 -->

Courbet, D., & Fourquet-Courbet, M. P. (2019). Usages des écrans, surpoids et obésité. Obésité, 14(3), 131-138.
Dauvilliers, Y. (2019). Les troubles du sommeil. Elsevier Health Sciences.
David, D., Giannini, C., Chiarelli, F., & Mohn, A. (2021). Text neck syndrome in children and adolescents. International journal of environmental research and public health, 18(4), 1565.
De Vasconcelos, A. P., Andreeva, V., Boarini, S., Bourdieu, A., Burkhardt, J. M., Chaumet-Riffaud, P., ... & Roth-Delgado, O. (2023). Avis de l'Anses relatif aux lignes directrices visant à limiter l’exposition des personnes aux champs électromagnétiques (100 kHz–300 GHz) (Doctoral dissertation, Anses).
Duclos, M. (2021). Épidémiologie et effets sur la morbi-mortalité de l’activité physique et de la sédentarité dans la population générale. Revue du Rhumatisme Monographies, 88(3), 177-182.
Fischer, J. P. (2023). L’utilisation précoce des écrans est-elle néfaste? Une première réponse avec la cohorte Elfe. Psychologie Française, 68(1), 55-70.
Flannery, J. S., Burnell, K., Kwon, S. J., Jorgensen, N. A., Prinstein, M. J., Lindquist, K. A., & Telzer, E. H. (2024). Developmental changes in brain function linked with addiction-like social media use two years later. Social Cognitive and Affective Neuroscience, 19(1), nsae008.
Foreman, J., Salim, A. T., Praveen, A., Fonseka, D., Ting, D. S. W., He, M. G., ... & Dirani, M. (2021). Association between digital smart device use and myopia: a systematic review and meta-analysis. The Lancet Digital Health, 3(12), e806-e818.
Grzybowski, A., Kanclerz, P., Tsubota, K., Lanca, C., & Saw, S. M. (2020). A review on the epidemiology of myopia in school children worldwide. BMC ophthalmology, 20, 1-11.
Haarman, A. E., Enthoven, C. A., Tideman, J. W. L., Tedja, M. S., Verhoeven, V. J., & Klaver, C. C. (2020). The complications of myopia: a review and meta-analysis. Investigative ophthalmology & visual science, 61(4), 49-49.
Hirshkowitz M, Whiton K, Albert SM, et al. National Sleep Foundation's sleep time duration recommendations: methodology and results summary. Sleep Health. 2015 Mar;1(1):40-43.
Howie, E. K., Joosten, J., Harris, C. J., & Straker, L. M. (2020). Associations between meeting sleep, physical activity or screen time behaviour guidelines and academic performance in Australian school children. BMC public health, 20, 1-10.
Jones-Jordan, L. A., Sinnott, L. T., Cotter, S. A., Kleinstein, R. N., Manny, R. E., Mutti, D. O., ... & Zadnik, K. (2012). Time outdoors, visual activity, and myopia progression in juvenile-onset myopes. Investigative ophthalmology & visual science, 53(11), 7169-7175.
Lanningham-Foster, L., Jensen, T. B., Foster, R. C., Redmond, A. B., Walker, B. A., Heinz, D., & Levine, J. A. (2006). Energy expenditure of sedentary screen time compared with active screen time for children. Pediatrics, 118(6), e1831-e1835.

---

<!-- _class: t-50 -->

Li, W., Liu, Q., Deng, X., Chen, Y., Liu, S., & Story, M. (2017). Association between obesity and puberty timing: a systematic review and meta-analysis. International journal of environmental research and public health, 14(10), 1266.
Lin, Y. J., Chiu, Y. N., Wu, Y. Y., Tsai, W. C., & Gau, S. S. F. (2022). Developmental changes of autistic symptoms, ADHD symptoms, and attentional performance in children and adolescents with autism spectrum disorder. Journal of autism and developmental disorders, 1-15.
Madigan, S., McArthur, B. A., Anhorn, C., Eirich, R., & Christakis, D. A. (2020). Associations between screen use and child language skills: a systematic review and meta-analysis. JAMA pediatrics, 174(7), 665-675.
Madore, K. P., Khazenzon, A. M., Backes, C. W., Jiang, J., Uncapher, M. R., Norcia, A. M., & Wagner, A. D. (2020). Memory failure predicted by attention lapsing and media multitasking. Nature, 587(7832), 87-91.
Marciano, L., & Camerini, A. L. (2021). Recommendations on screen time, sleep and physical activity: associations with academic achievement in Swiss adolescents. Public health, 198, 211-217.
Massaroni, V., Delle Donne, V., Marra, C., Arcangeli, V., & Chieffo, D. P. R. (2023). The Relationship between Language and Technology: How Screen Time Affects Language Development in Early Life—A Systematic Review. Brain Sciences, 14(1), 27.
Matamoros, E., Ingrand, P., Pelen, F., Bentaleb, Y., Weber, M., Korobelnik, J. F., ... & Leveziel, N. (2015). Prevalence of myopia in France: a cross-sectional analysis. Medicine, 94(45), e1976.
Miller, J., Mills, K. L., Vuorre, M., Orben, A., & Przybylski, A. K. (2023). Impact of digital screen media activity on functional brain organization in late childhood: evidence from the ABCD study. cortex, 169, 290-308.
Mounier-Vehier, C., Nasserdine, P., & Madika, A. L. (2019). Stratification du risque cardiovasculaire de la femme: optimiser les prises en charge. La Presse Médicale, 48(11), 1249-1256.
Mundy, L. K., Canterford, L., Hoq, M., Olds, T., Moreno-Betancur, M., Sawyer, S., ... & Patton, G. C. (2020). Electronic media use and academic performance in late childhood: A longitudinal study. PLoS One, 15(9), e0237908.
O'Donnell, M. J., Chin, S. L., Rangarajan, S., Xavier, D., Liu, L., Zhang, H., ... & Yusuf, S. (2016). Global and regional effects of potentially modifiable risk factors associated with acute stroke in 32 countries (INTERSTROKE): a case-control study. The lancet, 388(10046), 761-775.
Ophir, Y., Rosenberg, H., Tikochinski, R., Dalyot, S., & Lipshits-Braziler, Y. (2023). Screen Time and Autism Spectrum Disorder: A Systematic Review and Meta-Analysis. JAMA Network Open, 6(12), e2346775-e2346775.
Pedersen, J., Rasmussen, M. G. B., Sørensen, S. O., Mortensen, S. R., Olesen, L. G., Brønd, J. C., ... & Grøntved, A. (2022). Effects of limiting recreational screen media use on physical activity and sleep in families with children: a cluster randomized clinical trial. JAMA pediatrics, 176(8), 741-749.

---

<!-- _class: t-50 -->

Ramer, J. D., Santiago-Rodríguez, M. E., Vukits, A. J., & Bustamante, E. E. (2022). The convergent effects of primary school physical activity, sleep, and recreational screen time on cognition and academic performance in grade 9. Frontiers in Human Neuroscience, 16, 1017598.
Santos, R. M. S., Mendes, C. G., Marques Miranda, D., & Romano-Silva, M. A. (2022). The association between screen time and attention in children: a systematic review. Developmental neuropsychology, 47(4), 175-192.
Sauce, B., Liebherr, M., Judd, N., & Klingberg, T. (2022). The impact of digital media on children’s intelligence while controlling for genetic differences in cognition and socioeconomic background. Scientific reports, 12(1), 7720.
Sheppard, A. L., & Wolffsohn, J. S. (2018). Digital eye strain: prevalence, measurement and amelioration. BMJ open ophthalmology, 3(1), e000146.
Supper, W., Guay, F., & Talbot, D. (2021). The relation between television viewing time and reading achievement in elementary school children: A test of substitution and inhibition hypotheses. Frontiers in Psychology, 12, 580763.
Tansel, B. (2022). PFAS use in electronic products and exposure risks during handling and processing of e-waste: A review. Journal of Environmental Management, 316, 115291.
Wang, J., Lou, Y., Mo, K., Zheng, X., & Zheng, Q. (2023). Occurrence of hexabromocyclododecanes (HBCDs) and tetrabromobisphenol A (TBBPA) in indoor dust from different microenvironments: levels, profiles, and human exposure. Environmental Geochemistry and Health, 45(8), 6043-6052.
Wu, P. C., Tsai, C. L., Wu, H. L., Yang, Y. H., & Kuo, H. K. (2013). Outdoor activity during class recess reduces myopia onset and progression in school children. Ophthalmology, 120(5), 1080-1085.


---
# Références - Partie numérique et apprentissages 

<!-- _class: t-50 -->


Bachelard, G. (1953). Le matérialisme rationnel. Paris: Presses universitaires de France.
Bavelier, D., Bediou, B., & Green, C. S. (2018). Expertise and generalization: Lessons from action video games. Current opinion in behavioral sciences, 20, 169-173.
Biard, N., Cojean, S., & Jamet, E. (2018). Effects of segmentation and pacing on procedural learning by video. Computers in Human Behavior, 89, 411–417. https://doi.org/10.1016/j.chb.2017.12.002 
Bioulac, S., Baillieul, S., Charroud, C. (2023). Conférence sommeil et apprentissages. Consulté à https://videos.univ-grenoble-alpes.fr/video/26283-conference-sommeil-et-apprentissages/d6a9d07e4d2b514fe7537016dca0e758db529efc68a90b6f8b9375604bd52d1f/
Borst, G. (2019). Écrans et développement de l’enfant et de l’adolescent. Futuribles, (6), 41-49.
Britt, M. A., & Rouet, J. F. (2012). Learning with multiple documents: Component skills and their acquisition. Enhancing the quality of learning: Dispositions, instruction, and learning processes, 276-314.
Charroud, C., & Dessus, P. (2016). La charge cognitive dans l’apprentissage. Consulté à https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/chargecog.html .
Cojean, S., & Jamet, E. (2017). Facilitating information-seeking activity in instructional videos: The combined effects of micro- and macroscaffolding. Computers in Human Behavior, 74, 294–302. https://doi.org/10.1016/j.chb.2017.04.052
Cordier, A. (2020). Des usages juvéniles du numérique aux apprentissages hors la classe. Paris : Cnesco.
Dehaene, S. (2018). Apprendre!: Les talents du cerveau, le défi des machines. Odile Jacob.
Dogusoy-Taylan, B., & Cagiltay, K. (2014). Cognitive analysis of experts’ and novices’ concept mapping processes: An eye tracking study. Computers in human behavior, 36, 82-93.
Eichenbaum, A., Bavelier, D., & Green, C. S. (2014). Video games: Play that can do serious good. American Journal of Play, 7(1), 50-72.
Ferone, G. (2019). Numérique et apprentissages : prescriptions, conceptions et normes d’usage. Recherches en Éducation, 35, 63–75.
Ferrari P. F. (2014) « The neuroscience of social relation. A comparative-based approach to empathy and to the capacity of evaluating others’action value », Behavior, 151.
Green, C. S., & Bavelier, D. (2003). Action video game modifies visual selective attention. Nature, 423(6939), 534-537.

---

<!-- _class: t-50 -->

Houart, M. (2017). L’apprentissage autorégulé: quand la métacognition orchestre motivation, volition et cognition. Revue internationale de pédagogie de l’enseignement supérieur, 33(33-2).
Hutton, J. S., Dudley, J., Horowitz-Kraus, T., DeWitt, T., & Holland, S. K. (2020). Associations between screen-based media use and brain white matter integrity in preschool-aged children. JAMA pediatrics, 174(1), e193869-e193869.
Jamet, E. (2014). An eye-tracking study of cueing effects in multimedia learning. Computers in Human Behavior, 32, 47-53.
J-PAL Evidence Review. 2019. “Will Technology Transform Education for the Better?” Cambridge, MA: Abdul Latif Jameel Poverty Action Lab.
Kulik, J. A., & Fletcher, J. D. (2016). Effectiveness of intelligent tutoring systems: a meta-analytic review. Review of Educational Research, 86(1), 42-78.
Kersey, A. J., & James, K. H. (2013). Brain activation patterns resulting from learning letter forms through active self-production and passive observation in young children. Frontiers in psychology, 4, 567.
Lacelle, N., & Lebrun, M. (2016). La formation à l’écriture numérique: 20 recommandations pour passer du papier à l’écran. Revue de recherches en littératie médiatique multimodale, 3.
Lehmann, T., Hähnlein, I., & Ifenthaler, D. (2014). Cognitive, metacognitive and motivational perspectives on preflection in self-regulated online learning. Computers in human behavior, 32, 313-323.
Liu, T. C., Lin, Y. C., & Paas, F. (2014). Effects of prior knowledge on learning from different compositions of representations in a mobile learning environment. Computers & Education, 72, 328-338.
Mason, L., Tornatora, M. C., & Pluchino, P. (2013). Do fourth graders integrate text and picture in processing and learning from an illustrated science text? Evidence from eye-movement patterns. Computers & Education, 60(1), 95-109.
Mayer, C. P. (2009). Security and privacy challenges in the internet of things. Electronic Communications of the EASST, 17.
Milcent K, Gassama M, Dufourg MN, Thierry X, Charles MA, Bois C. Child health screening program in French nursery schools: Results and related socioeconomic factors. Front Pediatr. 2023 May 4;11:1167539. doi: 10.3389/fped.2023.1167539. PMID: 37215596; PMCID: PMC10192858.
Moriguchi, Y., & Hiraki, K. (2013). Prefrontal cortex and executive function in young children: A review of NIRS studies. Frontiers in Human Neuroscience, 7, Article 867
Salmerón, L., & García, V. (2011). Reading skills and children’s navigation strategies in hypertext. Computers in Human Behavior, 27(3), 1143-1151.
Sung, E., & Mayer, R. E. (2013). Online multimedia learning with mobile devices and desktop computers: An experimental test of Clark’s methods-not-media hypothesis. Computers in Human Behavior, 29(3), 639-647.

---

<!-- _class: t-50 -->

Selwyn, N. (2018). Approches critiques des technologies en éducation : un aperçu. Formation et profession, 27(3), 6-21.
Tijus, C., Poitrenaud, S., Bouchon-Meunier, B., & De Vulpillières, T. (2006). Le cartable électronique: sémantique de l'utilisabilité et aide aux apprentissages. Psychologie française, 51(1), 87-101.
Vogel, J. J., Vogel, D. S., Cannon-Browers, J., Browers, C. A., Muse, K., & Wright, M. (2006). Computer gaming and interactive simulations for learning: A meta-analysis. Journal of Educational Computing Research, 34. 229-243.
Wouters, P., & Van Oostendorp, H. (2013). A meta-analytic review of the role of instructional support in game-based learning. Computers & Education, 60(1), 412-425.
Zheng, B., Warschauer, M., Lin, C. H., & Chang, C. (2016). Learning in one-to-one laptop environments: A meta-analysis and research synthesis. Review of Educational Research, 86(4), 1052-1084


