---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# 
# UE Num801 "Culture numérique et apprentissage"
## Objectifs, organisation, attendus


<!-- page_number: true -->
<!-- footer: Pôle enseignement numérique INSPE - UGA - 2023 - 2024 - CC:BY-NC-SA -->

---
# L'UE Num801, c'est quoi?
* Des contenus théoriques sur les usages du numérique en situation d'apprentissage
* Des notions de littératie numérique
* Un travail d'analyse et de prospective sur l'usage du numérique en situation d'apprentissage

---
# Pourquoi?
* 	Pour être capable de se poser des questions sur les plus-values supposées des situations d'apprentissage intégrant le numérique lors de leur conception.
* 	Pour réduire le plus possible les risques juridiques, éthiques, physiologiques et effets négatifs en termes d'apprentissages induits par les usages du numérique.
* 	Pour être capable d'analyser objectivement une situation d'apprentissage déjà réalisée et pouvoir en proposer une ou des évolutions bénéfiques pour les élèves.


---
# Objectifs 
* UE Num801 :arrow_right: Etre capable de concevoir une situation d'apprentissage intégrant le numérique et justifier les éléments de conception. 
Le travail demandé dans cette UE pourra s’articuler avec un ou des travaux réalisés dans d’autres UE ou autour du mémoire et surtout il devra permettre de développer une partie de travail attendu pour répondre aux exigences du dispositif **PIX+EDU**.

* Hors UE Num801 (M1 ou M2) : Travail destiné à répondre aux exigences du dispositif **PIX+EDU**.

---
## Dispositif PIX+EDU
**:warning: À ce jour les textes officiels n'ont pas encore été publiés**

### Généralités 
* Prévision pour la rentrée 2024 : tous les étudiants sortant de master MEEF devont tenter de répondre aux exigences du dispositif PIX+EDU.
* Ce dispositif est destiné à attester d'un niveau de maitrise suffisant des compétences nécessaires pour utiliser le numérique en situation d'enseignement.
* :arrow_right: Délivrance d'une attestation à la sortie de l'INSPE.

---
## Dispositif PIX+EDU, 2 volets

#### Volet pratiques professionelles 
**Conception** d'une situation d'apprentissage intégrant le numérique (en M1 dans l'UE 801), puis **mise en oeuvre réelle** face à des apprenants et enfin analyse réflexive de cette mise oeuvre (Probablement en M2, à définir en fonction des textes officiels à paraître).

#### Volet automatisé 
Réalisation du parcours "Les essentiels" sur la plateforme PIX (initié en M1 dans l'UE 801).
Le parcours "les essentiels" est actuellement en test, il sera déployé dans les semaines à venir.


---

# Organisation UE Num801
#
### :arrow_right: Ce semestre en M1
#
### 16 H de TD

---
## Séances 1 à 5, apports thématiques  (10h)

* Outils et ressources numériques et problèmes juridiques associés (4h)
* Effets du numérique sur les apprentissages (4h)
* Enseigner avec le numérique : quelques zones à risque (2h)


:warning: Ordre aléatoire, formateurs différents



---
## Séances 6, 7 et 8 : “Conception d’une situation d’enseignement-apprentissage” (6h)
* La séance 6 comprend une brève évaluation des connaissances liées aux thématiques, puis les étudiants, par groupes de 2, imaginent une situation d'apprentissage intégrant le numérique et la décrivent. :warning: Cette situation d'apprentissage doit être réaliste et réalisable car il faudra la mettre en oeuvre soit en M1 soit en M2.
* La séance 7 est consacrée au travail de justification des éléments de conception mettant en application les apports des séances thématiques à la situation.
* La séance 8 est consacrée à une mise en commun des différents travaux de conception de situation d'apprentissage, pour les mutualiser et les améliorer par la discussion.

---
# Attendus et évaluation UE Num801

L’évaluation est sous la forme d’un contrôle continu. Elle sera constituée des deux évaluations suivantes :

* Evaluation de connaissances en séance 6 sur tout ou partie des apports théoriques, sous la forme d’un test automatisé (durée 30 min).
* Evaluation d’un dossier de 4 pages constitué en binôme au cours des séances 6, 7 et 8. La production sera déposée sur la plateforme pour évaluation **au plus tard le 17 mars 2024 à 23:59**



---
# Connexion à la plate-forme



* Adresse : <https://eformation.univ-grenoble-alpes.fr/>
* Connexion par « compte universitaire »

| Page d'accueil | Page de connexion |
| :-------------: | :---------: | 
|![40%](images/img-intro-801/Moodle1.png) |![30%](images/img-intro-801/moodle18.png) |


---
### Choix de l'établissement
Université Grenoble Alpes ou Université de Savoie
![50%](images/img-intro-801/Moodle2.png)

Authentification avec ses identifiants universitaires
![50%](images/img-intro-801/Moodle4.png)

---
## Recherche du cours
Retour à l'accueil du site
![50%](images/img-intro-801/Moodle5.png)
Descendre dans la page pour accéder au champ de recherche des cours
![50%](images/img-intro-801/Moodle6-M1-SD.png)
Sélectionner le cours Num801 : UE Culture numérique et apprentissage second degré
![40%](images/img-intro-801/Moodle7-M1-SD.png)

---
## Inscription par clé 
### Fournie par le formateur
![50%](images/img-intro-801/Moodle8-M1-SD.png)

Si vous vous êtes trompé-e de groupe, vous pouvez vous désinscrire et rentrer la bonne clé d'auto-inscription
![50%](images/img-intro-801/Moodle9.png)

---
