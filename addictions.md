---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)



# Addiction au numérique ?



### Mission éducation à la santé
   
 
<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - UGA - Prévention des conduites addictives - 2023 - CC:BY-NC-SA -->

---
# :one: Addiction : définition
 Moyen mnémotechnique : les 5 C
* #### **C**ompulsif 
* #### **C**hronique
* #### Perte de **C**ontrôle
* #### **C**raving ( = Envie irrésistible de consommer)
* #### **C**onséquences
> ### :warning: Sur une période de 12 mois = ADDICTION

---
# Écrans et addiction
#
### Etes-vous addicts aux écrans ?

:arrow_right: Test issu du DSM-5 et du programme SANPSY

Test silencieux, ne communiquez pas vos resultats.
Chaque fois que vous repondrez oui, comptez un point.


---
## Test 1/3

•	**Préoccupation** : Passez-vous beaucoup de temps à penser aux écrans, y compris quand vous n'en utilisez pas, ou à prévoir quand vous pourrez en utiliser à nouveau ?

•	**Sevrage** : Lorsque vous tentez d'utiliser moins d'écrans ou de ne plus en utiliser, ou lorsque vous n'êtes pas en mesure d'utiliser d'écran, vous sentez-vous agité, irritable, d'humeur changeante, anxieux ou triste ?

•	**Tolérance** : Ressentez-vous le besoin d'utiliser des écrans plus longtemps, d'utiliser des écrans plus excitants, ou d'utiliser du matériel informatique plus puissant, pour atteindre le même état d'excitation qu'auparavant ?

---
## Test 2/3

•	**Perte de contrôle** : Avez-vous l'impression que vous devriez utiliser moins d'écrans, mais que vous n'arrivez pas à réduire votre temps d'écran ?

•	**Perte d'intérêt** : Avez-vous perdu l'intérêt ou réduit votre participation à d'autres activités (temps pour vos loisirs, vos amis) à cause des écrans ?

•	**Poursuite malgré des problèmes** : Avez-vous continué à utiliser des écrans, tout en sachant que cela entrainait chez vous des problèmes tels que ne pas dormir assez, être en retard au collège, se disputer, négliger des choses importantes à faire... ?

---
## Test 3/3

•	**Mentir, dissimuler** : Vous arrive-t-il de cacher aux autres, votre famille, vos amis, à quel point vous utilisez des écrans, ou de leur mentir à propos de vos habitudes d'écrans ?

•	**Soulager une humeur négative** : Avez-vous utilisé des écrans pour échapper à des problèmes personnels, ou pour soulager une humeur indésirable (sentiments de culpabilité, d'anxiété, de dépression) ?

•	**Risquer ou perdre des relations ou des opportunités** : Avez-vous mis en danger ou perdu une relation amicale ou affective importante, des possibilités d'étude à cause des écrans ?

---
## Bilan du test

Si vous avez un **score égal à 5 points ou plus** alors votre comportement sera médicalement qualifié « d'addiction aux écrans ».

L'addiction est une maladie qui peut (et qui doit) être prise en charge.

---
## L'addiction aux écrans est-elle courante ?

* Les études actuelles montrent que **l'addiction au sens médical est relativement rare** parmi les adolescents et les adulte, elle concernerait  1,7% de la population.

* Mais 44,7% de la population présenterait au moins l'un des neuf critères recherchés :arrow_right: **usagers avec problèmes, mais sans addiction.** :arrow_right: il faut être vigilant.

(Alexandre, Auriacombe, Boudard, 2022)

