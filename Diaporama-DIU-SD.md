---
marp: true
theme: aqua
autoscale: true

---

![w:400](images/logo-espe-uga.svg)

# Module numérique 1 et 2
## DIU Second degré (SD)

INSPÉ / UGA -2022

---
<!-- paginate: true -->
<!-- footer: Équipe de formateurs(trices) au numérique - Inspé-UGA - 2022-23 - CC:BY-NC-SA -->


# Présentation

Téléchargeable ici : 
<https://link.infini.fr/dia-diu-sd> ou bien
<https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/Diaporama-DIU-SD.pdf>

---

# Objectifs du module

* Être capable de se poser des questions sur les plus‑values supposées des situations d'apprentissage intégrant le numérique lors de leur conception.
* Réduire le plus possible les risques juridiques, éthiques, physiologiques et effets négatifs en termes d'apprentissages induits par les usages du numérique.
* Être capable d'analyser objectivement une situation d'apprentissage déjà réalisée et pouvoir en proposer une ou des évolutions bénéfiques pour les élèves.

---

# Méthode

* *Via* une réflexion individuelle sur une situation d'apprentissage utilisant le numérique.
* Cette situation sera analysée en vous aidant des apports délivrés au cours de 5 séances thématiques :
  * Effets du numérique sur les apprentissages
  * Évaluer et s’auto‑évaluer avec le numérique
  * Production et usages de ressources numériques
  * Numérique et juridique
  * Enseigner avec le numérique : quelques zones à risque

---

## Processus

![w:600](images/diaporama-DIU-SD/ProcessusAnalyse.png)

---

# Modalités d’évaluation

* Semestre 1 : Assiduité
* Semestre 2 : Test de connaissances (QCM) + Support de présentation d’une situation analysée (poster)

---

# Plan des séances

* **Séances 1 à 5** : Apports thématiques (2 h en présence) suivi d’un travail en autonomie (1 h à distance)
* **Séance 6** : Présentation des situations analysées suivant les cinq thématiques et échanges collectifs, puis test de connaissances

---

# Travail en autonomie

* A la fin de la première séance : choisir, **individuellement**
, une situation à analyser
* À la fin de chaque séance : analyser cette dernière suivant la thématique abordée
* Avant la dernière séance : Produire un poster de synthèse et le déposer sur la plateforme de formation
  * Document explicatif de production d’un poster https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/atelier_poster_sci.html
* Modèle de poster à respecter

---

# En résumé

* Une formation appuyée sur des apports théoriques et pratiques
* Une évaluation fondée sur l'analyse d'une situation de classe

---

# Connexion au cours (1/4)

* Plate-forme : <https://eformation.univ-grenoble-alpes.fr/>
* Connexion par « compte universitaire »

| Page d'accueil | Page de connexion |
| :-------------: | :---------: |
|![width:400px](images/Moodle1.png) |![width:350px](images/moodle18.png) |

* Un accès anonyme sera possible pour les premières séances

---
# Connexion au cours (2/4)

Choix de l'établissement (UGA ou USMB)

![w:800](images/diaporama-DIU-SD/Moodle2.png)

Authentification avec ses identifiants universitaires 
![w:800](images/diaporama-DIU-SD/Moodle4.png)

---

# Connexion au cours (3/4)

* Retour à l'accueil du site ![w:300](images/Moodle5.png)
* Descendre dans la page pour accéder au champ de recherche des cours ![w:500](images/diaporama-DIU-SD/Moodle6module.png)
* Sélectionner le cours "Module numérique 1 et 2 SD" 
![w:300](images/diaporama-DIU-SD/Moodle7SDmodule.png)

---

# Connexion au cours (4/4)

* Vous devez vous auto-inscrire. La clé d’inscription est **ModuleNumeriqueSD**

![w:500](images/diaporama-DIU-SD/Moodle8SDmodule.png)

* Pour ceux qui n'ont pas encore leurs identifiants universitaires, le mot de passe pour l'accès anonyme est **ModuleNumeriqueSD**