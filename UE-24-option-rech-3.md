---
marp: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# UE 24 Option de recherche “Interactions enseignant-élèves“
## Philippe Dessus, Inspé, Univ. Grenoble Alpes

## Séance 3 — Les études à cas unique
##### Année universitaire 2024-25

![w:350](images/logo-espe-uga.png)

---
<!-- footer: Ph. Dessus • M1 PE • TD Option de recherche “Interactions enseignant-élèves • Inspé-UGA 2024-25 • CC:BY-NC-SA-->
<!-- paginate: true -->

# :three: 0. Introduction

- En contexte scolaire “naturel“, il est difficile de mener des expérimentations très contrôlées 
- Idéalement, il faudrait un grand nombre d'élèves, dans des classes différentes, **assignés aléatoirement** à différents groupess
- De plus, des problèmes éthiques se posent concernant les élèves assignés au groupe-contrôle, censé être moins efficace que le groupe expérimental
- Et les comparer à d'autres classes posent d'autres problèmes (sont-elles vraiment équivalentes ?)

---
# :three: 0. Une machine à remonter le temps ?

- Tester solidement l'effet d'une intervention sur la performance d'élèves est difficile 
- Si l'on observe des progrès suite à l'utilisation d'une méthode, comment s'assurer que ces progrès sont vraiment dus à l'intervention, ou bien à d'autres variables plus ou moins bien maîtrisées (développement des élèves, travail supplémentaire à la maison, etc.) ?
- Une manière (impossible !) serait d'utiliser une machine à remonter le temps : on compare la performance d'élèves après intervention, à celle sans intervention, une fois qu'on est remonté dans le temps au début de l'intervention (Ferry-Danini 2023)

---
# :three: 0. Le protocole “cas unique“

- Comme la recherche ne permet pas une telle machine, une solution à ce problème a été élaborée : le protocole “cas unique”
- Mesures multiples d'une ou plusieurs variable.s, une partie faite *avant* introduction d'une intervention pédagogique (phase *A*), l'autre *après* (phase *B*)
- Cette alternance sans/avec intervention peut être reproduite plusieurs fois (*ABABAB*...)
- Cela permet la mise au jour de relations causales entre un comportement donné (apprentissage) et les variables pouvant influer sur ce dernier (absence ou présence d'une intervention)

---
# :three: 1. Le protocole AB(ABAB...)

- Ce protocole permet de tester la sensibilité *d'une même  classe* aux changement entre une condition *A* (sans intervention, appelée aussi, niveau de base) et une condition *B* (mettant en œuvre une intervention éducative)
- On mesurera dans les 2 conditions un certain nombre de variables intéressantes (*e.g.*, performance à un test, nombre d'occurrences d'un comportement donné).
- Idéalement, on répétera cette alternance plusieurs fois
- On devrait observer un changement dans les valeurs des mesures à chaque changement de condition...
- ... même si les niveaux *A* successifs devraient s'améliorer (apprentissage des élèves)

---
# :three: 1. Le choix des indicateurs

- Les indicateurs (VD) doivent être les plus précis et univoques possible. Il est préférable qu'ils soient observables (*de visu*, *via* enregistrement, ou questionnaire) plutôt qu'inférés par jugement de l'enseignant.e concerné.e
- Quelques exemples : 
  - nb de prises de parole volontaires (ou leur durée) 
  - nb de mots écrits ou lus par un élève 
  - nb de jours d'absence, etc.
- Vérifier que les indicateurs ne sont pas colinéaires (*e.g.*, l'attention à la tâche et le dérangement des pairs covarient négativement)

---
# :three: 1. Un exemple plus précis

- **Intervention** : Effet d'une intervention améliorant la révision de la production écrite 
- **Tâche** : *Relire et corriger des erreurs d'une production écrite* : L'élève  lit la phrase qu'il a écrite à haute voix ou silencieusement et prend des mesures pour la corriger, si nécessaire 
- **Indicateurs** : Les exemples de correction sont :  la mise en majuscules de la première lettre d'une phrase, la correction d'un mot mal orthographié, l'ajout de ponctuation ou l'inversion de l'ordre des mots. Si l'élève ne parvient pas à repérer ou à corriger une erreur, l'enseignant note cette étape comme incorrecte

:book: Plavnick & Ferreri (2013)

---
# :three: 1. Quand prendre des mesures ?

- Pas nécessaire (et coûteux) de prendre des relevés de VD toute la journée
  - Constater, par observation, la fréquence du comportement observé
  - Réfléchir à des moments où la VD est  observable à une fréquence correcte (au moins 10-20 occurrences) et peut concerner le plus d'élèves possible
  - Se tenir au recueil des indicateurs dans ces moments, pour éviter l'influence de biais

---
# :three: 1. Quand changer de phase ?

- Afin, là encore, d'éviter des biais, il est préférable de choisir à l'avance, et au hasard, les différents changements de phase
  - A (1 jour, 3 moments de mesure)
  - B (2 jours, 6 moments)
  - A (2 jours, 6 moments)
  - B (3 jours, 9 moments)
  - A (1 jour, 3 moments)

---
# :three: 2. Représentation du niveau d'engagement d'élèves de MS (*N*=6) lors de la lecture d'albums. Effet du *cold calling* (Monti 2025)

![w:1200](images/protocoleABA.jpg)



---
# :three: Tâches

- Considérer l'utilisation d'un protocole à cas unique dans sa recherche
- Utiliser le logiciel [SCDA](https://tamalkd.shinyapps.io/scda/) ; [jeux de données exemples](https://github.com/tamalkd/ShinySCDA/tree/master/www)

---
# :three: Références

- La lecture attentive de [ce document](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/proto-single-case.html) pourra aider à la mise en œuvre d'un protocole à cas unique 
- Ferry-Danini, J. (2023). *Pilules roses*. Stock. 
- Monti, K. (2025). Le *cold-calling* en maternelle pour favoriser l’engagement comportemental des élèves et leur compréhension d’albums. Mémoire MEEF-PE, Univ. Grenoble Alpes.
- Plavnick, J. B., & Ferreri, S. J. (2013). Single-Case Experimental Designs in Educational Research: A Methodology for Causal Analyses in Teaching and Learning. *Educational Psychology Review, 25*(4), 549-569. https://doi.org/10.1007/s10648-013-9230-6 
