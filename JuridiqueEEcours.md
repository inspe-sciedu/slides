---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)


# Thématique « Numérique et juridique »

M2 et DIU MEEF Encadrement Éducatif


<!-- page_number: true -->
<!-- footer: L.Osete • M2 MEEF-DIU EE •  Inspé-UGA 2023-24 • ![CC:BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)  -->

---
# :zero: Rôles du CPE

![w:800](images/img-juridique/rolesEE.png)

---
# Compétences travaillées

- Capacité à **comprendre le contexte** dans lequel il évolue et à utiliser ses connaissances, recherches, réflexions à propos de la législation et des règlementations institutionnelles liés aux TICE **pour une mise en œuvre raisonnée et responsable** dans le cadre de son activité professionnelle.

---
# Les principaux sujets abordés

1. Le droit à la vie privée
2. Les droits des auteurs
3. La sécurité des élèves
4. Le principe de neutralité

---
# :one: Respect de la vie privée et numérique

Par groupe, répondre aux questions suivantes :

:interrobang: Quels outils de vie scolaire utilisez-vous pour : 
- garder une trace d'informations concernant les élèves ?
- communiquer avec les familles ?
- communiquer avec les AED ? 

:interrobang: Quels problèmes de vie privée cela pose ?

---
# 1.1 Lois en vigueur

![](images/img-juridique/RGPD.png)

Mais également le Code civil et le Code pénal.

---
# 1.2 Les données à caractère personnel

- Identité (identifiants, **biométrie**, intérêts, **opinions**)
- Contexte (déplacement, habitudes)
- Comportements (navigation web, consommation)
- Communication (messages, contacts, groupes) 
- **Santé (dossiers médicaux)**
- Finance (revenu, données bancaires)

À l'école les données à caractère personnel sont aussi liées à la scolarité des élèves (activités, comportements, évaluations, appréciations, ...)

---
# 1.3 Les grands principes de la règlementation

La collecte et le traitement doit être :
* **licite**, loyale et transparente ;
* **collectées pour des finalités déterminées**, explicites et légitimes ;
* adéquates, pertinentes et **limitées à ce qui est nécessaire** ;
* exactes et tenues à jour ;
* conservées pendant une **durée n'excédant pas celle nécessaire** au regard des finalités pour lesquelles elles sont traitées;
* traitées de façon à **garantir une sécurité** appropriée des données

---
# 1.4 Licéité du traitement

- nécessaire à l'exécution d'un contrat
- nécessaire au respect d'une obligation
- nécessaire à la sauvegarde des intérêts vitaux de la personne
- nécessaire à l'exécution d'une **mission d'intérêt public ou relevant de l'exercice de l'autorité publique**
- nécessaire aux fins des intérêts légitimes poursuivis par le responsable du traitement ou par un tiers.
- la personne concernée a **consenti** au traitement

---
# 1.5 Droits des personnes

- Droit d'être informé
- Droit d'accès
- Droit d'effacement de de rectification
- Droit à la portabilité
- Droit à la limitation de traitement
- Droit d'opposition
- Droits relatif à la prise de décision automatisé et au profilage.

---
# 1.6 Responsabilités

- Le responsable du traitement est responsable du respect du RGPD et est en mesure de le démontrer (responsabilité)
- Pour cela, il doit :
	- Tenir un registre des traitements et des violations. Outil de gestion de l'académie : [OURANOS](https://enora.ac-lyon.fr)
	- Informer les personnes concernées (élèves, parents, personnels, intervenants extérieurs, …)
	- S’assurer du consentement des personnes concernées si nécessaire.
	- S’assurer de la sécurité des données.
- Pour les collèges et lycées, le responsable est le **chef d'établissement**

---
# 1.7 Précautions à prendre (1/4)

Avant d'utiliser un logiciel ou une application collectant des DCP

- Vérifier s'il faut demander l'accord du chef d'établissement : https://drane.ac-lyon.fr/spip/Applications-et-RGPD
- Si c'est le cas, il est possible de remplir une fiche de demande sur  [OURANOS](https://enora.ac-lyon.fr) 
- Informer les élèves et leurs représentants légaux et demander le consentement si nécessaire.
- Assurer la sécurité des données (mot de passe fort, chiffrement)

---
# 1.7 Consentement des enfants en ce qui concerne les services de la société de l'information (2/4)

- est licite à partir de 16 ans. En dessous le consentement est donné ou autorisé par le titulaire de l'autorité parentale.
- Les États membres peuvent prévoir par la loi un âge inférieur entre 13 et 16 ans. 
- **La France a choisi l'âge de 15 ans.**

---
# 1.7 Cas particuliers des photos ou enregistrements d'élèves (3/4)

<!-- _class: t-70 -->

Autres lois applicables : [LOI n° 2024-120 du 19 février 2024 visant à garantir le respect du droit à l'image des enfants](https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000049163320).

#### Article 2

« Art. 372-1.-Les parents protègent en commun le droit à l'image de leur enfant mineur, dans le respect du droit à la vie privée mentionné à l'article 9.
« Les parents associent l'enfant à l'exercice de son droit à l'image, selon son âge et son degré de maturité. »

II.-L'avant-dernier alinéa de [l'article 226-1 du code pénal](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006417929/2024-05-14/) est complété par les mots : «, dans le respect de l'article 372-1 du code civil ».

#### Article 3

« Il peut également, en cas de désaccord entre les parents sur l'exercice du droit à l'image de l'enfant, interdire à l'un des parents de diffuser tout contenu relatif à l'enfant sans l'autorisation de l'autre parent. »

---
# 1.7 Cas particuliers des photos ou enregistrements d'élèves (4/4)

- Nécessite le consentement de l'élève et de ses tuteurs légaux pour les mineurs.
- Ne peut être faite pour l'année, pour tous types d'usages et de diffusion.
- Doit être donné par une déclaration ou un acte positif clair.
- Les articles de loi doivent apparaitre ainsi que les modalités d'exercice des droits des personnes (droit d'accès, de rectification, ...)

Modèle d'autorisation : https://eduscol.education.fr/398/protection-des-donnees-personnelles-et-assistance

---
# 1.8 Mise en application

:interrobang: Quelles précautions devez-vous prendre pour utiliser les outils listés ?

---
# :two: Le Droit d'auteur

:interrobang: Utilisez-vous des images, vidéos, documents pris sur Internet pour des actions de formation, ou de sensibilisation ?

:interrobang: Avez-vous le droit de les utiliser ? Justifier.


---
# 2.1 Loi en vigueur

Le code de la propriété intellectuelle, composé de :
- La propriété littéraire et artistique
- La propriété industrielle : brevets, dessins et modèles, marques.

![](images/img-juridique/DefDroitDauteur.png)

---
# 2.2 Les attribus du droit d'auteur.

![w:800](images/img-juridique/DroitDauteur.png)

---
# 2.3 Œuvres "libres de droit"

- L'auteur peut autoriser l'exploitation de son œuvre de son vivant, en la plaçant sous une licence dite "libre" ([Creative Communs](http://creativecommons.fr/), [GFDL](https://commons.wikimedia.org/wiki/Commons:GNU_Free_Documentation_License), [Art Libre](http://artlibre.org/), [GNU GPL](https://www.gnu.org/licenses/quick-guide-gplv3.fr.html), ...).
- L'auteur a déposé son œuvre sur un service ayant des conditions d'utilisation (CGU) particulières.
- L'exploitation n'est pas forcément gratuite.

---
# 2.4 Titulaires du droit d'auteur (Articles L113-1 à L113-10)

- Personne qui créé l'œuvre et la divulgue. Ses ayants droit après sa mort.
- Cas des œuvres créées à plusieurs :
	- tous les co-auteurs pour une œuvre collaborative 
	- la personne qui dirige la création et qui la divulgue pour une œuvre collective.
- Cas particulier d'une œuvre intégrant une autre œuvre. Elle est dite composite. Elle est la propriété de l'auteur qui l'a réalisée, sous réserve des droits de l'auteur de l'œuvre préexistante.

---
# 2.5 Droit d'auteur : Exceptions (Article L122-5)

Quelques exceptions utiles pour un usage scolaire,

3° sous réserve que soient indiqués clairement le nom de l'auteur et la source 
a)  Les analyses et courtes citations justifiées par le caractère critique, polémique, pédagogique, scientifique ou d'information de l’œuvre à laquelle elles sont incorporées ;

4° La parodie, le pastiche et la caricature, compte tenu des lois du genre;

11° Les reproductions et représentations d'œuvres architecturales et de sculptures, placées en permanence sur la voie publique, réalisées par des personnes physiques, à l'exclusion de tout usage à caractère commercial.

12° **"Exception pédagogique"**

---
# 2.5 Droit d'auteur : Exception pédagogique

- La représentation ou la reproduction d'extraits d’œuvres, […]
- à des fins exclusives d'illustration, 
- à un public composé majoritairement d'élèves, 
- compensée par une rémunération négociée.

---
# 2.5 Accords encadrant les usages

- Usages numériques
	- [Bulletin Officiel N°35 du 29 septembre 2016](https://www.education.gouv.fr/bo/16/Hebdo35/MENE1600684X.htm) (livres, œuvres musicales éditées, publications périodiques et œuvres des arts visuels)
	- [Avenant au protocole (Bulletin officiel n° 7 du 13 février 2020](https://www.education.gouv.fr/bo/20/Hebdo6/MENE2000032X.htm?cid_bo=148987)
	- Bulletin Officiel n° 5 du 4 février 2010 ([œuvres cinématographiques et audiovisuelles](https://www.education.gouv.fr/bo/2010/05/menj0901120x.html), [œuvres musicales](https://www.education.gouv.fr/bo/2010/05/menj0901121x.html))
- Photocopies : 
	- [Bulletin officiel n°13 du 1er avril 2021](https://www.education.gouv.fr/bo/21/Hebdo13/MENE2108987C.htm)

---
# 2.5 Cas particuliers

- n'utiliser que des **extraits** d'œuvres\*, sauf pour
	- Projection en classe d'une œuvre de l'écrit
	- œuvre courte (poème)
	- diffusion intégrale en classe d'une musique
	- diffusion en classe d'une vidéo provenant d'un service non payant. 
	- œuvres des arts visuels (limité à 20 œuvres par travail, définition max 800x800 px en 72 DPI)

\* Les œuvres doivent avoir été acquises légalement

Tableau de synthèse : https://classetice.fr/v2/wp-content/uploads/2022/07/tableau_exception_peda_2016.pdf

---
# 2.6 Exemple (1/2)

[Cette vidéo](https://enseignants.lumni.fr/fiche-media/00000005749) est-elle librement diffusable en établissement ?

---
# 2.6 Exemple (2/2)

[![w:200](https://upload.wikimedia.org/wikipedia/commons/b/bf/Online_harassment_of_women_journalists.png?uselang=fr)](https://commons.wikimedia.org/wiki/File:Online_harassment_of_women_journalists.png?uselang=fr)

- Cette image est-elle libre ?
- Est-elle intégrable dans une affiche créée par des élèves et publiée avec des droits réservés ?

---
# 2.7 Précautions à prendre
- Citer l'auteur, l'éditeur (obligatoire mais pas toujours suffisant).
- Consulter les conditions d'utilisation (licences)
- Ne pas diffuser les œuvres des élèves sans autorisation.
- Utiliser si possible des ressources sous licence "libre"(Creative Commons ou GNU GPL).
- Respecter les CGU des logiciels et services.

---
# 2.8 Banques de ressources Institutionnelles

- Vidéos et contenus intéractifs : [LUMNI](https://enseignants.lumni.fr/)
- Environnement Numérique de Travail [ELEA](https://dane.web.ac-grenoble.fr/elea)
- [Mathilda](https://matilda.education/) : Ressources pédagogiques sur les thématiques de l'égalité entre les sexes

---
# 2.8 Banques de ressources "Grand public"

- [Creative Commons Search](https://search.creativecommons.org/) : moteur de recherche d'images, vidéos, musiques sous licence Creative Commons
- [Wikimédia](https://commons.wikimedia.org/wiki/Accueil) : Banque d'images sous licence Creative Commons.
- [LaSonothèque](https://lasonotheque.org/) : banque de sons, bruitages libres et gratuits.
- [Gallica](https://gallica.bnf.fr/blog/recherche/?query=1857&mode=desktop) : Catalogue d'œuvres entrées dans le domaine public.
- [Free Music Archive](https://freemusicarchive.org/home)
- ...

---
# 2.9 Outils pour créer ou partager des ressources

- [Générateur de QR Code](https://dane.web.ac-grenoble.fr/outils-numeriques-0/creer-ses-qr-codes) de l'académie de Grenoble
- [ELEA](https://dane.web.ac-grenoble.fr/elea) : Environnement numérique de travail mis en place par l'EN.
- [AppsEducation](https://portail.apps.education.fr/signin) : ensemble d'outils proposés par le MEN

Pour élèves et enseignants :
- [DV-Fabrique](https://dv-fabrique.fr/) : création de documents multimédias interactifs.
- [Polymny](https://polymny.studio/) : création de vidéo.

---
# :three: Sécutité des élèves

- Vérifier le fonctionnement des systèmes de filtrage
- Mettre en place des règles --> Charte informatique
- Signaler les contenus inappropriés sur deux sites :
	- https://www.internet-signalement.gouv.fr/PharosS1/ est le portail officiel de signalement des contenus illicites de l'Internet
	- https://www.pointdecontact.net/ est le site de signalement mis en place par l'AFA (Association des Fournisseurs d'Accès et des Services Internet).
- Signaler les incidents aux parents et à la direction.

---
# :four: Neutralité commerciale

Document de référence : [Code de bonne conduite des interventions des entreprises en milieu scolaire](https://www.education.gouv.fr/botexte/bo010405/MENG0100585C.htm)

#### Article III.5 Le partenariat pour l'usage de produits multimédias
*"L'utilisation de produits multimédias par les établissements scolaires, à des fins d'enseignement, est libre. La consultation de sites Internet privés ou l'utilisation de cédéroms qui comportent des messages publicitaires ne sauraient être regardée comme une atteinte au principe de neutralité (9)."*

*"En revanche, la réalisation de sites Internet par les services de l'éducation nationale et les établissement scolaires est tenue au respect du principe de la neutralité commerciale."*

---
# 4.1 Précautions à prendre

- ne pas promouvoir, voire imposer de logiciels ou services.
- ne pas diffuser du contenu sur des services diffusant de la publicité.

- *éviter les outils ou service qui utilisent les traces des utilisateurs pour faire de la publicité ciblée (lien avec les problèmes éthiques).*
