---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
   
# Module Enseigner avec le numérique
## TP


Equipe de formateurs - UGA
 
<!-- page_number: true -->
<!-- footer: L.Osete • DIU-SD •  Inspé-UGA 2023-24 • ![CC:BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)  -->

---
# :zero: Rappels
* TD 1 : Mise en application des apports théoriques sur les ressources et les aspects juridiques et éthiques - Début de la conception :arrow_right: avoir une première proposition de situation d'apprentissage.
* TD 2 : Mise en application des apports théoriques sur les effets du numériques sur les apprentissages - Poursuite de la conception.
* TD 3 : Apports pratiques sur la formation AU numérique et l'évaluation des élèves en référence au CRCN - découverte de PIX et PIX+EDU - Finalisation de la conception.
* TP : **Mise en commun des conceptions et retour d'expérience pour celles et ceux qui auront réalisé une mise en oeuvre. :arrow_right: PIX+EDU.**

---
# :one: Organisation de la séance

- Rappels rendu du dossier (10 mn)
- Préparation d'une rapide présentation de votre situation (15-20 mn)
    - Description rapide de la situation (avec des illustrations si possible)
    - Justifications et analyses (effets positifs-négatifs, avantages, problème, précautions, ...). 
- Présentations (1h)
- Finalisation du dossier (40 mn)

---
# :two: Rendu du dossier

Plusieurs étapes sont nécessaires :

- Inscription au cours
- Déclaration du binôme (monôme ou trinôme)
- Dépôt des illustrations
- Dépôt du dossier

---
## 2.1 Inscription au cours

- Le cours se nomme "Enseigner avec le numérique 1 et 2 SD"
![w:500](images/img-Intro/ModuleSD.png)
- La clé d'auto-inscription dépend de votre groupe 
    - DIU_SD_GRE_C1
    - DIU_SD_GRE_C2_G1
    - DIU_SD_GRE_C2_G2
    - DIU_SD_CHY

---
## 2.2 Déclaration des binômes

Afin de déposer le dossier et d'être évalué en tant que binôme, vous devez créer le groupe sur la plateforme de cours à l'aide de l'outil suivant :

![w:500](images/TD/ConstitutionBinomes.png)

![width:500px](images/seance6M1SD/GroupeE2.png)
![width:500px](images/seance6M1SD/Groupe*E3PE.png)
Nom du groupe de la forme **Grenoble-C??-** ou **Chambery-**

---
## 2.2 Inscription des autres participants (suite)

![width:800px](images/seance6M1SD/GroupeE4PE.png)

Désinscription possible
![width:800px](images/seance6M1SD/GroupeE5PE.png)


---
## 2.3 Dépôt des illustrations

Des illustrations des ressources et/ou outils mobilisés dans la situation doivent être fournies afin de meiux comprendre cette dernière et les analyses que vous faites : 
- Soit par des liens directement dans le dossier vers des ressources en ligne.
- Soit par des liens vers copies d'écran déposées sur Nuage ou digipad, notamment pour montrer le fonctionnement des outils et/ou leur paramétrage.

Les copies d'écran pourront être déposées sur un mur de partage (type [digipad](https://digipad.app/) ou [padlet](https://padlet.com/)), ou sur l'application institutionnelle "[Nuage](https://nuage.apps.education.fr/)" de AppsEducation. 

---
## 2.4 Dépôt du dossier

- Un seul document de 4 pages maximum au format PDF sans annexes.
- Date limite : 2 jours maximum après cette séance.

