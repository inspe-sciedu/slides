---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# Culture numérique et apprentissages
## TD 5
 
Christophe Charroud - UGA
 
<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2023 - 2024 - CC:BY-NC-SA -->

---
# :zero: Rappels
* TD 1 : Mise en application des apports théoriques sur les effets du numériques sur les apprentissages - Début de la conception :arrow_right: avoir une première proposition de situation d'apprentissage.
* TD 2 : Mise en application des apports théoriques sur les ressources et les aspects juridiques et éthiques - Poursuite de la conception
* TD 3 : Apports pratiques sur la formation AU numérique et l'évaluation des élèves en référence au CRCN - découverte de PIX et PIX+EDU - Poursuite de la conception
* TD 4 : Finalisation de la conception. :arrow_right: Évalué cette année  
* TD 5 : **Retour d'expérience pour celles et ceux qui auront réalisé une mise en oeuvre. :arrow_right: PIX+EDU**

 
---
# :one: Retour d'expérience 

##### Qui a mis en oeuvre une situation d'apprentissage intégrant du numérique ?
###
Ou
###
##### Qui souhaite présenter sa situation d'apprentissage et la simuler avec les autres membres du groupe ?
*En cas d'absence de volontaire => tirage au sort*


---
# :two: Présentation ou simulation
Objectifs :
* Apporter un regard critique sur la réalistion (forces, faiblesses,...)
* Essayer d'analyser les apprentissages permis par cette situation
* Y a-t-il des plus-values ?
* Extraire les idées principales :arrow_right: bilan réflexif


---
# :three: Finalisation de la rédaction du document à déposer sur la plateforme
Date limite de dépot : 48h après ce TD.
Grille d'évaluation :arrow_right: voir le cours sur Eformation

