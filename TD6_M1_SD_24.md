---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# Culture numérique et Apprentissage

## UE - Num 801 - Séance 6  
Equipe formateurs numérique - UGA - 2024
 
---
<!-- page_number: true -->
<!-- footer: 2023 - CC:BY-NC-SA -->

# Séance en deux phases :
### Phase 1 
* Évaluation  : durée 30 minutes 
(40 mn pour ceux bénificiant d'un tiers temps)

### Phase 2
#### Analyse réflexive sur les usages du numérique en situation de classe
* Constitution de binôme 
* Choix d'une situation d'apprentissage intégrant le numérique
* Rédaction de la description de la situation d'apprentissage

---
# :one: Évaluation CC1

Code d'accès au test :
## 


---
# :two: Conception d'une situation d'apprentissage intégrant le numérique 
#### Production attendue
Un document rédigé en **binôme** de 4 pages maximum comportant :
* un descriptif de la situation d’apprentissage avec obligatoirement des liens pointants sur les éléments de conception (liens vers les ressouces, copies d’écran des interfaces élèves et/ou enseignants, …),
* une argumentation sur les choix opérés lors de la conception, notamment au regard des apports théoriques,
* un bilan personnel.

---
## 2.1 Recherche de situation 

* L'idée de la situation d'apprentissage peut provenir d'une situation **vécue, observée ou rapportée**. 
* Elle doit intégrer au moins une ressource et/ou un outil numérique, ou hybride.
* Elle peut être commune à plusieurs personnes mais les analyses devront prendre en compte un contexte de mise en œuvre.

---
## 2.2 Description de la situation
#### Eléments attendus dans la description

```
- Discipline 
- Niveau de classe
- Période de l'année, ou place dans une séquence, où la situation a été (ou serait) mise en œuvre.
- Objectif(s) d’apprentissages : 
	- Compétences disciplinaires visées en termes de « être capable de…», (ces compétences sont issues des programmes)
	- Compétences numériques développées (issues du CRCN)
	
- Environnement numérique impliqué dans la situation :
    - Outils physiques (TBI, tablettes, ordinateurs fixes ou portables, caméras, enregistreurs, ...)
    - Organisation matérielle (nb de matériels par élève/classe, salle informatique ou salle de classe)
    - Logiciels ou applications utilisés.
    - Ressources utilisées, ou produites

-  Organisation pédagogique : quelles activités, quand, comment, ...
    
```

---
## 2.3 Analyse et justification des choix

- Justification des choix d'outils et de ressources numériques, en s'appuyant notamment sur le modèle SAMR et les compétences visées.
- Analyse des potentiels effets positifs ou néfastes pour les apprentissages et les apprenants.
- Analyse des problèmes juridiques (droits d'auteurs, droit à la vie privée) et éthiques soulevés.


:warning: Les analyses et justifications doivent tenir compte d'un contexte précis.

---
# En avant !
* Choisir une situation
* Rédiger la description de la situation et si possible commencer les analyses.
* Restez réaliste :arrow_right: Mise en oeuvre conseillée. 

---
# :three: Inscription des binômes sur Moodle

2 étapes:

* L’un des étudiants crée un groupe en indiquant les deux noms dans le titre
* Le deuxième étudiant s’inscrit dans le groupe qui vient d’être ouvert

---
## 3.1. Création du groupe

Inscription du premier participant
![width:400px](images/seance6M1SD/GroupeE1M1SD.png)
![width:500px](images/seance6M1SD/GroupeE2.png)
![width:500px](images/seance6M1SD/GroupeE3PE.png)
Nom du groupe de la forme Chambery-GX ou Grenoble-GX

---
## 3.2. Inscription du 2e participant

Inscription du deuxième participant
![width:800px](images/seance6M1SD/GroupeE4PE.png)

Désinscription possible
![width:800px](images/seance6M1SD/GroupeE5PE.png)




