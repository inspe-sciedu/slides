---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

## Diplôme Inter-universitaire PE-SD
## Éducation fondée sur les faits : Comment intégrer la recherche ?
### Philippe Dessus 
### Inspé, LaRAC, Univ. Grenoble Alpes

### ![w:200](images/logo-espe-uga.png)

---

<!-- footer: Ph. Dessus • DIU-SD • Éducation fondée sur les faits • Inspé-UGA 2024-25 • CC:BY-NC-SA-->
<!-- paginate: true -->

# 0.1 Informations préliminaires  

- Les références citées figurent en fin de présentation
- Les mots en bleu sont des liens vers les ressources mentionnées
- Cette présentation est disponible à [https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/DIU-CM-SD-Rech.pdf](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/DIU-Rech.pdf) ou par le raccourci [https://link.infini.fr/diu-cm-sd-rech](https://link.infini.fr/diu-cm-sd-rech)

![bg right:45%](images/diu-cm-sd.jpg)

---
# 0.2 Questions traitées dans ce cours

1. Les connaissances issues de la recherche permettent-elles de réfléchir et/ou améliorer la pratique des enseignants ? Avec quels avantages et inconvénients ?
2. Une brève “activité d'enquête“ sur cette question peut-elle orienter les enseignants vers une meilleure connaissance des produits de la recherche ? 

---
# 0.3 Plan du cours

0. Questions introductives
1. À quoi la recherche peut-elle bien servir aux enseignant·es ?
2. Les facteurs influant l'utilisation de recherches par les enseignant·es
3. Éléments pratiques
4. Quelques activités d'utilisation de recherches
5. “Ateliers” de mise en pratique et mise en commun 

---
# 0.4 Questions pour commencer

1. Pourquoi utiliseriez-vous (ou utilisez) dans votre pratique d'enseignement des informations venant de la recherche ?
   a. Pourquoi ces informations-là (plutôt que d'autres) ?
   b. Pour quoi faire (à quelles fins) ?
2. Quels autres types de sources d'informations pourriez-vous utiliser ? (les citer)

---
# 0.5 Résumé des réponses : Pourquoi utiliser la recherche ?

- Pour changer mes pratiques, avoir d'autres idées
- Pour m'informer
- Pour être plus compétent·e, me former
- Pour rompre ma routine
- Parce que c'est une compétence professionnelle du [référentiel de compétences des métiers du professorat et de l'éducation](https://www.education.gouv.fr/bo/13/Hebdo30/MENE1315928A.htm?cid_bo=73066) 
  - (Art. 14 : “Se tenir informé des acquis de la recherche afin de pouvoir s'engager dans des projets et des démarches d'innovation pédagogique visant à l'amélioration des pratiques.”)
- etc.

---
# 0.6 Résumé des réponses : Pourquoi ces infos (plutôt que d'autres venant d'autres sources) ?

- Plus **représentatives** (fondées sur des échantillons de participants plus importants, donc plus représentatives de la population ciblée : élèves français)
- Plus **objectives** (fondées sur des méthodes de recueil ou d'analyse plus élaborées, systématiques)
- Plus **reproductibles** (mieux expliquées, pouvant mieux être réutilisées)
- Plus “pointues”, spécialisées
- etc.

---
# 0.7 Résumé des réponses : Ces infos pour quoi faire ?

- **Conceptuelle** (dirigé par les connaissances) : Utiliser le modèle *X* de lecture me permet de mieux comprendre comment les élèves apprennent à lire, de me faire une meilleure idée des tenants et aboutissants de cette activité complexe, de prendre des décisions appropriées
- **Symbolique** (illumination): Utiliser le modèle *X* de lecture me permet de justifier une pratique, par exemple pour l'expliquer dans un projet, à mes collègues
- **Instrumentale** (pour résoudre des problèmes) : Utiliser le modèle *X* de lecture me permet de mieux concevoir des exercices, des ressources, des rétroactions pour aider mes élèves

:books: Weiss (1979)

---
# 0.8 Résumé des réponses : Autres types de sources d'informations ?

- L'avis de mes élèves, de leurs parents
- Mon avis personnel 
- L'avis de mes pairs (plus ou moins expérimenté·es)
- L'avis de mes supérieurs hiérarchiques
- Mes lectures (ouvrages, blogs, informations sur internet)
- etc.

---
# :one: À quoi la recherche peut-elle bien servir quand on est enseignant·e ?

---
# 1.1 A quoi la recherche peut-elle servir quand on est enseignant·e ?

- Pour régler quelque chose dans sa pratique immédiate (outil, méthode, technique...)
- Pour travailler, réfléchir à plus large échéance : améliorer sa compréhension des phénomènes rencontrés, des domaines enseignés
- Pour mieux discuter avec mes collègues
- Pour apporter “ma propre pierre”

---
# 1.2 Mais ai-je vraiment besoin des produits de la recherche pour savoir comment travailler ?

- En éducation, comme dans tous les domaines, les connaissances sont souvent établies par “le peuple”, qui trouve lui-même, par essais-erreurs, les pratiques les plus efficaces (Conner 2011)
- Voir C. Freinet, M. Montessori, P. Freire, etc., qui ont pu élaborer des pratiques montrant une certaine efficacité sans être des chercheurs “professionnels”

---
# 1.3 Toutefois, les recherches peuvent présenter des avantages

- Avoir une meilleure idée des concepts, des processus à l'œuvre
- Difficile de comprendre quelque chose (un état, un processus) tant qu'on n'a pas essayé de le mesurer 
- Fonder cette idée sur des échantillons plus grands, plus représentatifs de la population visée
- Combattre ou freiner de possibles “mythes”, idées fondées sur des problèmes de perception, des *a priori*

---
# 1.4 Mais, attention, d'éventuels problèmes sont toujours possibles !

- *Mutations létales* : Des interprétations de pratiques efficaces peuvent être modifier jusqu'à se révéler inefficaces (*e.g*., proposer du texte avec des illustrations distractives n'est pas du double codage efficace) [[article EEF](https://www.tes.com/magazine/tes-explains/what-are-lethal-mutations)]
- *Effets secondaires* : Toute intervention ayant des effets positifs peut également avoir des effets secondaires. Mesurer des apprentissages avec des tests standardisés ne suffit pas : il faut vérifier que la motivation et d'autres caractéristiques importantes ne sont pas impactées (Zhao 2017)

---
# :two: Facteurs influant l'usage de recherches chez les enseignant·es 

---
# 2.1 Facteurs influant l'usage de recherches chez les enseignant·es ? (1/2)

Il y a des éléments qui peuvent favoriser l'utilisation de recherches :

- **Liés aux enseignant·es** : compétences méthodologiques ; participation à des recherches antérieures, expérience d'enseignement ; auto-efficacité, volonté d'innover
- **Liés aux recherches** : facilité d'accès ; de compréhension ; objectivité et véracité, en lien avec le contexte ; pertinence
- **Liés à la communication** : accès aux services ; aux recherches et leurs données ; qualité de la recherche ; discussion entre collègues ; collaboration avec chercheurs ; collaboration soutenue ; médias


---
# 2.2 Facteurs influant l'usage de recherches chez les enseignant·es ? (2/2)

- **Liés aux écoles/établissements** : besoin d'innovation, de soutien extérieur, capacité à soutenir des recherches, des initiatives, incitation au développement pro., allocation de temps et ressources

- Mais chaque élément peut, à l'inverse jouer comme un **frein**. Par exemple, si les recherches ne sont pas faciles d'accès, si aucune plage de temps n'est allouée, etc.

- En Annexe 2, un questionnaire pour s'interroger sur son utilisation de la recherche (Ramdé 2012)


:book: Dagenais *et al*. 2012

---
# 2.3 Une personne intermédiaire : courtier·e en connaissances

<!-- _class: t-80 -->

- “Le courtage [*brokering*] de connaissances vise à créer des liens entre les chercheurs et les décideurs de façon à faciliter l’interaction entre eux. Ils peuvent ainsi mieux comprendre les objectifs et la culture professionnelle propres à chacun. Ils pourront aussi s’influencer mutuellement dans leur travail, créer de nouveaux partenariats et favoriser l’utilisation des données probantes de la recherche.” (Munerol et al. 2013, p. 591, citant le CHRSF)
- Rôles :
  - Accompagnement de projets
  - Dissémination des connaissances
  - Créer du lien entre acteurs
  - Aide dans l'utilisation des données, procurer des outils pour utiliser les données probantes

:books: Monod-Ansaldi et al. 2017 ; Munerol et al. 2013


---
# :three: Éléments pratiques

---
# 3.1 En pratique, que faire ?

- Il existe plusieurs activités possibles pour avancer dans la connaissance de la recherche en éducation
- Il est conseillé de se choisir un nombre réduit de centres d'intérêt sur lesquels on se sent compétent·e et motivé·e et de réaliser une veille à leur sujet

---
# 3.2 Une attitude de veilleur·se (1/3)

- Veiller, c'est-à-dire collecter régulièrement (activement ou plus passivement) du matériel sur un sujet donné
- Partir d'un domaine qu'on connaît bien, se spécialiser
- Si on tombe sur un contenu intéressant, le lire en détail et, selon le temps, appliquer une “boule de neige” en lisant quelques références citées, pour se faire un avis plus précis (lecture latérale)
- Faire des formations (MOOC, etc.)
- S'inscrire à une communauté de pratique d'enseignants (*voir plus loin*)

[Ressources de veille académique](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/veille-sci-ressources.html#scientometrie)

---
# 3.3 Une attitude de veilleur.se *critique* (2/3)

- Se méfier des **pyramides**, avec une hiérarchie de niveaux pas toujours prouvée (_cf_. pyramide de Bloom, cône de Dale)
- Se méfier des **dichotomies** (_cf_. pédagogie nouvelle vs. traditionnelle) et des effets de balancier. Préférer envisager les phénomènes dans des *continua*
- Se méfier des **valeurs tranchées et sans consensus** : “on ne peut soutenir son attention que pendant *n* minutes” ; “on utilise *n* % de son cerveau”, etc.
- Cf. Annexe 3 pour plus de renseignements critiques sur les études quantitatives



:books: Fillon 2024

---
# 3.4 Le problème avec les mythes (1/2)

- Revenons un moment sur les mythes sur l'apprentissage… 
- Ils sont souvent, en premier lieu, créés et propagés par les chercheur·es mêmes
- Ils ne sont pas anodins. Ils ont un effet sur la perception que les enseignant·es et parent·es ont sur l'intelligence et la réussite des enfants/élèves
- Des parent·es (*N* = 100) et enseignant·es (*N* = 100) a qui on a présenté 2 élèves fictifs de CM2 (tous les parents et 85 % des enseignants croyaient aux styles d'apprentissage)
  - l'un étant plutôt “visuel”
  - l'autre étant “kinesthésique” (“manuel”)

:books: Sun et al. 2023

---
# 3.5 Le problème avec les mythes (2/2)

- Les participant·es ont jugé l'élève “visuel” significativement plus intelligent que l'élève “manuel” ; l'élève “manuel” a été jugé travaillant plus durement que l'élève “visuel”
- L'élève “visuel” a été jugé ayant significativement de meilleurs résultats que l'élève “manuel” en maths, langues, et sciences sociales (mais pas en sciences)
- L'élève “manuel” a été jugé ayant significativement de meilleurs résultats que l'élève “visuel” en arts, EPS, et musique

---
# :four: Quelques activités de mise en œuvre de recherches

---
# 4.0 Critères pour utiliser une recherche ? 

1. Est-ce qu'elle permet de résoudre un problème que j'ai ?
2. Est-ce qu'elle permet une meilleure réussite ou performance ? À quelle hauteur ?
3. Qu'est-ce qu'elle coûtera (en argent et temps de travail) ?
4. Est-ce que je peux la mettre en œuvre dans mon école ou établissement ?
5. Est-ce que je comprends quoi faire ?

[Wiliam 2023](https://youtu.be/p6oukyR2F0Q)

---
# 4.1 Cadre pour trouver des travaux

Ce tutoriel intitulé [“Trouver des travaux de recherche”](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/tuto-trouver-travaux.html) est un guide pour pré-sélectionner quelques travaux scientifiques sur votre thème.

---
# 4.2 Explorer et sélectionner : les moteurs de recherche

- [Google Scholar](https://scholar.google.com) est le moteur de recherche de travaux académiques le plus complet et le plus utilisé

(*voir en Annexe 1 plus de moteurs de recherche*)

---
# 4.3 Lire et comprendre : les fiches PICO (1/2)

- La célèbre base de données [Cochrane](https://france.cochrane.org), répertoriant les résultats de la recherche médicale, structure ses informations sous la forme PICO (*voir diapositive suivante*)
- Cette forme peut être utilisée pour annoter et synthétiser les résultats de recherches “empiriques”, c'est-à-dire récupérer des données pour les analyser et comprendre une situation d'enseignement-apprentissage

---
# 4.4 Lire et comprendre : les fiches PICO (2/2)

- **Participants/population** : Détails de la population (principales caractéristiques) et de l’échantillon : nombre de participants
- **Intervention** : Détailler ce à quoi sont exposé·es les participant·es (méthode pédagogique, etc.) : quoi ? comment ? où ? quand ? combien ?
- **Comparaison** : Préciser ce qui est comparé : quels sont les différents groupes de participant·es étudiés et quelles comparaisons sont réalisées (*e.g.*, pédagogie “traditionnelle” avec pédagogie “innovante”, groupe travaillant sur ordinateur avec groupe papier-crayon)
- **Outcomes** (Résultats) : Détailler les principaux résultats de l'étude


:books: [Plus d'informations](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/tuto-lire-article.html#une-methode-issue-de-pico)


---
# 4.5 Lire et comprendre : Les travaux de l'EEF

- L'[Education Endowment Foundation](https://educationendowmentfoundation.org.uk) est une fondation indépendante visant à améliorer le lien entre enseignement et apprentissage en utilisant des données probantes
- Son [Teaching and Learning Toolkit](https://educationendowmentfoundation.org.uk/education-evidence/teaching-learning-toolkit) classe les interventions éducatives par coût, efficacité et impact dans le temps
- En français, voir aussi les [conférences de consensus du CNESCO](https://www.cnesco.fr/presentation-dune-conference-de-consensus/), les ressources du [CSEN](https://www.reseau-canope.fr/conseil-scientifique-de-leducation-nationale-site-officiel/publications-et-ressources.html) et les [dossiers Veilles et analyses de l'Ifé](http://veille-et-analyses.ens-lyon.fr)

:books: Pour plus d'informations sur [la recherche fondée sur les preuves](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/ens-preuves.html) et sur des [ressources](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/res-ens-preuves.html)

---
# 4.6 Essayer de débusquer des mythes

- Comme les praticiens de tous les domaines, les enseignants peuvent avoir des “idées reçues” amoindrissant l'efficacité de leurs pratiques
- Chercher et comprendre ces mythes peut être utile
- :warning: Ne pas se servir de ces connaissances pour culpabiliser ou inférioriser ses pairs

:books: [Dessus (2017)](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/mythes_education.html)

---
# 4.7 Lire et comprendre : les méta-analyses et revues systématiques

- Recherches compilant les résultats *quantitatifs* (méta-analyse) ou qualitatifs (revue systématique) d'un ensemble d'études ayant le même but, dont les différentes étapes (sélection, extraction, analyse des biais, résultats) sont standardisées pour en permettre la réplication ou l'extension
- Aisées à trouver dans les moteurs de recherche : “meta-analysis” OR “méta-analyse” / “systematic review“ OR “revue systématique” / “scoping review”
- :warning: Nécessite d'avoir une plutôt bonne connaissance d'un sujet pour s'y pencher

:books: Document “[Comprendre les méta-analyses](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/meta-analyse.html)”


---
# 4.8 Intégrer une communauté de pratique (collaborer)

Les ateliers profs-chercheurs, une communauté de pratique en ligne et en présence pour relever les défis de l'éducation.
- Formuler un **défi** répondant à un problème précis
- Définir des **actions** qui seront testées en situation, et leurs critères de réussite
- Écrire des **retours d'expérience** analysant cette réussite
- Écrire des synthèses comparatives de plusieurs retours d'expérience

:warning: Faible participation des chercheurs à ce jour, et moteur de recherche peu performant


:books: [Présentation des ateliers](https://www.profschercheurs.org/fr) ; [Plate-forme](https://plateforme.profschercheurs.org)

---
# 4.9 PEGASE    

[PEGASE](https://www.polepilote-pegase.fr) (Académies de Grenoble et Guyane, Univ. Grenoble Alpes, Univ. Savoie-Mont-Blanc, Univ. de Guyane), pôle pilote de formation des enseignants et de recherche pour l'éducation, est un ensemble d'initatives dont le but est de renforcer les apprentissages fondamentaux dans les premier et second degrés

- nombreux stages de formation, conférences
- EducLabs, lieux de rencontre enseignants-chercheurs dans des établissements
- Études développementales sur les compétences des élèves
- Ressources (principalement des vidéos) librement utilisables

---
# :five: Ateliers

---
# 5. Tâche (1/2)

- Choisissez (par groupes de 2-3) un sujet en éducation qui vous intéresse et dans lequel vous êtes compétente (vous avez de bonnes connaissances théoriques et savez bien l'enseigner)
- Choisissez l'une des méthodes de travail et parcourez le ou les document.s mentionnés dans sa description
- Si nécessaire, utilisez la [fiche de recherche : http://pdessus.fr/tmp/rech-trav-sci-LPI](http://pdessus.fr/tmp/rech-trav-sci-LPI)
- Vous les présenterez au groupe en fin de séance

---
# 5. Tâche (2/2)
<!-- _class: t-80 -->

1. **Moteurs de recherche**: Formulez une requête dans un moteur de recherche académique sur le sujet choisi en suivant la  [fiche de recherche](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/tuto-trouver-travaux.html), et faites un résumé du contenu des références trouvées
2. **Fiche PICO** : Réalisez une fiche PICO à partir d'un article de votre choix sur votre sujet
3. **EEF** : Parcourez les résultats de la base de l'EEF à propos de votre sujet et faites-en une synthèse en lisant de plus près les recommandations
4. **Essayer de débusquer un mythe** : Avec des requêtes sur internet à propos de votre sujet et en y ajoutant “mythe” ou “myth“, sélectionnez quelques articles débusquant un mythe
5. **Ateliers profs-chercheurs** : Connectez-vous à la plate-forme et sélectionnez un défi proche de votre sujet. Lisez la démarche entreprise et faites-en une synthèse
6. **Méta-analyse ou revue systématique** : Choisissez un article de ce type sur votre sujet et faites-en un résumé des principaux résultats

---
# 6. Discussion : Trois types de “chercheur·e-enseignant·e”

- **Enseignant·e-technicien·ne** : Centré sur le contrôle, la prédiction. Quelle stratégie de gestion de la classe est la plus efficace ?
- **Enseignant·e-interpréteur·e** : Centré sur l'explication d'un processus ou d'un phénomène. Comment les élèves font-ils l'expérience du harcèlement en classe ?
- **Enseignant·e-narrateur·e** : Centré sur les moyens d'améliorer une pratique de classe. Comment puis-je aider des élèves allophones dans la production d'écrits ?

:book: Dana & Yendol-Hoppey (2014)

---
# Merci de votre attention !

- Et vous, lequel ou laquelle pensez-vous être ?

- philippe.dessus@univ-grenoble-alpes.fr
- Merci à Pleen le Jeune pour ses commentaires d'une version antérieure de cette présentation, et sa traduction des facteurs de Dagenais _et al_.

---
# Annexe 1 : Ressources internet (1/2)
## Ressources de cours
- [Base de cours sur l'éducation, Inspé UGA](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/)
- [Base de ressources du projet IDEE](https://www.idee-education.org/ressources)
- [Classement des interventions (J. Hattie)](https://visible-learning.org/fr/john-hattie-classement-facteurs-reussite-apprentissage/)

---
# Ressources internet (2/2)
## Quelques moteurs de recherche académiques

- [Matilda](https://matilda.science)
- [Beluga, UGA](http://beluga.univ-grenoble-alpes.fr)
- [Connected Papers](https://www.connectedpapers.com)
- [Consensus](https://consensus.app/search/)
- [Elicit](https://elicit.org) 
- [Scinapse](https://www.scinapse.io/)

---
# Annexe 2 : Questionnaire sur l'utilisation de la recherche (Ramdé 2012)

<!-- _class: t-60 -->

## 1. Type d'utilisation de l'information
- Améliorer votre pratique professionnelle
- Réfléchir à vos attitudes et à vos pratiques
- Résoudre des problèmes dans votre pratique quotidienne

## 2. Opinion sur les informations issues de la recherche 
- Sont pertinentes par rapport à votre réalité
- Offrent des informations à des moments propices
- Sont utiles pour guider ou améliorer vos pratiques
- Sont faciles à transférer dans la pratique

---
# Annexe : Questionnaire de Ramdé (2012)

<!-- _class: t-60 -->

## 3. Stratégies pour vous informer sur les résultats de la recherche

- Résultats de recherche accompagnés de recommandations claires et précises
- Occasions de discuter des résultats avec une équipe de recherche
- Contacts réguliers avec des gens qui diffusent des informations issues de la recherche
- Démonstrations sur la façon d’appliquer les recommandations issues de la recherche
- Discussions entre collègues sur les informations issues de la recherche

---
# Annexe : Questionnaire de Ramdé (2012)

<!-- _class: t-60 -->

## 4. Habiletés utiles dans votre pratique professionnelle
- Habileté à lire et à comprendre les publications de recherche
- Habileté à utiliser les technologies de l’information telles qu’Internet, les bases de données, etc.
- Capacité d’évaluer la qualité des informations issues de la recherche
- Expertise dans le transfert des résultats de recherche dans la pratique

## 5. Facteurs organisationnels
- Des occasions de remettre en question les habitudes et les traditions établies
- L’importance accordée par votre établissement au développement professionnel
- Un environnement soutenant
- Les ressources humaines (p. ex. la disponibilité de personnel qualifié)

---
# Annexe 3 : Les signaux d'alerte des recherches quantitatives

1. Les données et analyses ne sont pas en libre accès
2. Les valeurs de *p* d'intérêt sont proches de .05
3. Il n'y a pas de pré-enregistrement
4. Il n'y a pas de visualisation de la distribution des données
5. L'article est publié dans une revue prédatrice, peu filtrante, ou hors du domaine d'expertise
6. L'article n'a pas été publié mais posté dans un dépôt de *preprints*
7. Il est impossible d'accéder à la procédure exacte (pour reproduire l'étude)
8. Il n'y a pas de filtre ou de vérification des données


---
# Références (1/2)

<!-- _class: t-60 -->


- Conner, C. D. (2011). *Une histoire populaire des sciences*. L’échappée.
- Dagenais, C., Lysenko, L., Abrami, P. C., Bernard, R. M., Ramde, J., & Janosz, M. (2012). Use of research-based information by school practitioners and determinants of use: a review of empirical research. *Evidence & Policy: A Journal of Research, Debate and Practice, 8*(3), 285-309. https://doi.org/10.1332/174426412x654031  
- Dana, N. F., & Yendol-Hoppey, D. (2014). *The Reflective Educator’s Guide to Classroom Research*. Corwin.
- Monod-Ansaldi, R., Prieur, M., Joseph, B., Meslin, B., Lermigeaux-Sarrade, I., & Thiboud, S. (2017). Les fonctions de passeur à l’épreuve de l’expérimentation au sein de l’institut Carnot de l’Éducation. *Revue Française de Pédagogie, 201*, 61-70. https://doi.org/10.4000/rfp.7242 
- Munerol, L., Cambon, L., & Alla, F. (2013). Le courtage en connaissances, définition et mise en œuvre : une revue de la littérature. *Santé Publique, 25*(5), 587-597. https://doi.org/10.3917/spub.135.0587

--- 
# Références (2/2)

<!-- _class: t-70 -->

- Neville, P. (2023). [Recherches sur, en, pour l’éducation : comment ça marche ?](https://eduveille.hypotheses.org/16894). Éduveille, Ifé. 
- Ramdé, J. (2012). Utilisation des connaissances issues de la recherche en éducation. Université de Montréal. Thèse de psychologie. 
- Sun, X., Norton, O., & Nancekivell, S. E. (2023). Beware the myth: learning styles affect parents', children's, and teachers' thinking about children's academic potential. *NPJ Sci Learn, 8*(1), 46. https://doi.org/10.1038/s41539-023-00190-x 
- Weiss, C. H. (1979). The many meanings of research utilization. *Public Administration Review, 39*(5), 426–431.  
- Zhao, Y. (2017). What works may hurt: Side effects in education. *Journal of Educational Change*, *18*(1), 1-19. https://doi.org/10.1007/s10833-016-9294-4