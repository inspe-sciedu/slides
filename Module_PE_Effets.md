---
marp: true
paginate: true
autoscale: true
theme: aqua
---



![70%](images/img-effets/Effets-logo-nouvelle-uga.png)

# Effets du numérique sur les apprentissages
DIU PE
## Apports théoriques

Christophe Charroud - UGA

<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - UGA - INSPE - 2022 - 2023 - CC:BY-NC-SA -->


  
---

# 1. Introduction : Quelles sont vos représentations sur le numérique en classe ?


À votre avis, si l'on s'intéresse aux apprentissages des élèves, utiliser le numérique en classe c'est :
* Positif
* Négatif
...ou entre les deux....



---
# Vos représentations
## :pencil2: TP : Durée 5 minutes
* Sur une feuille comportant une colonne "effets positifs" et une colonne "effets négatifs", listez des effets du numérique sur les apprentissages.



---

# Vous êtes-vous appuyés sur des arguments scientifiques, sur des représentations ou sur vos opinions  ?

#
### Pourquoi une telle question?

---
#
## « Il y a une rupture entre connaissance commune et connaissance scientifique »

> Bachelard : le matérialisme rationnel, 1953






## Dans l'éducation, quand on parle de numérique, les doxas* tiennent souvent lieu de pensée. *(Ferone. G, 2019)*





>Doxa : Ensemble d'opinions, de préjugés, de présuppositions généralement admises et évaluées positivement ou négativement.


---

# Alors, que dit la recherche ?


Les effets **positifs se limitent à des usages circonscrits**.

Hors de ces usages, le numérique produit (souvent) des effets négatifs sur les apprentissages.




---


# 2. La recherche dit que numérique pour les apprentissages, ce n'est pas magique


Mais alors :
* Pourquoi, souvent, on pense que ça marche ?
* Pourquoi ça ne marche pas aussi bien que ça ?
* Pourquoi on vous incite à l'utiliser ?



### Des promesses, des représentations...et souvent des déceptions 
### :arrow_right: quelques exemples



---

## 2.1. Le numérique à tout prix !

![100%](images/img-effets/Effets-tapis.png)

>Réaliser exactement la même tâche avec ou sans technologie :arrow_right: sans intérêt !


# 


---

## 2.2. Le numérique c'est spectaculaire, mais...
#



### Un exemple d'activité "wahoo!"
## [Manipulation d'objets tangibles et réalité augmentée](./images/img-effets/Effets-epfl.mp4)

Accès en ligne https://videos.univ-grenoble-alpes.fr/video/5221-epfl/


---
# Résultats de l'expérimentation :
* Les élèves sont super-contents, motivés, excités ;
* Les enseignants aussi ;

---


# mais

## les apprentissages ne sont pas meilleurs qu'avec la méthode papier crayon... :thumbsdown:

---

# Chronologie classique d'une telle expérimentation :
* On cherche
* On conçoit
* On expérimente en établissement
* On rentre
* **On pleure**

(Dillenbourg, 2018)






---
## 2.3. Des pratiques de terrain souvent basées sur la connaissance commune...



###  Mythes et légendes autour du numérique en classe

---

### :pencil2: TP : Durée 5 minutes

En Binôme, réfléchissez à ces affirmations, sont-elles vraies ou fausse ?

* Le numérique permet de s'adapter aux styles d’apprentissage des élèves.
* Les *Digital Natives* apprennent différemment.
* Le numérique est motivant pour les élèves.
* Les jeux vidéo permettent des apprentissages.
* Le numérique favorise l'autonomie des apprenants.
* L'écriture au clavier favorise l'apprentissage de la lecture.
* La lecture sur écran réduit les compétences de lecture traditionnelle.




---

# Les mythes et légendes autour du numérique en classe

* Le numérique permet de s'adapter aux styles d’apprentissage des élèves :arrow_right: Mythe...

* Les *Digital Natives* sont différents :arrow_right: Mythe...

#
>[Quelques mythes dans la recherche en éducation](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/mythes_education.html) (Dessus & Charroud, 2017)



---
## Des "mythes" récurrents

* **Le numérique est motivant pour les élèves** :arrow_right: Vrai dans certains cas mais pas de lien avec les performances réelles d'apprentissages. (Oviatt & Cohen, 2010) (Sung & Mayer, 2013)
#
* **Les jeux vidéo permettent des apprentissages** :arrow_right: Possible seulement si le jeu contient un scénario pédagogique adapté. (Wouter & Van Oostendrop, 2013).

:warning: Le mot "ludique" employé dans le sens faible, désignant le fait que l'environnement est amusant, **n'a pas de rapport établi avec les apprentissages** (Vogel et al., 2006) :warning:

---
## Autres "mythes" récurrents
* **Le numérique favorise l'autonomie des apprenants** :arrow_right: faux, l'autonomie est une compétence à acquérir pour pouvoir apprendre avec le numérique et non le contraire. (Lehmann, 2014).
* **L'écriture au clavier favorise l'apprentissage de la lecture** :arrow_right: faux, l'écriture au clavier diminue la capacité de reconnaissance des lettres (Kersey & James, 2013).

---
## Autres "mythes" récurrents

* **La lecture sur écran réduit les compétences de lecture traditionnelle** :arrow_right: faux, la lecture numérique fait appel à des compétences partagées avec la lecture papier. (Baccino, 2004) (Britt & Rouet, 2012)
#
:warning: Recheche web :arrow_right: lecteur expert pour parcourir efficacement l’information (Salmeron & Garcia , 2011).
:warning: Recheche web :arrow_right: habiletés qui aident à s’autoréguler. (Dogusoy-Taylan & Cagiltay, 2014)


---

### :pencil2: TP : Un bilan intermédiaire (durée 1 minutes)

Reprenez la liste effets positifs/effets négatifs que vous avez rédigée, faite un premier bilan...

---

#  Numérique et apprentissages, vous voulez vous lancer ?

---

# 3. Numérique et apprentissages, quelles précautions ?

## Limiter les effets négatifs

---


# 3.1. Le numérique est-il dangereux pour la santé?


---
## 3.1.1 Des craintes récurrentes et médiatisées 1/2

* Les écrans :
	* 	Des effets sur le développement cérébral des enfants en âge préscolaire (Hutton & al., 2020)(Lin & al., 2022)
	* 	Sédentarité :arrow_right: Limitation du temps d'écran = activité physique (Pedersen,2022)
	* 	Sommeil :arrow_right: Conférence sommeil et apprentissages
	* 	Problèmes ophtalmiques [Ecole numérique, écrans et santé ophtalmique (Aptel & Charroud, 2017)](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/cours_Ophtalmo.html)
	* 	Des effets psychologiques et sociaux (Desmurget, 2019) ... mais peut-être simplement amplifiés par le numérique (Sauce, Liebherr, Judd & Klingberg, 2022)
	
---
## 3.1.2. Des craintes récurrentes et médiatisées 2/2


* Wifi, les ondes de la discorde, quid des autres ondes électromagnétiques ? [Numérique, wifi, téléphone, les ondes à l’école (Charroud & Choucroune, 2018)](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/cours_Ondes.html)
* Et l'impact du numérique sur l'environnement, vous y pensez ? [Numérique et environnement (Berthoud, Charroud, Renard , 2018)](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/cours_Environnement.html)




---


# 3.2. Analyses d'usage courant en classe 
## Il est indispensable d'analyser ses pratiques au regard des résultats de la recherche si l'on veut éviter les problèmes....

---


##  :warning: Les effets que l'on pense positifs peuvent :warning: rapidement se révéler négatifs :

Outre une perte de temps, d'énergie et de crédibilité: le risque principal est de générer une

## **augmentation de la différence entre les élèves**
> (Zheng, 2016)


---

## 3.2.1. Un exemple courant : On veut différencier avec le numérique


> On raconte qu'il suffit de présenter à l'apprenant une même information sous différents formats pour être efficace.





### Que dit la recherche ?

> Effects of prior knowledge on learning from different compositions of representations in a mobile learning environment (Liu, Lin & Paas, 2014)


---
![140%](images/img-effets/Effets-liu11.png)

Texte + Photo

---
![140%](images/img-effets/Effets-liu12.png)

Texte + Photo + Exemple réel

---
![140%](images/img-effets/Effets-liu13.png)

Texte + Schéma + Exemple réel


---
# À votre avis quel groupe a le mieux appris ?

Groupe 1 : Texte + Image
Groupe 2 : Texte + Image + Plante réelle
Groupe 3 : Texte + Schéma + Plante réelle



---
# Résultats
Les meilleurs apprentissages ont été obtenus par :
## le premier groupe (texte+image)

---

## Explication rationnelle
La multiplication des sources d’information (texte + image + objet réel) a provoqué une division de l’attention qui a gêné l’apprentissage des élèves.


---

# :exclamation: À retenir :exclamation:

**Pour différencier, ne vous laissez pas entraîner par la facilité à multiplier les sources avec le numérique.**

Utilisez un document avec 2 sources :
* **une source verbale** (écrite ou sonore)
* **une source picturale** (fixe ou animée)




***Sinon il sera profitable seulement aux meilleurs élèves.***


---
## 3.3 Toutes les sources numériques sont-elles efficaces ?

### Avec le numérique on néglige souvent la charge cognitive...


#
### Que dit la recherche ?
> La charge cognitive dans l’apprentissage (Charroud & Dessus, 2016)




---
### 3.3.1. Charge cognitive 1/2
* Les sources doivent être épurées.

### Attention aux vidéos et/ou animations

Pour qu'une vidéo/animation soit efficace pour les apprentissages, il faut :
* que l'apprenant ait un contrôle minimal sur le rythme de défilement d’une animation ou d’une vidéo;
* présenter de façon animée des informations elles-mêmes dynamiques;
* limiter le nombre d’informations à maintenir en mémoire pendant le visionnage.

Vérifiez bien les vidéos (même celles issues de sites institutionnels) avant de les utiliser....

---

### 3.3.2 Charge cognitive 2/2
### Apprentissage de règle logique : écran vs réelle

> Prefrontal cortex and executive function in young children. (Moriguchi & Hiraki, 2013). 

L’expérimentateur présente des règles à un apprenant soit en étant présent soit filmé et diffusé via en écran. L’apprenant doit inférer les règles.
*  :warning: Il y a une différence au niveau des neurones miroirs !
	* Avec l’écran le réseau des neurones miroirs n’est pas activé.
	* Chez l'élève ce système est en développement.
	* :arrow_right: l'apprentissage va demander beaucoup d'effort à l'élève.
	(Ferrari, 2014) 

---
#  :exclamation: À retenir :exclamation:

Pensez à la charge cognitive :

* Des ressources épurées.
* Des vidéos contrôlables par l'élève.
* Ne pas oublier que la perception de l'élève est différente de celle de l'adulte (neurones miroirs)

---

### :pencil2: TP : Un bilan intermédiaire (durée 1 minutes)

Reprenez la liste effets positifs/effets négatifs que vous avez rédigée, corrigez si besoin...


---
# 4.  Des effets positifs du numérique sur les apprentissages

## et oui, il y en a ....


---

## L’utilisation du numérique ***peut*** avoir un effet positif sur l’apprentissage

- **Pour accéder à des informations**

- **Pour communiquer**

- **Pour s'entraîner sur des tâches ciblées**

> (J-PAL, 2019)


---


# Et en pratique, comment exploiter ces effets positifs potentiels ?

## Illustration basée sur les piliers fondamentaux de l’apprentissage identifiés par les neurosciences.


---
# 4.1. Numérique et attention
## L’attention
– Alerte (quand faire attention)
– Orientation (à quoi faire attention)
– Contrôle exécutif (comment faire)

> (Dehaene, 2018)

---
## Ressources numériques et attention de l'apprenant.


Les élèves sont-ils égaux devant un document numérique ? Même bien conçu...


## Que dit la recherche ?

> Eye-movement patterns (Mason, Tornatora, & Pluchino, 2013).

---
![110%](images/img-effets/Effets-masson1.png)

---
![110%](images/img-effets/Effets-masson2.png)

---
## Résultats :
Plus les apprenants réalisent de traitements d'intégration entre la source verbale (texte ou son) et la source picturale (illustrations) plus ils apprennent.

## Conclusions :
* Proposer 2 sources dans un document et pas une seule...

* :warning: À partir d'un même document, les apprenants n'ont pas la même stratégie d'apprentissage, **seuls les meilleurs profitent du document**.


---
## Comment inciter les transitions entre les sources ?

> Utilisation d'un document avec commentaires sonores (Jamet, 2014).

![150%](images/img-effets/Effets-jamet14.png)

---

Avec ce **guidage**, les apprenants accordaient davantage d’attention (temps de fixation total) aux informations pertinentes grâce à la signalisation.

## Résultats :
* Des effets sur la complétude et la rétention (Mémorisation)  
	
* Mais pas d'effet sur les apprentissages profonds (Compréhension)



---
# 4.2. Numérique et Feed-back


## Le retour sur erreur
– Nécessité de feed-back
– … même sur réponse correcte (tant que la connaissance n’est pas parfaite, le cerveau continue d’apprendre) => nécessité de surapprentissage (réviser, remettre à l’épreuve…)

> (Dehaene, 2018)

---


## Entrainement sur des tâches ciblées
![10%](images/img-effets/Effets-charge.jpg)

**Automatisation :**
Systématisation :arrow_right: Exerciseurs :arrow_right: **Tuteurs intelligents**

---
## Un peu d'histoire
* Années 50, Skinner et l'enseignement programmé
* Annèes 70-80, Intelligence Artificielle
* Années 2000, développement du numérique individuel

***Beaucoup d'espoirs et... des résultats positifs***

---
## Pourquoi ça marche ?

* Feedback immédiat (renforcement).
* Patience infinie....

mais pas que...

---
## Un exemple pour l'apprentissage de l'écriture
On apprend beaucoup mieux à écrire sur une tablette avec le doigt !
(Application ayant servi à l'expérimentation : *Writing wizard*)

:arrow_right: Retour proprioceptif

:arrow_right: Correction immédiate, donc retour sur erreur

> (Patchan & Puranik, 2016)

---
## Aider des enfants qui ont de troubles de l’apprentissage ou de la cognition numérique
Ex. Opération simple ( ex. 5+9=14)  sous observation I.R.M
* Dyscalculiques et typiques utilisent les mêmes zones cérébrales.
* Dyscalculique recrutent beaucoup plus ces zones.

:arrow_right: entraînement avec feedback immédiat pendant trois semaines sur les éléments simples (ex. 3+4=7),  pas besoin de compter ou de réfléchir mais simplement de mémoriser.
:arrow_right: Très forte réduction de la différence entre les dyscalculiques et les typiques, normalisation du réseau neurologique qui est impliqué dans la réalisation de petites opérations
:arrow_right: Exerciseurs ou tuteurs intelligents pour généraliser ?
(Iuculano & al. 2015)

---
## Apprentissage de la lecture avec une application 1/2
:arrow_right: Aider l’enfant à faire des appareillements grapho-phonémiques.


Rappel sur l'apprentissage de la lecture :
* Réseau ventral activé par les lecteurs experts = reconnaissance automatiquement la forme globale du mot -> passage direct des aires visuelles de la reconnaissance de la forme des mots (et des lettres) au sens du mot en passant par le cortex frontal.
* Pour les novices, passage par les aires phonologiques et par les aires langages pour déconstruire les phonèmes et le cerveau essaie de faire un appareillement entre le graphème et le phonème= beaucoup plus lent...

---
## Apprentissage de la lecture avec une application 2/2
:arrow_right: Aider l’enfant à faire des appareillements grapho-phonémiques.

Ex. Grapholearn = un jeu pouvant aider l’enfant dans ses premières étapes de la lecture à faire un appareillement grapho-phonémique.

Fonctionne sur une langue transparente (un graphème = un phonème) le français n’est pas très transparent donc ça fonctionne seulement sur les trois premiers mois d’apprentissage de la lecture.
Le numérique ça peut aider l’enfant à un moment de son apprentissage.
... à suivre !
(Lassault & Ziegler, 2018) (Bailly & al., 2020).

---
### Des applications en cours d'analyse ou de développement par la recherche
**Partenariat d'Innovation et Intelligence Artificielle (P2IA)**

Projet Fluence (Mandin et al., 2021)
> Evasion et Elargir :arrow_right: Lecture
> Luciole :arrow_right: Anglais

Français :
> KALIGO (Bonneton-Botté, N. & Anquetil, E., 2018)
> LALILO (Sergent, T., 2020)

Mathématiques :
> ADAPTIV MATHS (Luengo, V. & al., 2020)
> SCRATCH (Laurent & al., 2022)

... et d'autres, :warning: mais il faut attendre les résultats de la recherche
.


---

# Conclusion sur ces exerciseurs et/ou tuteurs intelligents
***De bons résultats MAIS qui ne dépassent pas le tutorat humain!***
(Kulik, & Fletcher 2016)

#

*...Pour votre employeur, il est plus facile de multiplier les tuteurs numériques que les tuteurs humains...*

---

## :pencil2: TP : Durée 5 minutes
Recherchez des applications numériques permettant un "bon" retour sur erreur.

**Rappel : Le retour sur erreur**
– Nécessité de feed-back
– … même sur réponse correcte (tant que la connaissance n’est pas parfaite, le cerveau continue d’apprendre) => nécessité de surapprentissage (réviser, remettre à l’épreuve…)


---

# 4.3. Numérique et mémorisation

## La consolidation
– Passer d’un traitement lent, conscient, avec effort (sous contrôle du cortex préfrontal) à un fonctionnement rapide, inconscient, automatique :
– Répéter :arrow_right: Automatisation
– Libérer des ressources cognitives pour éviter le « goulot d’étranglement cognitif »

> (Dehaene, 2018)

---
## Consolider, un sujet déjà assez ancien

![170%](images/img-effets/Effets-ebbinghaus.jpg)

---
### Mémoire : quelques rappels sur l'anatomie cérébrale



![](images/img-effets/Effets-myelinreal.jpg)

---
### Le processus de myélinisation

![50%](images/img-effets/Effets-myelinzoom.jpg)

---
### Importance de cette couche de Myéline

* L’épaisseur du gainage de myéline est en relation directe avec nos aptitudes et nos performances.
* L'épaisseur de la myéline dépend du nombre et de l'espacement des sollicitations (Rappel expansé)
* Le numérique permet de poser des questions **"au bon moment"** en s'adaptant à la physiologie

Exemple **non exhaustif** : L'application ANKI

---



![100%](images/img-effets/Effets-anki_ex.jpg)



---



# Comment aborder l'avenir ?

Défendons plutôt une posture critique ayant du sens, une
finalité, de l’objectivité et de la rigueur. 

Cela implique d’aborder les technologies avec méfiance et scepticisme 
mais toujours dans un esprit constructif plutôt qu’avec cynisme.

> (Selwyn, 2018)

---


# 4.4. Des entrainements intéressants 
# (ou pas)
Un exemple d'usage necessitant une posture critique

---


# Le numérique pour entrainer des compétences transversales nécessaires aux apprentissages :

* Améliorer la vision
* Réduire les déficits d'attention
* Favoriser le traitements multitâches





---
## Que dit la recherche ?
### :warning: C'est très contre-intuitif
Certains jeux vidéo souvent considérés comme les pires (les jeux de tir à la première personne) font partie des usages qui permettent le plus d'entraîner des circuits cérébraux impliqués dans des tâches transversales nécessaires aux apprentissages....

---
## Dans le détail 1/3
Les jeux vidéo d'action permettent d'améliorer la vision  en entraînant :

* l'identification de petits détails au milieu du désordre
* la capacité à distinguer et identifier les niveaux de gris

> (Eichenbaum, 2014)


---
## Dans le détail 2/3
Les jeux vidéo d'action permettent de réduire les déficits d'attention (Green, 2003) :	
* Résolution de conflits cognitifs plus rapide
* Augmentation de la capacité à suivre plus d'objets dans un environnement évolutif (3-4 objets pour un non joueur, jusqu'à 7 objets pour un joueur...)

IRM :arrow_right: des changements visibles sur des réseaux corticaux des joueurs:
	* Cortex pariétal qui contrôle l’attention et l’orientation
	* Cortex frontal qui nous aide à maintenir notre attention (inhibition)
	* Cortex cingulaire antérieur qui contrôle comment nous affectons et régulons notre attention et résolvons les conflits.

---
## Dans le détail 3/3
Les jeux vidéo d'action permettent d'entraîner aux traitements multitâches  (Bavelier, 2018).

Les gens qui font des jeux vidéo d’action sont très très bons, ils passent d’une tâche à l’autre très rapidement avec un faible coût cognitif.
Les gens qui pratiquent uniquement le multitâche multimédia (utilisation simultanée d'un navigateur web, en écoutant de la musique, en gardant un œil sur son smartphone...) sont très nettement moins performants.

---
## :warning: Que retenir de l’effet des jeux vidéo :
* La sagesse collective n’a pas de valeur :arrow_right: c'est très contre-intuitif
* Tous les médias ne naissent pas tous égaux, ils ont des effets totalement différents sur différents aspects de la cognition de la perception et de l’attention :arrow_right: chaque jeu doit faire l'objet de test...
* Ces jeux vidéo ont des effets puissants pour la plasticité cérébrale, l’attention, la vision.
## Mais aussi pleins d'effets non désirables (santé, psychologie, sociale) , il faudrait les consommer avec modération et au bon âge... 


---
## Les jeux vidéo pour apprendre, pour de vrai et sans effets négatifs... un jour peut-être ?
Il faudrait :
* Comprendre quels sont les "bons" ingrédients pour produire des effets positifs en termes d'apprentissages. (Brocolis)
* Réaliser des produits attirants auxquels on ne peut pas résister. (Chocolat)
* Réunir les deux :arrow_right: ce n'est pas simple (Le brocoli au chocolat ce n’est pas terrible)

### C'est en cours d'exploration....



---


## Avec le numérique, il est nécessaire de toujours évaluer le rapport bénéfices/risques 

![100%](images/img-effets/Effets-balance.jpeg)


---


# 5. Conclusion
## Le numérique peut être efficace pour les apprentissages... mais pas dans tous les cas, et pas à tous les âges.

# Avancez avec prudence, essayez de pas vous laisser entraîner par la connaissance commune, mais appuyez-vous sur la connaissance scientifique !

---

## Production attendue pour la thématique "effets du numérique sur les apprenants"

A partir d'une situation d'apprentissage, essayez d'évaluer les effets du numérique sur les apprentissages, sont-ils positifs ou risquent-ils d'êtres négatifs ?
Proposez une ou des évolutions destinées à limiter les effets négatifs et produire des effets positifs.




---
## À méditer :

Le numérique n’est pas une boîte à outils, une valise d’applications et de logiciels qui viennent agrémenter l’action pédagogique et les processus d’apprentissage ou se substituer à d’autres méthodes d’enseignement-apprentissage alors jugées moins innovantes. 

Le numérique est un objet complexe, englobant des acceptions multiples, et caractérisant des objets et outils dont il ne suffit pas de se saisir, de façon pragmatique, pour comprendre le monde et exercer un esprit critique."


*Extrait d'un rapport publié par le Centre national d’étude des systèmes scolaires (Cnesco) sur la thématique : Numérique et apprentissages scolaires. (Cordier, 2020)*




---

# Références

Baccino, T. (2004). La lecture électronique: Presses universitaires de Grenoble.

Bachelard, G. (1953). Le matérialisme rationnel. Paris: Presses universitaires de France.

Bailly, G., E. Godde, A.-L. Piat-Marchand & M,.-L. Bosse (2020).Predicting multidimensional subjective ratings of children’ readings from the speech signals for the automatic assessment of fluency. International Conference on Language Resources and Evaluation (LREC), Marseille, France.



Bonneton-Botté, N., & Anquetil, E. (2018, November). Kaligo: un cahier numérique pour l’apprentissage de l’écrit. Journée des technologies éducatives. Rennes, MSHB. In Journée des technologies éducatives, MSHB.

---

Britt, M. A., & Rouet, J. F. (2012). Learning with multiple documents: Component skills and their acquisition. Enhancing the quality of learning: Dispositions, instruction, and learning processes, 276-314.

Charroud, C. & Dessus, P. (2016). La charge cognitive dans l’apprentissage. Consulté à https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/chargecog.html .

Cordier, A. (2020). Des usages juvéniles du numérique aux apprentissages hors la classe. Paris : Cnesco.

Dillenbourg, P. (2018). Pensée computationnelle: pour un néopapertisme durable car sceptique. De 0 à 1 ou l’heure de l’informatique à l’école, 17.

Dehaene, S. (2018). Apprendre!: Les talents du cerveau, le défi des machines. Odile Jacob.

---

Desmurget, M. (2019). La fabrique du crétin digital-Les dangers des écrans pour nos enfants. Média Diffusion.

Dogusoy-Taylan, B., & Cagiltay, K. (2014). Cognitive analysis of experts’ and novices’ concept mapping processes: An eye tracking study. Computers in human behavior, 36, 82-93.


Ferone, G. (2019). Numérique et apprentissages : prescriptions, conceptions et normes d’usage. Recherches en Éducation, 35, 63–75.

Ferrari P. F. (2014) « The neuroscience of social relation. A comparative-based approach to empathy and to the capacity of evaluating others’action value », Behavior, 151.

---

Houart, M. (2017). L’apprentissage autorégulé: quand la métacognition orchestre motivation, volition et cognition. Revue internationale de pédagogie de l’enseignement supérieur, 33(33-2).

Hutton, J. S., Dudley, J., Horowitz-Kraus, T., DeWitt, T., & Holland, S. K. (2020). Associations between screen-based media use and brain white matter integrity in preschool-aged children. JAMA pediatrics, 174(1), e193869-e193869.

Iuculano, T., Rosenberg-Lee, M., Richardson, J., Tenison, C., Fuchs, L., Supekar, K., Menon, V. (2015). Cognitive tutoring induces widespread neuroplasticity and remediates brain function in children with mathematical learning disabilities. Nature Communications, 6:8453.

---

Jamet, E. (2014). An eye-tracking study of cueing effects in multimedia learning. Computers in Human Behavior, 32, 47-53.

J-PAL Evidence Review. 2019. “Will Technology Transform Education for the Better?” Cambridge, MA: Abdul Latif Jameel Poverty Action Lab.


Kulik, J. A., & Fletcher, J. D. (2016). Effectiveness of intelligent tutoring systems: a meta-analytic review. Review of Educational Research, 86(1), 42-78.

---

Kersey, A. J., & James, K. H. (2013). Brain activation patterns resulting from learning letter forms through active self-production and passive observation in young children. Frontiers in psychology, 4, 567.


Lassault, J., & Ziegler, J. C. (2018). Les outils numériques d’aide à l’apprentissage de la lecture. Langue française, (3), 111-121.

Laurent, M., Crisci, R., Bressoux, P., Chaachoua, H., Nurra, C., de Vries, E., & Tchounikine, P. (2022). Impact of programming on primary mathematics learning. Learning and Instruction, 82, 101667. https://doi.org/10.1016/j.learninstruc.2022.101667

Lacelle, N., & Lebrun, M. (2016). La formation à l’écriture numérique: 20 recommandations pour passer du papier à l’écran. Revue de recherches en littératie médiatique multimodale, 3.

---

Lehmann, T., Hähnlein, I., & Ifenthaler, D. (2014). Cognitive, metacognitive and motivational perspectives on preflection in self-regulated online learning. Computers in human behavior, 32, 313-323.

Lin Y, Lin S, Gau SS. Screen Timing May Be More Likely Than Screen Time to Be Associated With the Risk of Autism Spectrum Disorder. JAMA Pediatr. 2022;176(8):824–825. doi:10.1001/jamapediatrics.2022.1516

Liu, T. C., Lin, Y. C., & Paas, F. (2014). Effects of prior knowledge on learning from different compositions of representations in a mobile learning environment. Computers & Education, 72, 328-338.

Lunego, V., Bouchet, F., Carron, T., Muratet, M., Noel, Y., Yessad, A., & Giroire, H. Équipe MOCAH-LIP6: Modèles et Outils en ingénierie des Connaissances pour l’Apprentissage Humain. Bulletin N o 108, 21.

---

Mandin, S., Loiseau, M., Bailly, G., Blavot, A., Bosse, M. L., Briswalter, Y., ... & Valdois, S. (2021, June). EVASION, ELARGIR et LUCIOLE: 3 jeux tablettes du projet FLUENCE pour prévenir les difficultés d’apprentissage de la lecture et de l’anglais. In PRUNE II 2021-Colloque Perspectives de Recherches sur les Usages du Numérique dans l'Éducation.

Mason, L., Tornatora, M. C., & Pluchino, P. (2013). Do fourth graders integrate text and picture in processing and learning from an illustrated science text? Evidence from eye-movement patterns. Computers & Education, 60(1), 95-109.

Mayer, C. P. (2009). Security and privacy challenges in the internet of things. Electronic Communications of the EASST, 17.

---

Moriguchi, Y., & Hiraki, K. (2013). Prefrontal cortex and executive function in young children: A review of NIRS studies. Frontiers in Human Neuroscience, 7, Article 867

Oviatt, S. L., & Cohen, A. O. (2010). Toward high-performance communications interfaces for science problem solving. Journal of science education and technology, 19(6), 515-531.


Patchan, Melissa & Puranik, Cynthia. (2016). Using tablet computers to teach preschool children to write letters: Exploring the impact of extrinsic and intrinsic feedback. Computers & Education. 102.  

Pedersen J, Rasmussen MGB, Sørensen SO, et al. Effects of Limiting Recreational Screen Media Use on Physical Activity and Sleep in Families With Children: A Cluster Randomized Clinical Trial. JAMA Pediatr. 2022;176(8):741–749. doi:10.1001/jamapediatrics.2022.1519

---

Salmerón, L., & García, V. (2011). Reading skills and children’s navigation strategies in hypertext. Computers in Human Behavior, 27(3), 1143-1151.

Sauce, B., Liebherr, M., Judd, N., & Klingberg, T. (2022). The impact of digital media on children’s intelligence while controlling for genetic differences in cognition and socioeconomic background. Scientific reports, 12(1), 1-14.

Sergent, T. (2020). Améliorer l'autorégulation de jeunes élèves lors de l'apprentissage de la lecture. In 8e Rencontres Jeunes Chercheurs en EIAH.


Sung, E., & Mayer, R. E. (2013). Online multimedia learning with mobile devices and desktop computers: An experimental test of Clark’s methods-not-media hypothesis. Computers in Human Behavior, 29(3), 639-647.

---

Vogel, J. J., Vogel, D. S., Cannon-Browers, J., Browers, C. A., Muse, K., & Wright, M. (2006). Computer gaming and interactive simulations for learning: A meta-analysis. Journal of Educational Computing Research, 34. 229-243.


Wouters, P., & Van Oostendorp, H. (2013). A meta-analytic review of the role of instructional support in game-based learning. Computers & Education, 60(1), 412-425.


Zheng, B., Warschauer, M., Lin, C. H., & Chang, C. (2016). Learning in one-to-one laptop environments: A meta-analysis and research synthesis. Review of Educational Research, 86(4), 1052-1084
