---
marp: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# UE 24 Option de recherche “Interactions enseignant-élèves“
## Philippe Dessus, Inspé, Univ. Grenoble Alpes
## Séance 10 - Présenter le mémoire
##### Année universitaire 2023-24

![w:350](images/logo-espe-uga.png)

---
<!-- footer: Ph. Dessus • M1 PE • TD Option de recherche “Interactions enseignant-élèves • Inspé-UGA 2023-24 • CC:BY-NC-SA-->
<!-- paginate: true -->

# :one::zero: 0. But de la séance

1. Finalisation de l'écriture
2. Principaux écueils dans l'écriture
3. Conseils pour la soutenance
4. Principaux écueils dans la soutenance
5. L'après-mémoire


---
# :one::zero: 1. Finalisation de l’écriture (1/3)

- S’assurer qu’aucun élève/parent ne puisse être reconnu, même anonymé
- Attention aux jugements que vous portez
- S’assurer que le mémoire correspond aux documents de cadrage (Présentation générale et Guide de rédaction)
- Présentez vos résultats en lien avec vos hypothèses, **et seulement eux**. Reliez-les à l'état de l'art. 
- Essayer de trouver une explication pour la ou les hypothèse.s non validée.s. 

---
# :one::zero: 1. Mémoire : Finalisation de l’écriture (2/3)

- Référer, notamment en discussion, au programmes et au référentiel de compétences des enseignants en discussion (dire la ou les compétences favorisées tout au long du travail sur le mémoire)

- Le résumé en anglais peut être produit (et vérifié ensuite) par [deepl](https://www.deepl.com/translator), le meilleur traducteur gratuit

---
# :one::zero: 1. Mémoire : Finalisation de l’écriture (3/3)

- Les 1re et 4e de couverture ont des modèles à respecter (voir site e-formation)
- Vérifier que la page de couverture contient les logos nécessaires, et que la 4e de couv. contient le résumé et les mots-clés
- Le formulaire de non-plagiat est à remplir lors du dépôt du mémoire 
- Déposer séparément l'accord de dépôt du mémoire/ESR sur Dumas
- voir grille d'évaluation dans le site e-formation


---
# :one::zero: 2. Mémoire : Principaux écueils (écrit) (1/2)

- Parties plagiées (:warning: tous les mémoires seront vérifiés avec Compilatio et leurs rapports envoyés pour vérification)
- Maîtrise insuffisante de la langue française (faire relire le mémoire par des tiers)
- Pas de problématique claire
- Pas de liens (ou liens insuffisants) entre partie théorique et empirique 

---
# :one::zero: 2. Mémoire : Principaux écueils (écrit) (2/2)

- Partie empirique peu claire (on ne comprend pas ce que vous avez réalisé vous-même)
- Partie empirique non analysée
- Analyse peu fouillée des données recueillies
- Analyse réflexive faible, manque de recul
- Texte très court, mal documenté ; ou texte trop long et verbeux, explications embrouillées
- Pas de respect de la vie privée et/ou du droit à l’image (photos où l'on peut reconnaître des personnes, prénoms d'élèves non anonymés)

---
# :one::zero: 3. Soutenance : Rappel des conditions (1/3)

(voir doc. Cadrage mémoire)
- La soutenance (20 min) est individuelle (même quand ESR faits entièrement à 2, DU). Elle comporte deux parties :

- Une présentation (**10 min**) focalisée sur des aspects choisis du mémoire. Elle met en évidence le cheminement professionnel et souligne les aspects les plus formateurs du travail. La présentation orale s’appuie sur des supports de communication.

---
# :one::zero: 3. Soutenance : Rappel des conditions  (2/3)

(voir doc. Cadrage mémoire)
- Une discussion avec le jury (**10 min**), au cours de laquelle l’étudiant témoigne de son appropriation de la problématique, de sa maîtrise du sujet et d’une prise de recul.


---
# :one::zero: 3. Soutenance : Rappel des conditions  (3/3)

(voir doc. Cadrage mémoire)
- Quand ils ne font pas partie du jury, les professionnels de terrain sont invités à la soutenance mais ils ne participent pas aux délibérations. 
- Note d'écrit compte pour 65 % de la note finale, la note d'oral compte pour 35 %

---
# :one::zero: 3. Conseils pour la soutenance (1/2)

- Avoir lu la grille d'évaluation du mémoire, vérifier que votre travail répond aux différents critères
- Respecter **scrupuleusement** le temps (le jury peut vous arrêter une fois le temps écoulé), donc, **répéter**
- Réaliser des diapositives **lisibles**, sans animation, et en nombre réduit (une dizaine max.). Ne pas copier-coller de tableaux ou images qui seront illisibles


---
# :one::zero: 3. Conseils pour la soutenance (2/2)

- Le jury a lu le mémoire, donc lui rappeler les éléments essentiels (d’un point de vue théorique et empirique) ; insister sur les aspects liés au métier d’enseignant, ce qui a été appris en faisant ce mémoire
- Le jury ne cherche pas (la plupart du temps !) à vous tendre des pièges : il veut plutôt comprendre votre démarche, ce que vous avez fait et pourquoi. Répondez clairement et le plus **succinctement** possible à ses questions

---
# :one::zero: 3. Grille d’évaluation (résumé *cf.* la grille complète)

- Ecrit
	- Se former et innover par la rédaction d’un mémoire professionnel
	- Maîtriser la langue française
- Oral
	- Exposer d’une manière critique l’analyse d’une pratique pro.
	- Communiquer efficacement à l’oral
- Note modulée en fonction de l’implication de l’étudiant (avis de l’encadrant)

---
# :one::zero: 4. Écueils pendant la soutenance

- Lire les diapositives sans ajouter de plus-value (effet karaoké)
- Présenter des copies d’écran du mémoire illisibles (“vous ne pouvez pas lire cette diapo, mais...”)
- Présenter des diapositives trop chargées (“ce qui est important ici est ceci...”)
- Se perdre dans des anecdotes, ou dans la théorie



---
# :one::zero: 5. L'après-mémoire : Valorisation

- Les meilleurs mémoires feront l’objet d’une présentation orale lors de la Journée de l’école organisée en fin d’année, le **date à venir** à l'Inspé de Chambéry sur proposition de leurs encadrants

---
# :one::zero: 5. L'après-mémoire 

- Les mémoires ayant eu une note d’au moins **16/20** sont archivés publiquement (base DUMAS), après accord de leurs auteurs
- :warning: vous pouvez être démarché.es par des éditeurs à compte d’auteur. N’accepter l’édition qu’après avoir lu de près le contrat (notamment en ce qui concerne l’achat d’exemplaires et la cession des droits). Ce type d'édition ne compte pas ou peu pour un éventuel projet de carrière universitaire

---
:one::zero: 6. Tâches

- Mettre à profit cette fin de séance pour avancer dans l'écriture du document de contrôle continu... et de votre mémoire !