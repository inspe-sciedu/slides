---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# Effets du numérique sur les apprentissages 
### Apports théoriques

Christophe Charroud - UGA

<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2025 - CC:BY-NC-SA -->


---
# :zero: Préambule 

 
### À votre avis, si l'on s'intéresse aux apprentissages des élèves, utiliser le numérique en classe c'est ?
* Positif
* Négatif
...ou entre les deux....



---
## Numérique et apprentissage, des affirmations

* Le numérique permet de s'adapter aux styles d’apprentissage des élèves.
* Les *Digital Natives* apprennent différemment car leur cerveau est différent.
* Le numérique favorise l'autonomie des apprenants.
* La lecture sur écran réduit les compétences de lecture traditionnelle.
* Le numérique c'est bien pour les apprentissages car c'est ludique

---
## Numérique et apprentissage, un constat scientifique

* ~~Le numérique permet de s'adapter aux styles d’apprentissage des élèves.~~
* ~~Les *Digital Natives* apprennent différemment car leur cerveau est différent.~~
* ~~Le numérique favorise l'autonomie des apprenants.~~
* ~~La lecture sur écran réduit les compétences de lecture traditionnelle.~~
* ~~Le numérique c'est bien pour les apprentissages car c'est ludique.~~



---
# Pourquoi une telle dissonance entre représentations et réalité? 
### « Il y a une rupture entre connaissance commune et connaissance scientifique »

> Bachelard : le matérialisme rationnel, 1953



### Dans l'éducation, quand on parle de numérique, les doxas* tiennent souvent lieu de pensée. 
> (Ferone. G, 2019)



*Doxa : Ensemble d'opinions, de préjugés, de présuppositions généralement admises et évaluées positivement ou négativement.



---

# Alors, que dit la recherche ?


Les effets **positifs se limitent à des usages circonscrits**.

Hors de ces usages, le numérique produit (souvent) des effets négatifs sur les apprentissages.


## En clair, la recherche dit que numérique pour les apprentissages, ce n'est pas magique:exclamation: 





---
# :one: Des effets positifs
##  et oui, il y en a ....
 


---
## L’utilisation du numérique ***peut*** avoir un effet positif sur l’apprentissage

- **Pour accéder à des informations**

- **Pour communiquer**

- **Pour s'entraîner sur des tâches ciblées**

> (J-PAL, 2019)


---
## L’utilisation du numérique ***peut*** avoir un effet positif sur l’apprentissage
#### :arrow_right: Il est indispensable d'analyser ses pratiques au regard des résultats de la recherche si l'on veut éviter les problèmes....
# 
###  :warning: Les effets que l'on pense positifs peuvent :warning: rapidement se révéler négatifs :

Outre une perte de temps, d'énergie et de crédibilité: le risque principal est de générer une **augmentation de la différence entre les élèves**
(Zheng, 2016)


---
# :two: Accéder à l'information
### Des points positifs:
* Information disponible tout le temps et en tous lieux (ou presque).
* Quantité d'information 
* Supports variés 

### Des points négatifs:
* Quantité d'information :arrow_right: tri, validité, perte de temps
* Supports variés :arrow_right: nécessite matériels et applications 
* Informations payantes :arrow_right: frustration
* Recherche sur web :warning: lecture

---
## 2.1 :thumbsup: Exemple de bon usage
Donner accès **en ligne** à des **contenus numériques supplémentaires** peut aider les élèves à **approfondir le cours** et constitue un bon moyen d'accompagner **l'hétérogénéité** des niveaux. Il faut alors **encourager les élèves à se saisir de ces contenus** (accompagnement, notes bonus…), sans pénaliser ceux qui ne le peuvent pas.

(Gonthier, 2025)

---
# :three: Communiquer

### Des points positifs :
* Sources variées
* Attention

### Des points négatifs 
* Sources variées
* Attention

# :rage:


---
## 3.1 Quelques explications pour limiter les mésusages de la communication avec le numérique
* Multiplier les sources dans un même document, bonne ou mauvaise idée ?
* Écran et charge cognitive
* Capter et maintenir l'attention
* Vidéo et apprentissages...



---
### Multiplier les sources dans un même document, bonne ou mauvaise idée ?


> On raconte qu'il suffit de présenter à l'apprenant une même information sous différents formats pour être efficace.




### Que dit la recherche ?
> Effects of prior knowledge on learning from different compositions of representations in a mobile learning environment (Liu, Lin & Paas, 2014)

---
![140%](images/img-effets/Effets-liu11.png)

Texte + Photo

---
![140%](images/img-effets/Effets-liu12.png)

Texte + Photo + Exemple réel

---
![140%](images/img-effets/Effets-liu13.png)

Texte + Schéma + Exemple réel


---
### À votre avis quel groupe a le mieux appris ?

Groupe 1 : Texte + Image
Groupe 2 : Texte + Image + Plante réelle
Groupe 3 : Texte + Schéma + Plante réelle



---
### Résultats
Les meilleurs apprentissages ont été obtenus par :
# Le premier groupe (texte+image)

---

### Explication rationnelle
La multiplication des sources d’information (texte + image + objet réel) a provoqué une division de l’attention qui a gêné l’apprentissage des élèves.

---

# :exclamation: À retenir :exclamation:

**Ne vous laissez pas entraîner par la facilité à multiplier les sources avec le numérique.**

Utilisez un document avec 2 sources :
* **une source verbale** (écrite ou sonore)
* **une source picturale** (fixe ou animée)




***Sinon il sera profitable seulement aux meilleurs élèves.***

---
## 3.2 Écran et charge cognitive


#### Que dit la recherche ?
> La charge cognitive dans l’apprentissage (Charroud & Dessus, 2016)



---
#### Régle de base 
### Les sources doivent être épurées.
* Éviter les distracteurs 
* Utiliser des représentations simples et cohérentes
* Éliminer autant que possible la redondance


---

#### Apprentissage de règle logique : écran vs réelle

> Prefrontal cortex and executive function in young children. (Moriguchi & Hiraki, 2013). 

Adulte face à un écran (Neurones miroirs OK) :
:arrow_right: Apprentissage = Inférence de la règle

Élève face à un écran (Neurones miroirs en développement) :
:arrow_right: Apprentissage = Inférence de la règle + Interprétation empathique


> **L'apprentissage va demander beaucoup d'effort à l'élève.** (Ferrari, 2014) 

---
#### Ne pas oublier l'interface...


![30%](images/img-effets/Effets-geogebra.jpg)



Il faut respecter la simplicité d'utilisation, donnant accès aux fonctions facilitant les apprentissages scolaires. (Tijus, 2006)
    

---
## 3.3 Capter et maintenir l'attention

#### L’attention
– Alerte (quand faire attention)
– Orientation (à quoi faire attention)
– Contrôle exécutif (comment faire)

> (Dehaene, 2018)

---

*Les élèves sont-ils égaux devant un document numérique ? Même bien conçu...*


### Que dit la recherche ?

> Eye-movement patterns (Mason, Tornatora, & Pluchino, 2013).

---
![110%](images/img-effets/Effets-masson1.png)

---
![110%](images/img-effets/Effets-masson2.png)

---
### Résultats :

### Les meilleurs apprentissages sont réalisés pas le 3ème type d'élève.

Plus les apprenants réalisent de traitements d'intégration entre la source verbale (texte ou son) et la source picturale (illustrations) plus ils apprennent.

---
![110%](images/img-effets/Effets-masson2.png)

---

## bilan :
* Proposer 2 sources dans un document et pas une seule...

* :warning: À partir d'un même document, les apprenants n'ont pas la même stratégie d'apprentissage, **seuls les meilleurs profitent du document**.


---
### Comment inciter les transitions entre les sources ?

> Utilisation d'un document avec commentaires sonores (Jamet, 2014).

![150%](images/img-effets/Effets-jamet14.png)

---
# :exclamation: À retenir :exclamation:

Avec le **guidage**, les apprenants accordent davantage d’attention (temps de fixation total) aux informations pertinentes grâce à la signalisation.

### Résultats :
* Des effets sur la complétude et la rétention (Mémorisation)
	
* Mais pas d'effet sur les apprentissages profonds (Compréhension)





---

### 3.4 Vidéo et apprentissages...

Pour qu'une vidéo/animation soit efficace pour les apprentissages, il faut :
* que l'apprenant ait un contrôle minimal sur le rythme de défilement d’une animation ou d’une vidéo;
* que l'appenant fasse des pauses dans le défilement (ou imposer les pauses dans le défilement)
* présenter de façon animée des informations elles-mêmes dynamiques;

Vérifiez bien les vidéos (même celles issues de sites institutionnels) avant de les utiliser....

(Biard, Cojean & Jamet, 2018)(Cojean, Jamet2017)



---
# :exclamation: À retenir :exclamation:

* Utilisez un document avec 2 sources :
    * **une source verbale** (écrite ou sonore)
    * **une source picturale** (fixe ou animée)
* Des ressources épurées.
* Des vidéos contrôlables par l'élève, avec des pauses.
* Ne pas oublier que la perception de l'élève est différente de celle de l'adulte (neurones miroirs).
* Et enfin il ne faut pas que l'interface soit un obstacle aux apprentissages

---
## 3.5 :thumbsup: Exemples de bons usages
* **Projeter ponctuellement** des supports numériques qui **enrichissent** ce qui est possible sur papier (vidéo, modèle en 3D du corps humain, animation qui décompose un tracé ou un calcul…) aide beaucoup à la **compréhension**. 
* Projeter un texte identique à un support distribué aux élèves (correction d'exercices…) peut aussi aider à fluidifier un cours.
* Utiliser des **manuels numérique**s qui proposent du contenu de cours **enrichi** (vidéos, liens hypertextes, infobulles explicatives…) : ces supports aident à la **compréhension et
allègent les cartables**, mais ne doivent pas remplacer le papier (fracture numérique).
 
(Gonthier, 2025)

---
## :four: S'entrainer (évaluer, s'auto évaluer, mémoriser)

### Rappel sur les rétroactions pour l'appentissage
* Nécessité de feedback
* Feedback, si possible, immédiat 
* Si la réponse comporte une erreur :arrow_right: remédiation
* Si la réponse est correcte (tant que la connaissance n’est pas parfaite, le cerveau continue d’apprendre) => nécessité de surapprentissage (réviser, remettre à l’épreuve…)

(Dehaene, 2018)

---
### Entrainement sur des tâches ciblées
![10%](images/img-effets/Effets-charge.jpg)

**Automatisation :**
Systématisation :arrow_right: Exerciseurs :arrow_right: **Tuteurs intelligents**

---
## Un exemple pour l'apprentissage de l'écriture (PE)
On apprend beaucoup mieux à écrire sur une tablette avec le doigt !
(Application ayant servi à l'expérimentation : *Writing wizard*)

:arrow_right: Retour proprioceptif

:arrow_right: Correction immédiate, donc retour sur erreur

> (Patchan & Puranik, 2016)

---
## Aider des enfants qui ont de troubles de l’apprentissage ou de la cognition numérique (PE)
<!-- _class: t-90 -->

Ex. Opération simple ( ex. 5+9=14)  sous observation I.R.M
* Dyscalculiques et typiques utilisent les mêmes zones cérébrales.
* Dyscalculique recrutent beaucoup plus ces zones.

:arrow_right: entraînement avec feedback immédiat pendant trois semaines sur les éléments simples (ex. 3+4=7),  pas besoin de compter ou de réfléchir mais simplement de mémoriser.
:arrow_right: Très forte réduction de la différence entre les dyscalculiques et les typiques, normalisation du réseau neurologique qui est impliqué dans la réalisation de petites opérations
:arrow_right: Exerciseurs ou tuteurs intelligents pour généraliser ?
(Iuculano & al. 2015)

---
## Apprentissage de la lecture avec une application 1/2 (PE)
:arrow_right: Aider l’enfant à faire des appareillements grapho-phonémiques.


Rappel sur l'apprentissage de la lecture :
* Réseau ventral activé par les lecteurs experts = reconnaissance automatiquement la forme globale du mot -> passage direct des aires visuelles de la reconnaissance de la forme des mots (et des lettres) au sens du mot en passant par le cortex frontal.
* Pour les novices, passage par les aires phonologiques et par les aires langages pour déconstruire les phonèmes et le cerveau essaie de faire un appareillement entre le graphème et le phonème= beaucoup plus lent...

---
## Apprentissage de la lecture avec une application 2/2 (PE)
<!-- _class: t-90 -->

:arrow_right: Aider l’enfant à faire des appareillements grapho-phonémiques.

Ex. Grapholearn = un jeu pouvant aider l’enfant dans ses premières étapes de la lecture à faire un appareillement grapho-phonémique.

Fonctionne sur une langue transparente (un graphème = un phonème) le français n’est pas très transparent donc ça fonctionne seulement sur les trois premiers mois d’apprentissage de la lecture.
Le numérique ça peut aider l’enfant à un moment de son apprentissage.
... à suivre !
(Lassault & Ziegler, 2018) (Bailly & al., 2020).


---
### Pourquoi ça marche ?

* Feedback immédiat (renforcement).
* Patience infinie.... 
* et d'autres avantages (traces, adapatation, souplesse horaire...)





---
# :exclamation: À retenir :exclamation:


### Les exerciseurs et/ou tuteurs intelligents donnent de bons résultats MAIS qui ne dépassent pas le tutorat humain!
(Kulik, & Fletcher 2016)

#

Mais la technologie progresse...



---

# :five: Mémoire et numérique 
*Encore un entrainenement*
## La consolidation
– Passer d’un traitement lent, conscient, avec effort (sous contrôle du cortex préfrontal) à un fonctionnement rapide, inconscient, automatique :
– Répéter :arrow_right: Automatisation
– Libérer des ressources cognitives pour éviter le « goulot d’étranglement cognitif »

> (Dehaene, 2018)

---
## Consolider, un sujet déjà assez ancien

![170%](images/img-effets/Effets-ebbinghaus.jpg)

---
### Mémoire : quelques rappels sur l'anatomie cérébrale



![](images/img-effets/Effets-myelinreal.jpg)

---
### Le processus de myélinisation

![50%](images/img-effets/Effets-myelinzoom.jpg)

---
### Importance de cette couche de Myéline

* L’épaisseur du gainage de myéline est en relation directe avec nos aptitudes et nos performances.
* L'épaisseur de la myéline dépend du nombre et de l'espacement des sollicitations (Rappel expansé)
(Fields & Bukalo, 2020)(Pan & al., 2020)(Kern & Kheirbek,2022)

:arrow_right: Le numérique permet de poser des questions **"au bon moment"** en s'adaptant à la physiologie

Exemple **non exhaustif** : L'application ANKI

---


![100%](images/img-effets/Effets-anki_ex.jpg)

---
# :six: Numérique et apprentissages : un esprit critique nécessaire

Face au numérique, défendons plutôt une posture critique ayant du sens, une finalité, de l’objectivité et de la rigueur. 

Cela implique d’aborder les technologies avec **méfiance et scepticisme 
mais toujours dans un esprit constructif** plutôt qu’avec cynisme.

> (Selwyn, 2018)

---
# 

## Avec le numérique, il est nécessaire de toujours évaluer le rapport bénéfices/risques 

![100%](images/img-effets/Effets-balance.jpeg)

---

## A éviter !

![w:500](images/img-effets/Effets-tapis.png)

Réaliser exactement la même tâche avec ou sans technologie :arrow_right: Très faible intérêt !
:arrow_right: Modèle SAMR



---


#  Conclusion
#### Le numérique peut être efficace pour les apprentissages... mais pas dans tous les cas, et pas à tous les âges.

#### Avancez avec prudence, essayez de pas vous laisser entraîner par la connaissance commune, mais appuyez-vous sur la connaissance scientifique !

#### Gardez une posture critique et contructive



---
# :seven: Analyse de votre situation d'apprentissage


---
## À méditer :

Le numérique n’est pas une boîte à outils, une valise d’applications et de logiciels qui viennent agrémenter l’action pédagogique et les processus d’apprentissage ou se substituer à d’autres méthodes d’enseignement-apprentissage alors jugées moins innovantes. 

Le numérique est un objet complexe, englobant des acceptions multiples, et caractérisant des objets et outils dont il ne suffit pas de se saisir, de façon pragmatique, pour comprendre le monde et exercer un esprit critique."


> Extrait d'un rapport publié par le Centre national d’étude des systèmes scolaires (Cnesco) sur la thématique : Numérique et apprentissages scolaires. (Cordier, 2020)


---
# Références - Partie numérique et apprentissages 

<!-- _class: t-50 -->


Bachelard, G. (1953). Le matérialisme rationnel. Paris: Presses universitaires de France.
Bavelier, D., Bediou, B., & Green, C. S. (2018). Expertise and generalization: Lessons from action video games. Current opinion in behavioral sciences, 20, 169-173.
Biard, N., Cojean, S., & Jamet, E. (2018). Effects of segmentation and pacing on procedural learning by video. Computers in Human Behavior, 89, 411–417. https://doi.org/10.1016/j.chb.2017.12.002 
Bioulac, S., Baillieul, S., Charroud, C. (2023). Conférence sommeil et apprentissages. Consulté à https://videos.univ-grenoble-alpes.fr/video/26283-conference-sommeil-et-apprentissages/d6a9d07e4d2b514fe7537016dca0e758db529efc68a90b6f8b9375604bd52d1f/
Borst, G. (2019). Écrans et développement de l’enfant et de l’adolescent. Futuribles, (6), 41-49.
Britt, M. A., & Rouet, J. F. (2012). Learning with multiple documents: Component skills and their acquisition. Enhancing the quality of learning: Dispositions, instruction, and learning processes, 276-314.
Charroud, C., & Dessus, P. (2016). La charge cognitive dans l’apprentissage. Consulté à https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/chargecog.html .
Cojean, S., & Jamet, E. (2017). Facilitating information-seeking activity in instructional videos: The combined effects of micro- and macroscaffolding. Computers in Human Behavior, 74, 294–302. https://doi.org/10.1016/j.chb.2017.04.052
Cordier, A. (2020). Des usages juvéniles du numérique aux apprentissages hors la classe. Paris : Cnesco.
Dehaene, S. (2018). Apprendre!: Les talents du cerveau, le défi des machines. Odile Jacob.
Dogusoy-Taylan, B., & Cagiltay, K. (2014). Cognitive analysis of experts’ and novices’ concept mapping processes: An eye tracking study. Computers in human behavior, 36, 82-93.
Eichenbaum, A., Bavelier, D., & Green, C. S. (2014). Video games: Play that can do serious good. American Journal of Play, 7(1), 50-72.
Ferone, G. (2019). Numérique et apprentissages : prescriptions, conceptions et normes d’usage. Recherches en Éducation, 35, 63–75.
Ferrari P. F. (2014) « The neuroscience of social relation. A comparative-based approach to empathy and to the capacity of evaluating others’action value », Behavior, 151.
Green, C. S., & Bavelier, D. (2003). Action video game modifies visual selective attention. Nature, 423(6939), 534-537.
Gonthier, C. (2025). Conférence "Le numérique fait-il baisser le niveau des élèves ?", séminaire TNE.

---

<!-- _class: t-50 -->

Houart, M. (2017). L’apprentissage autorégulé: quand la métacognition orchestre motivation, volition et cognition. Revue internationale de pédagogie de l’enseignement supérieur, 33(33-2).
Hutton, J. S., Dudley, J., Horowitz-Kraus, T., DeWitt, T., & Holland, S. K. (2020). Associations between screen-based media use and brain white matter integrity in preschool-aged children. JAMA pediatrics, 174(1), e193869-e193869.
Jamet, E. (2014). An eye-tracking study of cueing effects in multimedia learning. Computers in Human Behavior, 32, 47-53.
J-PAL Evidence Review. 2019. “Will Technology Transform Education for the Better?” Cambridge, MA: Abdul Latif Jameel Poverty Action Lab.
Kulik, J. A., & Fletcher, J. D. (2016). Effectiveness of intelligent tutoring systems: a meta-analytic review. Review of Educational Research, 86(1), 42-78.
Kersey, A. J., & James, K. H. (2013). Brain activation patterns resulting from learning letter forms through active self-production and passive observation in young children. Frontiers in psychology, 4, 567.
Lacelle, N., & Lebrun, M. (2016). La formation à l’écriture numérique: 20 recommandations pour passer du papier à l’écran. Revue de recherches en littératie médiatique multimodale, 3.
Lehmann, T., Hähnlein, I., & Ifenthaler, D. (2014). Cognitive, metacognitive and motivational perspectives on preflection in self-regulated online learning. Computers in human behavior, 32, 313-323.
Liu, T. C., Lin, Y. C., & Paas, F. (2014). Effects of prior knowledge on learning from different compositions of representations in a mobile learning environment. Computers & Education, 72, 328-338.
Mason, L., Tornatora, M. C., & Pluchino, P. (2013). Do fourth graders integrate text and picture in processing and learning from an illustrated science text? Evidence from eye-movement patterns. Computers & Education, 60(1), 95-109.
Mayer, C. P. (2009). Security and privacy challenges in the internet of things. Electronic Communications of the EASST, 17.
Milcent K, Gassama M, Dufourg MN, Thierry X, Charles MA, Bois C. Child health screening program in French nursery schools: Results and related socioeconomic factors. Front Pediatr. 2023 May 4;11:1167539. doi: 10.3389/fped.2023.1167539. PMID: 37215596; PMCID: PMC10192858.
Moriguchi, Y., & Hiraki, K. (2013). Prefrontal cortex and executive function in young children: A review of NIRS studies. Frontiers in Human Neuroscience, 7, Article 867
Salmerón, L., & García, V. (2011). Reading skills and children’s navigation strategies in hypertext. Computers in Human Behavior, 27(3), 1143-1151.
Sung, E., & Mayer, R. E. (2013). Online multimedia learning with mobile devices and desktop computers: An experimental test of Clark’s methods-not-media hypothesis. Computers in Human Behavior, 29(3), 639-647.

---

<!-- _class: t-50 -->

Selwyn, N. (2018). Approches critiques des technologies en éducation : un aperçu. Formation et profession, 27(3), 6-21.
Tijus, C., Poitrenaud, S., Bouchon-Meunier, B., & De Vulpillières, T. (2006). Le cartable électronique: sémantique de l'utilisabilité et aide aux apprentissages. Psychologie française, 51(1), 87-101.
Vogel, J. J., Vogel, D. S., Cannon-Browers, J., Browers, C. A., Muse, K., & Wright, M. (2006). Computer gaming and interactive simulations for learning: A meta-analysis. Journal of Educational Computing Research, 34. 229-243.
Wouters, P., & Van Oostendorp, H. (2013). A meta-analytic review of the role of instructional support in game-based learning. Computers & Education, 60(1), 412-425.
Zheng, B., Warschauer, M., Lin, C. H., & Chang, C. (2016). Learning in one-to-one laptop environments: A meta-analysis and research synthesis. Review of Educational Research, 86(4), 1052-1084
