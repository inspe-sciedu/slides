![15%](images/logo-espe-uga.svg )

 
# UE 41 - Culture Numérique : système d’information et outils du CPE
Laurence Osete, Farah Guillot et Christophe Charroud - UGA - 2023

<!-- page_number: true -->
<!-- footer: 2022 - CC:BY-NC-SA -->

---
# L'U.E. 41, c'est quoi?

* Des contenus théoriques sur les risques liès à l'usage du numérique en situation scolaire
* Des notions de littératie numérique
* Mise en application dans un projet.

---
# Pourquoi?

* 	Pour réduire le plus possible les risques législatifs, éthiques, physiologiques et effets négatifs liès à l'utilisation du numérique.
* 	Pour être capable de participer à la sensibilisation et/ou l'éducation des élèves, des assistants d'éducation à l'usage des médias.

---
# Comment?

* Via la conception en **binôme** d'une action de formation/information/sensibilisation.
* Via une réflexion personnelle.

---
# Quelle évaluation?

* Un dossier décrivant l'action ainsi que ses supports.

---
# Production attendue :

Durant les heures en présence, vous devrez concevoir une action de formation/information/sensibilisation sur le numérique en établissement à destination d'un des publics suivant :
* Elèves
* Collègues enseignants
* AED
* Autres...

Vous devrez décrire cette production et la déposer sur la plateforme à l'issue de l'avant dernière séance.

---
# Critères d'évaluation et barème :

- Cohérence de l'action : Objectifs - publics cibles - activités proposées - ressources utilisées (/8)
- Usages, accès et fiabilité des ressources. (/4)
- Respect de la réglementation (droit d'auteur, collecte de DCP) (/3)
- Pertinance des modalités d'évaluation de l'action. (/3)
- Respect des règles de communication (/2)

---
# Organisation pédagogique

**Découpage des séances** : Les séances 1 à 6 seront découpée en 2 parties :

  - une partie TD d'une durée de 2h destinée à traiter une thématiques liant le numérique et l'enseignement. Les thématiques abordées sont :

    *  Les problèmes juridiques.
    *  Les problèmes éthiques.
    *  Le contexte et les usages des médias. 
    *  L'éducation aux médias et ses ressources.
    *  Les outils numériques pour l'évaluation
    *  L'impacts du numérique sur la santé et l'environnement

  - une partie de 1h30 dédiée à avancer sur le travail de groupe destiné à l'évaluation.

---
# Dernière séance

La dernière séance d'une durée de 3 heures permettra de mutualiser le travail de chaque groupe en réalisant une présentation orale. Elle permettra de faire le bilan de la formation. 

---
# En résumé, l'U.E. 41 c'est :
* Une formation appuyée sur des apports théoriques.
* Une évaluation basée sur une mise en application des notions et une prise de recul sur des enjeux sociétaux.

---
# Connexion au cours
http://eformation.univ-grenoble-alpes.fr


Sélectionner le cours "UE 41 - Culture numérique : système d’information et outils du CPE"
![25%](images/Moodle7.png)

## Clé d'auto-inscription
##### UE41-2023

---
# Lois et règlements à respecter


---
# Constitution des groupes

- Soit par affinité
- Soit en fonction d'un sujet commun
- ?

---
# Choix d'action 

Vous devrez concevoir une action de formation/information/sensibilisation sur le numérique en établissement à destination d'un des publics suivants :

* Élèves
* Collègues enseignants
* AED
* Autres ...

Vous avez libre choix sur le sujet que vous voulez traiter. Par contre, deux groupes ne peuvent pas proposer la même action. Il faut que cela diffère dans le sujet, dans la modalité, ou dans le public.

---
# Contenu du dossier

- Les objectifs de l'action
- Le(s) public(s) visé(s)
- Les compétences et/ou connaissances visées.
- L'organisation pédagogique : les activités proposées, leur durée.
- Une liste des ressources utilisées décrivant leur nature, leur modalité d'usage et d'accès, ainsi que les précautions à prendre.
- Une ébauche de support de formation utilisé, conçu par vous.
- Une modalité d'évaluation de l'action.
- Une justification des choix d'outils et de ressources.   

---
# Organisation 

#### Etape 1 : 
* Choisir un sujet, un public cible et une modalité et l'inscrire dans le document "Choix des sujets par groupe" (Rubrique "Evaluations")
* Lister les participants
* Faire valider par le formateur
	
#### Etape 2 :
* Réflechir à scénario et à un support pour votre action.
