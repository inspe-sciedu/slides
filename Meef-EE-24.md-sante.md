---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  

# Numérique et santé MEEF EE
 
### Christophe Charroud - Univ. Grenoble Alpes


<!-- page_number: true -->
<!-- footer:  christophe.charroud@univ-grenoble-alpes.fr – M2 MEEF EE – 2024 - CC:BY-NC-SA  -->

---
# Au programme

1. Introduction et composition des équipes pour le TD
2. Etude d'un sujet par équipe, recherche de sources fiables
3. Conception des présentation (après apports théoriques)
4. Présentation collective des résultats
5. Travail sur la production destinée à l'évaluation


---

# :one: Introduction et composition des équipes
Durée 5 minutes

---
## Numérique et santé, pourquoi cette question ?
* Problèmes de santé publique
* Informations véhiculées par les médias
* Que dit la recherche ?
* :arrow_right: Rôle du CPE


---
## Constitution des équipes
Chaque équipe va réaliser une présentation sur un sujet parmi la liste ci-dessous :
* Écrans, sommeil et problèmes ophtalmiques, quels liens, quels effets ?
* Numérique et impacts neurologiques (trouble du développement,TSA, TDAH, ...) quels liens, quels effets ?
* Numérique et impacts sur l'hygiène de vie (sédentarité, ondes WIFI, 4g, 5g , TMS...) quels liens, quels effets ?
* Numérique, cyberharcélement, prévention du suicide
* *ou autre sujet présenté par une équipe et validé par l'enseignant...*


---
# :two: :pencil: Recherche documentaire 
:warning: Fin de l'activité à 15h15

---
## Consignes première partie

Chaque équipe recherche des documents et des arguments **scientifquement étayés** sur son sujet (références obligatoires).




---

# 
# :three: Communiquer avec le numérique

  
### À votre avis, utiliser le numérique pour communiquer avec les élèves c'est ?
* Positif
* Négatif
...ou entre les deux....



---
# Vos représentations
## :pencil2: TP : Durée 5 minutes
* Sur une feuille comportant une colonne "positifs" et une colonne "négatifs", listez des effets du numérique sur la communication avec les élèves.



---

# Vous êtes-vous appuyés sur des arguments scientifiques, sur des représentations ou sur des opinions  ?

#
### Pourquoi une telle question?


---
# 
## « Il y a une rupture entre connaissance commune et connaissance scientifique »

> Bachelard : le matérialisme rationnel, 1953



## Dans l'éducation, quand on parle de numérique, les doxas* tiennent souvent lieu de pensée. 
> (Ferone. G, 2019)



*Doxa : Ensemble d'opinions, de préjugés, de présuppositions généralement admises et évaluées positivement ou négativement.



---

## 3.1 Alors, que dit la recherche ?


Les effets **positifs se limitent à des usages circonscrits**.

Hors de ces usages, le numérique produit (souvent) des effets négatifs.


## En clair, la recherche dit que le numérique, ce n'est pas magique :exclamation: 

---
## L’utilisation du numérique ***peut*** avoir un effet positif en milieu scolaire

- **Pour accéder à des informations**

- **Pour communiquer**

- **Pour s'entraîner sur des tâches ciblées**

> (J-PAL, 2019)



---
## 3.2 Le numérique pour communiquer

### Des points positifs :
* Sources variées
* Attention

### Des points négatifs 
* Sources variées
* Attention

# :rage:


---
### Quelques explications pour limiter les mésusages de la communication avec le numérique
* Multiplier les sources dans un même document, bonne ou mauvaise idée ?
* Écran et charge cognitive
* Capter et maintenir l'attention
* Vidéo et communication...



---
### 3.2.1 Multiplier les sources dans un même document, bonne ou mauvaise idée ?


> On raconte qu'il suffit de présenter à l'élève une même information sous différents formats pour être efficace.




### Que dit la recherche ?
> Effects of prior knowledge on learning from different compositions of representations in a mobile learning environment (Liu, Lin & Paas, 2014)

---
![140%](images/img-effets/Effets-liu11.png)

Texte + Photo

---
![140%](images/img-effets/Effets-liu12.png)

Texte + Photo + Exemple réel

---
![140%](images/img-effets/Effets-liu13.png)

Texte + Schéma + Exemple réel


---
### À votre avis quel groupe a retenu le plus d'élément ?

Groupe 1 : Texte + Image
Groupe 2 : Texte + Image + Plante réelle
Groupe 3 : Texte + Schéma + Plante réelle



---
### Résultats
Les meilleurs score de rétention ont été obtenus par :
# Le premier groupe (texte+image)

---

### Explication rationnelle
La multiplication des sources d’information (texte + image + objet réel) a provoqué une division de l’attention qui a gêné les élèves.

---

# :exclamation: À retenir :exclamation:

** Pour bien communiquer, ne vous laissez pas entraîner par la facilité à multiplier les sources avec le numérique.**

Utilisez un document avec 2 sources :
* **une source verbale** (écrite ou sonore)
* **une source picturale** (fixe ou animée)




***Sinon il sera profitable seulement aux meilleurs élèves.***

---
### 3.2.2 Écran et charge cognitive


#### Que dit la recherche ?
> La charge cognitive dans l’apprentissage (Charroud & Dessus, 2016)



---
#### Régle de base 
### Les sources doivent être épurées.
* Éviter les distracteurs 
* Utiliser des représentations simples et cohérentes
* Éliminer autant que possible la redondance


---

#### Apprentissage de règle logique : écran vs réelle

> Prefrontal cortex and executive function in young children. (Moriguchi & Hiraki, 2013). 

Adulte face à un écran (Neurones miroirs OK) :
:arrow_right: Apprentissage = Inférence de la règle

Élève face à un écran (Neurones miroirs en développement) :
:arrow_right: Apprentissage = Inférence de la règle + Interprétation empathique


> **L'assimilation d'une information va demander beaucoup d'effort à l'élève.** (Ferrari, 2014) 

---
#### Ne pas oublier l'interface...


![30%](images/img-effets/Effets-geogebra.jpg)



Il faut respecter la simplicité d'utilisation, donnant accès aux fonctions facilitant les apprentissages scolaires. (Tijus, 2006)
    

---
### 3.2.3 Capter et maintenir l'attention

#### L’attention
– Alerte (quand faire attention)
– Orientation (à quoi faire attention)
– Contrôle exécutif (comment faire)

> (Dehaene, 2018)

---

*Les élèves sont-ils égaux devant un document numérique ? Même bien conçu...*


### Que dit la recherche ?

> Eye-movement patterns (Mason, Tornatora, & Pluchino, 2013).

---
![110%](images/img-effets/Effets-masson1.png)

---
![110%](images/img-effets/Effets-masson2.png)

---
### Résultats :

### Les meilleurs résultats sont obtenus pas le 3ème type d'élève.

Plus les élèves réalisent de traitements d'intégration entre la source verbale (texte ou son) et la source picturale (illustrations) plus ils assimilent une information.

---
![110%](images/img-effets/Effets-masson2.png)

---

## bilan :
* Proposer 2 sources dans un document et pas une seule...

* :warning: À partir d'un même document, les élèves n'ont pas la même stratégie d'assimilation, **seuls les meilleurs profitent du document**.


---
### Comment inciter les transitions entre les sources ?

> Utilisation d'un document avec commentaires sonores (Jamet, 2014).

![150%](images/img-effets/Effets-jamet14.png)

---
# :exclamation: À retenir :exclamation:

Avec le **guidage**, les élèves accordent davantage d’attention (temps de fixation total) aux informations pertinentes grâce à la signalisation.

### Résultats :
* Des effets sur la complétude et la rétention (Mémorisation)
	
* Mais pas d'effet sur les apprentissages profonds (Compréhension)



---

###  3.2.4 Vidéo et apprentissages...

Pour qu'une vidéo/animation soit efficace pour communiquer, il faut :
* que l'élève ait un contrôle minimal sur le rythme de défilement d’une animation ou d’une vidéo;
* que l'élève fasse des pauses dans le défilement (ou imposer les pauses dans le défilement)
* présenter de façon animée des informations elles-mêmes dynamiques;

Vérifiez bien les vidéos (même celles issues de sites institutionnels) avant de les utiliser....

(Biard, Cojean & Jamet, 2018)(Cojean, Jamet2017)



---
### :exclamation: À retenir :exclamation:

* Des ressources épurées.
* Des vidéos contrôlables par l'élève, avec des pauses.
* Ne pas oublier que la perception de l'élève est différente de celle de l'adulte (neurones miroirs).
* Et enfin il ne faut pas que l'interface soit un obstacle aux apprentissages


---
# :four: Conception des présentations

Chaque équipe prépare une présentation d'une durée de 8 à 10 minutes avec l'appui d'un support visuel.

---
# :five: Présentations et débats

---
# :six: Rappel des modalités d’évaluation

Production attendue

**En binôme**, les étudiants devront produire un document décrivant une formation/information/sensibilastion sur un thème lié aux usages du numérique. Lors de chaque séance, un temps sera reservé à la construction de cette production. Cette production sera élaborée tout au long de la formation et elle peut bénéficier des retours formatifs donnés par tous les intervenants. Il sera à rendre.

La production sera déposée sur la plateforme pour évaluation **au plus tard le 19 mai 23h59.**

---

# En avant :exclamation: 

---


# Références


Bachelard, G. (1953). Le matérialisme rationnel. Paris: Presses universitaires de France.

Biard, N., Cojean, S., & Jamet, E. (2018). Effects of segmentation and pacing on procedural learning by video. Computers in Human Behavior, 89, 411–417. https://doi.org/10.1016/j.chb.2017.12.002 

Charroud, C., & Dessus, P. (2016). La charge cognitive dans l’apprentissage. Consulté à https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/chargecog.html .

---

Cojean, S., & Jamet, E. (2017). Facilitating information-seeking activity in instructional videos: The combined effects of micro- and macroscaffolding. Computers in Human Behavior, 74, 294–302. https://doi.org/10.1016/j.chb.2017.04.052

Ferone, G. (2019). Numérique et apprentissages : prescriptions, conceptions et normes d’usage. Recherches en Éducation, 35, 63–75.

Ferrari P. F. (2014) « The neuroscience of social relation. A comparative-based approach to empathy and to the capacity of evaluating others’action value », Behavior, 151.

---

Jamet, E. (2014). An eye-tracking study of cueing effects in multimedia learning. Computers in Human Behavior, 32, 47-53.

J-PAL Evidence Review. 2019. “Will Technology Transform Education for the Better?” Cambridge, MA: Abdul Latif Jameel Poverty Action Lab.


Liu, T. C., Lin, Y. C., & Paas, F. (2014). Effects of prior knowledge on learning from different compositions of representations in a mobile learning environment. Computers & Education, 72, 328-338.

---


Mason, L., Tornatora, M. C., & Pluchino, P. (2013). Do fourth graders integrate text and picture in processing and learning from an illustrated science text? Evidence from eye-movement patterns. Computers & Education, 60(1), 95-109.

Sung, E., & Mayer, R. E. (2013). Online multimedia learning with mobile devices and desktop computers: An experimental test of Clark’s methods-not-media hypothesis. Computers in Human Behavior, 29(3), 639-647.

Tijus, C., Poitrenaud, S., Bouchon-Meunier, B., & De Vulpillières, T. (2006). Le cartable électronique: sémantique de l'utilisabilité et aide aux apprentissages. Psychologie française, 51(1), 87-101.

---

Zheng, B., Warschauer, M., Lin, C. H., & Chang, C. (2016). Learning in one-to-one laptop environments: A meta-analysis and research synthesis. Review of Educational Research, 86(4), 1052-1084
