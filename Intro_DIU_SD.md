---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# 
# Module "Enseigner avec le numérique" 1 et 2 SD
## Objectifs, organisation, attendus


<!-- page_number: true -->
<!-- footer: Pôle enseignement numérique INSPE - UGA - 2024 - 2025 - CC:BY-NC-SA -->

---
# Objectifs du module "Enseigner avec le numérique" 1 et 2 SD
Cette UE a pour objectif de vous accompagner dans l'acquisition des compétences nécessaires pour utiliser le numérique en classe en prenant en compte les dimensions, efficacité de l’enseignement, respect des normes juridiques et éthiques et développement de la culture numérique des élèves. 

Cette formation articule apports théoriques et activités pratiques aboutissant à l’analyse réflexive d’une ou plusieurs situations d’apprentissage intégrant le numérique.


---
# Organisation

* 3 CM et 1 TD au semestre 1
* 2 TD et 1 TP au semestre 2
:warning: Vous aurez différents intervenants 

---
# Attendus du module "Enseigner avec le numérique" 1 et 2 SD
- Un document rédigé **en binôme** de 4 pages maximum consistant à l'analyse d'une situation d'apprentissage intégrant le numérique

:arrow_right: Syllabus

#### Cours sur la plateforme eformation :arrow_right:  https://eformation.univ-grenoble-alpes.fr


 





