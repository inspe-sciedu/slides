---
marp: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# UE 24 Option de recherche “Interactions enseignant-élèves“
## Philippe Dessus, Inspé, Univ. Grenoble Alpes
## Séance 9 - Rédaction du mémoire
##### Année universitaire 2023-24

![w:350](images/logo-espe-uga.png)

---
<!-- footer: Ph. Dessus • M1 PE • TD Option de recherche “Interactions enseignant-élèves • Inspé-UGA 2023-24 • CC:BY-NC-SA-->
<!-- paginate: true -->

# :nine: 0. But de la séance

- Donner des éléments de stratégie de rédaction de mémoire
- La méthode décrite plus bas peut donc tout aussi bien se réaliser avec papier-crayon, et/ou avec un ou des logiciel.s approprié.s 

---
# :nine: 0. Plan

0. Informations 
1. Prendre des notes
2. Quelques prescriptions (normes APA, notamment)
3. Tâche

---
# :nine: 0. Nouvelles consignes d'évaluation de l'UE (2/2)

Chaque étudiant·e aura à produire un document d'environ 6 pages qui sera structuré ainsi :
- env. 1/2 page sera consacrée à présenter les grandes lignes du travail de recherche : le contexte, la ou les question·s de recherche, la ou les principale·s hypothèse·s, une description rapide de l'intervention réalisée
- le restant du document (soit env. 5 pages) sera composé de *copies* de passages du mémoire (entre 4 et 6) en cours d'écriture qui auront bénéficié des apports des TD “outils de recherche”. Après chaque copie de passage, un paragraphe d'une dizaine de lignes indiquera le thème du TD concerné, et justifiera l'intérêt d'utiliser cet apport dans le mémoire. 

---
# 0. Nouvelles consignes d'évaluation de l'UE (2/2)

- Il est à noter que ces apports pourront tout aussi bien concerner des apports *effectifs* (donc, directement intégrés dans l'état de l'art ou la partie empirique), ou des apports *possibles*, dans ce cas, évoqués dans la partie Discussion du mémoire et mentionnant de possibles suites à donner à la recherche
- Il est enfin à noter que les étudiant·es par binôme pourront rendre un document pour deux, en mentionnant leurs noms au début du document
- :warning: Document à livrer par courriel au formateur (philippe.dessus@univ-grenoble-alpes.fr) le **mardi 7 mai 2024 à 23:59** au plus tard


---
# :nine: 1. Prendre des notes (Ahrens 2017)

- Trois types de fichiers :
  1. notes éphémères
  2. notes sur les travaux (littérature)
  3. notes permanentes

- :warning: Ces notes vont devenir la source principale du mémoire, à cela il faut ajouter les citations de textes d'autres auteurs, non décrites ici

---
# :nine: 1. Prendre des notes (Ahrens 2017)

1. Prendre des **notes éphémères** pour capturer les idées qui vous viennent : utiliser un petit carnet, un simple fichier de traitement de textes, un morceau de nappe...
2. Prendre des **notes sur les travaux lus** : Chaque fois que vous lisez quelque chose d'intéressant, en faire une note qui : 
   a. soit courte et donc sélective
   b. soit reformulée avec vos propres mots
   c. contienne la référence précise du travail lu (avec le n° de page)

---
# :nine: 1. Prendre des notes (2/2)(Ahrens 2017)

3. Régulièrement, relisez les notes écrites dans les étapes 1. et 2., et réfléchissez-y. 
   1. En quoi sont-elles utiles à votre travail ? 
   2. Sont-elles reliées à des notes permanentes déjà écrites ? En quoi aident-elles ou contredisent-elles des choses déjà écrites ? 
   3. Écrire, raffiner les arguments en gardant les références utiles dans de nouvelles notes permanentes
   4. Les relier thématiquement entre elles (p. ex., en ajoutant des mots-clés)
   5. Numéroter les notes thématiquement est utile pour y faire référence (p. ex., numérotation décimale)

---
# :nine: 1. Prendre des notes (Ahrens 2017)

4. Jeter ou rayer les notes éphémères traitées, et ranger dans votre fichier “Références” les fiches “travaux“ déjà traitées, p. ex. avec les fiches de citation
5. Utiliser le fichier de notes permanentes pour avancer dans la rédaction du mémoire, en les mettant dans l'ordre voulu. Pour cela, un logiciel de “cartes mentales” (*mind maps*) pourra aider

---
# :nine: 1. Prendre des notes, vue générale

![w:800](images/notes.jpeg)

---
# :nine: 1. Prendre des notes : quelques logiciels


- Quelques logiciels de gestion de notes : [Obsidian](https://obsidian.md), [Logseq](https://logseq.com), [Roam](http://roamresearch.com), [Zettlr](http://zettlr.com), etc.
- Quelques logiciels de cartes mentales (*mind maps*, cartes heuristiques) : [Framindmap, en ligne](https://framindmap.org/abc/fr/), [DigiMindMap, en ligne](https://ladigitale.dev/digimindmap/#/), [WiseMapping, en ligne](https://app.wisemapping.com/c/maps/3/try), [FreePlane, app](https://docs.freeplane.org)

---
# :nine: 2. Quelques prescriptions

- [Organiser la rédaction d'un mémoire](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/tuto-organiser-memoire.html)
- [Rédiger un mémoire](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/tuto-redaction-memoire.html)
- [Pour faire un tableau ou une figure aux normes APA](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/tuto-tab-fig-apa.html)
- [Écrire des références aux normes APA](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/tuto-references-apa.html)
- ... sans oublier, bien sûr, le [guide du mémoire MEEF-UGA](https://eformation.univ-grenoble-alpes.fr/pluginfile.php/651019/mod_resource/content/2/GuideDuMémoire_MasterMEEFMentionSD-PE-EE_vNov2023.pdf) dans le Moodle

---
# :nine: 3. Quelques écueils de l'état de l'art

- S'enfermer dans des définitions “standard“ (des dictionnaires de la langue française)
- Oublier de définir certaines notions centrales du mémoire
- “Dérouler“ des arguments sans les relier les uns aux autres
- Oublier, à la fin des sections, 
  - de donner son propre avis sur l'orientation de l'état de l'art
  - de faire le lien avec la section suivante 
- Oublier, en discussion, de revenir sur certains résultats notables de l'état de l'art pour les mettre en lien avec ses propres résultats


---
# :nine: 4. Tâches

- Parcourez un ou plusieurs des documents mentionnés ci-dessus
- Noter au moins 3 informations qui vous paraissent importantes à prendre en compte, et que vous avez laissées de côté pour l'instant,    
  - dans les documents de la diapositive précédente
  - dans le contenu des différentes présentations vues jusque-là (cf. liste diapositive suivante)
- Rédigez, ou modifiez un (ou plusieurs) paragraphe.s de votre mémoire qui les prenne en compte
- Archivez ces ajouts pour l'évaluation du TD
- Restitution collective

---
# :nine: 3. Liste des présentations

[TD 1. & 2, les IEE](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE-24-option-rech-1-2.pdf)
- [TD 3, protocoles cas unique](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE-24-option-rech-3.pdf)
- [TD 4, la veille pédagogique](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE-24-option-rech-4.pdf)
- [TD 5, les méta-analyses](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE-24-option-rech-5.pdf)
- [TD 6, observer les IEE](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE-24-option-rech-6.pdf)
- [TD 7, aspects légaux et éthiques](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE-24-option-rech-6.pdf)
- [TD 8, analyser et représenter des données](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE-24-option-rech-8.pdf)
- [TD 9, stratégies de rédaction du mémoire](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE-24-option-rech-9.pdf)
