---
marp: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# UE 24 Option de recherche “Interactions enseignant-élèves“
## Philippe Dessus, Inspé, Univ. Grenoble Alpes

## Séance 7 - Aspects légaux et éthiques
##### Année universitaire 2023-24

![w:350](images/logo-espe-uga.png)

---
<!-- footer: Ph. Dessus • M1 PE • TD Option de recherche “Interactions enseignant-élèves" • Inspé-UGA 2023-24 • CC:BY-NC-SA-->
<!-- paginate: true -->

<!--Ajouter questions de Brown 2010

Basic Questions About Research Purpose and Participants
• What is the purpose of this proposed research?
• What are my expectations of research participants?
• How will I make sure those I invite to participate understand the sincerity of my desire
to give them complete freedom of choice to participate without penalty of any sort?
• How can I collect consent (from an adult responsible for each participant under 18)
and assent (from participants themselves if under 18)?
• How can I honor the confidentiality of research participants and nonparticipants?
• What data do I plan to collect—audiotapes, videotapes, student work samples, journal
entries, observations?
• Might a layered consent/assent form be useful, in which participants check levels of
participation to which they agree, such as use of work samples, use of journal entries, use of audiotapes, use of digital video?
More Complex Questions About the Research
• How can I conduct member checks on my ongoing data analysis to make sure my understanding of meaning is confirmed by my participants?
• If I am working as part of a teacher inquiry community, how can I maintain participant confidentiality and still be part of the inquiry discussion?
• How can I make sure my research does not interfere with the academic mission of my role as a teacher?
• How will my participants and I understand the difference between the teaching/ learning relationship and the researcher/participant relationship? 
Ongoing Questions for Teacher Researchers Engaged in Self-Interrogation
• What is my place in terms of power? My students’ place?
• How will I separate a student’s participation as a research participant from her/his education as my student?
• How will I continue to ensure not only informed, noncoercive consent but also
confidentiality?
• Who might be negatively affected by my research? How do I guard against negative
impact?

-->

# 0. Objet de la séance

- Donner des éléments légaux et éthiques à propos de la recherche sur les IEE (et plus largement les pratiques éducatives)
- Avec deux facettes : 
  - la pratique d'enseignement en tant que telle ; 
  - la pratique de recherche

---
# 0. Plan de la séance

0. Définitions
1. Ethique dans les REE et les pratiques éducatives
2. Intégrité académique et scientifique
3. L'éthique dans la conduite de la recherche
4. Les données personnelles et leur protection
5. Résumé et tâches

---
# 0. Aspects légaux (et réglementaires) de la pratique d'enseignement

- De manière banale, il est bien sûr attendu que les activités mises en œuvre pour le mémoire respectent :
  - le cadre réglementaire de l'éducation nationale (et il est important, dans le mémoire, de justifier cela, p. ex. en référant aux programmes ou documents officiels)
  - divers cadres légaux, déontologiques, et éthiques, selon les activités réalisées

---
# 0. Les différences entre légal, déontologique, éthique

- **Légal** : qui s'inscrit dans un cadre légal (p. ex., la loi française, les textes de référence du MENJS, le Règlement général sur la protection des données, etc.)
- **Déontologique** : qui répond à un ensemble de règles, de recommandations, de devoirs qui régit une profession
- **Éthique** : ce qui est estimé bon, ne nuit pas aux individus (*d'abord ne pas nuire*),régule l'action et la conduite morale

:books: Gaussel 2023 ; Prairat 2009

---
# 1. Éthique dans les relations enseignant-élèves

- De nombreuses recherches montrent que, dans leur exercice quotidien, les enseignant.es peuvent sortir de ce cadre
- Cf. les travaux de Merle (“*L'élève humilié*”), montrant qu'humilier des élèves est une technique parmi d'autres de gestion de la discipline, mais aussi, plus largement, de reproduction sociale
- Cf. les travaux étatsuniens de Levitt & Dubner (2005), montrant la tricherie des enseignants sur les scores de réussite de leurs élèves, pour avoir des primes

:books: [Les droits des élèves dans la classe et dans l'établissement](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/droitel.html)

---
# 1. Principes éthiques fondamentaux

- *Respect des personnes* (elles sont autonomes et ont un libre arbitre)
- *Tendre à faire le bien* (peser le bénéfice/risque)
- *Justice* (les risques et bénéfices sont distribués convenablement, il y a compensation)
- *Respect de la loi et transparence*

:books: Salganik 2018 ; [Réfléchir à ses principes éducatifs](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/education.html)


---
# 1. Une catégorisation de dilemmes éthiques dans la pratique éducative

1. Climat de bienveillance vs. climat formel.
2. Justice distributive vs. normes scolaires.
3. Confidentialité vs. règles de l'école.
4. Loyauté envers les collègues vs. normes de l'école
5. L'agenda familial vs. les normes éducatives

:books: Shapira-Lishchinsky (2010)

---
# 2. Intégrité académique et scientifique

- Importance d'une bonne conduite des pratiques de recherche

:books: Les diapositives de cette section sont issues du cours de Fanny Gimbert, UE 24-option 4, Inspé UGA

---
# 2. Intégrité académique et scientifique

> - **Fiabilité** : autrement dit garantir la qualité de la recherche, qui transparaît dans la conception, la méthodologie, l’analyse et l’utilisation des ressources
> - **Honnêteté** : autrement dit élaborer, entreprendre, évaluer, déclarer et faire connaître la recherche d’une manière transparente, juste, complète et objective
> - **Respect** : envers les collègues, les participants à la recherche, la société, les écosystèmes, l’héritage culturel et l’environnement
> - **Responsabilité assumée** : pour les activités de recherche, de l’idée à la publication, leur gestion et leur organisation, pour la formation, la supervision et le mentorat, et pour les implications plus générales de la recherche. » 

:books: ALLEA (2018, p. 4)

---
# 2. Intégrité académique et scientifique

- Comme tous les milieux, le milieu académique et scientifique n'est pas à l'abri de pratiques éthiques douteuses, voire de fraudes
- Cela entache les résultats des recherches
- **Crise de la réplicabilité** : on a souvent du mal à reproduire des résultats d'études insuffisamment décrites, avec des biais non évalués, etc.
- D'autant plus qu'il existe un biais de publication : les travaux donnant des résultats significatifs ont plus de chances d'être publiés que ceux ayant des résultats non conclusifs

:books: Wade & Broad (1994)

---
# 2. Ce qui nuit à l'intégrité académique et scientifique (1/2)

Tout ce qui relève du « mensonge » volontaire des auteurs (**fraude**)
- Modification des données
- Création de données
- Appropriation du travail des autres : plagiat

---
# 2. Ce qui nuit à l'intégrité académique et scientifique (2/2)

… et tout ce qui relève d'une “zone grise” 
- Partage de données insuffisant
- Tripatouillage statistique (*p-hacking*)
- Harcking (*hypothesizing after the results are known*)
- Tripatouillage de l'échantillon (*cherry picking* ou report partiel des données)

---
# 2. Quelques solutions 
- Répliquer les études pour cumuler les éléments de preuve sur un phénomène
- Réaliser des méta-analyses (*voir Cours 5*) pour une vue plus extensive et globale d'un phénomène
- Mettre les données brutes en accès libre
- Tenir un cahier de laboratoire (ou un journal de bord)
- Pré-enregistrer l'étude pour limiter notamment les biais de harcking

---
# 3. L'éthique au niveau de la conduite d'une recherche

Informer les participant.es 
- de l’objectif de la recherche
- sa méthode, son déroulement succinct
- sa durée
- du caractère anonyme du recueil de données (le cas échéant)
- qu’ils sont libres de refuser de participer 
- qu’ils sont libres de quitter l’étude à tout moment
- des potentiels risques encourus

:books: Cette section reprend le cours de Fanny Gimbert, UE 24-option 4, Inspé UGA

---
# 3. L'éthique au niveau de la conduite d'une recherche

- Si les participant.es sont mineur.es et si l'étude a un recueil de données personnelles, il est nécessaire d'avoir un accord parental (ainsi que des élèves)

---
# 3. Recueil du consentement éclairé

J'accepte de participer à cette étude après avoir pris connaissance des informations suivantes :
1. Toutes les informations recueillies pendant cette étude resteront strictement confidentielles et seront utilisées uniquement à des fins de recherche
2. Ma participation est volontaire et je suis libre d'arrêter l'étude à tout moment
3. Les buts généraux de l'étude sont expliqués au début de l'étude et les hypothèses sont détaillées à la fin
4. Cette étude est une recherche de groupe et, dans ce cadre, seuls les résultats moyens sont traités
  
:books: Formulaire du LPNC, UGA

---
# 4. Les données à caractère personnel (DCP)?

<!-- _class: t-90 -->

DCP : "toute information se rapportant à une personne physique **identifiée ou identifiable** [...] ; est réputée être une «personne physique identifiable» une personne physique qui peut être identifiée, **directement ou indirectement**, notamment par référence à un identifiant, tel qu'un nom, un numéro d'identification, des données de localisation, un identifiant en ligne, ou à un ou plusieurs éléments spécifiques propres à son identité physique, physiologique, génétique, psychique, économique, culturelle ou sociale;" ([RGPD, art. 4](https://www.cnil.fr/fr/reglement-europeen-protection-donnees/chapitre1#Article4))

---
# 4. A quelles conditions leur collecte est-elle légale ?

<!-- _class: t-90 -->

[CHAPITRE II - Article 6 du RGPD](https://cnil.fr/fr/reglement-europeen-protection-donnees/chapitre2#Article6)
Il y a six bases légales, dont :
- la personne concernée qui a **consenti** au traitement
- nécessaire à l'exécution d'une **mission d'intérêt public** ou relevant de **l'exercice de l'autorité publique** 
- ...
**Consentement des enfants en ce qui concerne les services de la société de l'information**
- est licite, en France, à partir de 15 ans. En dessous le consentement est donné, ou autorisé, par le titulaire de l'autorité parentale


---
# 4. Principes de protection des données de vos élèves

- Le **responsable du traitement des données** (chef d'établissement pour le 2d degré, DASEN pour le 1er degré) doivent évaluer la politique de protection des données des logiciels utilisés dans leur périmètre, et éventuellement contractualiser leur utilisation
- Ils documentent les **informations utiles** pour les logiciels (finalité, personnes dont les données sont collectées, données collectées) et les **sous-traitants**, tiennent informés les utilisateurs des traitements liés aux services utilisés

---
# 4. Précautions à prendre

- Ne collecter que les **données nécessaires** (minimisation de la collecte)
- Limiter la conservation des données dans le temps
- **Informer les personnes**, et dans certains cas demander le consentement aux responsables légaux (notamment, lors de la prise d'images).
- Sécuriser les données
- Signaler au responsable de traitement toute perte ou vol de données.
- :warning: Données biométriques (physiques, médicales ou corporelles) sont spécialement sensibles et doivent faire l'objet d'un traitement spécifique. **Ne pas les recueillir en contexte scolaire.** 

---
# 4. Enregistrement de données personnelles

- Le [Formulaire Eduscol](https://eduscol.education.fr/398/protection-des-donnees-personnelles-et-assistance) peut être utilisé pour informer les parents des élèves d'un enregistrement (vidéo, audio, etc.) de données personnelles

---
# 5. Plagiat non intentionnel

- Reformuler les idées principales des notes prises
- Sans oublier de noter la référence initiale
- Toute citation “dans le texte” doit être précisément sourcée (Auteur.s, date, n° de page)
- Par précaution, faites passer votre mémoire final au détecteur de similitudes et modifiez les points problématiques

---
# 5. Utilisation d'intelligence artificielle générative

- [ChatGPT](https://chat.openai.com/auth/login), [Gemini](https://gemini.google.com), [Claude](http://claude.ai), etc. sont des outils permettant de générer du texte
- Il est assez difficile de leur faire produire des textes pertinents (ingénierie du prompt)
- Ils ne sont pas exempts de biais, d'informations fausses
- À utiliser avec grande précaution et en faisant preuve d'esprit critique, p. ex. pour des phases de “brainstorming”
- Leur utilisation et leurs apports précis doivent être déclarés
- Même si, à ce jour, aucun système de détection d'IA générative ne soit suffisamment fiable

:books: Plus d'infos dans ce [document](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/generation-textes.html)

---
# 5. Utilisations possibles de l'IA générative

- concevoir une séquence d'enseignement
- concevoir du matériel expérimental contrôlé (textes ou images)
- traduire un protocole, un extrait d'article en français 
- faire le résumé d'une section
- traduire le résumé de mémoire en anglais
- etc.
:warning: toutes les productions doivent être relues et les faits vérifiés soigneusement

---
# 5. De possibles conseils, à faire confirmer/infirmer par votre directeur.e

<!-- _class: t-90 -->

Cette UE a été conçue pour vous aider à développer des connaissances et à acquérir de nouvelles compétences qui vous seront utiles en tant que professionnels. Les outils d'IA peuvent être utilisés comme une aide au processus créatif, mais il est entendu qu'ils doivent être accompagnés d'une pensée critique et d'une réflexion. **Les étudiants qui choisissent d'utiliser ces outils sont responsables de toute erreur ou omission résultant de leur utilisation**. Il leur sera également demandé de fournir en annexe du dossier le nom de l’outil utilisé, les prompts utilisés, l'historique des résultats générés, ainsi qu'une réflexion approfondie sur ces résultats (notamment sur leur validité). Le cas échéant, les étudiants peuvent également être invités à examiner les coûts environnementaux et sociaux de l'utilisation des outils.
  
---
# 5. Conseils pour le mémoire

- Mentionner le plus clairement possible, dans la partie **“Méthode”** les étapes de la recherche
- Ne pas triturer les résultats et les hypothèses pour qu'ils correspondent les uns aux autres
- Dans la partie **Discussion**, expliquer les causes possibles des résultats non conformes aux hypothèses 
- Dans la partie **“Discussion>Limites”** mentionner les possibles problèmes reliés au légal, à l'éthique

---
# 5. Tâche

- Reprendre les différents éléments de cette présentation et questionner leurs répercussions :
  - dans les pratiques de classe (REE, pratiques éducatives)
  - dans les pratiques de votre mémoire : 
    - en quoi ai-je pu manquer d'intégrité scientifique dans mon travail ?
    - quelles données personnelles est-ce que je recueille ? le traitement que j'en fais est-il compatible avec le RGPD ?

---
# Pour en savoir plus

- [Site de l'INASED](https://inased.org/academic-integrity/) sur l'intégrité scientifique
- [Document sur l'analyse éthique de situations éducatives utilisant les technologies](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/design-ethique.html)

---
# Références

- ALLEA (All European Academies) (2018). [Code de conduite européen pour l’intégrité en recherche](https://www.allea.org/wp-content/uploads/2018/01/FR_ALLEA_Code_de_conduite_europeen_pour_lintegrite_en_recherche.pdf) (édition révisée)*. 
- Gaussel, M. (2023). [Au cœur de l'éthique enseignante](http://veille-et-analyses.ens-lyon.fr/DA-Veille/145-novembre-2023.pdf). *Dossier de veille de l'Ifé, 145*.
- Levitt, S. D., & Dubner, S. J. (2005). *Freakonomics*. Penguin. 
- Prairat, E. (2009). *De la déontologie enseignante*. P.U.F., coll. Quadrige. 
- Salganik, M. J. (2018). *Bit by bit. Social research in the digital age*. Princeton University Press. 
- Shapira-Lishchinsky, O. (2011). Teachers’ critical incidents: Ethical dilemmas in teaching practice. *Teaching and Teacher Education, 27*(3), 648-656. https://doi.org/10.1016/j.tate.2010.11.003 
- Wade, W., & Broad, N. (1994). *La souris truquée. Enquête sur la fraude scientifique*. Points Seuil.