---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# 
# UE Num801 "Culture numérique et apprentissage"
## Séance finale

 
<!-- page_number: true -->
<!-- footer: Pôle enseignement numérique INSPE - UGA - 2024 - 2025 - CC:BY-NC-SA -->

---
# Au programme aujourd'hui
* PIX - PIX+EDU
* QCM 
* Analyse de la situation d'apprentissage :arrow_right: dépôt du travail

---
# :one: Évaluation des compétences numérique des élèves et des enseignants

---
## 1.1 Pour les élèves - PIX
Le numérique, des compétences à développer :arrow_right: à évaluer
* Un cadre de référence : [CRCN](https://eduscol.education.fr/721/evaluer-et-certifier-les-competences-numeriques)
* :warning: Aucun niveau exigé par l'institution mais scruté par certains employeurs
* Un outil d’évaluation national pour l'école le collège et le lycée : [PIX](https://pix.fr) 
* Auto-évaluation des élèves
* Auto-formation (tutoriels)
* Certification d’un niveau d’acquisition : 3ème, Terminale, Université.


---
## 1.2 Pour les enseignants - PIX+EDU

Certification comportant deux volets :

1. Volet "automatisé" :arrow_right: sur la plateforme PIX.
2. Volet "pratique professionnelle" :arrow_right: sur le terrain, avec une mise en oeuvre d'une situation d'apprentissage intégrant le numérique. 

#### :warning: À ce jour, il n'y a pas de texte officiel concernant l'obligation de la certification PIX+EDU :arrow_right: à l'INSPE-UGA le dispositif PIX+EDU est mis en oeuvre très partiellement...

---
## PIX+EDU à la rentrée 2025 (prévision)

![w:600](images/img-pix_edu/pixedu.png)

---
## :pencil2: TP - Connexion à PIX 
* Si vous n'en n'avez pas encore, créez-vous un compte sur pix.fr 

Allez sur https://pix.fr

***Référentiel de formation : CRCN***
**:arrow_right: Public cible : élèves (école-collège-lycée), et étudiants 1er cycle (licence)**

---
# PIX+EDU Volet automatisé
## :pencil2: TP - Entrée dans le PIX+EDU

- Connectez vous à la campagne PIX+EDU "les essentiels" :warning: Campagne de test
- Cliquez sur "j'ai un code"

![](images/img-pix_edu/code_pix.png)
- Code de connexion - :warning: Second degré : **MGVYCX189**
- Répondez rapidement à un seul bloc de question (5 questions puis feedback)

---
# :two: QCM
1. Allez sur le cours Num801 sur eformation
2. Ouvrez la tuile "Evaluations - QCM et dossier" puis cliquez sur "CC1 - QCM 2025"
3. Verifiez que le temps alloué correspond à votre situation
4. Saisissez la clé et commencez le test

---
# :three: Travail sur le dossier - Finalisation



