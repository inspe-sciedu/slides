---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
# 


# Atelier sommeil et apprentissages

## Éducation à la santé
   
 
<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr- UGA - Éducation à la santé - 2023 - CC:BY-NC-SA -->

---
# Quelques rappels
Atelier en 3 temps : 
* 1er mars conférence 
* 20 mars "TD" conception de ressources
* 24 avril de 13h30 à 15h à l'INSPE ou en école?

---
# :one: Constitution de groupes
* Par niveau de stage
* Par affinités
* Par ...

:arrow_right: environ 5 par groupe


---
# :Two: Conception d'un action de sensibilisation

* Concevoir une action de sensibilisation à la notion de "bon" sommeil à destination de vos élèves et éventuellement de leurs parents.
* Action appuyée sur des résultats scientifiques
* :warning: Vocabulaire et supports de communcation adaptés au public


---
# :three: Plateforme de formation
* Sur eformation :arrow_right: cours "Éducation à la santé"
* Zone de dépôt des ressources 

