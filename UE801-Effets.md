---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# 
# Effets du numérique sur les apprentissages
## Apports théoriques
UE 801

Christophe Charroud - UGA

<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2022 - 2023 - CC:BY-NC-SA -->

 
  
---

# 
# 1. Introduction : Quelles sont vos représentations sur le numérique en classe ?


À votre avis, si l'on s'intéresse aux apprentissages des élèves, utiliser le numérique en classe c'est :
* Positif
* Négatif
...ou entre les deux....



---
# Vos représentations
## :pencil2: TP : Durée 5 minutes
* Sur une feuille comportant une colonne "effets positifs" et une colonne "effets négatifs", listez des effets du numérique sur les apprentissages.



---

# Vous êtes-vous appuyés sur des arguments scientifiques, sur des représentations ou sur des opinions  ?

#
### Pourquoi une telle question?


---
# 
## « Il y a une rupture entre connaissance commune et connaissance scientifique »

> Bachelard : le matérialisme rationnel, 1953



## Dans l'éducation, quand on parle de numérique, les doxas* tiennent souvent lieu de pensée. 
> (Ferone. G, 2019)



*Doxa : Ensemble d'opinions, de préjugés, de présuppositions généralement admises et évaluées positivement ou négativement.



---

# Alors, que dit la recherche ?


Les effets **positifs se limitent à des usages circonscrits**.

Hors de ces usages, le numérique produit (souvent) des effets négatifs sur les apprentissages.




---


# 2. La recherche dit que numérique pour les apprentissages, ce n'est pas magique


Mais alors :
* Pourquoi, souvent, on pense que ça marche ?
* Pourquoi ça ne marche pas aussi bien que ça ?
* Pourquoi on vous incite à l'utiliser ?



### Des promesses, des représentations...et souvent des déceptions 
### :arrow_right: quelques exemples




---

## 2.1. Le numérique à tout prix !

![100%](images/img-effets/Effets-tapis.png)

Réaliser exactement la même tâche avec ou sans technologie :arrow_right: Très faible  intérêt !


# 


---

## 2.2. Le numérique c'est spectaculaire, mais...




### Un exemple d'activité "wahoo!"
### [Manipulation d'objets tangibles et réalité augmentée](./images/img-effets/Effets-epfl.mp4)

---
# Résultats de l'expérimentation :
* Les élèves sont super-contents, motivés, excités ;
* Les enseignants aussi ;

---


# mais

## les apprentissages ne sont pas meilleurs qu'avec la méthode papier crayon ! :thumbsdown:

---

# 
# Chronologie classique d'une telle expérimentation :
* On cherche
* On conçoit
* On expérimente en établissement
* On rentre
* **On pleure**

(Dillenbourg, 2018)






---
# 
## 2.3. Des pratiques de terrain souvent basées sur la connaissance commune...



###  Mythes et légendes autour du numérique en classe

---


### :pencil2: TP : Durée 5 minutes

En Binôme, réfléchissez à ces affirmations, sont-elles vraies ou fausse ?

* Le numérique permet de s'adapter aux styles d’apprentissage des élèves.
* Les *Digital Natives* apprennent différemment.
* Le numérique est motivant pour les élèves.
* Les jeux vidéo permettent des apprentissages.
* Le numérique favorise l'autonomie des apprenants.
* La lecture sur écran réduit les compétences de lecture traditionnelle.




---

#
# Les mythes et légendes autour du numérique en classe

* Le numérique permet de s'adapter aux styles d’apprentissage des élèves :arrow_right: Mythe...

* Les *Digital Natives* sont différents :arrow_right: Mythe...

#
> [Quelques mythes dans la recherche en éducation](http://espe-rtd-reflexpro.u-ga.fr/docs/sciedu-general/fr/latest/mythes_education.html?highlight=mythes) (Dessus & Charroud, 2016)



---
## Des "mythes" récurrents

* **Le numérique est motivant pour les élèves** :arrow_right: Vrai dans certains cas mais pas de lien avec les performances réelles d'apprentissages. (Oviatt & Cohen, 2010) (Sung & Mayer, 2013)
#
* **Les jeux vidéo permettent des apprentissages** :arrow_right: Possible seulement si le jeu contient un scénario pédagogique adapté. (Wouter & Van Oostendrop, 2013).

:warning: Le mot "ludique" employé dans le sens faible, désignant le fait que l'environnement est amusant, **n'a pas de rapport établi avec les apprentissages** (Vogel et al., 2006) :warning:

---
## Autres "mythes" récurrents
* **Le numérique favorise l'autonomie des apprenants** :arrow_right: faux, l'autonomie est une compétence à acquérir pour pouvoir apprendre avec le numérique et non le contraire. (Lehmann, 2014).

* **La lecture sur écran réduit les compétences de lecture traditionnelle** :arrow_right: faux, la lecture numérique fait appel à des compétences partagées avec la lecture papier. (Baccino, 2004) (Britt & Rouet, 2012)


:warning: Recheche web :arrow_right: lecteur expert pour parcourir efficacement l’information (Salmeron & Garcia , 2011).
:warning: Recheche web :arrow_right: habiletés qui aident à s’autoréguler. (Dogusoy-Taylan & Cagiltay, 2014)


---

### :pencil2: TP : Un bilan intermédiaire (durée 1 minute)

Reprenez la liste effets positifs/effets négatifs que vous avez rédigée, faite un premier bilan...


---
# 

#  Numérique et apprentissages, 
## vous voulez vous lancer ?


---


# 
# 3. Numérique et apprentissages, quelles précautions ?

## Limiter les effets négatifs

---


# 3.1 Le numérique est-il dangereux pour la santé?

---
## 3.1.1 Des craintes récurrentes et médiatisées 1/2

* Les écrans :
	* 	Des effets sur le développement cérébral des enfants en âge préscolaire (Hutton & al., 2020)(Lin & al., 2022)
	* 	Sédentarité :arrow_right: Limitation du temps d'écran = activité physique (Pedersen,2022)
	* 	Sommeil :arrow_right: Conférence sommeil et apprentissages
	* 	Problèmes ophtalmiques [Ecole numérique, écrans et santé ophtalmique (Aptel & Charroud, 2017)](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/cours_Ophtalmo.html)
	* 	Des effets psychologiques et sociaux (Desmurget, 2019) ... mais peut-être simplement amplifiés par le numérique (Sauce, Liebherr, Judd & Klingberg, 2022)
	
	
---
## 3.1.2. Des craintes récurrentes et médiatisées 2/2


* Wifi, les ondes de la discorde, quid des autres ondes électromagnétiques ? [Numérique, wifi, téléphone, les ondes à l’école (Charroud & Choucroune, 2016)](http://espe-rtd-reflexpro.u-ga.fr/docs/scied-cours-num/fr/latest/cours_Ondes.html)
* Et l'impact du numérique sur l'environnement, vous y pensez ? [Numérique et environnement (Berthoud, Charroud, Renard , 2018)](http://espe-rtd-reflexpro.u-ga.fr/docs/scied-cours-num/fr/latest/cours_Environnement.html)



---



# 3.2 Usages courant en classe 
## Il est indispensable d'analyser ses pratiques au regard des résultats de la recherche si l'on veut éviter les problèmes....

---


# 
##  :warning: Les effets que l'on pense positifs peuvent :warning: rapidement se révéler négatifs :

Outre une perte de temps, d'énergie et de crédibilité: le risque principal est de générer une

## **augmentation de la différence entre les élèves**
(Zheng, 2016)


---
# 
## 3.2.1. Un exemple courant : On veut différencier avec le numérique


> On raconte qu'il suffit de présenter à l'apprenant une même information sous différents formats pour être efficace.

###



### Que dit la recherche ?
> Effects of prior knowledge on learning from different compositions of representations in a mobile learning environment (Liu, Lin & Paas, 2014)

---
![140%](images/img-effets/Effets-liu11.png)

Texte + Photo

---
![140%](images/img-effets/Effets-liu12.png)

Texte + Photo + Exemple réel

---
![140%](images/img-effets/Effets-liu13.png)

Texte + Schéma + Exemple réel


---
## À votre avis quel groupe a le mieux appris ?

Groupe 1 : Texte + Image
Groupe 2 : Texte + Image + Plante réelle
Groupe 3 : Texte + Schéma + Plante réelle



---
## Résultats
Les meilleurs apprentissages ont été obtenus par :
# Le premier groupe (texte+image)

---

## Explication rationnelle
La multiplication des sources d’information (texte + image + objet réel) a provoqué une division de l’attention qui a gêné l’apprentissage des élèves.

---

# :exclamation: À retenir :exclamation:

**Pour différencier, ne vous laissez pas entraîner par la facilité à multiplier les sources avec le numérique.**

Utilisez un document avec 2 sources :
* **une source verbale** (écrite ou sonore)
* **une source picturale** (fixe ou animée)




***Sinon il sera profitable seulement aux meilleurs élèves.***


---
## 3.3 Toutes les sources numériques sont-elles efficaces ?

### Avec le numérique on néglige souvent la charge cognitive...


#
### Que dit la recherche ?
> La charge cognitive dans l’apprentissage (Charroud & Dessus, 2016)



---
### 3.3.1. Charge cognitive 1/3
* Les sources doivent être épurées.

### Attention aux vidéos et/ou animations

Pour qu'une vidéo/animation soit efficace pour les apprentissages, il faut :
* que l'apprenant ait un contrôle minimal sur le rythme de défilement d’une animation ou d’une vidéo;
* présenter de façon animée des informations elles-mêmes dynamiques;
* limiter le nombre d’informations à maintenir en mémoire pendant le visionnage.

Vérifiez bien les vidéos (même celles issues de sites institutionnels) avant de les utiliser....

---

### 3.3.2 Charge cognitive 2/3
### Apprentissage de règle logique : écran vs réelle

> Prefrontal cortex and executive function in young children. (Moriguchi & Hiraki, 2013). 

Adulte face à un écran (Neurones miroirs OK) :
:arrow_right: Apprentissage = Inférence de la règle

Élève face à un écran (Neurones miroirs en développement) :
:arrow_right: Apprentissage = Inférence de la règle + Interprétation empathique


> **L'apprentissage va demander beaucoup d'effort à l'élève.** (Ferrari, 2014) 
    
    
---
### 3.3.3 Charge cognitive 3/3
### Ne pas oublier l'interface...


![30%](images/img-effets/Effets-geogebra.jpg)



Il faut respecter la simplicité d'utilisation, donnant accès aux fonctions facilitant les apprentissages scolaires. (Tijus, 2006)

---
# :exclamation: À retenir :exclamation:

Pensez à la charge cognitive :

* Des ressources épurées.
* Des vidéos contrôlables par l'élève.
* Ne pas oublier que la perception de l'élève est différente de celle de l'adulte (neurones miroirs).
* Et enfin il ne faut pas que l'interface soit un obstacle aux apprentissages

---

### :pencil2: TP : Un bilan intermédiaire (durée 1 minute)

Reprenez la liste effets positifs/effets négatifs que vous avez rédigée, corrigez si besoin...


---
# 4.  Des effets positifs du numérique sur les apprentissages

##  et oui, il y en a ....


---

## L’utilisation du numérique ***peut*** avoir un effet positif sur l’apprentissage

- **Pour accéder à des informations**

- **Pour communiquer**

- **Pour s'entraîner sur des tâches ciblées**

> (J-PAL, 2019)


---

# 
# Et en pratique, comment exploiter ces effets positifs potentiels ?

## Illustration basée sur les piliers fondamentaux de l’apprentissage identifiés par les neurosciences.


---
# 4.1. Communication et entraînement
## L’attention
– Alerte (quand faire attention)
– Orientation (à quoi faire attention)
– Contrôle exécutif (comment faire)

> (Dehaene, 2018)

---
## Ressources numériques et attention de l'apprenant.


*Les élèves sont-ils égaux devant un document numérique ? Même bien conçu...*


## Que dit la recherche ?

> Eye-movement patterns (Mason, Tornatora, & Pluchino, 2013).

---
![110%](images/img-effets/Effets-masson1.png)

---
![110%](images/img-effets/Effets-masson2.png)

---
## Résultats :

### Les meilleurs apprentissages sont réalisés pas le 3ème type d'élève.

Plus les apprenants réalisent de traitements d'intégration entre la source verbale (texte ou son) et la source picturale (illustrations) plus ils apprennent.

---

## Conclusions :
* Proposer 2 sources dans un document et pas une seule...

* :warning: À partir d'un même document, les apprenants n'ont pas la même stratégie d'apprentissage, **seuls les meilleurs profitent du document**.


---
## Comment inciter les transitions entre les sources ?

> Utilisation d'un document avec commentaires sonores (Jamet, 2014).

![150%](images/img-effets/Effets-jamet14.png)

---
# :exclamation: À retenir :exclamation:

Avec le **guidage**, les apprenants accordent davantage d’attention (temps de fixation total) aux informations pertinentes grâce à la signalisation.

## Résultats :
* Des effets sur la complétude et la rétention (Mémorisation)
	
* Mais pas d'effet sur les apprentissages profonds (Compréhension)



---
# 
# 4.2. Entrainement disciplinaire : numérique et Feedback

## Rappel sur les rétroactions pour l'appentissage
* Nécessité de feedback
* Feedback, si possible, immédiat 
* Si la réponse comporte une erreur :arrow_right: remédiation
* Si la réponse est correcte (tant que la connaissance n’est pas parfaite, le cerveau continue d’apprendre) => nécessité de surapprentissage (réviser, remettre à l’épreuve…)

(Dehaene, 2018)

---


## Entrainement sur des tâches ciblées
![10%](images/img-effets/Effets-charge.jpg)

**Automatisation :**
Systématisation :arrow_right: Exerciseurs :arrow_right: **Tuteurs intelligents**

---
## Un peu d'histoire
* Années 50, Skinner et l'enseignement programmé
* Années 70-80, Intelligence Artificielle
* Années 2000, développement du numérique individuel

***Beaucoup d'espoirs et... des résultats positifs***

---
## Pourquoi ça marche ?

* Feedback immédiat (renforcement).
* Patience infinie.... 
* et d'autres avantages (traces, adapatation, souplesse horaire...)



---
# :exclamation: À retenir :exclamation:


### Les exerciseurs et/ou tuteurs intelligents donnent de bons résultats MAIS qui ne dépassent pas le tutorat humain!
(Kulik, & Fletcher 2016)

#

*...Pour votre employeur, il est plus facile de multiplier les tuteurs numériques que les tuteurs humains...*


---
## :pencil2: TP : Durée 10 minutes
### :arrow_right: Recherche d'applications permettant d'avoir un "bon" feedback
* Feedback, si possible, immédiat 
* Si la réponse comporte une erreur :arrow_right: remédiation
* Si la réponse est correcte (tant que la connaissance n’est pas parfaite, le cerveau continue d’apprendre) => nécessité de surapprentissage (réviser, remettre à l’épreuve…)

En groupe, essayez de lister les applications pouvant fournir de "bons" feedback dans votre discipline.


---

# 4.3. Mémoire et numérique 
*Encore un entrainenement*
## La consolidation
– Passer d’un traitement lent, conscient, avec effort (sous contrôle du cortex préfrontal) à un fonctionnement rapide, inconscient, automatique :
– Répéter :arrow_right: Automatisation
– Libérer des ressources cognitives pour éviter le « goulot d’étranglement cognitif »

> (Dehaene, 2018)

---
## Consolider, un sujet déjà assez ancien

![170%](images/img-effets/Effets-ebbinghaus.jpg)

---
### Mémoire : quelques rappels sur l'anatomie cérébrale



![](images/img-effets/Effets-myelinreal.jpg)

---
### Le processus de myélinisation

![50%](images/img-effets/Effets-myelinzoom.jpg)

---
### Importance de cette couche de Myéline

* L’épaisseur du gainage de myéline est en relation directe avec nos aptitudes et nos performances.
* L'épaisseur de la myéline dépend du nombre et de l'espacement des sollicitations (Rappel expansé)
* Le numérique permet de poser des questions **"au bon moment"** en s'adaptant à la physiologie

Exemple **non exhaustif** : L'application ANKI

---


![100%](images/img-effets/Effets-anki_ex.jpg)

---


# Comment aborder l'avenir ?

Défendons plutôt une posture critique ayant du sens, une
finalité, de l’objectivité et de la rigueur. 

Cela implique d’aborder les technologies avec méfiance et scepticisme 
mais toujours dans un esprit constructif plutôt qu’avec cynisme.

> (Selwyn, 2018)

---


# 4.5. Des entrainements intéressants 
# (ou pas)
Un exemple d'usage necessitant une posture critique

---


# 
# Le numérique pour entrainer des compétences transversales nécessaires aux apprentissages :

* Améliorer la vision
* Réduire les déficits d'attention
* Favoriser le traitements multitâches





---
## Que dit la recherche ?
### :warning: C'est très contre-intuitif
Certains jeux vidéo souvent considérés comme les pires (les jeux de tir à la première personne) font partie des usages qui permettent le plus d'entraîner des circuits cérébraux impliqués dans des tâches transversales nécessaires aux apprentissages....

---
## Dans le détail 1/3
Les jeux vidéo d'action permettent d'améliorer la vision  en entraînant :

* l'identification de petits détails au milieu du désordre
* la capacité à distinguer et identifier les niveaux de gris

> (Eichenbaum, 2014)


---
## Dans le détail 2/3
Les jeux vidéo d'action permettent de réduire les déficits d'attention (Green, 2003) :	
* Résolution de conflits cognitifs plus rapide
* Augmentation de la capacité à suivre plus d'objets dans un environnement évolutif (3-4 objets pour un non joueur, jusqu'à 7 objets pour un joueur...)

IRM :arrow_right: des changements visibles sur des réseaux corticaux des joueurs:
	* Cortex pariétal qui contrôle l’attention et l’orientation
	* Cortex frontal qui nous aide à maintenir notre attention (inhibition)
	* Cortex cingulaire antérieur qui contrôle comment nous affectons et régulons notre attention et résolvons les conflits.

---
## Dans le détail 3/3
Les jeux vidéo d'action permettent d'entraîner aux traitements multitâches  (Bavelier, 2018).

Les gens qui font des jeux vidéo d’action sont très très bons, ils passent d’une tâche à l’autre très rapidement avec un faible coût cognitif.
Les gens qui pratiquent uniquement le multitâche multimédia (utilisation simultanée d'un navigateur web, en écoutant de la musique, en gardant un œil sur son smartphone...) sont très nettement moins performants.

---
## :warning: Que retenir de l’effet des jeux vidéo :
* La sagesse collective n’a pas de valeur :arrow_right: c'est très contre-intuitif
* Tous les médias ne naissent pas tous égaux, ils ont des effets totalement différents sur différents aspects de la cognition de la perception et de l’attention :arrow_right: chaque jeu doit faire l'objet de test...
* Ces jeux vidéo ont des effets puissants pour la plasticité cérébrale, l’attention, la vision.
## Mais aussi pleins d'effets non désirables (santé, psychologie, sociale) , il faudrait les consommer avec modération et au bon âge... 


---
# 
## Les jeux vidéo pour apprendre, pour de vrai et sans effets négatifs... un jour peut-être ?
Il faudrait :
* Comprendre quels sont les "bons" ingrédients pour produire des effets positifs en termes d'apprentissages. (Brocolis)
* Réaliser des produits attirants auxquels on ne peut pas résister. (Chocolat)
* Réunir les deux :arrow_right: ce n'est pas simple (Le brocoli au chocolat ce n’est pas terrible)

### C'est en cours d'exploration....



---
# 

## Avec le numérique, il est nécessaire de toujours évaluer le rapport bénéfices/risques 

![100%](images/img-effets/Effets-balance.jpeg)


---


# 5. Conclusion
## Le numérique peut être efficace pour les apprentissages... mais pas dans tous les cas, et pas à tous les âges.

# Avancez avec prudence, essayez de pas vous laisser entraîner par la connaissance commune, mais appuyez-vous sur la connaissance scientifique !

## Gardez une posture critique et contructive




---
## À méditer :

Le numérique n’est pas une boîte à outils, une valise d’applications et de logiciels qui viennent agrémenter l’action pédagogique et les processus d’apprentissage ou se substituer à d’autres méthodes d’enseignement-apprentissage alors jugées moins innovantes. 

Le numérique est un objet complexe, englobant des acceptions multiples, et caractérisant des objets et outils dont il ne suffit pas de se saisir, de façon pragmatique, pour comprendre le monde et exercer un esprit critique."


> Extrait d'un rapport publié par le Centre national d’étude des systèmes scolaires (Cnesco) sur la thématique : Numérique et apprentissages scolaires. (Cordier, 2020)

---

# Références

Baccino, T. (2004). La lecture électronique: Presses universitaires de Grenoble.

Bachelard, G. (1953). Le matérialisme rationnel. Paris: Presses universitaires de France.



Bavelier, D., Bediou, B., & Green, C. S. (2018). Expertise and generalization: Lessons from action video games. Current opinion in behavioral sciences, 20, 169-173.

---



Britt, M. A., & Rouet, J. F. (2012). Learning with multiple documents: Component skills and their acquisition. Enhancing the quality of learning: Dispositions, instruction, and learning processes, 276-314.

Charroud, C. & Dessus, P. (2016). La charge cognitive dans l’apprentissage. Consulté à http://espe-rtd-reflexpro.u-ga.fr/docs/sciedu-general/fr/latest/chargecog.html .

Cordier, A. (2020). Des usages juvéniles du numérique aux apprentissages hors la classe. Paris : Cnesco.

Dillenbourg, P. (2018). Pensée computationnelle: pour un néopapertisme durable car sceptique. De 0 à 1 ou l’heure de l’informatique à l’école, 17.


Dehaene, S. (2018). Apprendre!: Les talents du cerveau, le défi des machines. Odile Jacob.

---

Desmurget, M. (2019). La fabrique du crétin digital-Les dangers des écrans pour nos enfants. Média Diffusion.

Dogusoy-Taylan, B., & Cagiltay, K. (2014). Cognitive analysis of experts’ and novices’ concept mapping processes: An eye tracking study. Computers in human behavior, 36, 82-93.

Eichenbaum, A., Bavelier, D., & Green, C. S. (2014). Video games: Play that can do serious good. American Journal of Play, 7(1), 50-72.


Ferone, G. (2019). Numérique et apprentissages : prescriptions, conceptions et normes d’usage. Recherches en Éducation, 35, 63–75.

Ferrari P. F. (2014) « The neuroscience of social relation. A comparative-based approach to empathy and to the capacity of evaluating others’action value », Behavior, 151.

---

Green, C. S., & Bavelier, D. (2003). Action video game modifies visual selective attention. Nature, 423(6939), 534-537.

Houart, M. (2017). L’apprentissage autorégulé: quand la métacognition orchestre motivation, volition et cognition. Revue internationale de pédagogie de l’enseignement supérieur, 33(33-2).

Hutton, J. S., Dudley, J., Horowitz-Kraus, T., DeWitt, T., & Holland, S. K. (2020). Associations between screen-based media use and brain white matter integrity in preschool-aged children. JAMA pediatrics, 174(1), e193869-e193869.



---

Jamet, E. (2014). An eye-tracking study of cueing effects in multimedia learning. Computers in Human Behavior, 32, 47-53.

J-PAL Evidence Review. 2019. “Will Technology Transform Education for the Better?” Cambridge, MA: Abdul Latif Jameel Poverty Action Lab.


Kulik, J. A., & Fletcher, J. D. (2016). Effectiveness of intelligent tutoring systems: a meta-analytic review. Review of Educational Research, 86(1), 42-78.

Kersey, A. J., & James, K. H. (2013). Brain activation patterns resulting from learning letter forms through active self-production and passive observation in young children. Frontiers in psychology, 4, 567.


Lacelle, N., & Lebrun, M. (2016). La formation à l’écriture numérique: 20 recommandations pour passer du papier à l’écran. Revue de recherches en littératie médiatique multimodale, 3.

---

Lehmann, T., Hähnlein, I., & Ifenthaler, D. (2014). Cognitive, metacognitive and motivational perspectives on preflection in self-regulated online learning. Computers in human behavior, 32, 313-323.


Liu, T. C., Lin, Y. C., & Paas, F. (2014). Effects of prior knowledge on learning from different compositions of representations in a mobile learning environment. Computers & Education, 72, 328-338.



Mason, L., Tornatora, M. C., & Pluchino, P. (2013). Do fourth graders integrate text and picture in processing and learning from an illustrated science text? Evidence from eye-movement patterns. Computers & Education, 60(1), 95-109.

Mayer, C. P. (2009). Security and privacy challenges in the internet of things. Electronic Communications of the EASST, 17.

---

Moriguchi, Y., & Hiraki, K. (2013). Prefrontal cortex and executive function in young children: A review of NIRS studies. Frontiers in Human Neuroscience, 7, Article 867

Oviatt, S. L., & Cohen, A. O. (2010). Toward high-performance communications interfaces for science problem solving. Journal of science education and technology, 19(6), 515-531.
 
Sauce, B., Liebherr, M., Judd, N., & Klingberg, T. (2022). The impact of digital media on children’s intelligence while controlling for genetic differences in cognition and socioeconomic background. Scientific reports, 12(1), 1-14.

Salmerón, L., & García, V. (2011). Reading skills and children’s navigation strategies in hypertext. Computers in Human Behavior, 27(3), 1143-1151.

---

Sung, E., & Mayer, R. E. (2013). Online multimedia learning with mobile devices and desktop computers: An experimental test of Clark’s methods-not-media hypothesis. Computers in Human Behavior, 29(3), 639-647.


Selwyn, N. (2018). Approches critiques des technologies en éducation : un aperçu. Formation et profession, 27(3), 6-21.

Tijus, C., Poitrenaud, S., Bouchon-Meunier, B., & De Vulpillières, T. (2006). Le cartable électronique: sémantique de l'utilisabilité et aide aux apprentissages. Psychologie française, 51(1), 87-101.

Vogel, J. J., Vogel, D. S., Cannon-Browers, J., Browers, C. A., Muse, K., & Wright, M. (2006). Computer gaming and interactive simulations for learning: A meta-analysis. Journal of Educational Computing Research, 34. 229-243.

---

Wouters, P., & Van Oostendorp, H. (2013). A meta-analytic review of the role of instructional support in game-based learning. Computers & Education, 60(1), 412-425.


Zheng, B., Warschauer, M., Lin, C. H., & Chang, C. (2016). Learning in one-to-one laptop environments: A meta-analysis and research synthesis. Review of Educational Research, 86(4), 1052-1084
