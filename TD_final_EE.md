---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# UE 41 - Culture numérique : système d’information et outils du CPE 23-24
## TD Final
### M2 EE - Grenoble
 
Christophe Charroud - UGA
 
<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2023 - 2024 - CC:BY-NC-SA -->

---
# :one: Retour sur vos productions


---
# :two: Etes vous addict aux écrans?

:arrow_right: Test issu du DSM-5 et du programme SANPSY

Test silencieux, ne communiquez pas vos resultats.
Chaque fois que vous repondrez oui, comptez un point.


---
## 2.1 Test 1/3

•	**Préoccupation** : Passez-vous beaucoup de temps à penser aux écrans, y compris quand vous n'en utilisez pas, ou à prévoir quand vous pourrez en utiliser à nouveau ?

•	**Sevrage** : Lorsque vous tentez d'utiliser moins d'écrans ou de ne plus en utiliser, ou lorsque vous n'êtes pas en mesure d'utiliser d'écran, vous sentez-vous agité, irritable, d'humeur changeante, anxieux ou triste ?

•	**Tolérance** : Ressentez-vous le besoin d'utiliser des écrans plus longtemps, d'utiliser des écrans plus excitants, ou d'utiliser du matériel informatique plus puissant, pour atteindre le même état d'excitation qu'auparavant ?

---
## 2.2 Test 2/3

•	**Perte de contrôle** : Avez-vous l'impression que vous devriez utiliser moins d'écrans, mais que vous n'arrivez pas à réduire votre temps d'écran ?

•	**Perte d'intérêt** : Avez-vous perdu l'intérêt ou réduit votre participation à d'autres activités (temps pour vos loisirs, vos amis) à cause des écrans ?

•	**Poursuite malgré des problèmes** : Avez-vous continué à utiliser des écrans, tout en sachant que cela entrainait chez vous des problèmes tels que ne pas dormir assez, être en retard au travail, se disputer, négliger des choses importantes à faire... ?

---
## 2.3 Test 3/3

•	**Mentir, dissimuler** : Vous arrive-t-il de cacher aux autres, votre famille, vos amis, à quel point vous utilisez des écrans, ou de leur mentir à propos de vos habitudes d'écrans ?

•	**Soulager une humeur négative** : Avez-vous utilisé des écrans pour échapper à des problèmes personnels, ou pour soulager une humeur indésirable (sentiments de culpabilité, d'anxiété, de dépression) ?

•	**Risquer ou perdre des relations ou des opportunités** : Avez-vous mis en danger ou perdu une relation amicale ou affective importante, des possibilités d'étude à cause des écrans ?

---
## 2.4 Bilan du test

Si vous avez un **score égal à 5 points ou plus** alors votre comportement sera médicalement qualifié « d'addiction aux écrans ».

L'addiction est une maladie qui peut (et qui doit) être prise en charge.

---
## 2.5 L'addiction aux écrans est-elle courante ?

* Les études actuelles montrent que **l'addiction au sens médical est relativement rare** parmi les adolescents et les adulte, elle concernerait  1,7% de la population.

* Mais 44,7% de la population présenterait au moins l'un des neuf critères recherchés :arrow_right: **usagers avec problèmes, mais sans addiction.** :arrow_right: il faut être vigilant.

(Alexandre, Auriacombe, Boudard, 2022)


---
# :three: PIX+EDU mention EE

* Si vous n'en n'avez pas encore, créez-vous un compte sur pix.fr :warning: si possible avec une adresse @ac-grenoble.fr
#
***Référentiel de formation : CRCN***
**:arrow_right: Public cible : élèves (école-collège-lycée), et étudiants 1er cycle (licence)**

---
## :pencil2: TP - Découverte de PIX+EDU (durée 30 minutes)

* Connectez vous à la campagne PIX+EDU "les essentiels" :warning: Campagne de test
- Cliquez sur "j'ai un code"

![](images/img-pix_edu/code_pix.png)
* Code de connexion: **SSGGGA469**

* Effectuez quelques tests
***Référentiel de formation : CRCNé***
**:arrow_right: Public cible : étudiants MEEF, stagiaires, enseignants en poste**

---

# :four: Débat sans langue de bois

## Le numérique dans vos établissements, c'est bien ou pas ?

