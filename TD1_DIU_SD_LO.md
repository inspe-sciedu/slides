---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
   
# Culture numérique et apprentissages
## TD 1
 
Christophe Charroud - UGA
 
<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2023 - 2024 - CC:BY-NC-SA -->

---
# :zero: Préambule - Information sur PIX+EDU

### Historique

* **Janvier 2022** : Le MENJ décide de la création d'une nouvelle certification PIX+EDU concernant l'usage du numérique en situation d'enseignement :arrow_right: commande aux universités et aux rectorats.
Certification en deux volets :
    * Un volet pratique professionnelle réalisé à l'INSPE (conception, mise en oeuvre réelle ou simulée, analyse réflexive) :arrow_right: Obtention d'un niveau pour la certification (initié ou confirmé)
    * Un volet automatisé initié à l'INSPE et terminé lors des premières années de titularisation :arrow_right: certification
---
### Historique (suite)

* **2022 - 2023** : Année d'expérimentation dans certains INSPE (pas à Grenoble) :arrow_right: résultats d'expérimentation conduisant les universités à demander à une première série d'ajustements.
* **Septembre 2023** : Le MENJ annonce la généralisation de PIX+EDU mais en l'absence de texte officiel et sans attribution de moyen :arrow_right: l'INSPE - UGA refuse de mettre en oeuvre la certification PIX+EDU dans ces conditions :arrow_right: **discussion avec le Rectorat de Grenoble pour une entrée partielle dans le dispositif PIX+EDU.**

---
### Dernières avancées

* **Octobre 2023** : Plusieures universités font remonter des difficultés ou des blocages dans la mise en oeuvre de la certification PIX+EDU, idem du côté de certains Rectorats
* **Fin novembre 2023** : Le MENJ et le MESR proposent (enfin) un projet plus réaliste pour la rentrée 2024... mais toujours pas de texte officiel à ce jour.

---
### Prévision du dispositif PIX+EDU pour la rentrée 2024

![](images/img-pix_edu/dep_pix.png)

---
# :one: Objectifs 
### Objectif général de la formation INSPE-UGA 
* Concevoir une situation d'apprentissage intégrant le numérique réaliste et réalisable dans votre classe (Obligation)
* Si possible, réaliser la mise en oeuvre (pas d'obligation, pensez à bien conserver toutes les traces).
* Rentrer dans la partie automatisé de PIX+EDU.
### Objectifs du TD
* Mettre en pratique les concepts théoriques sur la thématique "Production, usages de ressources et droits d'auteur" et "Enseigner avec le numérique : infox, IA et éthique".
 
---
## Organisation du TD

1 - Introduction (5 minutes)
2 - Mise en pratique : Etude de cas (1h15)
3 - Recherche d'une situation d'apprentissage réaliste (30 minutes)

---

# :two: Rôles de l'enseignant

![](images/img-juridique/roles.png)

---

# Compétences visées

(source : référentiel de compétences des enseignants, voir aussi le [CRCN](<http://espe-rtd-reflexpro.u-ga.fr/docs/scied-cours-num/fr/latest/crcn.html>))

* CC2 : Connaître les grands principes législatifs qui régissent le système éducatif, le cadre réglementaire de l'école et de l'établissement scolaire, les droits et obligations des fonctionnaires ainsi que les statuts des professeurs et des personnels d'éducation.
* CC6 :
  * Respecter et faire respecter le règlement intérieur et les chartes d'usage.
  * Respecter la confidentialité des informations individuelles concernant les élèves et leurs familles
* CC9 : Participer à l'éducation des élèves à un usage responsable d'internet.

---

# Compétences travaillées

* Capacité à **comprendre le contexte** dans lequel il évolue et à utiliser ses connaissances, recherches, réflexions à propos de la législation et des règlementations institutionnelles liés aux TICE **pour une mise en œuvre raisonnée et responsable** dans le cadre de son activité professionnelle.

---

# Les responsabilités de l'enseignant (rappel)

* Responsabilité civile
* Responsabilité pénale
* Responsabilité administrative

---

# Quelques obligations de l'enseignant (rappel)


* D'obéissance hiérarchique
* De discrétion professionnelle
* De signalement
* De neutralité

---

# Les principaux sujets abordés

1. Le droit à la vie privée
2. Les droits des auteurs
3. La sécurité des élèves
4. Le principe de neutralité

---

# :three: Etudes de cas

## Par groupe de 3 à 6 personnes (30 mn)

- Consulter le cas attribué disponible ici : https://lstu.fr/casjuridiquessd-4
- En vous aidant des ressources documentaires et du cas similaire corrigé, répondre à la situation problème.

## Présentations (30 mn)

- En vous appuyant sur les questions de la partie "Synthèse", réaliser une rapide présentation **orale** de 3 min maximum.

---


# Présentations


---
# :four: Travail de conception

#### Rappel de l'objectif pour l'évaluation :
Concevoir une situation d'apprentissage intégrant le numérique réaliste et réalisable dans votre classe.

#### Rappel de l'objectif de la formation (donc à la sortie de l'INSPE):
Si possible, mise en oeuvre en classe :arrow_right: PIX+EDU.


---
## Modalités de conception

* **TD 1 : Mise en application des apports théoriques sur les ressources et les aspects juridiques et éthiques** - Début de la conception :arrow_right: avoir une première idée de situation d'apprentissage.
* TD 2 :  Mise en application des apports théoriques sur les effets du numériques sur les apprentissages - Poursuite de la conception
* TD 3 : Apports pratiques sur la formation AU numérique et l'évaluation des élèves en référence au CRCN - découverte de PIX et PIX+EDU - Poursuite de la conception
* TD 4 : Retour d'expérience et mutualisation des conceptions et/ou  mises en oeuvre. :arrow_right: PIX+EDU

---
# En avant !
Conseils :
* Regroupez vous par niveau de classe. ( :warning: une situation indivuelle au final)
* Essayez d'identifier un apprentissages pour lequel le numérique peut apporter une plus-value.
* Restez réaliste :arrow_right: Mise en oeuvre conseillée.

 

