---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

## Diplôme Inter-universitaire PE
## Éducation fondée sur les faits : Comment intégrer la recherche ?
### Philippe Dessus
### Inspé, LaRAC, Univ. Grenoble Alpes

### ![w:350](images/logo-espe-uga.png)

<!--{Lawlor, 2019 $396}-->

---

<!-- footer: Ph. Dessus • DIU-PE | Éducation fondée sur les faits • Inspé-UGA 2022-23 • CC:BY-NC-SA-->
<!-- paginate: true -->

:warning: Les diapositives dont le titre est précédé d'un astérisque proviennent du cours de Marie-Paule Jacques, UGA

---
# Question pour commencer

- pourquoi utiliseriez-vous (ou utilisez) dans votre pratique d'enseignement des informations venant de la recherche ?
  - pourquoi ces informations-là (plutôt que d'autres)
  - pour quoi (à quelles fins) ?


---
# Résumé des réponses (1/2): pour quoi ?

- **Conceptuelle** : Utiliser le modèle *X* de lecture me permet de mieux comprendre comment les élèves apprennent à lire, de me faire une meilleure idée des tenants et aboutissants de cette activité complexe
- **Symbolique** : Utiliser le modèle *X* de lecture me permet de justifier une pratique, par exemple pour l'expliquer dans un projet, à mes collègues
- **Instrumentale** : Utiliser le modèle *X* de lecture me permet de mieux concevoir des exercices, des ressources, des rétroactions pour aider mes élèves

---
# Résumé des réponses (2/2) : pourquoi celles-ci ?

- Plus représentatives (fondées sur des échantillons de participants plus importants)
- Plus objectives (fondées sur des méthodes plus élaborées)
- Plus reproductibles (mieux expliquées)

---
# 1. Pourquoi s'intéresser aux recherches quand on est enseignant ?


{Mỹ, 2018 $13621}
https://www.idee-education.org/ressources/où-sont-les-preuves-%3F-


---
# Quels sont les facteurs pouvant influer l'usage de recherches chez les enseignants ?

Dagenais et al. (2012) en listent de nombreux 

- **Liés aux enseignants** : compétences méthodologiques, participation à des recherches antérieures, degré d'implication dans ces recherches, dispositions envers les recherches, expérience d'enseignement, auto-efficacité, matière enseignée, volonté d'innover
- **Liés aux recherches** : facilité d'accès ; de compréhension ; objectivité et véracité, en lien avec le contexte, pertinence
- **Liés à la communication** : accès aux services, aux recherches et leurs données, qualité de la recherche, discussion entre collègues, collaboration avec chercheurs, collaboration soutenue, médias
- **Liés aux écoles/établissements** : besoin d'innovation, de soutien extérieur, capacité à soutenir des recherches, des initiatives, expériences antérieures de recherche, incitation au développement pro., allocation de temps et ressources

---
# Avantages

- Difficile de comprendre quelque chose (un état, un processus) tant qu'on n'a pas essayé de le mesurer 
- Avoir une meilleure idée des concepts, des processus à l'œuvre
- Fonder cette idée sur des échantillons plus grands

---
# Problèmes avec les recherches

- mesurer, c'est transformer des propriétés en nombres, pas changer des valeurs en nombres
- on ne mesure que ce qui est facile à mesurer, donc on laisse de côté des éléments plus qualitatifs
- les méta-analyses (le "graal") peuvent être mal réalisées
- crise de la réplicabilité : un nombre assez important d'études ont des résultats non répliqués


---
# Que faire ?

Il existe plusieurs activités possibles pour avancer dans la connaissance de la recherche en éducation.

Il est conseillé de se choisir un nombre réduit de centres d'intérêt et de faire une veille à leur sujet.

Les activités suivantes peuvent être intéressantes à poursuivre.



---
# Être un enseignant réflexif

the-reflective-educators-guide-to-classroom-research-learning-to-teach-and-teaching-to-learn-through-practitioner-inquiry_compress.pdf

---
# Les experts sont toujours en désaccord

{Deroover, 2023 $13604}

Les experts peuvent ne pas être d'accord pour les raisons suivantes :

- Les **experts** : les experts peuvent avoir des compétences ou des motivations (conflits) qui les empêchent de se mettre d'accord sur une preuve ;
- Les **informations** : les types de preuves amenées, les preuves disponibles, l'ambiguïté des concepts utilisés ou des résultats
- L'**incertitude** : la pertinence de l'expert, l'incertitude inhérente à la science, 


---
# Connaissances académique vs. vernaculaires

voir Conner hist. populaire des sciences ; la science s'est faite par le peuple, les métiers
Dire : la science ne le montre pas invalide-t-elle notre perception dans la classe ? (peut-être, pas toujours !)

---
# Essayer de débusquer des mythes

Comme dans tous les domaines, l'éducation a certains 

---
# Faire des fiches PICO

La célèbre base de données Cochrane, répertoriant les résultats de la recherche médicale, structure ses informations sous la forme PICO. 

- **Participants/population** : Détails de la population (principales caractéristiques) et de l’échantillon : nombre de participants
- **Intervention** : Détailler ce à quoi sont exposés les participants (méthode pédagogique, etc.) : quoi ? comment ? où ? quand ? combien ?
- **Comparaison** : Préciser ce qui est comparé : quels sont les différents groupes de participants étudiés et quelles comparaisons sont réalisées (e.g., pédagogie “traditionnelle” avec pédagogie “innovante”, groupe travaillant sur ordinateur avec groupe papier-crayon)
- **Outcomes** (résultats) : Détailler les principaux résultats de l'étude

:books: [base de recherches](https://docs.google.com/document/d/1w-HOfseF2wF9YIpXwUUtP65-olnkPyWcgF5BiAtBEy0/edit#) sur les liens entre dépression et usage des médias sociaux (Haidt & Wenge)


https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/tuto-lire-article.html#une-methode-issue-de-pico



Lien vers doc :


@book{RN13611,
   author = {Dana, Nancy Fichtman and Yendol-Hoppey, Diane},
   title = {The Reflective Educator’s Guide to Classroom Research},
   publisher = {Corwin},
   address = {Thousand Oaks},
   year = {2014},
   type = {Book}
}

{Fawns, 2022 $421} 

---
# Moyens

- lire (et écrire) des textes de réfutation ({Schroeder, 2022 $13572}) A refutation text explicitly states the misconception and explicitly refutes this information before the correct information is presented ({Catrysse, 2022 $13571}) : coactivation integration ; competing activation
  - Connaissances préalables (facteurs froids) (amount, accuracy, spécificité, cohérence) {Catrysse, 2022 $13571} ; sous-facteur chaud Force, cohérence, committment (pour voir si/comment la révision va se passer)
  - Intérêt (situational & Individual) {Catrysse, 2022 $13571}
- Faire (lire) des méta-analyses, des cochrane report (FIMO ou un truc  comme ça)


---
# Travailler en collaboration
Utiliser profs-chercheurs

---
# Professional Dev.
Sims (lien avec l'intérêt aussi)

https://www.idee-education.org/ressources/où-sont-les-preuves-%3F-


---

---
# SLED

Posture : 
- Ne pas hésiter à lire des ressources en anglais
- être curieux, avoir des sources en input (p. ex., réseaux sociaux, communautés d'enseignants flux RSS de revues, etc.)
- quand un contenu vous intéresse, le lire en détail et, selon le temps, appliquer une “boule de neige” en lisant quelques références citées, pour se faire un meilleur avis
- prendre des notes (papier ou numérique) en les étiquetant, pour les retrouver
- être critique
- être humble, ne pas
- faire des formations (MOOC, etc.)

[Veille académique](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/veille-sci-ressources.html#scientometrie)

---
# Attitudes à propos de la recherche en éducation

- Se méfier du buzz médiatique
- se méfier des pyramides
- Se méfier des dichotomies :
- ne pas rester dans le franco-français, s'ouvrir à l'international (≠ Anglo-Américain)
- éviter les oppositions quanti/quali {Kuehn, 2022 $13581}
- se méfier des oppositions binaires+dichotomies (appr. actif/passif)  {Fawns, 2022 $421} et les effets de balancier (Zhao 2017) Préférer les continua.
- se méfier des phrases compliquées
- toujours revenir au but initial des auteurs
---
# Par groupes de 3 écrire un petit texte de réfutation

- Prendre un sujet qui vous intéresse et dans lequel vous êtes compétent.e (vous avez de bonnes connaissances théoriques et savez l'enseigner)

---
Obj. :
- lutter contre les misconceptions


1/ objectifs des profs pour utiliser des recherches ?

The reflective educators' guide
Side effects (what works may hurt)


11
Scinapse

3 https://elicit.org 
Connected Papers

https://github.com/shaunabanana/snowball
https://github.com/shaunabanana/intrigue

https://welearn.learningplanetinstitute.org/ (mettre bcp de texte) (attendre instructions Ignacio)


---
# Merci de votre attention !

- Merci à Pleen le Jeune pour ses commentaires d'une version antérieure de cette présentation, et sa traduction des facteurs de Dagenais et al.


---
# Références