---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# Culture numérique et apprentissages
## TD Juridique
 
Laurence Osete - UGA
 
<!-- page_number: true -->
<!-- footer: laurence.osete@univ-grenoble-alpes.fr - 2024 - 2025 - ![CC:BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png) -->

---
# :zero: Modalités d'organisation


  **TD : Analyse des problèmes juridiques soulevés par l'usage du numérique à l'école**

- Étude de cas (1h15)
- Mise en application à votre situation (45 mn)

---
# 0. Cadre institutionnel

<!-- _class: t-70 -->

[Référentiel des compétences professionnelles](https://www.education.gouv.fr/le-referentiel-de-competences-des-metiers-du-professorat-et-de-l-education-5753)
  - Respecter et faire respecter le règlement intérieur et les chartes d'usage.
  - Respecter la confidentialité des informations individuelles concernant les élèves et leurs familles.
  - **Participer à l'éducation des élèves à un usage responsable d'internet.**

[Article L312-9](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006071191/LEGISCTA000006182400/#LEGISCTA000027682793) du Code de l'éducation 
  - La **formation à l'utilisation responsable** des outils et des ressources numériques est dispensée dans les écoles [ ]. Elle comporte une **éducation aux droits et aux devoirs** liés à l'usage de l'internet et des réseaux, dont **la protection de la vie privée** et le **respect de la propriété intellectuelle**, [ ], ainsi qu'aux règles applicables aux traitements de données à caractère personnel.


---
# :one: Étude d'un cas juridique

## Par groupe de 3 à 6 personnes (30 mn)

- Consulter le cas attribué disponible ici : https://lstu.fr/casjuridiquespe
- En vous aidant des ressources documentaires et du cas similaire corrigé, répondre à la situation problème.


## Présentations (30 mn)

- En vous appuyant sur les questions de la partie "Synthèse", réaliser une rapide présentation orale de 3 min maximum.

---
# :two: Rappel : Recherche de situation

* La situation d'apprentissage peut provenir d'une situation **vécue, observée ou rapportée**. 
* l'idée peut être issue d'une banque de scénarios comme :
    - [la banque de l'académie de Nantes](https://www.pedagogie.ac-nantes.fr/numerique/scenarios-pedagogiques/)
    - [Prim à bord](https://primabord.eduscol.education.fr/spip.php?page=selection&mots%5B%5D=133#content)
    - ou autre
* Elle doit intégrer au moins une ressource numérique, ou hybride.
* Elle peut être commune à plusieurs personnes mais les analyses devront prendre en compte le contexte de mise en œuvre.

---
# :two: Rappel : Description de la situation

Vous devez décrire la situation telle qu'elle a été conçue, ou telle qu'elle le sera si vous aviez à la mettre en œuvre. 

#### Eléments attendus dans la description

```
- Matière
- Niveau de classe
- Période de l'année, ou place dans une séquence, où la situation a été (ou serait) mise en œuvre.
- Objectif(s) d’apprentissages :
    - Compétences disciplinaires visées en termes de « être capable de...», (ces compétences sont issues des programmes) 
    - Compétences numériques développées (issues du CRCN)
- Environnement numérique impliqué dans la situation :
    - Supports physiques (TBI, tablettes, ordinateurs fixes ou portables, enregistreurs, ...)
    - Outils : logiciels ou applications utilisés.
    - Ressources numériques (ou hybrides) utilisées, ou produites par les enseignants et/ou les élèves.
- Organisation pédagogique : description des activités des élèves, et de leur planification dans le temps et l'espace.

    
```

---
# :two: Analyse et justification des choix

- Justification des choix d'outils et de ressources numériques, en s'appuyant notamment sur les questionnements des différents modèles (SAMR, passifs-participatifs).
- Analyse des potentiels effets positifs ou néfastes pour les apprentissages et les apprenants (cf. apports théoriques du premier cours).
- **Analyse des problèmes juridiques (droits d'auteurs, droit à la vie privée) et éthiques soulevés.**

:warning: Les analyses et justifications doivent tenir compte du contexte.

---
#  En avant !


