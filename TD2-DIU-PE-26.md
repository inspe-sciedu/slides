---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
   
# Module Enseigner avec le numérique
## TD 3 - DIU PE - Valence


Christophe Charroud - UGA
 
<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - DIU-PE - UGA 2024 • ![CC:BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)  -->

---
# :zero: Rappels
* TD 1 : Mise en application des apports théoriques sur les effets du numériques sur les apprentissages - - Début de la conception :arrow_right: avoir une première proposition de situation d'apprentissage
* TD 2 : Mise en application des apports théoriques sur les ressources et les aspects juridiques et éthiques - Poursuite de la conception.
* **TD 3 : Apports pratiques sur la formation AU numérique et l'évaluation des élèves en référence au CRCN - Finalisation de la conception.**.
* TP : Mise en commun des conceptions et retour d'expérience pour celles et ceux qui auront réalisé une mise en oeuvre. :arrow_right: PIX+EDU.(17 mai)
 
---
#
### Former avec le numérique
###          VS
###   Former au numérique

---
# :one: Former et évaluer au numérique
Le numérique, des compétences à développer :arrow_right: à évaluer
* Un cadre de référence : [CRCN](https://eduscol.education.fr/721/evaluer-et-certifier-les-competences-numeriques)
* S’applique à tous les niveaux de l’école
* :warning: Aucun niveau exigé par l'institution mais scruté par certains employeurs
* Un outil d’évaluation national pour le collège et le lycée : [PIX](https://pix.fr)
* Auto-évaluation des élèves
* Auto-formation (tutoriels)
* Certification d’un niveau d’acquisition : 3ème, Terminale, Université.

---
# 1. Compétences numériques au primaire dans le LSU

![center](images/img-ressources/Comp1.png)

---

![center](images/img-ressources/Comp2.png)

---

![center](images/img-ressources/Comp3.png)

---

![center](images/img-ressources/Comp4.png)

![center](images/img-ressources/Comp5.png)


---
# 
## PIX : Évaluation et certification de compétences d'usages des technologies numériques.
#

### Allez sur le site pix.fr

---
# Évaluer au numérique : PIX
## :pencil2: TP - Découverte de PIX 
* Si vous n'en n'avez pas encore, créez-vous un compte sur pix.fr :warning: si possible avec une adresse @ac-grenoble.fr
#
***Référentiel de formation : CRCN***
**:arrow_right: Public cible : élèves (école-collège-lycée), et étudiants 1er cycle (licence)**

---
# PIX+EDU
## :pencil2: TP - Découverte de PIX+EDU (durée 30 minutes)

* Connectez vous à la campagne PIX+EDU "les essentiels" :warning: Campagne de test
- Cliquez sur "j'ai un code"

![](images/img-pix_edu/code_pix.png)
* Code de connexion: **EFVZRR862**

* Effectuez quelques tests
#
***Référentiel de formation : CRCNé***
**:arrow_right: Public cible : étudiants MEEF, stagiaires, enseignants en poste**

---
# Retour d'expériences

* Qu'avez-vous pensé des questions ?
* Sur quoi portaient-elles ?
    * sur des compétences transversales  (7 compétence du CRCN)
    * sur des compétences professionnelles (12 compétences du CRCNE)
* Qu'avez-vous pensé des retours sur les réponses et les tutoriels ?

---
# :two: Conception d'une situation d'apprentissage intégrant le numérique 
#### Production attendue
Un document rédigé en binôme de 4 pages maximum comportant :
* un descriptif de la situation d’apprentissage avec obligatoirement des liens pointants sur les éléments de conception (liens vers les ressouces, copies d’écran des interfaces élèves et/ou enseignants, …),
* une argumentation sur les choix opérés lors de la conception, notamment au regard des apports théoriques,
* un bilan personnel.

---
## 2.1 Rappel recherche de situation 

* L'idée de la situation d'apprentissage peut provenir d'une situation **vécue, observée ou rapportée**. 
* Elle doit intégrer au moins une ressource numérique, ou hybride.
* Elle peut être commune à plusieurs personnes mais les analyses devront prendre en compte le contexte de mise en œuvre.

---
## 2.2 Description de la situation 
#### Eléments attendus dans la description

```
- Domaine(s) d’enseignement concerné(s) par la situation
- Niveau de classe
- Période de l'année, ou place dans une séquence, où la situation a été (ou serait) mise en œuvre.
- Objectif(s) d’apprentissages : 
	- Compétences disciplinaires visées en termes de « être capable de…», (ces compétences sont issues des programmes)
	- Compétences numériques développées (issues du CRCN)
	
- Environnement numérique impliqué dans la situation :
    - Outils physiques (TBI, tablettes, ordinateurs fixes ou portables, caméras, enregistreurs, ...)
    - Organisation matérielle (nb de matériels par élève/classe, salle informatique ou salle de classe)
    - Logiciels ou applications utilisés.
    - Ressources utilisées, ou produites

-  Organisation pédagogique : quelles activités, quand, comment, ...
    
```

---
## 2.3 Analyse et justification des choix

- Justification des choix d'outils et de ressources numériques, en s'appuyant notamment sur le modèle SAMR.
- Analyse des problèmes juridiques (droits d'auteurs, droit à la vie privée) et éthiques soulevés(TD 1).
- Analyse des potentiels effets positifs ou néfastes pour les apprentissages et les apprenants (TD 2).
- Analyse des compétences numériques nécessaires et/ou développées par les élèves (TD 3)

:warning: Les analyses et justifications doivent tenir compte du contexte.

---
## 2.3 Mise à disposition des éléments de conception

Des illustrations des ressources et/ou outils mobilisés dans la situation doivent être fournies afin de meiux comprendre cette dernière et les analyses que vous faites : 
- Soit par des liens vers des ressources en ligne
- Soit par des liens vers copies d'écran, notamment pour montrer le fonctionnement des outils et/ou leur paramétrage.

Les copies d'écran pourront être déposées sur un mur de partage (type [digipad](https://digipad.app/) ou [padlet](https://padlet.com/)), ou sur l'application "[Nuage](https://nuage.apps.education.fr/)" de AppsEducation. 

---
## 2.4 Critères d'évaluation

Le dossier sera évalué au regard de la grille d’évaluation suivante [:inbox_tray:](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/_downloads/f799c13851a0009b1c539d2b8d3441ce/Grille_UE801.pdf). 

Cette grille d’évaluation est issue de la grille de compétences du dispositif PIX+EDU.

---
## 2.5 Déclaration des binômes

Afin de déposer le dossier et d'être évalué en tant que binôme, vous devez créer le groupe sur la plateforme de cours à l'aide de l'outil suivant :

![w:500](images/TD/ConstitutionBinomes.png)

![width:500px](images/seance6M1SD/GroupeE2.png)
![width:500px](images/seance6M1SD/GroupeE3PE.png)
Nom du groupe de la forme GRE-C1

---

# 2.5 Inscription du 2e participant

Inscription du deuxième participant
![width:800px](images/seance6M1SD/GroupeE4PE.png)

Désinscription possible
![width:800px](images/seance6M1SD/GroupeE5PE.png)

