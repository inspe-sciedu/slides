---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# Culture numérique et Apprentissage

## UE - Num 801 - Séance 8
Equipe formateurs numérique - UGA - 2024
 
---
<!-- page_number: true -->
<!-- footer: 2023 - CC:BY-NC-SA -->

# :one: Finalisation des présentations (15 minutes)

### Attendus :arrow_right: une présentation visuelle de votre situation et des principaux arguments et analyses en 180 secondes.

### Eléments attendus dans la présentation

```
 - Description du contexte, la place et l'usage du numérique.
 - Argumentation de l'intérêt des outils et ressources numériques, des effets positifs appuyés par des illustrations.
 - Identification des limites et problèmes potentiels. Présentation des précautions prises.
 ```

---
# :two: Présentations et questionnements

* Chaque groupe présente sa situation. (180s)
* Un autre groupe, choisi au hasard, devra poser une question, ou faire un commentaire afin de clarifier la présentation et/ou d'améliorer leurs analyses (3 mn).
* Le formateur pourra approfondir le questionnement si nécessaire

### Critères d'observation 

```
 - Le contexte est donné. La place et l'usage du numérique sont décrits.
 - L'intérêt des outils et ressources numériques, les effets positifs sont argumentés
 - Les limites et problèmes sont identifiés. Des précautions sont anticipées.
 ```

---
# :three: Finalisation de votre document
- Un seul document au format PDF déposé sur la plateforme.
- Police Arial, taille 12, interligne 1,5 cm, marges 2,5 cm.
- 4 pages maximum

Date limite de rendu : **17 mars 23h59**


