---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

## Usages d'outils et ressources numériques.
### Laurence Osete
### Source : Farah Guillot, 2022 
### Inspé, Univ. Grenoble Alpes

### ![w:350](images/logo-espe-uga.png)

---

<!-- footer: L.Osete • M1-DIU -2D •  Inspé-UGA 2024-25 • ![CC:BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)-->
<!-- paginate: true -->

# 0. Lien vers la présenation

![bg right](images/img-ressources/qr-Ressources2D.png)

https://link.infini.fr/meef-uga-num-ressources2d

---
# 0. :pencil2:  Préparation de l'activité

- Prendre une feuille A4 en position paysage, et tracer un tabeau avec cinq colonnes.
![center](images/img-ressources/Tableau5.png)

---
# 0. Questions à se poser avant d'utiliser une ressource.

<!-- _class: t-80 -->

**1. Quoi ?**
<!-- De quelle nature de ressource ai-je besoin ? -->

**2. Pourquoi ?**
<!-- A quoi va servir cette ressource ? Quel est l'objectif pédagogique visé ? -->

**3. Quand et Comment ?**
<!-- A quel(s) moment(s) de la séquence (ou séance) l'utiliser ? Pendant le temps de classe, ou à la maison ? -->
<!-- Qui va l'utiliser? Sur quel support ?  -->

**4. Prérequis ?**
<!-- De quelles compétences a besoin l'élève (ou l'enseignant) pour l'utiliser ? -->

**5. Conditions d'utilisation ?**

Noter les questions dans les colonnes de votre tableau.

---
# :one: Qu'est-ce qu'une ressource pédagogique ?

#### Une ressource pédagogique est une entité, un "grain", numérique ou non, utilisée dans un processus d’enseignement, de formation ou d’apprentissage. 

| Les différents types de ressources pédagogiques | 
| :-------------: | 
| ![center](https://4.bp.blogspot.com/-bZMMqFxAcjk/Vhk9KCpMv9I/AAAAAAAAAEc/DPi0lLLbRBY/s640/pyramide%2Bobjets%2Bp%C3%A9dagogiques.jpg) ([1](#34))| 

---
# 1.1 :warning: Ne pas confondre Outil, Ressource et support

L'outil sert à afficher, diffuser, créer, ou modifier une ressource (un contenu) sur des supports qui peuvent varier.

<!-- _class: t-80 -->

##### Exemples

| Ressource (contenu) | Outil (logiciel) | Support (matériel) |
| ------ | ------ | ------ |
|    Une page ou un site Web    |    Navigateur    | Ordinateur |
|    Une carte mentale    |  Framindmap | Ordinateur + TBI |
|    Un parcours de course d'orientation | Vikazimut  | smartphones |
|    Un exercice d'orthographe    |  Projet Voltaire  | tablette |

* Parfois les deux sont indissociables.

---
# 1.2 Quelle nature de ressource ?

<!-- _class: t-80 -->

| Réelle-tangible | Hybride |Numérique|
| :------: | :------: | :-------: |
| ![w:300](https://cdn.pixabay.com/photo/2017/09/23/15/04/chart-2779132_1280.jpg) ([2](#34)) | ![w:300](https://upload.wikimedia.org/wikipedia/commons/f/f4/App_iSkull%2C_an_augmented_human_skull.jpg?20130221100233)([3](#34)) | ![w:300](https://cdn.pixabay.com/photo/2017/09/05/10/08/office-2717014_960_720.jpg) ([2](#34)) |

---
## 1.3 Les banques de ressources institutionnelles

- [LUMNI Enseignements](https://eduscol.education.fr/208/lumni-enseignement?menu_id=262) : tous types de ressources (textes, sons, images, vidéos, exercices), dans beaucoup de champs disciplinaires
- [Matilda](https://matilda.education/) : Vidéos sur les thématiques de l'égalité entre les sexes
- [English for schools](https://englishforschools.fr/) : Activités et ressources proposées par le CNED.
- [Audio Lingua](https://audio-lingua.ac-versailles.fr/) :  Enregistrements dans différentes langues pour l'école.
- [ÉLéathèque](https://communaute.elea.ac-versailles.fr/local/knowledgegate/index.php?way=search.page.bytags) : partage de ressources pédagogiques entre enseignants
- [Etincel](https://www.reseau-canope.fr/etincel/)

---
## 1.4 Les banques de ressources "grand public"

<!-- _class: t-80 -->

- Encyclopédies : [Universalis junior](https://junior.universalis.fr/),   [Vikidia](https://fr.vikidia.org), [Wikimini](https://fr.wikimini.org)
- [Le cartable Fantastique](https://www.cartablefantastique.fr/) : Leçons et exercices dans plusieurs disciplines adaptés pour les élèves dyspraxique.
- [LaSonothèque](https://lasonotheque.org/) : banque de sons, bruitages libres et gratuits.
- [La Méta-Librairie](https://www.meta-librairie.com/fr) : banque de musiques visuelles.
- [Wikimédia](https://commons.wikimedia.org/wiki/Accueil) : banque d'images et de vidéos de Wikipédia.
- [Gallica](https://gallica.bnf.fr/blog/recherche/?query=1857&mode=desktop) : Catalogue d'œuvres entrées dans le domaine public.
- [Free Music Archive](https://freemusicarchive.org/home)

[Tableau collaboratif de ressources](https://cloud.univ-grenoble-alpes.fr/s/Qmj5XBw9wxWSKkP)


---
# :pencil2: Activité

- Chercher et sélectionner dans les banques de ressources proposées une ressources numérique, ou hybride **connue**, et une **découverte**.
- Noter dans la colonne **"QUOI"** de votre tableau, sur deux lignes, les ressources sélectionnées avec leur type.
- Identifier le ou les outils permettant de les créer, les manipuler ou les diffuser.
- Identifier le ou les supports possibles.

---
## 1.5 Les outils pour créer et/ou partager des ressources

<!-- _class: t-70 -->

Pour enseignants : 
- [ELEA](https://dane.web.ac-grenoble.fr/elea) : Environnement numérique de travail mis en place par l'EN.
- [ELEDA](https://eleda.education/) : créer des exercices en mathématique.
- [RIDISI](https://ridisi.fr/) : créer des textes adaptés aux élèves à besoins particuliers.
- [AppsEducation](https://portail.apps.education.fr/signin) : ensemble d'outils proposés par le MEN

Pour élèves et enseignants :
- [DV-Fabrique](https://dv-fabrique.fr/) : création de documents multimédias interactifs.
- [Polymny](https://polymny.studio/) : création de vidéo.
- [Foxar](https://foxar.fr/) :  Création de contenus en réalité augmentée.

---
## 1.5 Les outils pour apprendre

<!-- _class: t-70 -->

- Maths : [Géogébra](https://www.geogebra.org/classic?lang=fr), [MathLive](https://mathlive.fr/), ...
- Lettres : [Projet Voltaire](https://tacit.univ-rennes2.fr/), [Lalilo](https://p2ia.lalilo.com/), 
- Lecture pour élèves BEP : [Corneille](https://corneille.io/), [Lilote](https://www.lilote.fr/lilote-ecole), [Lilémo](https://lilylearn.com/), ...
- Sciences et technologies : [Vittascience IA](https://vittascience.com/ia/), [AlphAI](https://learningrobots.ai/), [XpLive](https://xplive.fr/subjectChoice/)
- SVT : [Vertébrés](https://www.vertebres3d.fr/), [Neurolabo](https://www.practeex.com/)
- Langues : [Beegup](https://www.beegup.com/), [Plateforme eTwining](https://www.etwinning.fr/)
- Education musicale : [Music'S'Cool](https://dualo.com/education/), [PUCE MUSE](https://www.pucemuse.com/), [CoMo-Vox](https://vox.radiofrance.fr/ressource/como-vox-lapplication-de-direction-de-choeur)
- Gestes professionnels : [Mimbus réalité virtuelle](https://mimbus.com/)
- Géographie : [ÉduGéo](https://www.edugeo.fr/), [QGIS](https://qgis.org/)

---
## 1.5 Banques d'outils

- [Des ressources numériques innovantes et adaptées grâce au dispositif Édu-Up](https://eduscol.education.fr/2258/des-ressources-numeriques-innovantes-et-adaptees-grace-au-dispositif-edu) (Institutionnel)
- [Forge des communs numériques éducatifs](https://docs.forge.apps.education.fr/)
- [Annuaire logiciels éducatifs libres](https://wiki.faire-ecole.org/wiki/Annuaire_logiciels_%C3%A9ducatifs_libres#Lecture_de_fichiers_audios)

[Tableau collaboratif d'outils](https://cloud.univ-grenoble-alpes.fr/s/PGMQw7o2s2W5kot)

---
### 1.6 Les supports

<!-- _class: t-80 -->

- Les ordinateurs fixes ou portables
- Les tablettes numériques
- Les vidéoprojecteurs
- Les TBI (tableau blanc interactif)
- Les visualiseurs
- Les baladeurs MP3(audio) ou MP4(vidéo)
- Les dictaphones
- Les smartphones (secondaire)
- Les casques de réalité virtuelle (professionnel)
- Les liseuses, 
- Les robots compagnon, ou de téléprésence

---
# :two: Pourquoi utiliser une ressource numérique ?

Deux sous-questions :

* pour faire quoi ?
* qu'est-ce que cela change ?

---
## 2.1 Les fonctions pédagogiques possibles (Tricot, 2020)

<!-- _class: t-70 -->

[24 fonctions pédagogiques](https://cloud.univ-grenoble-alpes.fr/s/FKNkiGw6kke38tE), dont :

7. Poser des questions, demander de l’aide
9. Résoudre des problèmes et calculer
11. Jouer
12. Motiver
13. Coopérer
16. Faciliter l’accès à l’école et à l’apprentissage pour les élèves à besoins éducatifs particuliers
17. Créer un objet technique, une œuvre picturale ou sonore
18. Produire un texte, un document, seul ou à plusieurs
19. Programmer
20. Découvrir des concepts abstraits
21. Faire émerger des idées, développer sa créativité
22. Expérimenter


:scroll: Tricot, A. (2020). _[Quelles fonctions pédagogiques bénéficient des apports du numérique ?](https://www.cnesco.fr/wp-content/uploads/2024/07/210218_Cnesco_Tricot_Numerique_Fonctions_pedagogiques.pdf)_ . Paris : Cnesco-Cnam.

---
# :pencil2: Activité

- Dans la colonne **"POURQUOI"** du tableau, noter la fonction pédagogique des deux ressources séléctionnées
- Chercher une ressource, ou un outil ayant une fonction pédagogique différente des deux premières.
- Renseigner sur la troisième lignes les colonnes "QUOI" et "POURQUOI" pour cette nouvelle ressource.

---
## 2.2 Analyser les changements induits par l'usage des technologies : Deux modèles

---
### 2.2.1 Modèle SAMR (Puentedura, 2006 )

![h:500](https://primabord.eduscol.education.fr/local/cache-vignettes/L676xH599/samr-a89ff.jpg?1710153891)

([4](#34))

---
#### Les questions soulèvées par le modèle SAMR :

![](images/img-ressources/mermaid-diagram-2024-09-20-134904.png)

#### Avantage-inconvénient du modèle : 

* il permet de se poser des questions sur "l'intérêt" du numérique.
* il ne permet pas d'évaluer son efficacité.

---
# :pencil2: Activité

- pour vos trois ressources, suivant la fonction pédagogique visée et des outils utilisés, qu'est-ce que cela change pour l'apprenant ?
- _Que dit la recheche sur l'éfficacité de cet usage ? (cf. cours C.Charroud)_
- compléter la deuxième colonne.

---
### 2.2.2 Modèle passif-participatif (Romero, 2014)

Représentation des niveaux d’engagement créatif de l’apprenant dans des activités d’apprentissage médiatisées par le numérique.
![w:800](https://margaridaromero.blog/wp-content/uploads/2019/09/cinq_niveaux_dusages_des_technologies_de_m._romero_20151.png)
([5](#34))

- Ce modèle permet de questionner la posture de l'élève dans les activités.

--
# :pencil2: Activité

- pour vos trois ressources, quel est le niveau d'engagement des apprenants ?
- compléter la colonne "POURQUOI".

---
### 2.2.3 Exemples d'application des modèles

#### - Présenter de l'information

| **Niveau SAMR**| **Substitution** | **Augmentation** | **Modification** | 
| :------ | :------ | :------ | :------ |
| Exemple | Projeter un texte ou une image au vidéoprojecteur | Présenter des images dynamiques (vidéos), ou Ajouter des informations interactives sur des images ([exemple](https://ladigitale.dev/digiquiz/q/650063febaa5d)) | Questionner les élèves lors de la consultation d'une vidéo ([exemple](https://ladigitale.dev/digiquiz/q/6500495049035)) |
| Engagement de l'apprenant | Consommation passive|Consommation interactive | Consommation interactive | 

---
#### - Coopérer

| **Niveau SAMR**| **Substitution** | **Modification** | **Redéfinition** | 
| :------ | :------ | :------ | :------ |
| **Exemple** | Produire un document sur traitement de texte l'un à la suite de l'autre | Rédiger un document à plusieurs en même temps | Cocréer des contenus en échangeant (commentant) avec une autre classe à l'étranger |
| **Engagement de l'apprenant** | Création de contenu | Cocréation de contenu | Cocréation participative de connaissances | 

---
#### - Faciliter l’accès à l’école et à l’apprentissage pour les élèves à besoins éducatifs particuliers

| **Niveau SAMR**| **Substitution (compensation)** | **Amélioration** | **Amélioration** | 
| :------ | :------ | :------ | :------ |
| **Exemple** | Lire un texte par synthèse vocale | Prédire les mots au cours de la saisie en production d'écrits | Produire un texte à l'oral transformé à l'écrit (reconnaissance vocale) |
| **Engagement de l'apprenant** | Consommation passive | Création de contenu | Création de contenu |


---
# 3. Quand : A quel(s) moment(s) ? Pour quel(s) usage(s) ?

<!-- _class: t-70 -->

### Avant ? 
 * Présenter de l’information : des connaissances antérieures (Prérequis), ou des connaissances nouvelles (classe inversée)

### Pendant ?
 * Rechercher de l'information
 * Expérimenter
 * Créer un objet technique, une œuvre picturale ou sonore
 
### Après la classe ?
* Mémoriser, apprendre par cœur
* s'entrainer
* Évaluer, s’autoévaluer, suivre les progrès et les difficultés des élèves


---
# 3. Comment ?

### Quelle organisation pédagogique ?

* En classe entière :arrow_right: cours "magistral"
* En groupe :arrow_right: travaux de groupes
* En individuel :arrow_right: entrainement, évaluation, remédiation, différenciation, ou adaptation.

<!-- _class: t-60 -->

### Quel mode de diffusion ?

* Sur quel support ? tablette, ordinateur, baladeur mp3, ...
* Comment donner l'accès aux ressources ? 
    * projetées au tableau,
    * par fichier placé dans un dossier, sur une clé usb, ...
    * par un QrCode, 
    * par un lien dans les favoris, ou un raccourcis sur le bureau, 
    * dans une application sur tablette
    * dans un ENT (environnement numérique de travail)

Suivant le mode de diffusion, cela aura un impact sur les modalités d'accès (durée, lieu).

---
# 4. Prérequis : De quoi les élèves (et les enseignants) ont besoin ?

Au-delà du materiel (supports), des logiciels (outils), et des ressources, ils ont besoin : 
* **de compétences** : Carde de référence des compétences numériques 
  * pour les élèves [CRCN](https://eduscol.education.fr/721/evaluer-et-certifier-les-competences-numeriques#summary-item-1), 
  * pour les enseignants CRCNE (pas encore de cadrage officiel), TPACK
* **d'une culture numérique**

--- 
## 4.1 CRCN et niveaux d'acquisition

- Compétences à développer tout au long de la vie à partir de l'école primaire.
- [16 compétences; 5 niveaux d'acquisition jusqu'au lycée] (https://eduscol.education.fr/document/20392/download).
- Un outil d'évaluation : [PIX](https://pix.fr)

---
## 4.2 Compétences des enseignants : Modèle TPACK

![h:400](https://tpack.org/wp-content/uploads/2024/07/TPACK.png)

Être capable de mobiliser ses connaissances sur les contenus à enseigner, la pédagogie à mettre en place et les technologies numériques utilisables pour concevoir et mettre en œuvre son enseignement.


---
# :pencil2: Activité

- Remplir la colonne "Quant et Comment" du tableau
- Remplir la colonne "Prérequis" en indiquant :
  - Les compétences nécessaires aux élèves pour utiliser la ressource, l'outil, ou le support.
  - Les compétences potentiellement développées au cours de l'usage.

La colonne "Conditions d'utilisation" sera remplie lors d'un prochain TD.

<!---
# 6. Respect de la règlementation


---
# 6. Rappel sur le droit d'auteur

<!-- _class: t-80 --

### 6. Loi en vigueur en France : le code de la propriété intellectuelle, composé de :

- La propriété littéraire et artistique
- La propriété industrielle : brevets, dessins et modèles, marques.

### 6. Les grands principes de la propriété littéraire et artistique

* S'applique à toute création originale.
* Plusieurs droits protégés :
    * droits moraux : paternité, respect de l’intégrité de l’œuvre (Perpétuels, Inaliénables)
    * droits patrimoniaux : représentation, reproduction (sauf cas particuliers, 70 ans après la mort de l'auteur :arrow_right: Domaine public)

---
# 6. Droits voisins

* Pour les partenaires de la création :
  * Producteurs de phonogrammes ou vidéogramme
  * Interprètes
  * Entreprises de communication audiovisuelle

> Temporaire
> 50 ans après le 1er janvier de l'année civile suivant la première diffusion.

---
# 6. Titulaires du droit d'auteur (Articles L.113-1 à L.113-10)

* Personne qui créé l'œuvre et la divulgue
* Cas des œuvres créées à plusieurs :
	* tous les co-auteurs pour une œuvre **collaborative**
	* la personne qui dirige la création et qui la divulgue pour une œuvre **collective**
* Cas particulier d'une œuvre intégrant une autre œuvre. Elle est dite **composite**. Elle est la propriété de l'auteur qui l'a réalisée, sous réserve des droits de l'auteur de l'œuvre préexistante.

---
# 6. Œuvres produites par un agent de l'état

<!-- _class: t-80 --

**Article L111-1**
L'auteur d'une oeuvre de l'esprit jouit sur cette oeuvre, du seul fait de sa création, d'un droit de propriété incorporelle exclusif et opposable à tous.

**Article L121-7-1**
L'agent ne peut :
    - S'opposer à la modification de l'œuvre décidée dans l’intérêt du service par l'autorité investie du pouvoir hiérarchique, []
    - Exercer son droit de repentir et de retrait

**Article L131-3-1**
Dans la mesure **strictement nécessaire** à l'accomplissement d'une mission de service public, **le droit d'exploitation** d'une oeuvre créée par un agent de l'Etat dans l'exercice de ses fonctions ou d'après les instructions reçues est, dès la création, cédé de plein droit à l'Etat.

---
# 6. Droit d'auteur : Exceptions (Article L.122-5)

<!-- _class: t-80 --

Quelques exceptions pour un usage dans l'enseignement,

**3°** sous réserve que soient indiqués clairement le nom de l'auteur et la source 
**a)**  Les analyses et courtes citations justifiées par le caractère critique, polémique, pédagogique, scientifique ou d'information de l’œuvre à laquelle elles sont incorporées ;

**4°** La parodie, le pastiche et la caricature, compte tenu des lois du genre;

**11°** Les reproductions et représentations d'œuvres architecturales et de sculptures, placées en permanence sur la voie publique, réalisées par des personnes physiques, à l'exclusion de tout usage à caractère commercial.

**12°** La représentation ou la reproduction **d'extraits d'œuvres** à des fins exclusives d'illustration dans le cadre de l'enseignement et de la formation professionnelle, dans les conditions prévues à l'article L. 122-5-4 ;

---
# 6. Droit d'auteur : Exception pédagogique (article L. 122-5-4)

* La représentation ou la reproduction d'**extraits d’œuvres**, […]
* à des fins exclusives d'illustration, à l'exclusion de toute activité ludique ou récréative,
* à un public composé majoritairement d'élèves,
* compensée par une rémunération négociée.

> :scroll: Ressources du CFC pour les enseignants
> [Infographie](http://www.cfcopies.com/images/stories/pdf/Utilisateurs/Copies-pedagogiques-papier-et-numeriques/Etablissements-d-enseignement/Pave-ressources-commun/Plaquette-Comment-utiliser-oe-protegees.pdf) et [site web](www.cfcopies.com/site-pedagogique/index.html)
> 
<!-- Mise en application par des Accords signés entre le MEN et les sociétés de gestion de droits tels que Le Centre français d'exploitation du droit de copie (CFC), La société des Arts visuels associés (Ava), La Société des éditeurs et auteurs de musique (SEAM), La Société des producteurs de cinéma et de télévision (PROCIREP), la Société des auteurs, compositeurs et éditeurs de musique (SACEM) --
---
# 6. Conditions particulières (Accords)

Utilisation d'**extraits d'œuvres**\*, sauf pour :
  * œuvre courte (poème, article)
  * Projection en classe d'une œuvre de l'écrit
  * diffusion intégrale en classe d'une musique
  * diffusion intégrale en classe d'une vidéo provenant d'un service non payant.
  * œuvres des arts visuels : limité à 20 œuvres par travail, définition max 800x800px en 72 DPI <!-- (Avenant à l'accord) --

\* Les œuvres doivent avoir été acquises légalement

---
# 6. Précautions à prendre

### Obligations :

* Citer l'auteur, l'éditeur (obligatoire mais pas toujours suffisant).
* Ne pas diffuser les œuvres des élèves sans autorisation.
* Consulter les conditions d'utilisation (licences)
* Respecter les CGU des logiciels et services

### Précautions :
* Utiliser si possible des ressources sous licence "libre" et gratuite (Creative Commons ou GNU GPL).

---
# 6. :pencil2: Activité

Remplir la sixième colonne du tableau en indiquand le nom du titulaire des droits d'auteur.
* Indiquer si une licence d'utilisation est précisée, ou pas.
* Indiquer s'il y a des précautions à prendre pour l'utiliser.

---
# 8. Les textes de loi de référence :

<!-- _class: t-80 --

- Le code de la propriété intellectuelle : https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006069414
- Protocole d'accord sur l'utilisation des livres, des œuvres musicales éditées, des publications périodiques et des œuvres des arts visuels à des fins d'illustration des activités d'enseignement et de recherche : [Bulletin officiel n°35 du 29 septembre 2016](https://www.education.gouv.fr/bo/16/Hebdo35/MENE1600684X.htm)
- Avenant au protocole : [Bulletin officiel n° 7 du 13 février 2020](https://www.education.gouv.fr/bo/20/Hebdo6/MENE2000032X.htm?cid_bo=148987))
- Protocole d'Accord sur l'application de la loi DADVSI concernant la reproduction par reprographie : [Bulletin officiel n°13 du 1er avril 2021](https://www.education.gouv.fr/bo/21/Hebdo13/MENE2108987C.htm)
- Protocole d'Accord sur l'application de la loi DADVSI concernant l'utilisation des œuvres cinématographiques et audiovisuelles. [Bulletin officiel n° 5 du 4 février 2010](https://www.education.gouv.fr/bo/2010/05/menj0901120x.html)
- Accord sur l'interprétation vivante d'œuvres musicales, l'utilisation d'enregistrements sonores d'œuvres musicales et l'utilisation de vidéo-musiques à des fins d'illustration des activités d'enseignement et de recherche [Bulletin officiel n° 5 du 4 février 2010](https://www.education.gouv.fr/bo/2010/05/menj0901121x.html) -->

---
# Crédits

- (1) La pyramide des objets pédagogiques par [Laurent FLORY](https://www.enssib.fr/bibliotheque-numerique/documents/1234-les-caracteristiques-d-une-ressource-pedagogique-et-les-besoins-d-indexation-qui-en-resultent.pdf)
- (2) par [Lukas](https://pixabay.com/users/goumbik-3752482/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2717014) depuis [Pixabay](https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2717014)
- (3) par Hagustin, [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0), via Wikimedia Commons
- (4) Qu’est-ce que le modèle SAMR ? [Primàbord](https://primabord.eduscol.education.fr/local/cache-vignettes/L814xH234/samr_def-4cfa2.jpg?1710153891)
- (5)  [Romero, M., & Laferrière, T. (2015). Usages pédagogiques des TIC : de la consommation à la cocréation participative](https://margaridaromero.blog/models/693-2/)
