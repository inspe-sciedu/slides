---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# Intelligence artificielle et éducation
## DIU Encadrement éducatif
## Année univ. 2024-25
 
Philippe Dessus - Inspé, UGA
 


---
<!-- _backgroundColor: aqua -->
<!-- page_number: true -->
<!-- paginate: true -->
<!-- footer: IA et éducation – Ph. Dessus - DIU EE - Inspé-UGA - 2024-25 - CC:BY-NC-SA -->

# :zero: Introduction

---
# :zero: 1. Objectifs du TD

- **Argument** : Tout le long de votre carrière, de nouveaux systèmes d'IA vont être proposés au personnel éducatif (enseignant·es, CPE) et il est nécessaire de 
  - Réfléchir à ces applications et à leurs effets
  - Réfléchir aux problèmes actuels et à venir en proposant une vision critique de ces applications
  - Sinon, on peut aussi ne rien faire et attendre que ces systèmes deviennent obsolètes et non utilisés…

---
# :zero: 2. Informations

- Cette présentation peut être récupérée ici : https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/IA-Education-DIU-EE.pdf ou ici : **https://link.infini.fr/ee-ia-uga**
- Les références citées en fin de diapositives sont listées à la fin de la présentation

---
# :zero: 3. Plan du TD

- Partie théorique et informative
  1./ Usages éducatifs de l'IA
  2./ Aspects éthiques
  3./ Problèmes posés par l'IA en éducation
- 4./ Partie réflexive 

---
<!-- _backgroundColor: aqua -->
# :one: Usages éducatifs de l'IA


---
# :one: 1. Introduction

- On dit souvent que l'informatique est là pour aider l'éducation
  - Enseigner *avec* l'informatique signifie qu'on peut aussi enseigner *sans*, que **l'informatique n'est qu'un outil** qui s'*ajoute* à l'enseignement (pour l'aider, l'assister, l'améliorer)
  - On pourrait aussi dire que l'éducation **est là pour aider** l'apprentissage de l'usage de l'informatique, comme toute autre technologie (par exemple, le livre et les moyens de les comprendre, la lecture)

:books: Wegerif & Major (2024)

---
# :one: 2. IA omniprésente, même dans l'éducation

- Il est difficile de détecter l'usage de l'IA dans notre vie de tous les jours (dans nos voitures, non téléphones, nos maisons)… et dans l'éducation
- Usage à des niveaux toujours plus larges, dans les contextes suivants :

- **Machines comme médias**
  - Interactions humain·e-machine (invididuelles, e.g., tablette)
  - Interactions humain·es-machine (collectives) + interactions humains-humains (e.g., réseaux sociaux)
- **Machines comme acteurs ou agents**
  - Interactions humain·e machine (individuelles, e.g., robots conversationnels)
  - Interactions humain·e**s**-machine**s** (e.g., salles de classe “intelligentes”)

:books: Tsvetkova et al. (2024)

---
# :one: 3. Une définition de l'IA

- “Un système d’IA est un **système automatisé** qui, pour un ensemble
donné d’objectifs **définis par l’homme**, est en mesure **d’établir des
prévisions, de **formuler des recommandations, ou de prendre des
décisions** influant sur des environnements réels ou virtuels.
- Les systèmes d’IA interagissent avec nous et influent de façon directe ou
indirecte sur notre environnement. Ils **semblent** souvent fonctionner
de façon autonome et peuvent adapter leur comportement en fonction
du contexte. ”

:books: Unicef (2021, p. 16)

---
# :one: 4. Quelques usages de l'IA en éducation (1/2)

- **Adapter et personnaliser** : pour recommander un entraînement, une lecture…
- **Profiler et prédire** : pour détecter un désengagement, créer des classes…
- **Évaluer** : évaluer la compréhension, donner une note, des rétroactions…

:books: Bond et al. (2024)

---
# :one: 5. Quelques usages de l'IA en éducation (2/3)
## Les salles de classes "intelligentes"

- Salle de classe avec capteurs au niveau des élèves (caméras, bracelets) et de la salle (température, qualité de l'air, lumière) qui détecte *en temps réel* des informations sur les émotions et l'engagement des élèves
- Ces informations sont centralisées pour l'enseignant·e et des suggestions automatisées lui sont faites (changer d'activité si l'engagement baisse), des paramètres de la salle peuvent aussi être automatiquement changés

:books: Uniciti & Palau (2023)

---
# :one: 6. Quelques usages de l'IA en éducation (2/3)
## Les établissements intelligents (à venir !) (3/3)

- On entend de plus en plus parler d'“**environnements intelligents**“ : maisons, écoles, universités, villes…
- Vendre toujours plus de matériel, récolter toujours plus de données dont on ne sait toujours combien de temps elles seront stockées, et pour quoi faire
- **Utilisations multiples de l'IA** : enseignement à distance, gestion de la classe, santé et sécurité, gestion de l'établissement, communication parents-personnel éducatif…

:books: Kamruzzaman et al. (2023)

---
# :one: 7. Quelques avantages de l'IA

- **Aider à l'entraînement et évaluer le niveau** : des systèmes d'enseignement adaptatif comme [TACIT](https://tacit.univ-rennes2.fr) ou [Lalilo](https://lalilo.com/?language=fr) évaluent le niveau initial et proposent des exercices adaptés
- **Simuler l'apprentissage pour mieux le comprendre** : un enfant a porté une caméra pendant 1,5 ans, de ses 6 mois à ses 2 ans. L'IA entraînée sur ce corpus parvient automatiquement à associer des objets (Vong et al. 2024)
- **Tuteurer les tuteurs** : Un système d'aide au tutorat (maths, Cycles 3-4), suggérant aux tuteurs des réponses aux élèves, leur permet de mieux comprendre le contenu que s'ils étaient guidés par des tuteurs  (Wang et al. 2024)
- **Analyser des données massives** (1,3 M d'observations de 27 jeux de données) et montrer que la trajectoire d'apprentissage est similaire : début proche de 65 % de réussite, nécessité d'avoir 7 épisodes de pratique pour atteindre 80 % (Koedinger et al. 2023) 

---
<!-- _backgroundColor: aqua -->
# :two: Aspects éthiques

---
# :two: 1. L'éthique est souvent absente du questionnement

- Une revue systématique des travaux en IA appliquée à l'éducation montre que **seulement 2 articles sur les 146** analysés évoquent des aspects éthiques ou des risques 
  
:books: Zawacki-Richter et al. (2019) 

---
# :two: 2. Les frontières de l'observable

- Les systèmes d'IA et leurs capteurs repoussent les frontières de ce qui est observable et traitable comme événement éducatif
  - *limites naturelles* : l'émotion, le regard, certains paramètres physiologiques sont maintenant traqués
  - *limites sociales* : confidentialité
  - *limites spatiales et temporelles* : ce qu'on fait ici et maintenant est enregistré, **parfois sans même qu'on s'en aperçoive. On ne sait si on est observé ou pas**

---
# :two: 3. Collecter des données pour l'IA rend les utilisateur·es vulnérables

- **Collecte d'informations** : surveillance, incitation à divulguer de l'information
- **Traitement d'informations** : recoupement d'informations, identification, vol de données, comment sont stockées les données ?   
- **Dissémination d'informations** : rupture de confidentialité, exposition, chantage, détournement d'identité
- **Invasion** : impact des activités des autres, perte d'autonomie légale

:books: Prinsloo & Slade (2016)

---
# :two: 3. Principes éthiques fondamentaux

- Respect des personnes (autonomes et avoir leur consentement)
- Tendre à faire le bien (peser le bénéfice/risque)
- Justice (les risques et bénéfices sont distribués convenablement, il y a compensation des problèmes ou troubles)
- Respect de la loi et transparence

:books: Salganik (2018)

---
# :two: 4. Un début de régulation : Applications "à risque" de l'IA

- Utiliser l'IA pour réaliser des décisions automatiques à fort enjeu (i.e., obtenir un crédit, être recruté·e, passer un examen) est prohibée par le RGPD (art. 22) et la plus récente Loi européenne sur l'IA (*AI Act*)



:books: [Loi européenne sur l'IA](https://artificialintelligenceact.eu/fr/)

---
# :two: 5. IA à haut risque dans  l'éducation

<!-- _class: t-80 -->

1. les systèmes d'IA destinés à être utilisés pour **déterminer l'accès, l'admission ou l'affectation** de personnes physiques à des établissements d'enseignement […]
2. les systèmes d'IA destinés à être utilisés pour **évaluer les résultats de l'apprentissage**, y compris lorsque ces résultats sont utilisés pour orienter le processus d'apprentissage des personnes physiques dans les établissements d'enseignement […]
3. les systèmes d'IA destinés à être utilisés pour évaluer le niveau d'éducation approprié qu'une personne recevra ou pourra atteindre, dans le cadre ou au sein des établissements d'enseignement […]
4. les systèmes d'IA destinés à être utilisés pour **surveiller et détecter les comportements interdits** des étudiants lors des tests dans le cadre ou au sein des établissements d'enseignement […]

:books: [Annexe III de la Loi européenne sur l'IA](https://artificialintelligenceact.eu/fr/annex/3/)


---
# :two: 6. Régulation et usage quotidien

- Si les cadres de loi sont utiles (l'Europe est en avance sur ce point), et destinés aux entreprises concevant ces systèmes, le personnel éducatif doit ou devrait :
  - connaître les grandes lignes de ces cadres
  - garder à l'esprit que des systèmes destinés aux élèves peuvent aussi les concerner
  - utiliser son esprit critique pour évaluer le bien-fondé de systèmes d'IA “émergents”, donc pas forcément encore prévus par les cadres de loi
  
---
<!-- _backgroundColor: aqua -->

# :three: Quelques problèmes posés par l'IA en éducation

---
# :three: 1. Le capitalisme de surveillance

- Les entreprises les plus performantes en IA sont **monopolistiques** et **collectent et utilisent les données personnelles** des utilisateur·es pour prédire leur comportement 
- :warning: Chercher à qui profite les données

:books: Zuboff (2020)

---
# :three: 2. Le technosolutionnisme

- Tout problème social a une solution technologique
- Tout outil technologique pourrait servir à un problème social (chercher à l'identifier)
- :warning: “**Si tout ce que vous avez est un marteau, tout ressemble à un clou.**” (Maslow, 1966)

:books: Morozov (2014)

---
# :three: 3. Le manque de transparence

- Les algorithmes de systèmes d'IA échappent la plupart du temps à la compréhension de leurs utilisateur·es (et parfois même de leurs programmeur·es)
- Cela a pour effet de ne pouvoir corriger les possibles biais
- :warning: Comment accorder une confiance suffisante à un tel système ?

:books: Longo et al. (2024)

--- 
# :three: 4. Le glissement de fonctions (*function creep*)

- L'usage d'un système technique au-delà de celui pour lequel il a été conçu initialement et intentionnellement, amenant des inconvénients imprévus et souvent délétères, notamment concernant la protection de la vie privée

:books: Koops (2021) 

---
# :three: 5. Alors, que peut-on faire ?

- Se former aux systèmes
- Mieux connaître les pratiques réelles des élèves et du personnel éducatif
- Ne pas anthropomorphiser, ni “magifier” (donner des attributs magiques à l'IA), ni enjoliver les processus et résultats de l'IA

---
<!-- _backgroundColor: aqua -->
# :four: Partie réflexive

---
# :four: 1. Tâche

1. Par bi- ou trinôme, choisissez un des thèmes dans les diapositives qui suivent (vous pouvez également réfléchir à votre propre thème, à valider par le formateur)
2. Élaborez un scénario (cas d'usage concret) de l'outil dans votre établissement en essayant d'imaginer ses conséquences probables, positives et négatives (voir fiche ci-après)
3. Il est conseillé d'utiliser internet et d'utiliser un robot conversationnel pour une première liste d'idées, à peaufiner ensuite

Légende : :computer: : n'utilisant pas nécessairement d'IA.

---
# :four: 2. Liste de questions de réflexion (1/2)

<!-- _class: t-60 -->

### Surveillance

1. *ProNote* pour surveiller les élèves et le personnel éducatif
2. Téléphones portables pour surveiller les déplacements :computer:
3. (Més)usages des caméras de vidéosurveillance dans les établissements
4. Glissements de fonctions de la reconnaissance biométrique (gérer les absences ou les repas en restaurant scolaire)
5. Jusqu'où aller dans la surveillance de processus éducatifs (de la place dans la classe aux ondes cérébrales) ?


### Adaptabilité

6. Exerciseurs utilisant l'IA qui ne dépendent plus des enseignant·es (façon liste de lecture)
7. IA et prise en compte des élèves à besoins particuliers ou racisés
  
### Frontières de l'observable

8. Effets de recueillir et analyser des comportements qui ne peuvent être toujours consciemment dirigés par les personnes observées (direction du regard, émotions…)

---
# :four: 2. Liste de questions de réflexion (2/2)

<!-- _class: t-80 -->

### Communication

9. Effets des groupes de messages privés parentaux :computer:
10. Effets de la diffusion de fausses images et vidéos dans un établissement

### Intégrité académique

11.  Détection d'inconduites académiques (plagiat…) et leur incidence sur le travail du personnel éducatif
12.  Effets de l'extension de l'usage de l'IA générative (par les élèves, le personnel éducatif)

### Mise à l'échelle

13. Vie privée des élèves et du personnel dans un “établissement intelligent”
14. Ébauche de règlement intérieur sur l'usage d'IA générative

### Droits des personnes

15. Droits fondamentaux des personnes à opposer au déploiement des systèmes d'IA, ou pour les restreindre

---
# 3. Fiche descriptive du scénario

- Brève description d'un scénario (1 diapositive si possible illustrée)
1. identifiant un **public-cible** (enseignant·es, élèves, parent·es, personnel administratif et éducatif…) 
2. à qui il arrive un **événement** (en lien direct ou non avec l'IA)
3. Décrire ensuite quelques **effets de l'usage** (positifs ou négatifs) d'un ou plusieurs système·s d'IA (dont le fonctionnement sera rapidement décrit) sur l'évolution de cet événement…
4. Clore la description par **une phrase en forme de maxime**, de conseil, ou prescription sur l'usage de l'IA en éducation
- Proposer 1 référence pour en savoir plus
- Copier le ou les prompt·s utilisé·s avec le robot conversationnel
- Envoyer la description au formateur par courriel

---
# 3. Usage de robots conversationnels

- Il est conseillé d'utiliser le système [AIChat](https://duckduckgo.com/?q=DuckDuckGo+AI+Chat&ia=chat&duckai=1) proposé par *DuckDuckGo*, sans inscription et qui ne se sert pas des inputs pour entraîner les systèmes

---
# Références (1/2)

<!-- _class: t-70 -->

- Kamruzzaman, M. M. et al. (2023). AI- and IoT-Assisted Sustainable Education Systems during Pandemics, such as COVID-19, for Smart Cities. *Sustainability, 15*(10). https://doi.org/10.3390/su15108354 
- Koedinger, K. R. et al. (2023). An astonishing regularity in student learning rate. *PNAS, 120*(13), e2221311120. https://doi.org/10.1073/pnas.2221311120 
- Koops, B.-J. (2021). The concept of function creep. *Law, Innovation and Technology, 13*(1), 29-56. https://doi.org/10.1080/17579961.2021.1898299 
- Longo, L. et al. (2024). Explainable Artificial Intelligence (XAI) 2.0: A manifesto of open challenges and interdisciplinary research directions. *Information Fusion, 106*. https://doi.org/10.1016/j.inffus.2024.102301 
- Maslow, A. (1966). *The psychology of science*. Harper & Row.
- Morozov, E. (2014). *Pour tout résoudre cliquez ici*. Fyp.
- Prinsloo, P., & Slade, S. (2016). Student Vulnerability, Agency and Learning Analytics: An Exploration. *Journal of Learning Analytics, 3*(1). https://doi.org/10.18608/jla.2016.31.10  
- Salganik, M. J. (2018). *Bit by bit. Social research in the digital age*. Princeton University Press.
- Unicef (2021). [Orientations stratégiques sur l'IA destinée aux enfants](https://www.unicef.org/innocenti/media/1346/file/UNICEF-Global-Insight-policy-guidance-AI-children-2.0-2021_FR.pdf). Unicef.

---
# Références (2/2)

<!-- _class: t-70 -->

- Vong, W. K. et al. (2024). Grounded language acquisition through the eyes and ears of a single child. *Science*, 383, 504–511. https://doi.org/10.1126/science.adi1374 
- Wang, R. E. et al. (2024). Tutor CoPilot: A Human-AI Approach for Scaling Real-Time Expertise. *ArXiv preprint*. https://arxiv.org/abs/2410.03017 
- Wegerif, R., & Major, L. (2024). *The theory of educational technology*. Routledge. 
- Zawacki-Richter, O. et al. (2019). Systematic review of research on artificial intelligence applications in higher education – where are the educators? *International Journal of Educational Technology in Higher Education, 16*(1). https://doi.org/10.1186/s41239-019-0171-0 
- Zuboff, S. (2020). *L’âge du capitalisme de surveillance*. Zulma. 
