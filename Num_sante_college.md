---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# 
# Écrans et santé
## Collège Aimé Césaire
### Lundi 13 mai 2024
Christophe Charroud - UGA

<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2024 - CC:BY-NC-SA -->

 
---

# 
# :zero: Préambule 

 
### À votre avis, utiliser des écrans c'est :
* Dangereux pour votre santé et négatif pour vos comportements.
* Ça n'a pas d'effet sur votre santé et ça ne crée pas de problème de comportement.

---

# Vous êtes-vous appuyés sur des arguments scientifiques, ou sur ce que vous entendez tous les jours ?

#
### Pourquoi une telle question?


---
# 
## « Il y a une rupture entre connaissance commune et connaissance scientifique »

> Bachelard : le matérialisme rationnel, 1953

### Il y a souvent une grande différence entre ce que dit la science et ce qu'on entend dans les médias, dans la rue, lors de discussions ou sur les réseaux sociaux...

---
## Au programme


1 - Écrans et problèmes physiques
2 - Écrans et sommeil
3 - Écrans et développement du cerveau
4 - Écrans et problèmes de comportement
5 - Écrans et addictions



---
# :one: Écrans et problèmes physiques
## Des effets identifiés par la recherche 

---
## 1.1 Sédentarité
Si vous ne bougez pas assez, vous risquez de graves problèmes de santé.

![w:400](images/img-sante/obesite.webp)

**Sédentarité = augmentation du risque d'obésité**

---
## 1.1 Sédentarité
Si vous ne bougez pas assez, vous risquez de graves problèmes de santé.

![w:400](images/img-sante/cardio.jpeg)

**Sédentarité = augmentation du risque d'accident cardiaque**

---
## 1.1 Sédentarité
Si vous ne bougez pas assez, vous risquez de graves problèmes de santé.

![w:400](images/img-sante/avc.jpeg)

**Sédentarité = augmentation du risque d'Accident Vasculaire Cérébral**

---
## 1.2 Problèmes ophtalmiques

![w:400](images/img-sante/Myopie.jpeg)
* Myopie précoce 
* Préservez votre vue!
**à surveiller** (Aptel & Charroud, 2017)


---
## 1.3 Pensez à votre posture

![w:500](images/img-sante/textneck.jpeg)


---
## 1.3 Pensez à votre posture

![w:600](images/img-sante/textneckpoids.png)
*   Text Neck Syndrome (David & al., 2021) 

---
## 1.3 Pensez à votre posture

![w:400](images/img-sante/carpien.jpeg)
Troubles Muscolo-Squeltiques (Charroud, 2023)
* Pensez à vos cervicales, à vos mains et au reste de votre corps !

---
## 1.4 Comment éviter ces problèmes ?

Expérimentation au Danemark (Pedersen, 2022)

* On demande à des ados de poser leur téléphone une heure par jour.
* Les chercheurs constatent que "l'heure sans téléphone" est remplacée par de l'activité physique :arrow_right: amélioration de leur état de santé


---
## 1.5 Bilan écrans et problèmes physiques

**L'écran en-lui même ne pose pas de problème.**
**L'usage de l'écran** peut entrainer :
* de la sédentarité (obésité, maladies cardiovsaculaires...);
* des problèmes de vue;
* des problème articulaires.
### 
### Si vous bougez suffisament dans la journée (30 minutes de sport par jour) ça réduit les risques, alors bougez !

---

# :two: Écrans et sommeil


Sommeil et écrans un vrai problème pour la santé (Bioulac, Baillieul, Charroud, 2023).


---
## 2.1 Régulation du sommeil
#### Comment ça marche ? 
![w:600](images/img-sante/circamela.jpg)

#### :arrow_right: « Je dors car il est l'heure de dormir »

---
## 2.2 Pourquoi dormons-nous ?
* Récupérer
* Croissance
* Régénération des tissus et ces cellules (cicatrisation par ex.) 
* Maintenir un métabolisme sain (manger ce dont votre corps à besoin)
* Résistance aux infections
* Régulation des émotions (moins nerveux, moins irritable)
* Maturation cérébrale/ Développement du cerveau
* Apprendre / consolidation mémoire

---
## 2.3 Que ce passe-t-il si on ne dort pas assez (souvent à cause des écrans)?
* Mal de tête
* Bouche sèche 
* Trouble de la concentration
* Irritabilité
* Agitation chez l’ado
* **Altération du développement du cerveau**

---
## Bilan écrans et sommeil
**L'écran en lui-même ne pose pas de problème**
**L'usage de l'écran** le soir va retarder l'endormissement et altérer le sommeil donc altérer la santé.
### 
### Il faut poser les écrans 2 heures avant d'aller se coucher pour avoir un bon sommeil

---
# :three: Écrans et développement du cerveau

**Avant 3 ans**
*   :warning: Écrans passifs = manque de communication :arrow_right: mauvais développement du cerveau entrainant des difficultés de langage (Hutton & al., 2020).

**Après 3 ans**
* :smiley: Des effets positifs reconnus (Fischer, 2023) :arrow_right: Etude Elfe.
* :rage: Manque de sommeil = altréation du développement du cerveau. (cause indirecte)

---
## 3.1 Écrans le soir (manque de sommeil) et développement du cerveau
<!-- _class: t-80 -->

Mesures en imagerie cérébrale 
![w:300](images/img-sante/urrilaconclusions.jpg)

En cas de manque de sommeil on constate, une diminution de volume de matière grise dans le cortex frontal et le cortex cingulaire (ce qui sert à prendre des décisions) :arrow_right: qui implique de mauvais résultats scolaires.
*(Urrila & al., 2017)*

---
## 3.2 Écrans et développement du cerveau
### Des liens non avérés 

*   TDA/H : pas une cause et parfois des effets positifs (Bioulac, Charroud, Pellencq, 2022).
*   TSA : un renforcement potentiel **mais pas une cause démontrée** (Lin & al., 2022)(Ophir & al. ,2023).

 
---
## 3.3 Bilan écrans et développement du cerveau
La recherche montre que les écrans ne semblent pas la cause de troubles comme le TDA/H et le TSA.
L'usage d'écrans "passif" pour les enfant de moins de 3 ans est très négatif pour le développement du cerveau.
Le manque de sommeil causé par l'usage d'écran avant d'aller se coucher est très négatif pour le développement du cerveau.
### 
### Avant 3 ans : Pas d'écran - Après 3 ans : Pas d'écran 2 heures avant d'aller se coucher


---
# :four: Écrans et problèmes de comportement scolaires

Des effets psychologiques et sociaux (Desmurget, 2019) chez les "gros" utilisateurs d'écrans
     ... mais ces effets semblent simplement amplifiés par le numérique (Sauce, Liebherr, Judd & Klingberg, 2022).


---
## 4.1 Liens utilisation des écrans et réussite scolaire

Une méta-analyse de plus de 5 500 études (480 000  participants de 4-18 ans), d'Adelantado-Renau et al. 2019) montre :

* qu'il n'y a pas de lien entre temps d'écran (qq soit l'écran) et la réussite scolaire;
* que l'usage de la TV et des jeux vidéo impactent négativement la réussite scolaire.

**On en peut pas dire que les écrans ont un effet négatif sur la réussite scolaire, c'est certains usages des écrans qui ont un effet négatif.**



---
## 4.2 Liens utilisation des écrans et bien-être

Une étude à large échelle (+11 000) sur des adolescents et avec rapports quotidiens du temps d'utilisation montre un effet négatif mais très faible (Orben & Przybylski 2019).

Les chercheurs ont calculé qu'il faudrait un temps d'usage (extrapolé) de 63 h par jour pour que les ados perçoivent une daisse de leur bien être et comme il n'y a que 24h dans une journée...les ados ne resentent pas de mal-être en utilisant beaucoup les écrans.

---
## 4.3 Cyberharcèlement (Blaya 2018)

- Violence hors ligne :left_right_arrow: violence en ligne
- *Anonymat amplifié* : limite les niveaux d’empathie des agresseurs
- *Dissémination démultipliée*, diffusion instantanée, pas de répit pour les victimes ; pas de maîtrise sur la diffusion ; nombre de témoins potentiels illimités
- *Information persistante* : Contrairement au harcèlement, pas de nécessité de répétition de l'acte pour parler de cyberharcèlement

**L’écran n'est pas la cause du hacèlement, il a un effet loupe, il amplifie les faits.**

---
## 4.4 Écrans et apprentissages
Pour les apprentissages les écrans ont des effets positifs pour :
* Communiquer
* Accéder à l'information
* S'entraîner sur des tâches ciblées

(J-Pal, 2019)


---
## 4.5 Bilan écrans et problèmes de comportement scolaires
Les écrans n'ont pas d'effet sur le bien-être ou le mal être.
Certains types d'usages (TV=passif, jeux vidéo) ont des effets négatifs sur la scolarité.
L'usage des écrans peut amplifier les comportements liés au harcèlement.
L'usage des écrans peut être positif pour les apprentissages.
### 
### Certains usages d'écrans sont négatifs, certains sont neutres, d'autres sont positifs

---
# :five: Écrans et addictions
#
### Etes-vous addicts aux écrans ?

:arrow_right: Test issu du DSM-5 et du programme SANPSY

Test silencieux, ne communiquez pas vos resultats.
Chaque fois que vous repondrez oui, comptez un point.


---
## 5.1 Test 1/3

•	**Préoccupation** : Passez-vous beaucoup de temps à penser aux écrans, y compris quand vous n'en utilisez pas, ou à prévoir quand vous pourrez en utiliser à nouveau ?

•	**Sevrage** : Lorsque vous tentez d'utiliser moins d'écrans ou de ne plus en utiliser, ou lorsque vous n'êtes pas en mesure d'utiliser d'écran, vous sentez-vous agité, irritable, d'humeur changeante, anxieux ou triste ?

•	**Tolérance** : Ressentez-vous le besoin d'utiliser des écrans plus longtemps, d'utiliser des écrans plus excitants, ou d'utiliser du matériel informatique plus puissant, pour atteindre le même état d'excitation qu'auparavant ?

---
## 5.2 Test 2/3

•	**Perte de contrôle** : Avez-vous l'impression que vous devriez utiliser moins d'écrans, mais que vous n'arrivez pas à réduire votre temps d'écran ?

•	**Perte d'intérêt** : Avez-vous perdu l'intérêt ou réduit votre participation à d'autres activités (temps pour vos loisirs, vos amis) à cause des écrans ?

•	**Poursuite malgré des problèmes** : Avez-vous continué à utiliser des écrans, tout en sachant que cela entrainait chez vous des problèmes tels que ne pas dormir assez, être en retard au collège, se disputer, négliger des choses importantes à faire... ?

---
## 5.3 Test 3/3

•	**Mentir, dissimuler** : Vous arrive-t-il de cacher aux autres, votre famille, vos amis, à quel point vous utilisez des écrans, ou de leur mentir à propos de vos habitudes d'écrans ?

•	**Soulager une humeur négative** : Avez-vous utilisé des écrans pour échapper à des problèmes personnels, ou pour soulager une humeur indésirable (sentiments de culpabilité, d'anxiété, de dépression) ?

•	**Risquer ou perdre des relations ou des opportunités** : Avez-vous mis en danger ou perdu une relation amicale ou affective importante, des possibilités d'étude à cause des écrans ?

---
## 5.4 Bilan du test

Si vous avez un **score égal à 5 points ou plus** alors votre comportement sera médicalement qualifié « d'addiction aux écrans ».

L'addiction est une maladie qui peut (et qui doit) être prise en charge.

---
## 5.5 L'addiction aux écrans est-elle courante ?

* Les études actuelles montrent que **l'addiction au sens médical est relativement rare** parmi les adolescents et les adulte, elle concernerait  1,7% de la population.

* Mais 44,7% de la population présenterait au moins l'un des neuf critères recherchés :arrow_right: **usagers avec problèmes, mais sans addiction.** :arrow_right: il faut être vigilant.

(Alexandre, Auriacombe, Boudard, 2022)


---
# Bilan général
* Il est difficile de trancher radicalement quant à la nécessité ou non d’éviter d’exposer les enfant/ados aux écrans, (Borst, 2019)
* Ce qui est important :
    * Le **temps d'exposition** au détriment du reste (bouger, échanger, se sociabiliser)
    * Relation négative entre le temps d’exposition et le développement, mais cette relation n’est pas vraie pour tous les domaines de la cognition. (Kamga Fogno & al., 2023)(Milcent & al., 2023).
    * Des effets positifs reconnus (Fischer, 2023) :arrow_right: Etude Elfe.

**Les écrans ne sont pas directement responsables, ce sont les usages que l'on en fait qui sont problématiques** 


---
# Conclusions et recommandations

* Essayez de ne pas utiliser d'écran avant d'aller dormir :arrow_right: meilleur sommeil :arrow_right: meilleurs comportements :arrow_right: meilleure santé.

* Essayez de diminuer votre temps d'écran en journée afin de vous libérer du temps pour :
    * Bouger
    * Echanger, discuter, avoir des relations sociales
    * et vous ennuyer... c'est très bon pour l'imagination.


---

# Merci pour votre attention

