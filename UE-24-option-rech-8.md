---
marp: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# UE 24 Option de recherche “Interactions enseignant-élèves“
## Philippe Dessus, Inspé, Univ. Grenoble Alpes

## Séance 8 — Analyser et représenter des données
##### Année universitaire 2023-24

![w:350](images/logo-espe-uga.png)

---
<!-- footer: Ph. Dessus • M1 PE • TD Option de recherche “Interactions enseignant-élèves • Inspé-UGA 2023-24 • CC:BY-NC-SA-->
<!-- paginate: true -->

# :eight: 0. But de la séance

- Donner quelques méthodes pour analyser les données issues de l'observation de REE (et plus largement de **documents textuels**)
- Sans s'occuper des aspects statistiques (traités dans une autre UE)

---
# :eight: 0. Plan de la séance

1. Analyse sociométrique
2. Transcription des conversations ou des entretiens
3. Analyse textuelle automatique
4. Analyse qualitative ("manuelle") de discours
5. Analyse automatique de discours
6. Analyse automatique de construits psychologiques

---
# :eight: 1. Analyse sociométrique

<!-- _class: t-80 -->

(sera réintégré à la séance sur les méthodes d'observation)

- Un questionnaire permet de mettre au jour le réseau des affinités des élèves d'une classe
- Utile pour constituer des groupes de travail par affinité, pour mieux comprendre les relations entre élèves
- Permet différentes visualisations (en réseau), différents calculs (de cohésion d'une classe)
- :warning: rend compte des relations à un moment donné, pas des dynamiques
- :warning: orienter les questions sur le travail plutôt que sur les préférences amicales (qui sont des questions trop personnelles) : “*Avec qui préfères-tu travailler la matière M ?*“)

:books: [Réseaux d'affinité en classe](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/sociometrie.html)

---
# :eight: 2. Transcrire des entretiens ou des conversations

- Activité coûteuse en temps, sujette à erreurs
- Il existe des applications de parole-vers-texte, mais attention à ne pas diffuser des conversations contenant des données personnelles à n'importe quelle compagnie...
- [TADDAM, UGA](https://www.univ-grenoble-alpes.fr/actualites/agenda/agenda-recherche/taddam-retranscrire-efficacement-des-entretiens-1253809.kjsp) est un service interne proposant ce type de transcriptions. Contacter la personne qui dirige votre mémoire pour un accès
- :warning: non approprié pour transcrire des conversations de classe avec de multiples locuteurs

:books: [Présentation de TADDAM](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-rech/23-outils-gricad-larac.pdf) ; voir aussi [NoScribe](https://github.com/kaixxx/noScribe/) et [textra](https://github.com/freedmand/textra)


---
# :eight: 3. Analyse textuelle de documents (1/2)

- Des outils informatiques permettent d'avoir une vue globale et rapide de certaines caractéristiques de documents textuels (traitement automatique des langues)
- Buts : 
  - connaître les statistiques de l’usage des mots (ou la capacité à les orthographier), par niveau 
  - avoir un aperçu global du contenu d’un texte
  - évaluer la lisibilité et complexité des textes donnés à lire aux élèves
  - évaluer la complexité des textes écrits par les élèves 

---
# :eight: 3. Analyse textuelle de documents (2/2)

- Les documents peuvent être 
  - des textes donnés à lire et comprendre
  - des productions d'élèves
  - des discours oraux (conversations, débats)
  - des entretiens pour une recherche
  - etc.

---
# :eight: 3. Types de systèmes d'analyse textuelle

- Usage et sens des mots ([Manulex](http://www.manulex.org/fr/home.html))
- Lisibilité des textes ([Scolarius](https://www.scolarius.com))
- Caractéristiques lexicométriques d'un document (fréquences, types de mots, etc.) ([AnaText](http://phraseotext.univ-grenoble-alpes.fr/anaText/))


:books: Dessus & Rinck, [Outils d'analyse textométrique pour l'enseignement](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/textometrie-educ.html)

---
# :eight: 4. Analyse qualitative (thématique) de discours

<!-- _class: t-60 -->

- Une fois que le discours (entretien, débat en classe, document) est sous forme textuelle, une analyse qualitative permet d'en dégager les principaux thèmes
- Il y a 6 principales phases :
  1. **Se familiariser avec les données** : relire les entretiens
  2. **Générer les codes initiaux** : attribuer aux passages des “codes” expliquant ce qui se dit (de manière plus ou moins explicite)
  3. **Chercher des thèmes** : regrouper les codes dans des thèmes plus larges et récupérer toutes les données reliées à un thème
  4. **Relire les thèmes** : Vérifier la cohérence des liens Codes-Thèmes tout au long des données. Créer une carte des thèmes
  5. **Définir et nommer les thèmes** : Définir clairement chaque thème (le contenu à quoi il réfère), et lui donner un nom. Faire un catalogue de thèmes/codes
  6. **Produire le rapport** : Choisir les extraits à présenter et commenter, en lien avec la question de recherche. Produire des statistiques sur les thèmes
   
:books: Braun & Clarke (2006)

---
# :eight: 4. Analyse thématique : un exemple 

<!-- _class: t-80 -->

- Entretien non directif d'enseignant.es de CP sur leurs pratiques (leur rôle, celui des élèves), leurs relations avec eux (en lien avec les domaines du CLASS)

| Thèmes | Codes |
| --- | --- |
| Climat + | Climat serein, de confiance ; Droit à l'erreur ; Acceptation de tous et respect…
| Climat – | Climat répressif ; Stigmatisation par les élèves ou l'enseignante ; Pas d'encadrement |
| Sensibilité de l'enseignante | Être à l'écoute ; Ressentir les émotions…|

:books: Haïat (2020 pp. 99-100)


---
# :eight: 4. Logiciels d'analyse qualitative

- De nombreux logiciels (*qualitative data analysis*), souvent chers et complexes (nVivo, MaxQDA, etc.)
- [Taguette](https://www.taguette.org) est un logiciel multi-plates-formes, gratuit, et permet de réaliser simplement une analyse thématique de documents — bien que moins perfectionné que les précédents 
- **Démo rapide de Taguette**

---
# :eight: 5. Analyse automatique de discours

- [ReaderBench](http://readerbench.com) est un outil d'analyse automatique du discours, multilingue et en ligne (gratuit, sur inscription)
- L'outil CSCL de *ReaderBench* permet d'analyser et visualiser, à partir d'une transcription de discours à plusieurs locuteurs :
  - les contributions de chaque locuteur 
  - le “poids” des contributions dans la conversation, soit en terme d'importance, soit en terme d'influence sur les autres locuteurs
- :warning: Suivre précisément le mode d'emploi ci-dessous pour formater le fichier à analyser (nécessite des connaissances minimales sur les tableurs et les formats de fichier)

:books: Dessus & Mandran [Utiliser ReaderBench pour analyser automatiquement des discussions](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/tuto-rb-conpa.html)

---

# :eight: 6. ChatGPT et analyse automatique de construits psychologiques

- Des travaux récents montrent que les robots conversationnels peuvent être utilisés pour réaliser des analyses automatiques de textes, notamment pour mesurer des construits psychologiques

:books: Rathje et al. (2024)

---
# :eight: 6. Exemples de prompts à utiliser

- “Le sentiment de ce texte est-il positif, neutre, négatif ?“ ; 
- “Quel est le nombre relié aux émotions suivantes qui représente le mieux l'état mental de la personne qui l'a écrit ? (1 : émotion *E1*, 2 : émotion *E2*, etc.)”
- Sur une échelle de 1 à 7 (1 étant “très négative” et 7 étant “très positive”), comment se situe la phrase suivante ?
- Sur une échelle de 1 à 7 (1 étant “pas d'émotion” et 7 étant “Très grande présence de l'émotion), combien l'émotion *E* est présente dans la phrase suivante ?
- :warning: **Ne communiquez aucune donnée personnelle de vos élèves**

:books: Rathje et al. (2024)

---
# :eight: 7. Tâche

- Choisir un outil approprié au matériel que vous avez recueilli (ou comptez recueillir)
- Copiez un extrait de ce matériel et utilisez l'outil pour le traiter
- Analysez les résultats présentés et discutez de leur intérêt pour votre recherche

---
# :eight: Pour en savoir plus

- [Iramuteq](http://iramuteq.org) : logiciel d'analyse thématique automatique (sous *R*)

---
# :eight: Références

- Braun, V., & Clarke, V. (2006). Using thematic analysis in psychology. *Qualitative Research in Psychology, 3*(2), 77-101. https://doi.org/10.1191/1478088706qp063oa 
- Haïat, S. (2020). *Étude de la relation enseignant-élèves en 1re année du primaire dans le cadre d’une comparaison des systèmes éducatifs québécois et français UQÀM*. Montréal. Thèse de master. [[PDF](https://archipel.uqam.ca/15578/)]
- Rathje, S., Mirea, D.-M., Sucholutsky, I., Marjieh, R., Robertson, C. E., & Bavel, J. J. V. (2024). GPT is an effective tool for multilingual psychological text analysis. *PsyArXiv Preprint*. https://osf.io/preprints/psyarxiv/sekf5 