---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# Culture numérique et Apprentissage

## UE - Num 801 - TD "Choix de situation" 
Equipe formateurs numérique - UGA - 2025


<!-- page_number: true -->
<!-- footer: 2025 - CC:BY-NC-SA -->


---
# Analyse d'une situation d'apprentissage intégrant le numérique 
#### Production attendue
Un document rédigé en **binôme** de 6 pages maximum comportant :
* un descriptif de la situation d’apprentissage avec obligatoirement des liens pointants sur les éléments de conception (liens vers les ressouces, copies d’écran des interfaces élèves et/ou enseignants, …),
* Une analyse de la situation selon 3 axes:
    * Impact du recours au numérique sur les apprentissages disciplinaires
    * Impact du recours au numérique en lien avec la vie en société
    * Impact du recours au numérique sur l’élève
* un bilan personnel.


---
## 1 Recherche de situation 

* L'idée de la situation d'apprentissage peut provenir d'une situation **vécue, observée ou rapportée**. 
* Elle doit intégrer au moins une ressource et/ou un outil numérique, ou hybride.
* Elle peut être commune à plusieurs personnes mais les analyses devront prendre en compte un contexte de mise en œuvre.

---
## 2 Description de la situation
#### Eléments attendus dans la description

```
- Discipline 
- Niveau de classe
- Période de l'année, ou place dans une séquence, où la situation a été (ou serait) mise en œuvre.
- Objectif(s) d’apprentissages : 
	- Compétences disciplinaires visées en termes de « être capable de…», (ces compétences sont issues des programmes)
	- Compétences numériques développées (issues du CRCN)
	
- Environnement numérique impliqué dans la situation :
    - Outils physiques (TBI, tablettes, ordinateurs fixes ou portables, caméras, enregistreurs, ...)
    - Organisation matérielle (nb de matériels par élève/classe, salle informatique ou salle de classe)
    - Logiciels ou applications utilisés.
    - Ressources utilisées, ou produites

-  Organisation pédagogique : quelles activités, quand, comment, ...
    
```

---
# En avant !
* Formation des binômes et inscription sur le cours eformation
* Choisir une situation
* Rédiger la description de la situation et si possible commencer les analyses.

---
