---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

## Production, usages de ressources numériques.
### Laurence Osete
### Source : Farah Guillot, 2022 
### Inspé, Univ. Grenoble Alpes

### ![w:350](images/logo-espe-uga.png)

---

<!-- footer: L.Osete • M2-DIU -PE •  Inspé-UGA 2024-25 • ![CC:BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)-->
<!-- paginate: true -->


# 0. :pencil2: Activité

- Prendre une feuille A4 en position paysage, et tracer un tabeau avec six colonnes de largeur égales.
![center](images/img-ressources/Tableau.png)

---
# 0. Questions à se poser avant d'utiliser une ressource.

<!-- _class: t-80 -->

**1. Quoi ?**
<!-- De quelle nature de ressource ai-je besoin ? -->

**2. Pourquoi ?**
<!-- A quoi va servir cette ressource ? Quel est l'objectif pédagogique visé ? -->

**3. Quand ?**
<!-- A quel(s) moment(s) de la séquence (ou séance) l'utiliser ? Pendant le temps de classe, ou à la maison ? -->

**4. Comment ?**
<!-- Qui va l'utiliser? Sur quel support ?  -->

**5. Prérequis ?**
<!-- De quelles compétences a besoin l'élève (ou l'enseignant) pour l'utiliser ? -->

**6. Conditions d'utilisation ?**

---
# :one: Qu'est-ce qu'une ressource pédagogique ?

#### Une ressource pédagogique est une entité, un "grain", numérique ou non, utilisée dans un processus d’enseignement, de formation ou d’apprentissage. 


![center](https://4.bp.blogspot.com/-bZMMqFxAcjk/Vhk9KCpMv9I/AAAAAAAAAEc/DPi0lLLbRBY/s640/pyramide%2Bobjets%2Bp%C3%A9dagogiques.jpg) (1)


---
# 1. :warning: Ne pas confondre Outil et Ressources

L'outil sert à afficher, diffuser, créer, ou modifier une ressource (un contenu)

##### Exemples

| Outil | Ressource |
| ------ | ------ |
|    Navigateur    |    Une page ou un site Web    |
|    Framindmap    | Une carte mentale  |
|    VLC    | Une vidéo  |
|    Lalilo    | Un exercice de lecture  |

* Parfois les deux sont indissociables.

---
# 1. Quelle nature de ressource ?

<!-- _class: t-80 -->

| Réelle | Hybride |Numérique|
| :------: | :------: | :-------: |
| ![w:300](https://cdn.pixabay.com/photo/2017/09/23/15/04/chart-2779132_1280.jpg) (2) | ![w:300](https://upload.wikimedia.org/wikipedia/commons/f/f4/App_iSkull%2C_an_augmented_human_skull.jpg?20130221100233)(3) | ![w:300](https://cdn.pixabay.com/photo/2017/09/05/10/08/office-2717014_960_720.jpg) (2) |

---
# 1. :pencil2: Activité

* Lister dans la première colonne de votre tableau les ressources numériques, ou hybrides déjà vues utilisées en classe (une par ligne, et maximum 3).
* Pour ceux qui n'ont jamais utilisé de ressource numérique, prendre un exemple d'usage dans la banque de scénarios de l'académie de Nantes : https://link.infini.fr/scenarios-nantes

![w:100](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG8AAABvAQAAAADKvqPNAAABLUlEQVQ4jdXUO47DIBAG4N9yQRdfAIlr0HEl+wI4uYBzJTquYSkXgI5ilNnxJtlHsUCxWmkRsvxZss0MM4C/DfwPJsA7mkGAaTIzzVafHK3cwaC9XAvNqouz3b3TnVyjyaWPTEskqM9FVijxLlHL/Ai/wvehp2K+JPZHJmgonNxts23mwMnRErA8M1ljsjvsuCmaHj+qkuO4gZYCb9vMQTaU7zJVm8y3axxz2Ac2TeYj53IP//hUlXd729R4Yb6WDgJw2r9WVWcCX+LxcIimSYl3CubsaIjcQXMJtAaspYMFJ/DZYgimSRnJSR2ahDalYmfJpLTPM94apRdWHnMx+dUaNR5vae/wirdJCUSSaXq4SCaVbBW3eZw5NEm8ts2jf4MUzD7FNv/sBP41vgHFdCblk1nPyQAAAABJRU5ErkJggg==)
* Avec quel outil est-elle utilisée (affichée, ou créée) ?

---
# 2. Pourquoi utiliser une ressource numérique ?

Deux sous-questions :

* pour faire quoi ? pour quelles fonctions pédagogiques ?
* qu'est-ce que cela change ?

---
## 2.1 Les fonctions pédagogiques possibles (Tricot, 2020)

24 fonctions pédagogiques, dont :

* Écouter un document sonore, écouter un texte sonorisé 
* Regarder / lire un document multimédia
* Regarder une vidéo, une animation 
* Chercher de l'information,
* S'entrainer
* Résoudre des problèmes et calculer
* Apprendre à faire sur simulateur, ou réalité virtuelle
* ...

:scroll: Tricot, A. (2020). _[Quelles fonctions pédagogiques bénéficient des apports du numérique ?](https://www.cnesco.fr/wp-content/uploads/2024/07/210218_Cnesco_Tricot_Numerique_Fonctions_pedagogiques.pdf)_ . Paris : Cnesco-Cnam.

---
## 2.2 Modèles d'analyse des changements apportés par les technologies

### 2.2.1 Modèle SAMR (Puentedura, 2006 )

![center](https://primabord.eduscol.education.fr/local/cache-vignettes/L676xH599/samr-a89ff.jpg?1710153891)(4)

---
### 2.2.2 Modèle passif-participatif (Romero, 2014)

Représentation des niveaux d’engagement créatif de l’apprenant dans des activités d’apprentissage médiatisées par le numérique.
![w:800](https://margaridaromero.blog/wp-content/uploads/2019/09/cinq_niveaux_dusages_des_technologies_de_m._romero_20151.png)

---
### 2.2.3 Exemples

#### - Présenter de l'information

| **Niveau SAMR**| **Substitution** | **Augmentation** | **Modification** | 
| :------ | :------ | :------ | :------ |
| **Exemple** | Projeter un texte ou une image au vidéoprojecteur | Présenter des images dynamiques (vidéos), ou Ajouter des informations ponctuelles sur des images ([exemple](https://ladigitale.dev/digiquiz/q/650063febaa5d)) | Questionner les élèves lors de la consultation d'une vidéo ([exemple](https://ladigitale.dev/digiquiz/q/6500495049035)) |
| **Engagement de l'apprenant** | Consommation passive|Consommation interactive | Consommation interactive | 

#### - Lire et comprendre un texte, apprendre à lire

| **Niveau SAMR**| **Substitution** | **Augmentation** | **Modification** | 
| :------ | :------ | :------ | :------ |
| **Exemple** | Lire un document sur support numérque (pdf, ou html)| Coupler le son d’un phonème et la mise en exergue du graphème correspondant | S'entrainer à comprendre l'écrit avec des batteries d'exercices numériques comme Lalilo ou TACIT |
| **Engagement de l'apprenant** | Consommation passive|Consommation interactive | Consommation interactive | 

---
#### - Coopérer

| **Niveau SAMR**| **Substitution** | **Modification** | **Redéfinition** | 
| :------ | :------ | :------ | :------ |
| **Exemple** | Produire un document sur support numérque l'un à la suite de l'autre | Rédiger un document à plusieurs en même temps | Cocréer des contenus en échangeant (commentant) avec une autre classe à l'étranger |
| **Engagement de l'apprenant** | Consommation passive|Cocréation de contenu | Cocréation participative de connaissances | 


<!---


---
# 2. Les images fixes vs animées
![w:250](https://framapad.org/abc/img/fr/screenshot.png) (7) | ![w:250](images/img-ressources/Vikidia.png) (8) 
Afficher un texte en fonction de besoins spécifiques ![w:250](images/img-ressources/LireCouleur.png) (6) 
* **S** - Projection d'une image au vidéoprojecteur
* **A** - 
* **A** - les [exercices intéractifs](https://www.lumni.fr/jeu/dissocier-les-nombres-pairs-et-impairs)
* **M** - 

---
# 2. Les sons

* **S** - Diffuser un enregistrement, faire s'enregistrer un élève pour se réécouter.
* **A** - Lire un texte écrit (synthèse vocale)- Traduire une langue étrangère en direct.
* **MR** - Interagir avec un assistant vocal (Exemple : [Captain Kelly](https://eduscol.education.fr/2974/enseigner-l-anglais-l-ecole-avec-captain-kelly))

  * Autres exemples par [l'Académie de Bordeaux](https://dane.ac-reims.fr/images/experimenter/pix/id233/Fiches_dexploitation_pe%CC%81dagogique_SAMR_-_avec_exemples.pdf)
--->

---
# 2. :pencil2: Activité

Pour chacune des ressources utilisées :
- Quels étaient les objectifs pédagogiques ?
- Selon le modèle SAMR, à quel niveau d'usage la ressource était utilisée ?
- A quel niveau d'engagement seront les élèves (Romero) ?

---
# 3. Quand : A quel(s) moment(s) ? Pour quel(s) usage(s) ?

<!-- _class: t-70 -->

### Avant ? 
 * Rapeller des connaissances antérieurs requises. (Prérequis)
 * Apporter des connaissances nouvelles (classe inversée)

### Pendant ?
 * Illustrer
 * Faire rechercher des informations
 * Évaluer, s'auto-évaluer, s'entrainer

### Après la classe ?
* revoir les connaisasnces pour les mémoiriser
* s'entrainer
* garder une trace pour l'élève, pour les parents, ou pour l'enseignant•e

<!-->
---
# 3. :pencil2: Activité

Remplir la troisième colonne en indiquand à quel(s) moment(s) la ressource était utilisée.
-->

---
# 4. Comment : Quelle organisation pédagogique ?

* En classe entière :arrow_right: cours "magistral"
* En groupe :arrow_right: travaux de groupes
* En individuel :arrow_right: entrainement, évaluation, remédiation, ou différenciation.


---
# 4. Comment : Quel mode de diffusion ?

* Un fichier sur l'ordinateur de l'école, ou de l'enseignant•e, sur une clé USB, un lecteur MP3, ou MP4
* Dans une application sur tablette, ou en ligne
* Un lien dans les favoris du navigateur, un raccourcis sur le bureau pointant vers une page internet (ENT d'école comme [ELEA](https://dane.web.ac-grenoble.fr/elea), mur de partage, ...).

<!-->
---
# 4. :pencil2: Activité

Remplir la quatrième colone du tableau en indiquand le mode d'accès à la ressource.
-->

---
# 5. Prérequis : De quoi ont besoin les élèves, ou les enseignants ?

* De materiels
* De logiciels
* **De compétences** : Carde de référence des compétences numériques 
  * des élèves [CRCN](https://eduscol.education.fr/721/evaluer-et-certifier-les-competences-numeriques#summary-item-1), 
  * des enseignants CRCNE (pas encore de cadrage officiel)

---
# 5.1 Compétences des élèves du LSU

![center](images/img-ressources/Comp1.png)

---

![center](images/img-ressources/Comp2.png)

---

![center](images/img-ressources/Comp3.png)

---

![center](images/img-ressources/Comp4.png)

![center](images/img-ressources/Comp5.png)

--
### Compértences des enseignants : Modèle TPACK

![](https://tpack.org/wp-content/uploads/2024/07/TPACK.png)

Être capable de mobiliser ses connaissances sur les contenus à enseigner, la pédagogie à mettre en place et les technologies numériques à utiliser.

---
### 

---
# 5. :pencil2: Activité

Remplir les colonnes 3 à 5 du tableau en indiquand quand, comment, de quels matériels, logiciels vous aviez besoin. Identifier également les compétences nécessaires pour utiliser la ressource.

---
# 6. Droits des auteurs


---
# 6. Rappel

<!-- _class: t-80 -->

### 6. Loi en vigueur en France : le code de la propriété intellectuelle, composé de :

- La propriété littéraire et artistique
- La propriété industrielle : brevets, dessins et modèles, marques.

### 6. Les grands principes de la propriété littéraire et artistique

* S'applique à toute création originale.
* Plusieurs droits protégés :
    * droits moraux : paternité, respect de l’intégrité de l’œuvre (Perpétuels, Inaliénables)
    * droits patrimoniaux : représentation, reproduction (sauf cas particuliers, 70 ans après la mort de l'auteur :arrow_right: Domaine public)

---
# 6. Droits voisins

* Pour les partenaires de la création :
  * Producteurs de phonogrammes ou vidéogramme
  * Interprètes
  * Entreprises de communication audiovisuelle

> Temporaire
> 50 ans après le 1er janvier de l'année civile suivant la première diffusion.

---
# 6. Titulaires du droit d'auteur (Articles L.113-1 à L.113-10)

* Personne qui créé l'œuvre et la divulgue
* Cas des œuvres créées à plusieurs :
	* tous les co-auteurs pour une œuvre **collaborative**
	* la personne qui dirige la création et qui la divulgue pour une œuvre **collective**
* Cas particulier d'une œuvre intégrant une autre œuvre. Elle est dite **composite**. Elle est la propriété de l'auteur qui l'a réalisée, sous réserve des droits de l'auteur de l'œuvre préexistante.

---
# 6. Œuvres produites par un agent de l'état

<!-- _class: t-80 -->

**Article L111-1**
L'auteur d'une oeuvre de l'esprit jouit sur cette oeuvre, du seul fait de sa création, d'un droit de propriété incorporelle exclusif et opposable à tous.

**Article L121-7-1**
L'agent ne peut :
    - S'opposer à la modification de l'œuvre décidée dans l’intérêt du service par l'autorité investie du pouvoir hiérarchique, []
    - Exercer son droit de repentir et de retrait

**Article L131-3-1**
Dans la mesure **strictement nécessaire** à l'accomplissement d'une mission de service public, **le droit d'exploitation** d'une oeuvre créée par un agent de l'Etat dans l'exercice de ses fonctions ou d'après les instructions reçues est, dès la création, cédé de plein droit à l'Etat.

---
# 6. Droit d'auteur : Exceptions (Article L.122-5)

<!-- _class: t-80 -->

Quelques exceptions pour un usage dans l'enseignement,

**3°** sous réserve que soient indiqués clairement le nom de l'auteur et la source 
**a)**  Les analyses et courtes citations justifiées par le caractère critique, polémique, pédagogique, scientifique ou d'information de l’œuvre à laquelle elles sont incorporées ;

**4°** La parodie, le pastiche et la caricature, compte tenu des lois du genre;

**11°** Les reproductions et représentations d'œuvres architecturales et de sculptures, placées en permanence sur la voie publique, réalisées par des personnes physiques, à l'exclusion de tout usage à caractère commercial.

**12°** La représentation ou la reproduction **d'extraits d'œuvres** à des fins exclusives d'illustration dans le cadre de l'enseignement et de la formation professionnelle, dans les conditions prévues à l'article L. 122-5-4 ;

---
# 6. Droit d'auteur : Exception pédagogique (article L. 122-5-4)

* La représentation ou la reproduction d'**extraits d’œuvres**, […]
* à des fins exclusives d'illustration, à l'exclusion de toute activité ludique ou récréative,
* à un public composé majoritairement d'élèves,
* compensée par une rémunération négociée.

> :scroll: Ressources du CFC pour les enseignants
> [Infographie](http://www.cfcopies.com/images/stories/pdf/Utilisateurs/Copies-pedagogiques-papier-et-numeriques/Etablissements-d-enseignement/Pave-ressources-commun/Plaquette-Comment-utiliser-oe-protegees.pdf) et [site web](www.cfcopies.com/site-pedagogique/index.html)
> 
<!-- Mise en application par des Accords signés entre le MEN et les sociétés de gestion de droits tels que Le Centre français d'exploitation du droit de copie (CFC), La société des Arts visuels associés (Ava), La Société des éditeurs et auteurs de musique (SEAM), La Société des producteurs de cinéma et de télévision (PROCIREP), la Société des auteurs, compositeurs et éditeurs de musique (SACEM) -->
---
# 6. Conditions particulières (Accords)

Utilisation d'**extraits d'œuvres**\*, sauf pour :
  * œuvre courte (poème, article)
  * Projection en classe d'une œuvre de l'écrit
  * diffusion intégrale en classe d'une musique
  * diffusion intégrale en classe d'une vidéo provenant d'un service non payant.
  * œuvres des arts visuels : limité à 20 œuvres par travail, définition max 800x800px en 72 DPI <!-- (Avenant à l'accord) -->

\* Les œuvres doivent avoir été acquises légalement

---
# 6. Précautions à prendre

### Obligations :

* Citer l'auteur, l'éditeur (obligatoire mais pas toujours suffisant).
* Ne pas diffuser les œuvres des élèves sans autorisation.
* Consulter les conditions d'utilisation (licences)
* Respecter les CGU des logiciels et services

### Précautions :
* Utiliser si possible des ressources sous licence "libre" et gratuite (Creative Commons ou GNU GPL).

---
# 6. :pencil2: Activité

Remplir la sixième colonne du tableau en indiquand le nom du titulaire des droits d'auteur.
* Indiquer si une licence d'utilisation est précisée, ou non.
* Indiquer s'il y a des précautions à prendre pour l'utiliser.

---
# 7. Banques de ressources Institutionnelles

- Ressources pédagogiques pour le primaire : [Primàbord](https://primabord.eduscol.education.fr/)
- Vidéos et contenus intéractifs : [LUMNI](https://enseignants.lumni.fr/partenaires)
- [Services pour accompagner les apprentissages des fondamentaux au cycle 2](https://eduscol.education.fr/1911/l-intelligence-artificielle-pour-accompagner-les-apprentissages-des-fondamentaux-au-cycle-2)
- Environnement Numérique de Travail [ELEA](https://dane.web.ac-grenoble.fr/elea)
- Assistant vocal [Captain Kelly](https://eduscol.education.fr/2974/enseigner-l-anglais-l-ecole-avec-captain-kelly)
- Banque de ressources numériques pour l'Ecole : [BRNE](https://eduscol.education.fr/228/brne)
- Des ressources numériques adaptées : [Édu-Up](https://eduscol.education.fr/2258/des-ressources-numeriques-innovantes-et-adaptees-grace-au-dispositif-edu)
- [Générateur de QR Code](https://dane.web.ac-grenoble.fr/outils-numeriques-0/creer-ses-qr-codes) de l'académie de Grenoble

---
# 7. Banques de ressources "Grand public"

- [Creative Commons Search](https://search.creativecommons.org/) : moteur de recherche d'images, vidéos, musiques sous licence Creative Commons
- [Wikimédia](https://commons.wikimedia.org/wiki/Accueil) : Banque d'images sous licence Creative Commons.
- [LaSonothèque](https://lasonotheque.org/) : banque de sons, bruitages libres et gratuits.
- [Gallica](https://gallica.bnf.fr/blog/recherche/?query=1857&mode=desktop) : Catalogue d'œuvres entrées dans le domaine public.
- [Free Music Archive](https://freemusicarchive.org/home)
- ...

---
# 8. Les textes de loi de référence :

<!-- _class: t-80 -->

- Le code de la propriété intellectuelle : https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006069414
- Protocole d'accord sur l'utilisation des livres, des œuvres musicales éditées, des publications périodiques et des œuvres des arts visuels à des fins d'illustration des activités d'enseignement et de recherche : [Bulletin officiel n°35 du 29 septembre 2016](https://www.education.gouv.fr/bo/16/Hebdo35/MENE1600684X.htm)
- Avenant au protocole : [Bulletin officiel n° 7 du 13 février 2020](https://www.education.gouv.fr/bo/20/Hebdo6/MENE2000032X.htm?cid_bo=148987))
- Protocole d'Accord sur l'application de la loi DADVSI concernant la reproduction par reprographie : [Bulletin officiel n°13 du 1er avril 2021](https://www.education.gouv.fr/bo/21/Hebdo13/MENE2108987C.htm)
- Protocole d'Accord sur l'application de la loi DADVSI concernant l'utilisation des œuvres cinématographiques et audiovisuelles. [Bulletin officiel n° 5 du 4 février 2010](https://www.education.gouv.fr/bo/2010/05/menj0901120x.html)
- Accord sur l'interprétation vivante d'œuvres musicales, l'utilisation d'enregistrements sonores d'œuvres musicales et l'utilisation de vidéo-musiques à des fins d'illustration des activités d'enseignement et de recherche [Bulletin officiel n° 5 du 4 février 2010](https://www.education.gouv.fr/bo/2010/05/menj0901121x.html)

---
# Crédits

- (1) La pyramide des objets pédagogiques par [Laurent FLORY](https://www.enssib.fr/bibliotheque-numerique/documents/1234-les-caracteristiques-d-une-ressource-pedagogique-et-les-besoins-d-indexation-qui-en-resultent.pdf)
- (2) par [Lukas](https://pixabay.com/users/goumbik-3752482/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2717014) depuis [Pixabay](https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2717014)
- (3) par Hagustin, [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0), via Wikimedia Commons
- (4) Qu’est-ce que le modèle SAMR ? [Primàbord](https://primabord.eduscol.education.fr/local/cache-vignettes/L814xH234/samr_def-4cfa2.jpg?1710153891)
- (5) Cendrillon de Charles Perrault, depuis [Gallica (BnF)](https://gallica.bnf.fr/ark:/12148/bpt6k5846867t/f12.item.zoom)
- (6) [LireCouleur](http://lirecouleur.arkaline.fr/web.html)
- (7) [Framasoft](https://framapad.org/abc/fr/)
- (8) [Vikidia](https://fr.vikidia.org/wiki/Vikidia:Accueil)

