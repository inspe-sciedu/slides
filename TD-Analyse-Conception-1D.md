---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# Culture numérique et apprentissages
## TD 1
 
Equipe de formateurs - Inspé, UGA
 
<!-- page_number: true -->
<!-- footer: Culture numérique & Apprentissages – Inspé-UGA - 2024 - 2025 - CC:BY-NC-SA -->


---
# Références
 
* Présentation disponible [ici](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/TD-Analyse-Conception-1D.pdf) ou ici : https://link.infini.fr/td-analyse-num-1d
* Page e-formation (Moodle) : https://eformation.univ-grenoble-alpes.fr/course/view.php?id=1993 , sur auto-inscription (voir section : TD Analyse de pratiques)

---
# :zero: Objectifs
### Objectif général de la formation
Préparer à la concevoir de situations d'apprentissages intégrant du numérique réaliste et réalisable dans une classe à partir de l'analyse d'une situation existante.
### Objectifs du TD
* Mettre en pratique les concepts théoriques abordés lors des trois première séances.

---
# 0. Production attendue pour l'évaluation

Un document de **4 pages** maximum rédigé en binôme.

Un document de structure de la production est dans le cours.

---
# 0. Format général

- Un seul document au format **PDF** déposé sur la plateforme.
- Police Arial, taille 12, interligne 1,5 cm, marges 2,5 cm.

---
# 0. Modalités de travail

* **TD 4 : Recherche et description d'une situation réelle déjà mise en œuvre**
* TD 5 : Evaluer, s'auto-évaluer et s'entraîner avec le numérique 
* TD 6 : Etude de cas juridiques et apports théoriques sur les règlementations en vigueur 
* TD 7 : Compétences numériques des élèves et des enseignants
* TD 8 : Mise en commun et finalisation.

:warning: L'ordre des TD 5 à 7 est indicatif


---
# :one: Recherche de situation (1 h)

* Constituez des binômes (vous inscrire sur le tableau)
* La situation d'apprentissage peut provenir d'une situation **vécue, observée ou rapportée**. 
* l'idée peut être issue d'une banque de scénarios comme :
    - [la banque de l'académie de Nantes](https://www.pedagogie.ac-nantes.fr/numerique/scenarios-pedagogiques/)
    - [Edubase](https://edubase.eduscol.education.fr/)
    - ou une autre
* Elle doit intégrer au moins une ressource et un outil numérique (en situation totalement numérique ou hybride).
* Elle peut être similaire à plusieurs binômes, mais les analyses devront se placer dans des contextes différents.

---
# :two: Description de la situation

Vous devez décrire la situation telle qu'elle a été conçue, ou telle qu'elle le sera si vous aviez à la mettre en œuvre. 

#### Eléments attendus dans la description

```
- Domaine(s) d’enseignement concerné(s) par la situation
- Niveau de classe
- Période de l'année, ou place dans une séquence, où la situation a été (ou serait) mise en œuvre.
- Objectif(s) d’apprentissages : 
	- Compétences disciplinaires visées en termes de « être capable de…», (ces compétences sont issues des programmes)
	- Compétences numériques développées (issues du CRCN)
	
- Environnement numérique impliqué dans la situation :
    - Supports physiques (TBI, tablettes, ordinateurs fixes ou portables, caméras, enregistreurs, ...)
    - Outils : logiciels ou applications utilisés.
    - Ressources numériques (ou hybrides) utilisées, ou produites par les enseignants et/ou les élèves.

-  Organisation pédagogique : description des activités des élèves, et de leur planification dans le temps et l'espace.
    
```

---
# :two: Grille d'analyse (à débuter, et continuer lors des prochains TD)

Construire une grille d'analyse collective sur le document collaboratif disponible dans le cours (plate-forme e-formation).

Cette grille sera à compléter lors des prochaines séances.

---
#  En avant !
Conseils :
* Essayez d'identifier un apprentissage pour lequel le numérique peut apporter une plus-value.
* Restez réaliste :arrow_right: la mise en œuvre doit être possible.


