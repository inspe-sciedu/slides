---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
   
# Module "Enseigner avec le numérique"
## TD 2
### DIU SD
 
Laurence Osete - UGA
 
<!-- page_number: true -->
<!-- footer: L.Osete • DIU SD • Inspé-UGA 2023-24 • ![CC:BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png) -->

---
# :zero: Rappels
* TD 1 : Mise en application des apports théoriques sur les effets du numériques sur les apprentissages - Début de la conception :arrow_right: avoir une première proposition de situation d'apprentissage.
* **TD 2 : Mise en application des apports théoriques sur les ressources et les aspects juridiques et éthiques - Poursuite de la conception**.
* TD 3 : Apports pratiques sur la formation AU numérique et l'évaluation des élèves en référence au CRCN  - Finalisation de la conception.
* TP : Mise en commun des conceptions et retour d'expérience pour celles et ceux qui auront réalisé une mise en oeuvre. :arrow_right: PIX+EDU.


---
## :zero: Organisation du TD

1 - Introduction (5 minutes)
2 - Mise en pratique : Etude de cas (1h15)
3 - Recherche d'une situation d'apprentissage réaliste (40 minutes)

---

# :one: Rôles de l'enseignant

![w:900](images/img-juridique/roles.png)

---

# 1. Compétences visées

(source : référentiel de compétences des enseignants )

* CC2 : Connaître les grands principes législatifs qui régissent le système éducatif, le cadre réglementaire de l'école et de l'établissement scolaire, les droits et obligations des fonctionnaires ainsi que les statuts des professeurs et des personnels d'éducation.
* CC6 :
  * Respecter et faire respecter le règlement intérieur et les chartes d'usage.
  * Respecter la confidentialité des informations individuelles concernant les élèves et leurs familles
* CC9 : Participer à l'éducation des élèves à un usage responsable d'internet.

---

# 1. Compétences travaillées

* Capacité à **comprendre le contexte** dans lequel il évolue et à utiliser ses connaissances, recherches, réflexions à propos de la législation et des règlementations institutionnelles liés aux TICE **pour une mise en œuvre raisonnée et responsable** dans le cadre de son activité professionnelle.

---

# 1. Les responsabilités de l'enseignant (rappel)

* Responsabilité civile
* Responsabilité pénale
* Responsabilité administrative

---

# 1. Quelques obligations de l'enseignant (rappel)


* D'obéissance hiérarchique
* De discrétion professionnelle
* De signalement
* De neutralité

---

# 1. Les principaux sujets abordés

1. Le droit à la vie privée
2. Les droits des auteurs
3. La sécurité des élèves
4. Le principe de neutralité

---

# :two: Etudes de cas

## Par groupe de 3 à 6 personnes (30 mn)

- Consulter le cas attribué disponible ici : https://lstu.fr/casjuridiquessd-4
- En vous aidant des ressources documentaires et du cas similaire corrigé, répondre à la situation problème.

## Présentations (30 mn)

- En vous appuyant sur les questions de la partie "Synthèse", réaliser une rapide présentation **orale** de 3 min maximum.

---

# 2. Présentations

---
# :three: Conception d'une situation d'apprentissage intégrant le numérique 
#### Production attendue
Un document rédigé en binôme de 4 pages maximum comportant :
* un descriptif de la situation d’apprentissage avec obligatoirement des liens pointants sur les éléments de conception (liens vers les ressouces, copies d’écran des interfaces élèves et/ou enseignants, …),
* une argumentation sur les choix opérés lors de la conception, notamment au regard des apports théoriques,
* un bilan personnel.


---
## 3.1 Recherche de situation 

* L'idée de la situation d'apprentissage peut provenir d'une situation **vécue, observée ou rapportée**. 
  * Exemples d'usages sur [ÉduBase](https://edubase.eduscol.education.fr/)
* Elle doit intégrer au moins une ressource numérique, ou hybride.
* Elle peut être commune à plusieurs groupes mais les analyses devront prendre en compte un contexte de mise en œuvre.

---
## 3.2 Description de la situation
#### Eléments attendus dans la description

```
- Domaine(s) d’enseignement concerné(s) par la situation
- Niveau de classe
- Période de l'année, ou place dans une séquence, où la situation a été (ou serait) mise en œuvre.
- Objectif(s) d’apprentissages : 
	- Compétences disciplinaires visées en termes de « être capable de…», (ces compétences sont issues des programmes)
	- Compétences numériques développées (issues du CRCN)
	
- Environnement numérique impliqué dans la situation :
    - Outils physiques (TBI, tablettes, ordinateurs fixes ou portables, caméras, enregistreurs, ...)
    - Organisation matérielle (nb de matériels par élève/classe, salle informatique ou salle de classe)
    - Logiciels ou applications utilisés.
    - Ressources utilisées, ou produites

-  Organisation pédagogique : quelles activités, quand, comment, ...
    
```

---
## 3.3 Analyse et justification des choix

- Justification des choix d'outils et de ressources numériques, en s'appuyant notamment sur le modèle SAMR.
- Analyse des potentiels effets positifs ou néfastes pour les apprentissages et les apprenants (TD 1).
- Analyse des problèmes juridiques (droits d'auteurs, droit à la vie privée) et éthiques soulevés.


:warning: Les analyses et justifications doivent tenir compte d'un contexte.


---
# En avant !
* Essayez d'identifier un apprentissages pour lequel le numérique peut apporter une plus-value réelle.
* Restez réaliste :arrow_right: Mise en oeuvre conseillée. 

 


