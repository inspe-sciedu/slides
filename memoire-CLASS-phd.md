---
marp: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# UE 24 Atelier Mémoire “Interactions enseignant-élèves“
## Philippe Dessus, Inspé, Univ. Grenoble Alpes

##### Année universitaire 2024-25

![w:350](images/logo-espe-uga.png)

---
<!-- footer: Ph. Dessus • M1-M2 PE • Atelier mémoire “Interactions enseignant-élèves • Inspé-UGA 2024-25 • CC:BY-NC-SA-->
<!-- paginate: true -->

# Séance 1 M1 PE - 9/10/24

- Introduction, présentation de chacun.e
- Présentation du CLASS

---
:zero: Introduction à l'atelier

---
# 1.0 M1 - Présentation rapide
- prénom ; niveau de classe de stage ; caractéristiques principales de l'école, de la classe (si connues)

---
# 1.0 M1 - Syllabus des séances de mémoire

- Site plate-forme :  [https://eformation.univ-grenoble-alpes.fr/group/index.php?id=332&group=80](https://eformation.univ-grenoble-alpes.fr/group/index.php?id=332&group=80) ;  mot de passe pour l’auto-inscription : **MemoirePhD**
- Le site : https://eformation.univ-grenoble-alpes.fr/course/view.php?id=2728
- Syllabus : https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/class/syl_CLASS_espe.html
- Présentation : https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/memoire-CLASS-phd.pdf 
  


---
# 1.0 M1 - Préambule (1/2) : Règles de fonctionnement du groupe

- Une partie du travail de l’atelier est fondée sur la confrontation entre participants de leurs pratiques de classe, leur compréhension et leur amélioration. Il s’agit de les rapporter le plus précisément possible et, en contre-partie, d’éviter de porter des jugements sur la personne qui a rapporté les pratiques, mais d’évaluer le plus objectivement possible ce qui s’est passé (et non la personne qui est à l’origine possible de ces pratiques).
- Ce travail d’analyse des pratiques peut être outillé par le CLASS (un outil d’observation de la qualité des interactions enseignant-élèves). Le formateur et les participants mettront tout en œuvre pour permettre une parole libre, positive et constructive à propos des pratiques de chacun.e.

---
# 1.0 M1 - Préambule (2/2)

- Le résultat final de l’atelier mémoire fera l’objet d’une évaluation, et non les pratiques relatées ou visionnées au cours de l’atelier
- Les propos tenus au sein du groupe reposent sur la **confidentialité** (c.-à-d. que les participants ne peuvent rapporter ce qui s’est dit dans l’atelier à des personnes n’en faisant pas partie) et le respect de la parole de chacun des membres
- Le CLASS n'est pas un outil de l'évaluation de l'enseignant·e, mais bien de ce qui se joue entre l'enseignant et ses élèves

---
# 1.0 M1 - Organisation sommaire de l'année (1/2)

- en Master 1, le travail lié au mémoire est à réaliser en binôme ;
- le contexte théorique, la méthode de l’étude et le recueil de données seront réalisés dans l’année de Master 1 ;
- la partie du mémoire rendue en fin de Master 1 comportera donc : 
	– la partie théorique, 
	– la problématique, 
	– la méthode ; 
	– le recueil de données ;

---
# 1.0 M1 - Organisation sommaire de l'année (2/2)

- les données recueillies le seront **exclusivement** pendant le stage filé (de janvier à mai 2023), sauf pour les AED, qui pourront commencer leur recueil plus tôt ;
- la partie “méthode” ne sera pas évaluée en Master 1 ; il est toutefois vivement conseillé d’en écrire une version la plus aboutie possible, nécessaire pour le recueil de données et pour comprendre la démarche ;
- le mémoire sera fini durant le premier semestre de Master 2 : écriture de l’analyse des données, de la discussion/conclusion ; peaufinage des parties livrées en Master 1.

---
:one: Présentation du CLASS : *Classroom Assessment Scoring System*

---
# 1.1 M1 - Les dimensions de la qualité éducative

                                                       Qualité éducative

                               ┌──────────────────────────────┬───────────────────────────┐
                               │                              │                           │
                       ┌───────┴───────┐            ┌─────────┴───────┐      ┌────────────┴─────────────┐
                       │    Qualité    │            │   Qualité des   │      │ Qualité des orientations │
                       │ Structurelle  │            │    processus    │      │       pédagogiques       │
                       └───────┬───────┘            └────────┬────────┘      └────────────┬─────────────┘
                               │                             │                            │
           Ratio               │                             │      Interactions          │        Croyances,
    enseignante-enfants ───────┼─────── Programme            ├───enseignante-enfants      ├─── attitudes, valeurs
                               │         éducatif            │                            │
                               │                             │                            │        Approches
                               │                             │     Interactions           ├───  psychologiques
                               │                             ├─── enfants-enfants         │
       Formation initiale ─────┴────── Environnement         │                            │    Perception de son
          et continue                    physique            │     Interactions           └───  rôle auprès de
                                                             └─── parents-école                    l'enfant

:books: Duval *et al*. 2021 p. 225


---
# 1.1 M1 - Le rôle de CLASS
- CLASS est un outil d'observation de la qualité des interactions enseignant·e/élèves
- Nous allons l'utiliser plutôt, dans ces séances de tutorat, comme un outil
	- permettant d'avoir un vocabulaire commun
	- permettant de vous centrer sur des pistes d'analyse de situations et d'amélioration

---
# 1.1 M1 - :warning: Ce que cet atelier n'est pas :warning:

- Une session de certification au CLASS
- Un moyen de tout connaître sur le CLASS
- L’atelier-mémoire est centré sur le CLASS en tant qu’outil de réflexion et de compréhension (des sortes de lunettes) de ce qui se passe dans votre classe. 
- Il est un cadre, pas le sujet de recherche lui-même : **ce n’est pas obligatoire de référer au CLASS dans son mémoire**

---
# 1.1 M1 - Le CLASS

- Classroom Assessment Scoring System: Système d’observation et d’évaluation des interactions enseignant-élèves
- Conçu par Robert Pianta, Karen La Paro & Bridget Hamre, univ. Virginie (USA)
- Éditeur : Teachstone (https://teachstone.com)
Dérivé sur tous les niveaux scolaires :  primaire-secondaire. 
- Une version en français pour les maternelles 3-5 ans existe
- La version 2, toujours pour ce niveau, vient de sortir

---
# 1.1. M1 - Les fondations théoriques du CLASS

- S’intéresser aux processus de classe proximaux vs (centrés expérience de l’élève) plutôt que distaux (centrés expérience de l’enseignant)
- Arrière-plan **Soutien émotionnel**
	- Théorie de l’attachement (Ainsworth et al. 1978; Bowlby, 1969) :arrow_right: sécurité affective
	- Théorie de l’autodétermination (Ryan & Deci, 2017)
- Arrière-plan **Organisation de la classe**
	- Habiletés d’autorégulation (Blair, 2002; Raver, 2004)
- Arrière-plan **Soutien à l’apprentissage**
	- Développement cognitif et langagier de  l’enfant (Piaget, etc.)

---
# 1.1. M1 - Les recherches sur le CLASS

- Testé sur plus de 4 000 classes, l'un des systèmes d'observation les plus étudiés
- Structure des dimensions du CLASS
- Fidélité inter-observateurs 
- Stabilité des mesures dans le temps (cycles, jours,  année)
- Validité de construit : corrélations entre CLASS et d’autres tests de but similaire
- Validité prédictive : corrélation entre les scores CLASS et les performances des élèves

---
# 1.1 M1 - Les domaines et dimensions du CLASS

                                                      ┌────────────────────────────────┐                                  
                                                      │     Interactions en classe     │                                  
                                                      └────────────────────────────────┘                                  
                                                                       │                                                  
                                                                       │                                                  
                                                                       │                                                  
                                       ┌───────────────────────────────┼───────────────────────────────────┐              
                                       │                               │                                   │              
                                       │                               │                                   │              
                                       ▼                               ▼                                   ▼              
                            ┌─────────────────────┐     ┌────────────────────────────┐      ┌────────────────────────────┐
    Domaines                │ Soutien émotionnel  │     │ Organisation de la classe  │      │ Soutien à l'apprentissage  │
                            └─────────────────────┘     └────────────────────────────┘      └────────────────────────────┘

---
# 1.1 M1 - Pour résumer !

Un environnement propice à l’apprentissage et engageant les élèves :
- Est émotionnellement sûr et aidant
- Offre des expériences bien structurées et prédictibles
- Est riche et structuré du point de vue conceptuel et langagier

---
# 1.1 M1 - Domaine 1 – Soutien émotionnel

- Le fonctionnement émotionnel et social de l’élève au sein de sa classe est reconnu comme un indicateur important de réussite scolaire
- Un élève motivé et en relation avec ses pairs dès les plus petites classes a plus de chances que d’autres de réussir ultérieurement, que ce soit d’un point de vue social ou même académique

---
# 1.1 M1 - Domaine 1 – Soutien émotionnel
- Des relations chaleureuses, soutenantes avec les adultes et les autres enfants
- La joie et le désir d'apprendre
- Une motivation à s'engager dans les activités d'apprentissage
- Un sentiment de confort (aisance) dans le milieu éducatif
- La volonté de relever des défis sur le plan social et académique
- Un niveau approprié d'autonomie

---
# 1.1 M1 - Soutien émotionnel - 1 Climat positif (C+)
Reflète le lien émotionnel entre l'enseignant et l'enfant, entre les enfants eux-mêmes ainsi que la chaleur, le respect et le plaisir communiqué dans les interactions verbales et non-verbales

**Indicateurs**
- Relations
- Affect positif
- Communication positive 
- Respect

---
# 1.1 M1 - Soutien émotionnel - 2 Climat négatif (C–)
N'est pas simplement l'absence de climat positif mais bien la présence de comportements négatifs. Reflète l'ensemble de la négativité exprimée dans la classe. La fréquence, l'intensité et la nature de la négativité de l'enseignant et des enfants sont des éléments-clés de cette dimension

**Indicateurs**
- Affect négatif
- Contrôle punitif
- Sarcasme/Irrespect
- Sévère négativité

---
# 1.1 M1 - Soutien émotionnel - 3 Sensibilité de l'enseignant (SE)
Englobe la capacité de l'enseignant à être attentif·ve et à répondre aux besoins émotionnels et académiques des enfants. Un haut niveau de sensibilité offre un soutien aux enfants dans l'exploration et l'apprentissage puisque l'enseignant leur offre de manière constante du réconfort et de l'encouragement

**Indicateurs**
- Conscience/vigilance
- Réceptivité
- Réponse aux problèmes
- Confort des élèves

---
#  1.1 M1 - Soutien émotionnel - 4 Prise en considération du point de vue de l'enfant (PVE)
Permet de rendre compte dans quelle mesure les interactions de l'enseignant avec les enfants et les activités offertes mettent l'accent sur les intérêts des enfants, leur motivation et leur point de vue tout en encourageant la responsabilité de l'enfant et son autonomie

**Indicateurs**
- Souplesse et attention centrée sur l'élève
- Soutien à l'autonomie et au leadership
- Expression des élèves
- Restriction de mouvement

---
# 1.1 M1 - Domaine 2 – Organisation de la classe

Comment l'enseignant organise et gère différents aspects de la classe (comportement des élèves, temps, attention). Il y a une meilleure opportunité d'apprentissage et la classe fonctionne mieux si les élèves ont un comportement adéquat, cohérent avec les tâches qui leur sont allouées, et sont intéressés et engagés dans ces dernières.

---
# 1.1 M1 - Organisation classe - 5 Gestion des comportements (GC)
Englobe l'habileté de l'enseignant à établir des attentes comportementales claires et à utiliser des méthodes efficaces pour prévenir et rediriger les comportements inappropriés

**Indicateurs**
- Attentes comportementales claires
- Proactivité
- Redirection des comportements
- Comportements des enfants

---
# 1.1 M1 - Organisation classe  – 6 Productivité (P)

Dans quelle mesure l'enseignant gère le déroulement des activités et des routines de manière à optimiser le temps disponible à l'apprentissage ?

**Indicateurs**
- Maximisation du temps d'apprentissage
- Routines
- Transitions
- Préparation du matériel nécessaire à la séance

---
# 1.1 M1 - Organisation classe - 7 Modalités d'apprentissage (MA)

Reflète la manière dont l'enseignant maximise l'intérêt des enfants, leur engagement et leur capacité à apprendre au cours des leçons et activités offertes

**Indicateurs**
- Accompagnement efficace
- Diversité des modalités et des matériels
- Intérêt de l'enfant
- Clarté des objets d'apprentissage

---
# 1.1 M1 - Domaine 3 – Soutien à l'apprentissage

S'intéresse à décrire comment les enseignants aident les enfants à 
- apprendre à résoudre des problèmes, à raisonner et à penser
- utiliser les rétroactions pour approfondir des habiletés et des connaissances
- développer des habiletés langagières plus élaborées

---
# 1.1 M1 - Soutien appr — 8. Développement de concepts (DC)

Utilisation par l'enseignant de stratégies diverses (discussions, activités) afin d'amener l'enfant à développer une pensée plus élaborée, une plus grande compréhension des phénomènes de son environnement plutôt que de favoriser un apprentissage par cœur

**Indicateurs**

- Analyse et raisonnement
- Créativité
- Intégration d'éléments déjà rencontrés dans les cours précédents
- Lien avec la vie réelle

---
# 1.1 M1 - Soutien appr — 9. Qualité de la rétroaction (QR)

Le niveau selon lequel l'enseignant offre des rétroactions aux enfants qui optimisent l'apprentissage et qui favorisent une participation maintenue

**Indicateurs**
- Étayage
- Rétroactions en boucle
- Stimuler les processus de la pensée
- Fournir de l'information
- Encouragement et affirmation
  
---
# 1.1 M1 - Soutien appr — 10. Modelage langagier (ML)

Rend compte de l'efficacité et de la quantité des techniques de stimulation du langage utilisées par l'enseignant.

**Indicateurs**
- Conversations fréquentes
- Questions ouvertes
- Répétition et extension
- *Self-talk* et *parallel-talk*
- Niveau de langage élaboré


---
# 1.1 M1 - Tour de table (1/2)
- Quel est le domaine (ou des dimensions dans un domaine) pour lequel vous vous sentez le/la plus à l'aise ?
- Quel est le domaine (ou des dimensions dans un domaine) pour lequel vous pensez avoir des difficultés ?
- :warning: Ces 2 impressions sont autant causées par vous que par vos élèves
- Considérez chaque dimension et annotez les possibles points forts/faibles

---
# 1.1 M1 - Liste des dimensions
1. Climat positif (C+)
2. Climat négatif (C–)
3. Sensibilité de l'enseignant (SE)
4. Prise en considération du point de vue de l'enfant (PVE)
5. Gestion des comportements (GC)
6. Productivité (P)
7. Modalités d'apprentissage (MA)
8. Développement de concepts (DC)
9. Qualité de la rétroaction (QR)
10. Modelage langagier (ML)

---
# 1.1 M1 Pour la prochaine séance (20/10/24)

- Lire le cours sur internet (introd, et description des 3 domaines)
- Visionner la vidéo « une matinée à la maternelle : https://www.youtube.com/watch?v=ovb4nICmYkM 
- Observer les dimensions du CLASS en vous centrant sur le soutien émotionnel. 
- :warning: Séance en visioconférence

---
# 2. Séance 2 du 20/11/24

- Débriefing de la vidéo “Matinée à la maternelle“
- Le domaine “Soutien à l'apprentissage“
- Visionnage d'une vidéo de classe (Arts plastiques)

---
# 2. M1 Débriefing “Matinée à la maternelle“ (1/2)

<!-- _class: t-70 -->

- **Climat positif** (=) : l'ATSEM discute avec une élève pendant qu'elle chausse une autre. L'enseignante emploie peu de formules de politesse “pose tes joujous peut-être”.
- **Climat négatif** (—) : Ton un peu sec, mécanique “on va s'asseoir...“, “MAINTENANT j'appelle”...
- **Sensibilité** (=) : L'enseignante tient la main des élèves ; dit “tu fais avec moi” à un élève un peu perdu. Ne répond pas toujours aux “Maîtresse ?”
- **Prise en considération PVE** (=) : Pas de latitude de choix de parcours, revenir sur le tapis jaune entre 2 ateliers, assez peu d'expression des élèves.
- **Gestion des comportements** (+) : Quelques rappels à l'ordre réactifs, mais les élèves se comportent de manière non problématique


--- 
# 2. M1 Débriefing “Matinée à la maternelle“ (1/2)

<!-- _class: t-70 -->

- **Productivité** (+) : Le matériel est installé. Une routine de chant lors du retour en classe. Les élèves savent quoi faire, ne perdent pas de temps.
- **Modalités d'apprentissage** (-) : Pas d'explication précise sur le but du “jeu des parcours”, “aujourd'hui on va faire passer les garçons et les filles”, “les filles au parcours, les garçons au toboggan“. Pas de choix de couleurs dans le coloriage.
- **Développement conceptuel** (-) : “On fait le parcours correctement et sans parler” (mais comment ?). Ne guide ni ne vérifie les postures des élèves pendant l'étirement. “coloriage en vous appliquant bien“. Pas d'explications sur l'identification des animaux (coloriage)
- **Qualité des rétroactions** (-) : Très peu de rétroactions. 
- **Modelage langagier** (-) : Pose qq questions ouvertes “qu'est-ce qu'on peut faire avec un coquillage qui a un trou ?”, mais sans insister. “Ce truc qui pique“. Donne des mots nouveaux : “bogue“, mais ne poursuit que rarement l'échange.

---
# 2. M1 Visionnage d'une vidéo “Arts Plastiques CE1-CE2“

- [Lien vers la Vidéo Canopé BSD, avec identification](https://www.reseau-canope.fr/BSD/sequence.aspx?bloc=886114). Enseignante : C. Etienne.

---
# 2. M1 - Pour la prochaine séance

- Visionner la vidéo “Une matinée dans la PS d'Émilie & Stéphanie”, accessible ici [https://link.infini.fr/video-ps-emilie-stephanie](https://link.infini.fr/video-ps-emilie-stephanie) en évaluant les 10 dimensions du CLASS.
- Lire le Document “[Créer un climat de classe favorable](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/class/tuto_CLASS_que_faire.html)“, une synthèse complète de prescriptions à l'enseignement dérivées du CLASS.


---
# 3. Séance du 4/12/24

-  Debriefing : Arts plastiques
- Visualisation et debriefing : Calcul mental CE1.
- Quizz de révision des dimensions

---
# 3. M1 - Vidéo Arts plastiques (vue semaine dernière)

---
# 6. Proposition de cotation Vidéo “Arts plastiques” (1/3)


* **Climat + \: 6 (+=++).** M a une voix très posée, calme et douce. Elle est proche des E et leur procure son assistance. Elle s'adresse à eux de manière très respectueuse ; pas beaucoup de sourires.
			
* **Climat – \: 1 (----)**. Pas d'exemples de climat négatif relevés.
		
* **Sensibilité de l’enseignant \: 7 (++++)**. Les E participent librement et prennent des risques dans leurs réalisations. M passe du temps pour chaque groupe en se mettant à leur disposition pour les aider. Les aide à trouver des solutions à leurs problèmes (réalisations qui ne tiennent pas debout toutes seules, etc.)
			
* **Prise en considération du PV de l’enfant \: 7 (++++)**. Les élèves réalisent leur travail en autonomie et M leur demande leur démarche en les encourageant à parler. Les E ont tout loisir de réaliser le travail qu'ils souhaitent. M est flexible : “il y a des artistes qui font ça”, “on peut inventer une histoire”.
		
* **Gestion des comportements \: 7 (++++)**. Aucun signe pouvant laisser voir des problèmes de gestion de comportements. Reprend un élève par un seul geste de la main, ce qui est efficace.

---
# 4. Proposition de cotation  Vidéo “Arts plastiques” (2/3)

<!-- _class: t-80 -->

* **Productivité \: 7 (++++)**. Même si on ne voit pas toutes les transitions entre parties de la leçon (film monté), le travail des élèves est maximisé. Les élèves savent à quoi s'attendre ; M leur précise le temps alloué à chaque partie. Le matériel est prêt.

* **Modalités d’apprentissage \: 7 (++++)**. Grande diversité et richesse du matériel utilisé et des médias pédagogiques (paper-board, tableau blanc, ardoise, vidéo-projecteur). Les tâches sont très clairement expliquées (oralement et sur tableau blanc). Les E sont engagés.
	
* **Développement de concepts \: 7 (++++)**. M fait préciser les actions des élèves, évoque des séances précédentes, et le monde des arts. Le volet créativité est riche (être capable de créer une œuvre ; leur donner un nom). Il y a des épisodes d'analyse et de raisonnement à partir des œuvres (faites par les élèves ou présentées). Plusieurs références à du travail antérieur ("est-ce qu'il y a un lien avec ce que vous avez fait?" ; “les natures mortes, vous vous souvenez ?”). 

---
# 4. Proposition de cotation  Vidéo “Arts plastiques” (3/3)

* **Qualité de la rétroaction \: 6 (+=+++)**. M fournit des informations supplémentaires sur les œuvres (Duchamp), et encourage les E à les réaliser, notamment ceux qui ont des problèmes (étayage). Adresse des rétroactions à tous. 
		
* **Modelage Langagier \: 6 (+++=+)**. M a un niveau de langage élaboré et fait travailler le vocabulaire en lien avec la création des œuvres. La conversation est essentiellement dirigée par M, mais il y a de nombreuses questions ouvertes (“quelle est la différence ?”) ; et il y a peu ou pas de self-parallel talk. De nombreuses répétitions/extensions en lien avec le vocabulaire et questions ouvertes?

---
# 3. M1 - Débriefing “Une matinée dans la PS d'É & S“ (1/2)
<!--
- **C+** HAUT Bonnes relations entre élèves, les adultes sont assez proches des élèves, leur voix est calme, félicitent les élèves.
- **C–** BAS pas d’exemples notés
- **SE**  MOYEN+ Elèves globalement à l’aise. Certains élèves non occupés et pas orientés, n’aident pas nécessairement les élèves qui ne réussissent pas (grenouille). Ens. Attentives à aider élèves 
- **PVE** MOYEN+ choisissent librement leur activité, leur matériel, ne paraissent pas contraints. Peu d’expression des élèves favorisée.
- **GC** HAUT : les élèves savent en général ce qu’il faut faire. Pas de comportement inapproprié

---
# 3. M1 - Débriefing “Une matinée dans la PS d'É & S“ (1/2)

- **Prod**. : HAUT : matériel prêt, les transitions sont l’occasion d’apprendre (à mettre l’anorak), rythme correct 
- **MA** : MOYEN+ Bonne variété des ateliers (papier à déchirer monocolores?), les élèves paraissent participer volontiers, mais les objectifs des ateliers paraissent peu orienter les élèves par rapport aux but des ateliers.
- **DC** : BAS. Pas de dialogue (notamment avec le puzzle du cœur).
- **QR** : BAS Fait chercher les élèves lorsqu’ils se trompent ; très peu de rétroactions
- **ML** : BAS : pas ou peu d’interactions verbales ; certains ateliers (dont la motricité) auraient pu être de bons moyens de faire de la « parole parallèle). Un ou 2 exemples de parallel talk (habillement).

-->
---
# 3. M1 - Visionnage de “Calcul mental en CE1“

- Une fois sans sous-titres
- Une fois avec sous-titres

---
# 3. M1 - Proposition de cotation vidéo 1 "CE1 Calcul mental” (1/3)

<!-- _class: t-80 -->

* **Climat + (– = — =) \: 3**.  Quelques sourires (pendant le calcul mental), mais plutôt rares ; enthousiasme moyen (quelques soupirs) ; peu de formules de politesse ; pas de proximité physique. Parfois rassurante (“tu vas y arriver” au facteur). 

* **Climat – (– – – –) \: 1**. Ton souvent un peu sec (“tu t'appelles Pavel ?”). Pas d'exemples de climat négatif repérés.

* **Sensibilité de l’enseignant (= = = =) \: 4**. Remarque et aide certaines actions des élèves (date au tableau), sollicite de voir certaines ardoises ("montre-moi comment tu as écrit 11"), mais de manière peu systématique (l'élève qui écrit la date est laissée sans aide trop longtemps que ). Les élèves se mettent peu en avant et partagent peu leurs idées
			
* **Prise en considération du PV de l’enfant (– + = =) \: 4**. Des élèves s'occupent à de petites tâches (facteur, date). Fait expliquer le calcul des doubles. N'encourage pas le débat à propos des résultats. 

---
# 3 -M1 - Proposition de cotation vidéo "CE1 Calcul mental”(2/3)

<!-- _class: t-80 -->

* **Gestion des comportements (+ + + +) \: 7**. "Quelques redirections"" ; "Malak et Maka, est-ce que je dois vous séparer ?" ; lève le doigt quand un élève parle sans permission (rappel subtil) pour rappeler la règle. Pas de problèmes de discipline particuliers.

* **Productivité \: (= + + +) \: 6**. Lance de courtes activités (prod. d'écrit ; calcul mental ; écriture de nombres), que les élèves s'attachent à réaliser (même si la tâche d'écriture traîne un peu en longueur). Les élèves savent quoi faire et sont peu distraits. Le matériel est préparé. Quelques interruptions (cantine) qui gênent le cours de l'activité. Le matériel est prêt.

* **Modalités d’apprentissage (= = + -) \: 3**. Lance l'appel sans transition avec le compte-rendu des phrases sur la girafe, les élèves sont assez impliqués tout au long de la séance ; lance le calcul mental sans prévenir. La taille du modèle pour écrire "girafe" est très petite. Travail centré sur l'ardoise sans autre support proposé. Pas de buts énoncés, ni de points sur le travail.

---
# 3. Proposition de cotation vidéo "CE1 Calcul mental” (3/3)

<!-- _class: t-80 -->

* **Développement de concepts (– = – –) \: 2**. Quelques appels à la créativité : que pourrait faire la girafe ? Un seul calcul de double alors qu'il aurait été sans doute nécessaire d'approfondir. Une évocation de travail fait "toute la semaine on a travaillé sur les doubles".
	
* **Qualité de la rétroaction (— = — — –) \: 2**. Peu ou pas de rétroactions autres que "Ok". Peu de rétroactions en boucles, qui auraient pu être faites lors des phrases sur la girafe (un échange lors de l'utilisation de doubles). Quelques rares étayages et un appel à explication (utilisation de doubles). Ne vérifie que peu les productions des élèves (écriture de nombres) pour donner des rétroactions pertinentes. Dit ce qu'il faut écrire à l'élève au tableau (date) plutôt de le lui faire trouver.
	
* **Modelage Langagier \: ( – – – - +) \: 2**. Attentive aux "belles phrases" ("je mange à la cantine"" ; "aujourd'hui nous sommes le"), "quantième jour". Quelques rares  répétitions/extensions. Pas d'analyse dizaines-unités lors de l'écriture de nombres. Très peu d'extensions et de parallel talk. Un self-talk-autoverbalisation (comptage).

-->
---
# 3. M1 - Quizz

---
# 1. Question dimension 1

Les interactions enseignant-élèves et les activités de la classe sont centrées sur leurs intérêts, leur motivation, tout en encourageant leur autonomie et responsabilité

R1 : Considération du point de vue des élèves (PVE)
R2 : Sensibilité de l'enseignant
R3 : Climat positif

---
# 1. Réponse dimension 1

**Considération du point de vue des élèves (PVE)** (Domaine soutien émo :heartpulse:)

:mag: non-rigidité dans l’organisation du cours, choix offerts, responsabilités données, expression des élèves suscitée, mouvements dans la classe pas trop contraints

---
# 1. Question dimension 2

La gestion du temps disponible pour l’apprentissage, sans perte de temps, que ce soit dans le cours des séances ou les transitions

R1 : Modalités d'apprentissage
R2 : Gestion des comportements
R3 : Productivité

--- 
# 1. Réponse dimension 2

 **Productivité (P)** (Domaine Org. de la classe) :school: 

:mag: consignes claires, élèves engagés dans le travail, le matériel de l’enseignant et des élèves est prêt

---
# 1. Question dimension 3

Offrir des rétroactions aux élèves qui peuvent ainsi mieux apprendre et participer (suite à une production d’un élève).

R1 : Prise en considération du point de vue de l'enfant
R2 : Qualité des rétroactions 
R3 : Développement conceptuel

---
# 1. Réponse dimension 3

**Qualité des rétroactions** (QR)(Domaine Soutien à l’apprentissage) :microscope:

:question: Offrir des rétroactions aux élèves qui peuvent ainsi mieux apprendre et participer (suite à une production d’un E).

:mag: stimuler la pensée, engager les élèves dans des boucles de rétroactions, fournir de l’information supplémentaire, encourager et faire s’affirmer les E dans leurs productions, en leur procurant des rétroactions signifiantes (pas seulement « c’est bien ! »)

---
# 1. Question dimension 4

Reflète le lien émotionnel entre l’enseignant et les élèves (et celui des élèves entre eux), ainsi que la chaleur, le respect, et le plaisir, communiqués dans les interactions verbales et non-verbales.

R1 : Sensibilité de l'enseignant (SE)
R2 : Climat négatif (C-)
R3 : Climat positif (C+)

---
#  1. Réponse dimension 4

Climat positif (C+)(Domaine Soutien émo) :heartpulse:

:question: Reflète le lien émotionnel entre l’enseignant et les élèves (et celui des élèves entre eux), ainsi que la chaleur, le respect, et le plaisir, communiqués dans les interactions verbales et non-verbales.

:mag: sourires, proximité physique de M avec E, enthousiasme, paroles respectueuses

---
# 1. Question dimension 5

:question: Maximisation de l’intérêt des E, leur engagement, et leur capacité à apprendre aux cours des leçons et activités offertes.

R1 : Développement conceptuel
R2 : Modalités d’apprentissage 
R3 : Qualité de la rétroaction

---
# 1. Réponse dimension 5

Réponse : **Modalités d’apprentissage** (MA)(Domaine Organisation de la classe) :school:

:question: maximisation de l’intérêt des E, leur engagement, et leur capacité à apprendre aux cours des leçons et activités offertes.

:mag: richesse et diversité des matériels proposés, intérêt des élèves, l’enseignant résume, explique, réoriente, accompagne l’activité des élèves

---
# 1. Question dimension 6

:question: Reflète le niveau de négativité (irritation, colère, agressivité) exprimé dans la classe.

R1 : Productivité
R2 : Climat positif
R3 : Climat négatif

---
# 1. Réponse dimension 6

**Climat négatif** (C–)(Domaine Soutien émo) :heartpulse:

:question: reflète le niveau de négativité (irritation, colère, agressivité) exprimé dans la classe.

:mag: ton de voix négatif de M et E (ennui, dureté, sarcasme), menaces, cris, humiliation, victimisation d’E, punitions sévères, bagarres entre élèves

---
# 1. Question dimension 7

L’utilisation par l’enseignant de stratégies diverses (discussions, activités) pouvant amener ses élèves à développer une pensée plus élaborée, une meilleure compréhension des phénomènes de son environnement, plutôt que de favoriser un apprentissage par cœur

R1 : Modelage langagier
R2 : Modalités d'apprentissage
R3 : Développement conceptuel

---
# 1. Réponse question 7

**Développement conceptuel** (DC)(Domaine Soutien à l’apprentissage) :microscope:

:question: l’utilisation par l’enseignant de stratégies diverses (discussions, activités) pouvant amener ses élèves à développer une pensée plus élaborée, une meilleure compréhension des phénomènes de son environnement, plutôt que de favoriser un apprentissage par cœur

:mag: sollicitation par des questions amenant des réflexions de haut niveau plutôt que des faits et des réponses par oui/non, ou factuelles, intégration de notions antérieures, de la vie « réelle », créativité

---
# 1. Question dimension 8

Attention aux besoins émotionnels et académiques des élèves et réponse efficace à ces besoins.

R1: Sensibilité de l'enseignant
R2: Qualité de la rétroaction
R3: Prise en considération du point de vue de l'enfant

---
# 1. Réponse dimension 8

**Sensibilité de l’enseignant** (SE)(Domaine Soutien émo) :heartpulse:

:question: attention aux besoins émotionnels et académiques des élèves et réponse efficace à ces besoins.

:mag: M circule dans les rangs, répond aux questions et aide, E participent volontiers aux activités, prennent des risques

---
# 1. Question dimension 9

Établir des attentes comportementales claires et à utiliser des méthodes efficaces pour prévenir et rediriger les comportements inappropriés

R1: Productivité
R2: Gestion des comportements
R3: Climat positif

---
# 1. Réponse dimension 9

**Gestion des comportements** (GC)(Domaine Organisation de la classe) :school: 

:question:  établir des attentes comportementales claires et à utiliser des méthodes efficaces pour prévenir et rediriger les comportements inappropriés

:mag: attentes comportementales claires, proactivité, redirection efficace, comportement des élèves non problématique

:warning: Attribuer un haut niveau en l’absence de marqueurs : cela signifie que les élèves ont intégré ces aspects

---
# 1. Question dimension 10

Efficacité et de la quantité des stratégies de stimulation du langage utilisées par l’enseignant (initiées par lui)

R1: Développement de concepts
R2: Qualité des rétroactions
R3: Modelage langagier

---
# 1. Réponse dimension 10

**Modelage langagier** (ML)(Domaine Soutien à l’apprentissage) :microscope:

:question: efficacité et de la quantité des stratégies de stimulation du langage utilisées par l’enseignant (initiées par lui)

:mag: conversations fréquentes, questions ouvertes, répétition/extension, *self-parallel talk*, niveau de langage élaboré

---
# M1
- Discussion sur le projet de problématique du mémoire

---
# M1 Pour la prochaine séance (plage non encore déterminée)
## Choses à faire courant janvier 2025 

1. Description du contexte 
2. Réponse au questionnaire de compréhension du CLASS

---
# M1 - Questionnaire de compréhension du CLASS

Envoyer au formateur, **avant le 22 décembre 2024**, les réponses argumentées aux questions suivantes :

- Qu’est-ce que j’ai appris (synthétiser environ 5 points qui vous semblent essentiels)
- Qu’est-ce qu’il me reste encore à comprendre sur le CLASS ?
- Quelles sont les dimensions avec lesquelles je suis le plus à l’aise ? Celles qui me paraissent le plus difficiles à comprendre et à mettre en place dans ma classe ?

---
<!--
# 4. Séance du 24/01/24

Ordre du jour :
- point sur le contexte de classe
- point sur la problématique, les questions de recherche, les hypothèses
- point sur l'organisation de la suite
---
# 4. M1 - Pour commencer : la description du contexte

La description du contexte de vos écoles (env. 1 page) figurera au début du mémoire

- niveau, nombre d’élèves, matières enseignées
- taille de l’école, autres spécificités, notamment matérielles et humaines (ATSEM),
- PCS (professions et catégorie socioprofessionnelles) majoritaires des parents (IPS trouvable dans cette [carte](https://www.data.gouv.fr/fr/reuses/cartographie-de-lindice-de-position-sociale-des-ecoles-et-colleges-france-metropolitaine-et-drom/)),
- difficultés particulières de la classe, des élèves
- toute autre information permettant de mieux comprendre le contexte (matériel spécifique, emplacement de la classe, etc.)
- :warning: **attention à préserver l’anonymat de l’école et de la classe**




---
# 4. Ces 2 documents à rendre au plus tôt


  
---
# 2 M1 - Récapitulatif - 1/2

 **Problème** : Issu de votre description du contexte. Par exemple : Certains élèves de PS ne parviennent pas à prendre la parole en groupe
- **Problématique** : Décrit mieux le problème et l'une de ses possibles solutions. Par exemple : Les rétroactions langagières de l'enseignant permettraient d'aider à la prise de parole des petit-parleurs de PS.
- **Question de recherche** : Reformule la problématique en C=F(S,E). Quelles rétroactions langagières de l'enseignant (E) pourraient rendre des élèves petit-parleurs de PS (S) plus participatifs (C) dans des séances de langage ?

---
# 4. M1 - Récapitulatif - 2/2 

- **Hypothèses** : Question de recherche dérivée en plusieurs sous-questions, formulées en prédictions plus précises et observables : 
	- **H1** - Les rétroactions langagières de l'enseignant vont permettre aux élèves petit-parleurs de s'exprimer plus souvent ; 
	- **H2** - Les rétroactions langagières de l'enseignant vont permettre aux élèves petit-parleurs de s'exprimer avec un meilleur niveau de langage.
- Possibilité de faire intervenir une autre variable qui pourrait avoir une influence dans le processus étudié. Par exemple : avoir plusieurs interventions différentes au lieu d'une seule ; ou une variable liée aux élèves : p. ex., leur niveau d'engagement dans les séances.

---
# 4. Plus d'informations

Cette [page](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/tuto-problematique.html)

---
# 4. Quelques sujets traités les années antérieures  (1/3)

<!-- _class: t-60 --> 

Voir cette [page](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/class/syl_CLASS_espe.html)

## Pratiques d'enseignement/IEE
- Effet du style motivationnel de l’enseignant sur la motivation des élèves
- La communication avec les parents à propos du comportement de leurs enfants a-t-elle un effet sur ce dernier ?
- Adapter ses pratiques (structurer, expliciter, laisser du choix) pour favoriser la motivation des élèves.
- Réguler les comportements difficiles des élèves par la proaction.
- Le modelage langagier de l’enseignant pour aider l’entrée en communication de PS.
- Les effets de la directivité conversationnelle de l’enseignant sur l’expression des élèves.
- Les rétroactions de l’enseignant pour aider les élèves petit-parleurs.
- Climat positif et bien-être à l’école
- Étude des interactions enseignant-élèves pour un climat de classe positif


---
# 4. Sujets (2/3)

## Interventions

- Apprendre à reconnaître les émotions de base à l’EM
- Evaluation de la méthode “*Freedom Writers*” 
- La méthode ATOLE pour favoriser l’attention d’élèves de CP
- Le yoga en maternelle pour favoriser l’engagement des élèves
- Lutter contre les stéréotypes de genre à l’EM
- Jeux et littérature de jeunesse pour le développement langagier d’élèves de maternelle

---
# 4. Sujets  (3/3)

<!-- _class: t-60 --> 

## Matériel et méthodes
- Le plan de travail et l’auto-régulation des élèves
- Effets d’un plan de travail sur l’autonomie des élèves
- Les effets d’outils de régulation explicite du comportement des élèves
- “L’album écho” pour favoriser la production orale d’élèves en difficulté.
- Le jeu pour favoriser l’autorégulation comportementale d’élèves de PS.
- Etablir des règles de productivité pour réguler les comportements des élèves
- Effet de l’espace sur le bien-être des élèves.
- La régulation des émotions par les messages clairs, la boîte du calme.
- Influence du tutorat entre pairs sur le sentiment d’auto-efficacité
- La pédagogie de l’écoute en question
- La régulation de la parole à l’aide de l’outil porte-clés
- Les carnets de défis : un outil pour soutenir l’auto-régulation
- Engagement comportemental des élèves pendant les moments de regroupement
- Effet du port du masque sur les interactions enseignant-élèves
- Pédagogie de projet pour favoriser l’engagement des élèves

---
# 4. M1 - Agenda prévisionnel



	    ┌────────┐                     ┌───────────┐   ┌ ─ ─ ─ ─ ─ ┐
	    │Pré-test│                     │Post-test 1│    Post-test 2
	    └────────┘                     └───────────┘   └ ─ ─ ─ ─ ─ ┘
	        ■                                ■               ■
	        │                                │
	        │      ┌───────────────────┐     │               │
	        │      │ Interventions ≥ 3 │     │
	        │      └───────────────────┘     │               │
	        │                                │
	        │                                │               │
	        │                                │
	────────┴────────────────────────────────┴───────────────┴─────────▶

	  Début mars                     Début avril        Fin avril



---
# 4. Les types de recherche possibles

- Étude documentaire (collecter et analyser des documents pour répondre à une problématique donnée) : e.g., l'évolution de la pédagogie de projet de 1970 à nos jours, en école maternelle
- Étude interventionnelle avec/sans groupe-contrôle : e.g., étudier l'insertion d'une pratique pédagogique entre 2 mesures de performances, avec ou sans recourir à un groupe de comparaison dans lequel la pratique n'est pas insérée
- Étude avec protocole à cas unique : voir ensuite.

---
# 4. Les protocoles de recherche à cas unique

- voir plus d'informations dans ce [document](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/proto-single-case.html)

---
# 4. Présentation
- Voici le lien vers la [présentation](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides/UE-24-option-rech-3.pdf) sur ce type de protocoles
  

---
# 4. Pour la prochaine séance (du 7/02/24)

1. Envoyer au formateur le descriptif du contexte
2. Commencer à formuler le problème de recherche, la problématique, et, pour suivre, les hypothèses


<!--
---
# 2. - M1 Travail pour la prochaine séance

:warning: dates des prochaines séances à déterminer

- Finaliser la problématique, la ou les question(s) de recherche, les hypothèses ( -> envoi au formateur)
- Concevoir un premier jet de l'intervention (-> envoi au formateur)
- Commencer la revue de la question (après envoi d'un plan général au formateur courant février)

---
# 1 M1 - Travail à faire pour la prochaine séance

- Visionner la [vidéo](https://videos.univ-grenoble-alpes.fr/video/25619-formuler-un-probleme-de-recherche-memoire-meef-interactions-enseignants-eleve/) présentant une méthode pour déterminer une problématique, une ou des question.s de recherche, et des hypothèses.
- La lecture de ce [document](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/tuto-problematique.html) sur la problématique pourra aider
- Rédiger la présentation de votre contexte de classe (voir diapo précédente).
- Envoyer ces 2 documents au formateur pour commentaires avant le **20 janvier 2023**.




---
# 3. M1 - Séance du 8/02/23
##  Travail pour la prochaine séance (le 22/02/23 16:00-18:00)

:heavy_check_mark: Contexte 
:heavy_check_mark: Problématique, question de recherche, hypothèses
:construction: Évaluation du "niveau de base" dans votre classe
:construction: Finalisation de l'intervention, pour la passer dans les semaines suivantes (pendant env. 3 semaines)
⏭️ Plan de l'état de l'art
⏭️ État de l'art
  
---

# 4. M1 - Séance du 22/02/23
##  Travail pour la prochaine séance (le 15/03/23 13:45-15:45)

:heavy_check_mark: Contexte 
:heavy_check_mark: Problématique, question de recherche, hypothèses
:heavy_check_mark: Évaluation du "niveau de base" dans votre classe
:heavy_check_mark: Finalisation de l'intervention, pour la passer avant les vacances de printemps (pendant env. 3 semaines)
:construction: Plan de l'état de l'art
:construction: État de l'art

---
# 5. M1 - Séance du 15/03/23
##  Travail pour la prochaine séance (le 5/04/23 10:15-12:15)

:heavy_check_mark: Contexte 
:heavy_check_mark: Problématique, question de recherche, hypothèses
:heavy_check_mark: Évaluation du "niveau de base" dans votre classe
:heavy_check_mark: Finalisation de l'intervention, pour la passer avant les vacances de printemps (pendant env. 3 semaines)
:heavy_check_mark: Plan de l'état de l'art
:heavy_check_mark: État de l'art
:heavy_check_mark: Expérimentation


---
# 6 M1 - Séance du 5/04/23 Travail pour finaliser le mémoire

- :warning: La séance du 19/04 sera remplacée par une visio-conférence ou un rendez-vous en présence, par binômes (à solliciter au formateur)
- Les sections à livrer : la rédaction de la partie théorique, de la problématique et de la partie méthode du mémoire (sans les résultats et la discussion)
- **La version quasi-finale** du travail est à livrer pour le **26 mai**, pour relecture complète par le formateur
- Date de livraison pour évaluation dans le cadre du M1 (une note de CC sera donnée par binôme, avec des commentaires) : **le 12 mai 2023**
- :warning: Il est vivement conseillé de faire votre possible pour terminer la rédaction complète de votre mémoire d'ici à début septembre 2023


<!--
# :warning: **Attention, les diapositives qui suivent n'ont pas été mises à jour**

--- 
# M1&M2 - Séance du 16 février 2022

- M1 : avez-vous des questions sur la vidéo ?

---
# M1&M2 Récapitulatif (1/2)
- **Problème** : Issu de votre description du contexte. Par exemple : Certains élèves de PS ne parviennent pas à prendre la parole en groupe
- **Problématique** : Décrit mieux le problème et l'une de ses possibles solutions. Par exemple : Les rétroactions langagières de l'enseignant permettraient d'aider à la prise de parole des petit-parleurs de PS.
- **Question de recherche** : Reformule la problématique en C=F(S,E). Quelles rétroactions langagières de l'enseignant (E) pourraient rendre des élèves petit-parleurs de PS (S) plus participatifs (C) dans des séances de langage ?



---
# :warning:
Ce qui suit est destiné aux M2

---
# Séance M2 PE : Point sur la soutenance (début janvier 2024)

---
# Buts et ressources

Buts : 
- point sur la soutenance orale

Site Moodle du mémoire :  https://frama.link/UE406
[Site Inspé](https://inspe.univ-grenoble-alpes.fr/formations/professorat-des-ecoles-/le-memoire-master-meef-pe-252524.kjsp?RH=1498479210386)

---
# Dépôt du mémoire

- la date-butoir de dépôt : **8 janvier 2024**
- soutenance le **16 janvier 2024**
- l’envoi par le dispositif demandé (Moodle e-formation, site dépôt des mémoires) est le seul légal. Le formateur encadrant enverra les liens des mémoires aux jurés.
- doubler par un envoi du mémoire par courriel à l'encadrant
- pas de version sur papier nécessaire pour le jury

---
# Mémoire : Finalisation de l’écriture (1/3)

- S’assurer qu’aucun élève/parent ne puisse être reconnu, même anonymé. Attention aux jugements que vous portez
- S’assurer que le mémoire correspond aux documents de cadrage (Présentation générale et Guide de rédaction)
- Présentez vos résultats en lien avec vos hypothèses, **et seulement eux**. Essayez de trouver une explication pour celle.s non validée.s
- N'oubliez pas que les 1re et 4e de couverture ont des modèles à respecter (voir site e-formation)

---
# Mémoire : Finalisation de l’écriture (2/3)

- Référer, notamment en discussion, au programmes et au référentiel de compétences des enseignants en discussion (dire la ou les compétences favorisées tout au long du travail sur le mémoire)
- Prendre notamment soin de vérifier que la page de couverture contient les logos nécessaires, et que la 4e de couv. contient le résumé et les mots-clés
- Le résumé en anglais peut être produit (et vérifié ensuite) par [deepl](https://www.deepl.com/translator), le meilleur traducteur gratuit

---
# Mémoire : Finalisation de l’écriture (3/3)

- Le formulaire de non-plagiat est à remplir lors du dépôt du mémoire 
- Déposer séparément l'accord de dépôt du mémoire/ESR sur Dumas https://cloud.univ-grenoble-alpes.fr/index.php/s/goLQ4Tws944DqXr
- voir grille d'évaluation dans le site e-formation


---
# Mémoire : Principaux écueils (écrit) (1/2)

- Parties plagiées (:warning: tous les mémoires seront vérifiés avec Compilatio et leurs rapports envoyés pour vérification)
- Maîtrise insuffisante de la langue française (faire relire le mémoire par des tiers)
- Pas de problématique claire
- Pas de liens (ou liens insuffisants) entre partie théorique et empirique 

---
# Mémoire : Principaux écueils (écrit) (2/2)

- Partie empirique peu claire (on ne comprend pas ce que vous avez réalisé vous-même)
- Partie empirique non analysée
- Analyse peu fouillée des données recueillies
- Analyse réflexive faible, manque de recul
- Texte très court, mal documenté ; texte trop long et verbeux, explications embrouillées
- Pas de respect de la vie privée et/ou du droit à l’image (photos où l'on peut reconnaître des personnes, prénoms d'élèves non anonymés)

---
# Soutenance : Rappel des conditions / 1. Présentation (1/2)

(voir doc. Cadrage mémoire)
La soutenance (20 min) est individuelle (même quand ESR faits entièrement à 2, DU). Elle comporte deux parties :

- Une présentation (**10 min**) focalisée sur des aspects choisis du mémoire. Elle met en évidence le cheminement professionnel et souligne les aspects les plus formateurs du travail. La présentation orale s’appuie sur des supports de communication.

---
# Soutenance : Rappel des conditions  / 2. Discussion (1/2)
(voir doc. Cadrage mémoire)
- Une discussion avec le jury (**10 min**), au cours de laquelle l’étudiant témoigne de son appropriation de la problématique, de sa maîtrise du sujet et d’une prise de recul.
- La soutenance étant publique, vous pouvez inviter des personnes (étudiants de l’atelier, tuteur/trices, parents, amis, etc.) à venir écouter (ils n'interviendront pas)

---
# Soutenance : Rappel des conditions (voir doc. Cadrage mémoire) / 2. Discussion (2/2)

- Quand ils ne font pas partie du jury, les professionnels de terrain sont invités à la soutenance mais ils ne participent pas aux délibérations. 
- Note d'écrit compte pour 65 % de la note finale, la note d'oral compte pour 35 %

---
# Conseils pour la soutenance (1/2)

- Avoir lu la grille d'évaluation du mémoire, vérifier que votre travail répond aux différents critères
- Respecter **scrupuleusement** le temps (le jury peut vous arrêter une fois le temps écoulé), donc, **répéter**
- Réaliser des diapositives **lisibles**, sans animation, et en nombre réduit (une dizaine max.). Ne pas copier-coller de tableaux ou images qui seront illisibles
- (Si soutenance en présence, venir avec une clé USB et la présentation en PDF (pour gagner du temps) : l'ordinateur est fourni)

---
# Conseils pour la soutenance (2/2)

- Le jury a lu le mémoire, donc lui rappeler les éléments essentiels (d’un point de vue théorique et empirique) ; insister sur les aspects liés au métier d’enseignant, ce qui a été appris en faisant ce mémoire
- Le jury ne cherche pas à vous tendre des pièges : il veut plutôt comprendre votre démarche, ce que vous avez fait et pourquoi. Répondez clairement et le plus **succinctement** possible à ses questions

---
# Écueils pendant la soutenance

- Lire les diapositives sans ajouter de plus-value (effet karaoké)
- Présenter des copies d’écran du mémoire illisibles (“vous ne pouvez pas lire cette diapo, mais...”)
- Présenter des diapositives trop chargées (“ce qui est important ici est ceci...”)
- Se perdre dans des anecdotes, ou dans la théorie

---
# Grille d’évaluation (résumé *cf.* la grille complète)

- Ecrit
	- Se former et innover par la rédaction d’un mémoire professionnel
	- Maîtriser la langue française
- Oral
	- Exposer d’une manière critique l’analyse d’une pratique pro.
	- Communiquer efficacement à l’oral
- Note modulée en fonction de l’implication de l’étudiant (avis de l’encadrant)

---
# Valorisation

- Les meilleurs mémoires feront l’objet d’une présentation orale lors de la Journée de l’école organisée en fin d’année, le **date à venir** à l'Inspé de Chambéry sur proposition de leurs encadrants

---
# Après le mémoire

- Les mémoires ayant eu une note d’au moins **16/20** sont archivés publiquement (base DUMAS), après accord de leurs auteurs
- :warning: vous pouvez être démarché.es par des éditeurs à compte d’auteur. N’accepter l’édition qu’après avoir lu de près le contrat (notamment en ce qui concerne l’achat d’exemplaires et la cession des droits). Sachez que ce type d'édition ne compte pas ou peu pour un éventuel projet de carrière universitaire.

---
# Des questions ? 

- N'hésitez pas à les poser par courriel à votre formateur
