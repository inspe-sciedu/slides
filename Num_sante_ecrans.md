---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# 
# Écrans et santé

Christophe Charroud - UGA

<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2024 - CC:BY-NC-SA -->

 
---

# 
# :zero: Préambule 

 
### À votre avis, utiliser des écrans c'est :
* Dangereux pour votre santé et négatif pour vos comportements.
* Ça n'a pas d'effet sur votre santé et ça ne crée pas de problème de comportement.

---

# Vous êtes-vous appuyés sur des arguments scientifiques, ou sur ce que vous entendez tous les jours ?

#
### Pourquoi une telle question?


---
# 
## « Il y a une rupture entre connaissance commune et connaissance scientifique »

> Bachelard : le matérialisme rationnel, 1953

### Il y a souvent une grande différence entre ce que dit la science et ce qu'on entend dans les médias, dans la rue, lors de discussions...

---
## Au programme
### Les écrans, coupables désignés...souvent par les médias


1 - Écrans et problèmes physiques
2 - Écrans et sommeil
3 - Écrans et développement du cerveau
4 - Écrans et problèmes de comportement
5 - Écrans et addictions



---
# :one: Écrans et problèmes physiques
## Des effets physiologiques connus

* 	Sédentarité  :arrow_right: Limitation du temps d'écran = activité physique (Pedersen, 2022)
    * Si vous ne bougez pas assez, vous risquez de grâves problèmes de santé.
* 	Problèmes ophtalmiques : à surveiller (Aptel & Charroud, 2017)
    * Préservez votre vue!
*   Text Neck Syndrome (David & al., 2021) et autres TMS (Charroud, 2023)
    * Pensez à vos cervicales...



---

# :two: Écrans et sommeil
## Un impact important

* 	Sommeil : un vrai problème pour l'école (Bioulac, Baillieul, Charroud, 2023).


---
### Régulation du sommeil
#### Le processus circadien
![w:600](images/img-sante/circamela.jpg)

#### :arrow_right: « Je dors car il est l'heure de dormir »

---
## Pourquoi dormons-nous ?
* Récupérer
* Croissance
* Régénération organique tissulaire et cellulaire 
* Maintenir un métabolisme sain
* Résistance aux infections
* Régulation des émotions
* Maturation cérébrale/ Développement 
* Apprendre / consolidation mémoire

---
## Que ce passe-t-il si on ne dort pas assez (souvent à cause des écrans)?
* Céphalées matinales
* Hypersialorrhée,bouche sèche 
* Trouble de la concentration
* Irritabilité
* Agitation chez l’ado
* **Altération du développement du cerveau**

---
# :three: Écrans et développement du cerveau

**Avant 3 ans**
*   Ecrans passifs = manque de communication :arrow_right: difficultés de langage (Hutton & al., 2020).

**Après 3 ans**
* Manque de sommeil = altréation du développement du cerveau.
* Des effets positifs reconnus (Fischer, 2023) :arrow_right: Etude Elfe.

---
## 3.1 Écrans le soir (manque de sommeil) et développement du cerveau
<!-- _class: t-80 -->

Mesures en imagerie cérébrale 
![w:300](images/img-sante/urrilaconclusions.jpg)

En cas de durée plus courte passée au lit pendant les jours de semaine et d'horaires de sommeil plus tardif le WE (classique...) la recherche constate :
* Une diminution de volume de matière grise dans le cortex frontal et le cortex cingulaire;
* Ce qui implique de mauvais résultats scolaires associés.
*(Urrila & al., 2017)*

---
## 3.2 Écrans et développement du cerveau
### Des liens non avérés 

*   TDA/H : pas seulement négatif (Bioulac, Charroud, Pellencq, 2022).
*   TSA : un renforcement potentiel **mais pas une cause démontrée** (Lin & al., 2022)(Ophir & al. ,2023).

**Les écrans ne semblent pas la cause de ces troubles!**

---
# :four: Écrans et problèmes de comportement

* 	Des effets psychologiques et sociaux (Desmurget, 2019) ... mais simplement amplifiés par le numérique (Sauce, Liebherr, Judd & Klingberg, 2022).

### :arrow_right: question à laquelle il est difficile de répondre 

---
## 4.1 Pourquoi il est difficile de répondre ?

Les études sur le temps passé par les adolescents sur le numérique manquent de fiabilité (Orben & Przybylski 2019).

- on perçoit difficilement le temps passé dans toute activité, surtout si elle est prenante.
- on a beaucoup de raisons personnelles de distordre ces données (biais de désirabilité), les grands utilisateurs les sous-estiment, les faibles utilisateurs les sur-estiment.

---
## 4.2 Liens utilisation des écrans et réussite scolaire

Une méta-analyse de plus de 5 500 études (480 000  participants de 4-18 ans), d'Adelantado-Renau et al. 2019) montre :

* qu'il n'y a pas de lien entre temps d'écran (qq soit l'écran) et réussite scolaire;
* que l'usage de la TV et des jeux vidéo sont négativement associés à la réussite scolaire.

Le contexte d'utilisation de l'écran, le type d'écran, l'activité (ce qui est favorisé/gêné), jouent un rôle important qu'il faut analyser avant de se prononcer sur l'effet d'un écran.


---
## 4.3 Liens utilisation des écrans et bien-être

Une étude à large échelle (+11 000) sur des adolescents et avec rapports quotidiens du temps d'utilisation montre un effet négatif mais très faible (Orben & Przybylski 2019) :

- il faudrait un temps d'usage (extrapolé) de 63 h par jour pour que les ados baissent leur bien-être d'un demi écart-type (valeur considérée comme perceptible).

---
## 4.4 Cyberharcèlement (Blaya 2018)

- Violence hors ligne :left_right_arrow: violence en ligne
- *Anonymat amplifié* : limite les niveaux d’empathie des agresseurs
- *Dissémination démultipliée*, diffusion instantanée, pas de répit pour les victimes ; pas de maîtrise sur la diffusion ; nombre de témoins potentiels illimités
- *Information persistante* : Contrairement au harcèlement, pas de nécessité de répétition de l'acte pour parler de cyberharcèlement

**L’outil numérique a un effet loupe, il est plus le symptôme de comportements inadéquats que leur cause et le contexte est un facteur-clé.**


---
# :five: Écrans et addictions
#
### Etes-vous addicts aux écrans ?

:arrow_right: Test issu du DSM-5 et du programme SANPSY

Test silencieux, ne communiquez pas vos resultats.
Chaque fois que vous repondrez oui, comptez un point.


---
## 5.1 Test 1/3

•	**Préoccupation** : Passez-vous beaucoup de temps à penser aux écrans, y compris quand vous n'en utilisez pas, ou à prévoir quand vous pourrez en utiliser à nouveau ?

•	**Sevrage** : Lorsque vous tentez d'utiliser moins d'écrans ou de ne plus en utiliser, ou lorsque vous n'êtes pas en mesure d'utiliser d'écran, vous sentez-vous agité, irritable, d'humeur changeante, anxieux ou triste ?

•	**Tolérance** : Ressentez-vous le besoin d'utiliser des écrans plus longtemps, d'utiliser des écrans plus excitants, ou d'utiliser du matériel informatique plus puissant, pour atteindre le même état d'excitation qu'auparavant ?

---
## 5.2 Test 2/3

•	**Perte de contrôle** : Avez-vous l'impression que vous devriez utiliser moins d'écrans, mais que vous n'arrivez pas à réduire votre temps d'écran ?

•	**Perte d'intérêt** : Avez-vous perdu l'intérêt ou réduit votre participation à d'autres activités (temps pour vos loisirs, vos amis) à cause des écrans ?

•	**Poursuite malgré des problèmes** : Avez-vous continué à utiliser des écrans, tout en sachant que cela entrainait chez vous des problèmes tels que ne pas dormir assez, être en retard au collège, se disputer, négliger des choses importantes à faire...) ?

---
## 5.3 Test 3/3

•	**Mentir, dissimuler** : Vous arrive-t-il de cacher aux autres, votre famille, vos amis, à quel point vous utilisez des écrans, ou de leur mentir à propos de vos habitudes d'écrans ?

•	**Soulager une humeur négative** : Avez-vous utilisé des écrans pour échapper à des problèmes personnels, ou pour soulager une humeur indésirable (sentiments de culpabilité, d'anxiété, de dépression) ?

•	**Risquer ou perdre des relations ou des opportunités** Avez-vous mis en danger ou perdu une relation amicale ou affective importante, des possibilités d'étude à cause des écrans ?

---
## 5.4 Bilan du test

Si vous avez un **score égal à 5 points ou plus** alors votre comportement sera médicalement qualifié « d'addiction aux écrans ».

L'addiction est une maladie qui peut être prise en charge.

---
## 5.5 L'addiction aux écrans est-elle courante ?

* Les études actuelles montrent que **l'addiction au sens médical est relativement rare** parmi les adolescents et les adulte, elle concernerait  1,7% de la population.

* Mais 44,7% de la population présenterait au moins l'un des neuf critères recherchés :arrow_right: **usagers avec problèmes, mais sans addiction.**

(Alexandre, Auriacombe, Boudard, 2022)


---
# Bilan 
* Il est difficile de trancher radicalement quant à la nécessité ou non d’éviter d’exposer les enfant/ados aux écrans, (Borst, 2019)
* Ce qui est important :
    * Le **temps d'exposition** au détriment du reste (bouger, échanger, se sociabiliser)
    * Relation négative entre le temps d’exposition et le développement, mais cette relation n’est pas vraie pour tous les domaines de la cognition. (Kamga Fogno & al., 2023)(Milcent & al., 2023).
    * Des effets positifs reconnus (Fischer, 2023) :arrow_right: Etude Elfe.

**Tout dépend des usages** 


---
# Conclusions et recommandations

* Essayez de ne pas utiliser d'écran avant d'aller dormir :arrow_right: meilleur sommeil :arrow_right: meilleurs comportements :arrow_right: meilleure santé.

* Essayez de diminuer votre temps d'écran en journée afin de vous libérer du temps pour :
    * Bouger
    * Echanger, discuter, avoir des relations sociales
    * et vous ennuyer... c'est très bon pour l'imagination.


---

# Merci pour votre attention

