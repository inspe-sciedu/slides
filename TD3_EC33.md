---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# Culture numérique et apprentissages
## TD 3
 
Christophe Charroud - UGA
 
<!-- page_number: true -->
<!-- footer: christophe.charroud@univ-grenoble-alpes.fr - 2023 - 2024 - CC:BY-NC-SA -->

---
# :zero: Rappels
* TD 1 : Mise en application des apports théoriques sur les effets du numériques sur les apprentissages - Début de la conception :arrow_right: avoir une première proposition de situation d'apprentissage.
* TD 2 : Mise en application des apports théoriques sur les ressources et les aspects juridiques et éthiques - Poursuite de la conception
* **TD 3 : Apports pratiques sur la formation AU numérique et l'évaluation des élèves en référence au CRCN - découverte de PIX et PIX+EDU - Poursuite de la conception**
* TD 4 : Finalisation de la conception. :arrow_right: Évalué cette année  
* TD 5 : Retour d'expérience pour celles et ceux qui auront réalisé une mise en oeuvre. :arrow_right: PIX+EDU


---
# :one: Évaluer au numérique
Le numérique, des compétences à évaluer
* Un cadre de référence : [CRCN](https://eduscol.education.fr/721/evaluer-et-certifier-les-competences-numeriques)
* S’applique à tous les niveaux de l’école
* :warning: Aucun niveau exigé par l'institution mais scruté par certains employeurs
* Un outil d’évaluation national : [PIX](https://pix.fr)
* Auto-évaluation des élèves
* Auto-formation (tutoriels)
* Certification d’un niveau d’acquisition : 3ème, Terminale, Université.

---
# 
## PIX : Évaluation et certification de compétences d'usages des technologies numérique.
#

### Allez sur le site pix.fr

---
# Évaluer au numérique : PIX
## :pencil2: TP - Découverte de PIX 
* Si vous n'en n'avez pas encore, créez-vous un compte sur pix.fr :warning: si possible avec une adresse @ac-grenoble.fr
#
***Référentiel de formation : CRCN***
**:arrow_right: Public cible : élèves (école-collège-lycée), et étudiants 1er cycle (licence)**

---
# PIX+EDU
## :pencil2: TP - Découverte de PIX+EDU

* Connectez vous à la campagne PIX+EDU (voir cours sur eformation)
* Effectuez quelques tests
#
***Référentiel de formation : CRCNé***
**:arrow_right: Public cible : étudiants MEEF, stagiaires, enseignants en poste**

---
# :two: Conception - lien avec le CRCN
* Dans la situation que vous avez sélectionné, essayez d'identifier si vous couvrez des compétences du CRCN avec les élèves.
* Continuez le travail de conception

---
# Description de la situation

Vous devez décrire la situation telle qu'elle a été conçue, ou telle qu'elle le sera si vous aviez à la mettre en œuvre. 

#### Eléments attendus dans la description

```
- Domaine(s) d’enseignement concerné(s) par la situation
- Niveau de classe
- Période de l'année, ou place dans une séquence, où la situation a été (ou serait) mise en œuvre.
- Objectif(s) d’apprentissages : 
	- Compétences disciplinaires visées en termes de « être capable de…», (ces compétences sont issues des programmes)
	- Compétences numériques développées (issues du CRCN)
	
- Environnement numérique impliqué dans la situation :
    - Outils physiques (TBI, tablettes, ordinateurs fixes ou portables, caméras, enregistreurs, ...)
    - Organisation matérielle (nb de matériels par élève/classe, salle informatique ou salle de classe)
    - Logiciels ou applications utilisés.
    - Ressources utilisées, ou produites

-  Organisation pédagogique : quelles activités, quand, comment, ...
    
```

---
# Analyse et justification des choix

- **Justification des choix** d'outils et de ressources numériques, en s'appuyant notamment sur le modèle SAMR.
- **Analyse des potentiels effets positifs ou néfastes pour les apprentissages** et les apprenants (TD 1).
- **Analyse des problèmes juridiques (droits d'auteurs, droit à la vie privée) et éthiques** soulevés.


:warning: Les analyses et justifications doivent tenir compte du contexte.


---
# En avant !
* Essayez d'identifier un apprentissages pour lequel le numérique peut apporter une plus-value réelle.
* Restez réaliste :arrow_right: Mise en oeuvre conseillée. 
