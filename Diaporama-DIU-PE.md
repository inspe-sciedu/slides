---
marp: true
theme: default 
paginate: true
---

![](images/logo_UGA_couleur_cmjn.jpg)

# Module numérique 1 et 2

## DIU Premier degré (PE)

INSPÉ / UGA -2022

<!-- footer: Equipe de formateurs(trices) numérique - Inspé-UGA - 2022 - CC:BY-NC-SA -->

---

# Objectifs deu module

* Être capable de se poser des questions sur les plus‑values supposées des situations d'apprentissage intégrant le numérique lors de leur conception.
* Réduire le plus possible les risques juridiques, éthiques, physiologiques et effets négatifs en termes d'apprentissages induits par les usages du numérique.
* Être capable d'analyser objectivement une situation d'apprentissage déjà réalisée et pouvoir en proposer une ou des évolutions bénéfiques pour les élèves.

---

# Méthode

* Via une réflexion individuelle sur une situation d'apprentissage utilisant le numérique.
* Cette situation sera analysée en vous aidant des apports délivrés au cours de 5 séances thématiques :
  * Effets du numérique sur les apprentissages
  * Évaluer et s’auto‑évaluer avec le numérique
  * Production et usages de ressources numériques
  * Numérique et juridique
  * Enseigner avec le numérique : quelques zones à risque


---

# Modalité d’évaluation

* Semestre 1 : Assiduité
* Semestre 2 : Test de connaissances (QCM)

---

# Plan des séances

* **Séances 1 & 5** : Apports thématiques (2h en présence)
* **Séance 6** : Présentation de situations. Analyses suivant les cinq thématiques et échanges collectifs. Test de connaissances.

---

# Pour la dernière séance ...

... au cours des différentes séances thématiques, noter des situations potentiellement intéressantes à analyser et présenter.

---

# En résumé

* Une formation appuyée sur des apports théoriques et pratiques
* Une évaluation fondée sur l'analyse d'une situation de classe

---

# Connexion au cours (1/4)

* Plate-forme : <https://eformation.univ-grenoble-alpes.fr/>
* Connexion par « compte universitaire »

| Page d'accueil | Page de connexion | 
| :-------------: | :---------: | 
|![width:400px](images/Moodle1.png) |![width:350px](images/moodle18.png) |

* Un accès anonyme sera possible pour les premières séances.

---

# Connexion au cours (2/4)

#### Choix de l'établissement

Université Grenoble Alpes ou Université de Savoie ![width:700px](images/img-Intro/Moodle2.png)

Authentification avec ses identifiants universitaires ![width:600px](images/img-Intro/Moodle4.png)

---

# Connexion au cours (3/4)

* Retour à l'accueil du site ![width:300px](images/img-Intro/Moodle5.png)
* Descendre dans la page pour accéder au champ de recherche des cours ![50%](images/img-Intro/Moodle6module.png)
* Sélectionner le cours "Module numérique 1 et 2 PE" 
![width:300px](images/img-Intro/Moodle7PEmodule.png)

---

# Connexion au cours (4/4)

* Vous devez vous auto-inscrire. La clé d’inscription vous sera donnée par le formateur.

![center 60%](images/img-Intro/Moodle8PEmodule.png)

* Pour ceux qui n'ont pas encore leurs identifiants universitaires, le mot de passe pour l'accès anonyme est **ModuleNumeriquePE**
