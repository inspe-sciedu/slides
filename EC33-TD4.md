---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)
  
# Culture numérique et apprentissages
## TD 4
 
Laurence Osete - UGA
 
<!-- page_number: true -->
<!-- footer: laurence.osete@univ-grenoble-alpes.fr - 2023 - 2024 - CC:BY-NC-SA -->

---
# :zero: Objectifs
### Objectif général de la formation
Concevoir une situation d'apprentissage intégrant le numérique réaliste et réalisable dans votre classe :arrow_right: Objectif mise en oeuvre. (PIX+EDU).
### Objectifs du TD
* Finaliser le dossier d'évaluation.

---
# 0. Modalités de conception

- TD 1 : Mise en application des apports théoriques sur les effets du numérique sur les apprentissages - Début de la conception :arrow_right: avoir une première proposition de situation d'apprentissage.
- TD 2 : Mise en application des apports théoriques sur les ressources numériques, les droits d'auteurs et les problèmes éthiques - Début de la conception
- TD 3 : Apports pratiques sur la formation AU numérique et l'évaluation des élèves en référence au CRCN - découverte de PIX et PIX+EDU - Poursuite de la conception
- **TD 4 : Finalisation de la conception. :arrow_right: Évalué cette année** 
- TD 5 : Retour d'expérience pour celles et ceux qui auront réalisé une mise en oeuvre. :arrow_right: PIX+EDU

---
# :one: Structuration du dossier

- Description de la situation avec obligatoirement des liens pointants sur les éléments de conception (1 à 2 pages).
- Justifications et analyses de la conception (1 à 2 pages).
- Bilan personel, notamment sur les compétences numériques développées (1/2 page maximum).
 
---
# Rappel des éléments attendus dans la description


```
- Domaine(s) d’enseignement concerné(s) par la situation
- Niveau de classe
- Période de l'année, ou place dans une séquence, où la situation a été (ou serait) mise en œuvre.
- Objectif(s) d’apprentissages : 
	- Compétences disciplinaires visées en termes de « être capable de…», (ces compétences sont issues des programmes)
	- Compétences numériques développées (issues du CRCN)
	
- Environnement numérique impliqué dans la situation :
    - Outils physiques (TBI, tablettes, ordinateurs fixes ou portables, caméras, enregistreurs, ...)
    - Organisation matérielle (nb de matériels par élève/classe, salle informatique ou salle de classe)
    - Logiciels ou applications utilisés.
    - Ressources utilisées, ou produites

-  Organisation pédagogique : quelles activités, quand, comment, ...
    
```

Vous êtes libre de les présenter sous la forme et l'ordre que vous souhaitez.

---
# :two: Format du document

- Un seul document au format PDF déposé sur la plateforme.
- Police Arial, taille 12, interligne 1,5 cm, marges 2,5 cm.
- 4 pages maximum

Date limite de rendu : 2 jours après la dernière séance (entre le 6 et le 10 décembre)

---
# :three: Compétences évaluées

1.1 Recourir au numérique lorsque cela répond à une problématique d’apprentissage, d’enseignement ou d’éducation. 
1.2 Choisir des moyens / supports / pratiques / outils / ressources / solutions / données numériques sélectionnés par rapport aux objectifs visés et activités prévues.
2.1 Mobiliser le numérique comme moyen de conception et pour formaliser son projet, son scénario
2.2 Respecter des principes éthiques et juridiques
3.1 Identifier les compétences numériques des apprenants pré requises et développées	
3.2 Prendre en compte des besoins éducatifs particuliers, et identifier les usages, outils et/ou ressources numériques qui permettraient d’y répondre
4.1 Préparer et gérer le matériel, les outils, la partie technique
8.2 Être en mesure de faire le point sur les apports professionnels

---
# :four: Barème

- 2 points par compétences
- 4 points pour le respect des règles de mise en forme et les attendus.

---
# :five: Dépôt des éléments de conception (Annexes)

Déposer les documents, ou des copies d'écran sur une espace de partage en ligne tel que :
- Nuage de Apps Education : https://portail.apps.education.fr/services/nuage
- Partage de vidéos de l'UGA : https://videos.univ-grenoble-alpes.fr/
- Mur de partage de la Digitale : https://digipad.app/
- Framageda https://framagenda.org
